# Exercicis DDL (Estructura)

## Exercici 1

Crear una base de dades anomenada "biblioteca". Dins aquesta base de dades:
+ crear una taula anomenada llibres amb els camps següents:
	+ "ref": clau primaria numèrica que identifica els llibres i s'ha d'assignar automàticament.
	+ "titol": títol del llibre.
	+ "editorial": nom de l'editorial.
	+ "autor": identificador de l'autor, clau forana, per defecte ha de
	referenciar al primer valor de la taula autors que simbolitza l'autor "Anònim".
+ Crear una altre taula anomenada "autors" amb els següents camps:
	+ "autor": identificador de l'autor.
	+ "nom": Nom i cognoms de l'autor.
	+ "nacionalitat": Nacionalitat de l'autor.
+ Ambdues taules han de mantenir integritat referencial. Cal que si es trenca
  la integritat per delete d'autor, la clau forana del llibre apunti a "Anònim".
+ Si es trenca la integritat per insert/update s'ha d'impedir l'alta/modificació.
+ Cal inserir l'autor "Anònim" a la taula autors.

 - Creació taules:
```
CREATE TABLE autors (
        autor           INT,
        nom             VARCHAR(20) NOT NULL
                                    DEFAULT 'Anònim',
        nacionalitat    VARCHAR(20),
        PRIMARY KEY(autor)      
);

CREATE TABLE llibres (
        ref             SERIAL,
        titol           VARCHAR(50) NOT NULL,
        editorial       VARCHAR(20),
        autor           INT         NOT NULL
                                    DEFAULT 1,
        PRIMARY KEY(ref)
        FOREIGN KEY (autor) REFERENCES autors ON DELETE SET DEFAULT ON UPDATE RESTRICT
);
```
 - Inserció de dades
```
INSERT INTO autors VALUES(2, 'Anna Roca', NULL);
INSERT INTO autors VALUES(3, 'Vicent Estel', NULL);

INSERT INTO llibres (titol, editorial, autor) VALUES ('Primer llibre', 'Ed1',1);
INSERT INTO llibres (titol, editorial, autor) VALUES ('Segon llibre', 'Ed2',2);
INSERT INTO llibres (titol, editorial, autor) VALUES ('Tercer llibre', 'Ed3',4);
INSERT INTO llibres (titol, editorial, autor) VALUES ('Quart llibre', 'Ed1',3);
```

## Exercici 2

A l'anterior BD biblioteca:
+ Afegirem una taula anomenada _socis_ amb els següents camps:
	+ num_soci: clau primària
	+ nom: nom i cognoms del soci.
	+ dni: DNI del soci.
+ Afegirem també una taula anomenada _prestecs_ amb els següents camps:
	+ ref: clau forana, que fa referència al llibre prestat.
	+ soci: clau forana, que fa referència al soci.
	+ data_prestec: data en que s'ha realitzat el préstec.
+ No cal que prestecs tingui clau principal ja que només és una taula de relació.
+ En eliminar un llibre cal que s'eliminin els seus préstecs automàticament.
+ No s'ha de poder eliminar un soci amb préstecs pendents.

 - Creació taules i inserció de dades:
```
CREATE TABLE socis (
        num_soci        INT,
        nom             VARCHAR(20) NOT NULL,
        dni             VARCHAR(9) NOT NULL,
        PRIMARY KEY(num_soci)
);

CREATE TABLE prestecs (
        ref             INT NOT NULL,
        soci            INT NOT NULL,
        data_prestec    DATE,
        FOREIGN KEY(ref) REFERENCES llibres ON DELETE CASCADE,
        FOREIGN KEY(soci) REFERENCES socis ON DELETE RESTRICT  
);

INSERT INTO socis VALUES (7, 'Jordi', '47717874G');
INSERT INTO prestecs VALUES (6, '7','2023-01-01');
```

## Exercici 3

A la base de dades training crear una taula anomenada "rep_vendes_baixa" que
tingui la mateixa estructura que la taula rep_vendes i a més a més un camp
anomenat "baixa" que pugui contenir la data en que un representant de vendes
s'ha donat de baixa.
```
CREATE TABLE rep_vendes_baixa AS (SELECT * FROM rep_vendes);
ALTER TABLE rep_vendes_baixa ADD baixa DATE;

CREATE TABLE rep_vendes_baixa (
    baixa DATE
) INHERITS (rep_vendes);
```
## Exercici 4

A la base de dades training crear una taula anomenada
"productes_sense_comandes" omplerta amb aquells productes que no han tingut mai
cap comanda. A continuació esborrar de la taula "productes" aquells productes
que estan en aquesta nova taula.
```
CREATE TABLE productes_sense_comandes AS (SELECT * 
					    FROM productes 
					   WHERE (id_producte, id_fabricant) NOT IN (SELECT producte, fabricant 
				            FROM comandes));

DELETE FROM productes 
 WHERE (id_producte, id_fabricant) IN (SELECT id_producte, id_fabricant 
  					 FROM productes_sense_comandes);
```
## Exercici 5

A la base de dades training crear una taula temporal que substitueixi la taula
"clients" però només ha de contenir aquells clients que no han fet comandes i
tenen assignat un representant de vendes amb unes vendes inferiors al 110% de
la seva quota.
```
CREATE TEMP TABLE clients AS (
    SELECT * 
      FROM clients
     WHERE num_clie NOT IN
           (SELECT clie 
              FROM comandes)
       AND rep_clie IN
           (SELECT num_empl 
              FROM rep_vendes 
             WHERE vendes < 1.1 * quota));
```
## Exercici 6

Escriu les sentències necessàries per a crear l'estructura de l'esquema
proporcionat de la base de dades training. Justifica les accions a realitzar en
modificar/actualitzar les claus primàries.
```
CREATE TABLE PRODUCTES (
    id_fabricant character(3),
    id_producte character varying(5),
    descripcio character varying(20) NOT NULL,
    preu numeric(7,2) NOT NULL,
    estoc integer
);

CREATE TABLE OFICINES (
    oficina smallint CONSTRAINT PK_OFICINES_OFICINA PRIMARY KEY,
    ciutat character varying(15) NOT NULL,
    regio character varying(10) NOT NULL,
    director smallint,
    objectiu numeric(9,2),
    vendes numeric(9,2)
);

CREATE TABLE REP_VENDES (
    num_empl smallint CONSTRAINT PK_REP_VENDES_NUM_EMPL PRIMARY KEY,
    nom character varying(30) NOT NULL,
    edat smallint,
    oficina_rep smallint,
    carrec character varying(20),
    data_contracte date NOT NULL,
    cap smallint,
    quota numeric(8,2),
    vendes numeric(8,2)
);

CREATE TABLE CLIENTS (
    num_clie smallint CONSTRAINT PK_CLIENTS_NUM_CLIE PRIMARY KEY,
    empresa character varying(20) NOT NULL,
    rep_clie smallint NOT NULL,
    limit_credit numeric(8,2)
);

CREATE TABLE COMANDES (
    num_comanda integer CONSTRAINT PK_COMANDA_NUM_COMANDA PRIMARY KEY ,
    data date NOT NULL,
    clie smallint NOT NULL,
    rep smallint,
    fabricant character(3) NOT NULL,
    producte character varying(5) NOT NULL,
    quantitat smallint NOT NULL,
    import numeric(7,2) NOT NULL
);
```

## Exercici 7

Escriu una sentència que permeti modificar la base de dades training
proporcionada. Cal que afegeixi un camp anomenat "nif" a la taula "clients" que
permeti emmagatzemar el NIF de cada client. També s'ha de procurar que el NIF
de cada client sigui únic.
```
ALTER TABLE clients
  ADD nif CHAR(9) UNIQUE CHECK (nif LIKE '_________');
```
## Exercici 8

Escriu una sentència que permeti modificar la base de dades training
proporcionada. Cal que afegeixi un camp anomenat "tel" a la taula "clients" que
permeti emmagatzemar el número de telèfon de cada client. També s'ha de
procurar que aquest contingui 9 xifres.
```
ALTER TABLE clients
  ADD tel INT UNIQUE CHECK (tel >= 100000000 AND tel <= 999999999);
```
## Exercici 9

Escriu les sentències necessàries per modificar la base de dades training
proporcionada. Cal que s'impedeixi que els noms dels representants de vendes i
els noms dels clients estiguin buits, és a dir que ni siguin nuls ni continguin
la cadena buida.
```
ALTER TABLE rep_vendes ALTER nom SET NOT NULL, ADD CHECK(nom NOT LIKE '');

ALTER TABLE clients ALTER empresa SET NOT NULL, ADD CHECK(empresa NOT LIKE '');
```
## Exercici 10

Escriu una sentència que permeti modificar la base de dades training
proporcionada. Cal que procuri que l'edat dels representants de vendes no sigui
inferior a 18 anys ni superior a 65.
```
ALTER TABLE rep_vendes ADD CHECK(edat >= 18 AND edat <= 65);
```
## Exercici 11

Escriu una sentència que permeti modificar la base de dades training
proporcionada. Cal que esborri el camp "carrec" de la taula "rep_vendes"
esborrant també les possibles restriccions i referències que tingui.
```
ALTER TABLE rep_vendes DROP carrec CASCADE;
```
## Exercici 12

Escriu les sentències necessàries per modificar la base de dades training
proporcionada per tal que aquesta tingui integritat referencial. Justifica les
accions a realitzar per modificar les dades.

 - Arreglem la comanda amb el producte inexistent i afegim constraint per mantenir integritat referencial:
```
UPDATE comandes SET producte = 'xk47' WHERE producte = 'k47';
```
```
ALTER TABLE comandes
  ADD FOREIGN KEY (fabricant, producte) REFERENCES producte(id_fabricant, id_producte) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE clients
  ADD FOREIGN KEY (rep_clie) REFERENCES rep_vendes(num_empl) ON DELETE SET DEFAULT ON UPDATE CASCADE;

ALTER TABLE oficines
  ADD FOREIGN KEY (director) REFERENCES rep_vendes(num_empl) ON DELETE SET NULL ON UPDATE CASCADE; 

ALTER TABLE comandes
  ADD FOREIGN KEY (clie) REFERENCES clients(num_clie) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD FOREIGN KEY (rep) REFERENCES rep_vendes(num_empl) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD FOREIGN KEY (fabricant, producte) REFERENCES productes(id_fabricant, id_producte) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE rep_vendes
  ADD FOREIGN KEY (oficina_rep) REFERENCES oficines(oficina) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD FOREIGN KEY (cap) REFERENCES rep_vendes(num_empl) ON DELETE SET DEFAULT ON UPDATE CASCADE; 
```