# Modificació

## Exercici 1:

Inseriu un nou venedor amb nom "Enric Jimenez" amb identificador 1012, oficina 12, títol "Dir Vendes", contracte d'1 de febrer del 2012, cap 101 i vendes 0.

```
ALTER TABLE rep_vendes 
 DROP CONSTRAINT "ck_rep_vendes_vendes",
  ADD CONSTRAINT "ck_rep_vendes_vendes"
      CHECK (vendes >= 0::numeric);

INSERT INTO rep_vendes
VALUES (1012, 'Enric Jimenez', NULL, 12, 'Dir Vendes', '2012-02-01', 101, NULL, 0);

INSERT INTO rep_vendes (num_empl, nom, oficina_rep, carrec, data_contracte, cap, vendes)
VALUES (1012, 'Enric Jimenez', 12, 'Dir Ventas', '2012-02-01', 101, 0);
```

## Exercici 2:

Inseriu un nou client "C1" i una nova comanda pel venedor anterior.

```
INSERT INTO clients 
VALUES (2125, 'C1', 1012, NULL);

INSERT INTO comandes
VALUES (113100, '2018-11-17', 2125, 1012, 'aci', 41004, 7, 888);
```
## Exercici 3:

Inseriu un nou venedor amb nom "Pere Mendoza" amb identificador 1013, contracte del 15 de agost del 2011 i vendes 0. La resta de camps a null.

```
INSERT INTO rep_vendes
VALUES (1013, 'Pere Mendoza', NULL, NULL, NULL, '2011-08-15', NULL, NULL, 0);
```

## Exercici 4:

Inseriu un nou client "C2" omplint els mínims camps.

```
INSERT INTO clients (num_clie, empresa, rep_clie)
VALUES (2126, 'C2', 1012);
```

## Exercici 5:

Inseriu una nova comanda del client "C2" al venedor "Pere Mendoza" sense especificar la llista de camps pero si la de valors.

```
INSERT INTO comandes 
VALUES (113101, '2022-05-23', 2126, 1013, 'fea', '114', 5, 255);
```

## Exercici 6:

Esborreu de la còpia de la base de dades el venedor afegit anteriorment anomenat "Enric Jimenez".

```
DELETE FROM rep_vendes
WHERE nom = 'Enric Jimenez';

Ens dona el següent error ja que és un client amb comanda

ERROR:  update or delete on table "rep_vendes" violates foreign key constraint "fk_clients_rep_clie" on table "clients"
DETAIL:  Key (num_empl)=(1012) is still referenced from table "clients".
```

## Exercici 7:

Elimineu totes les comandes del client "C1" afegit anteriorment.

```
DELETE FROM comandes 
 WHERE clie = (SELECT num_clie 
                 FROM clients 
                WHERE empresa = 'C1');
```

## Exercici 8:

Esborreu totes les comandes d'abans del 15-11-1989.

```
DELETE FROM comandes 
 WHERE data < '1989-11-15';

DELETE 4
```

## Exercici 9:

Esborreu tots els clients dels venedors: Adams, Jones i Roberts.

```
DELETE FROM clients 
 WHERE rep_clie IN (SELECT num_empl 
                      FROM rep_vendes 
                     WHERE nom LIKE '%Adams%' 
                        OR nom LIKE '%Jones%' 
                        OR nom LIKE '%Roberts%');

ERROR:  update or delete on table "clients" violates foreign key constraint "fk_comandes_clie" on table "comandes"
DETAIL:  Key (num_clie)=(2103) is still referenced from table "comandes".
```


## Exercici 10:

Esborreu tots els venedors contractats abans del juliol del 1988 que encara no se'ls ha assignat una quota.

```
DELETE FROM rep_vendes 
 WHERE data_contracte < '1988-07-01' 
   AND quota IS NULL;
```

## Exercici 11:

Esborreu totes les comandes.

```
DELETE FROM comandes;
```

## Exercici 12:

Esborreu totes les comandes acceptades per la Sue Smith (cal tornar a disposar de la taula comandes)

```
 DELETE FROM comandes 
  WHERE rep = (SELECT num_empl 
                 FROM rep_vendes 
                WHERE nom = 'Sue Smith' );

DELETE 4
```

## Exercici 13:

Suprimeix els clients atesos per venedors les vendes dels quals són inferiors al 80% de la seva quota.

```
DELETE FROM clients 
 WHERE rep_clie IN (SELECT num_empl 
                      FROM rep_vendes 
                     WHERE quota * 0.80 > vendes);

ERROR:  update or delete on table "clients" violates foreign key constraint "fk_comandes_clie" on table "comandes"
DETAIL:  Key (num_clie)=(2124) is still referenced from table "comandes".
```

## Exercici 14:

Suprimiu els venedors els quals el seu total de comandes actual (imports) és menor que el 2% de la seva quota.

```
DELETE FROM rep_vendes 
 WHERE quota * 0.2 > (SELECT SUM(import) 
                        FROM comandes 
                       WHERE rep = num_empl);

ERROR:  update or delete on table "rep_vendes" violates foreign key constraint "fk_clients_rep_clie" on table "clients"
DETAIL:  Key (num_empl)=(105) is still referenced from table "clients".
```

## Exercici 15:

Suprimiu els clients que no han realitzat comandes des del 10-11-1989.

```
DELETE FROM clients
 WHERE num_clie NOT IN 
       (SELECT clie 
          FROM comandes 
         WHERE data > '1989-11-10');

ERROR:  update or delete on table "clients" violates foreign key constraint "fk_comandes_clie" on table "comandes"
DETAIL:  Key (num_clie)=(2102) is still referenced from table "comandes".
```

## Exercici 16:

Eleva el límit de crèdit de l'empresa Acme Manufacturing a 60000 i la reassignes a Mary Jones.

```
UPDATE clients 
   SET limit_credit = 60000, rep_clie = (SELECT num_empl 
                                           FROM rep_vendes 
                                          WHERE nom = 'Mary Jones') 
 WHERE empresa = 'Acme Mfg.';

UPDATE 1
```

## Exercici 17:

Transferiu tots els venedors de l'oficina de Chicago (12) a la de Nova York (11), i rebaixa les seves quotes un 10%.

```
UPDATE rep_vendes 
   SET oficina_rep = 11, quota = quota - quota * 0.10 
 WHERE oficina_rep = 12;

UPDATE 3
```

## Exercici 18:

Reassigna tots els clients atesos pels empleats 105, 106, 107, a l'empleat 102.

```
UPDATE clients 
   SET rep_clie = 102 
 WHERE rep_clie IN (105,106,107);

UPDATE 5
```

## Exercici 19:

Assigna una quota de 100000 a tots aquells venedors que actualment no tenen quota.

```
UPDATE rep_vendes 
   SET quota = 100000 
 WHERE quota IS NULL;

UPDATE 1
```

## Exercici 20:

Eleva totes les quotes un 5%.

```
UPDATE rep_vendes 
   SET quota = quota + quota * 0.05;

UPDATE rep_vendes 
   SET quota = quota * 1.05;
```

## Exercici 21:

Eleva en 5000 el límit de crèdit de qualsevol client que ha fet una comanda d'import superior a 25000.

```
UPDATE clients 
   SET limit_credit = limit_credit + 5000 
 WHERE num_clie IN (SELECT clie 
                      FROM comandes 
                     WHERE import > 25000);
```

## Exercici 22:

Reassigna tots els clients atesos pels venedors les vendes dels quals són menors al 80% de les seves quotes. Reassignar al venedor 105.

```
UPDATE clients 
   SET rep_clie = 105 
 WHERE rep_clie IN (SELECT num_empl 
                      FROM rep_vendes 
                     WHERE vendes < quota * 0.80);
```

## Exercici 23:

Feu que tots els venedors que atenen a més de tres clients estiguin sota de les ordres de Sam Clark (106).

```
UPDATE rep_vendes 
   SET cap = 106 
 WHERE num_empl IN (SELECT rep_clie 
                      FROM clients 
                     GROUP BY rep_clie 
                    HAVING COUNT(*) > 3);

UPDATE rep_vendes
   SET cap = 106
 WHERE 3 <
       (SELECT COUNT(*) 
          FROM clients 
         WHERE rep_clie = num_empl);
```

## Exercici 24:

Augmenteu un 50% el límit de credit d'aquells clients que totes les seves comandes tenen un import major a 30000.

```
UPDATE clients 
   SET limit_credit = limit_credit + limit_credit * 0.50 
 WHERE num_clie IN (SELECT clie 
                      FROM comandes 
                     WHERE import > 30000);
```

## Exercici 25:

Disminuiu un 2% el preu d'aquells productes que tenen un estoc superior a 200 i no han tingut comandes.

```
UPDATE productes 
   SET preu = preu - preu * 0.02 
 WHERE estoc > 200 
   AND (id_fabricant, id_producte) NOT IN (SELECT (fabricant,producte) 
                                            FROM comandes);

UPDATE productes 
   SET preu = preu - preu * 0.02 
 WHERE estoc > 200 
   AND NOT EXISTS (SELECT num_comanda 
                     FROM comandes 
                    WHERE fabricant = id_fabricant
                      AND producte = id_producte);
```

## Exercici 26:

Establiu un nou objectiu per aquelles oficines en que l'objectiu actual sigui inferior a les vendes. Aquest nou objectiu serà el doble de la suma de les vendes dels treballadors assignats a l'oficina.

```
UPDATE oficines 
   SET objectiu = (SELECT SUM(vendes) * 2 
                     FROM rep_vendes 
                    WHERE oficina_rep = oficina) 
 WHERE objectiu < vendes;
```

## Exercici 27:

Modifiqueu la quota dels caps d'oficina que tinguin una quota superior a la quota d'algun empleat de la seva oficina. Aquests caps han de tenir la mateixa quota que l'empleat de la seva oficina que tingui una quota més petita.

```
UPDATE rep_vendes
   SET quota =
       (SELECT MIN(quota)
          FROM rep_vendes
         WHERE oficina_rep =
               (SELECT oficina
                  FROM oficines
                       JOIN rep_vendes
                       ON director = num_empl
                 WHERE quota > ANY
                       (SELECT quota
                          FROM rep_vendes
                         WHERE oficina_rep = oficina)))
 WHERE num_empl IN
      (SELECT director           
         FROM oficines
              JOIN rep_vendes directors
              ON director = num_empl
        WHERE quota > ANY
              (SELECT quota
                 FROM rep_vendes 
                WHERE oficina_rep = oficina));
```

## Exercici 28:

Cal que els 5 clients amb un total de compres (quantitat) més alt siguin transferits a l'empleat Tom Snyder i que se'ls augmenti el límit de crèdit en un 50%.

```
UPDATE clients 
   SET rep_clie = (SELECT num_empl 
                     FROM rep_vendes 
                    WHERE nom = 'Tom Snyder'), limit_credit = limit_credit + limit_credit * 0.50 
 WHERE num_clie IN (SELECT clie 
                      FROM comandes 
                     GROUP BY clie 
                     ORDER BY SUM(quantitat) DESC 
                     LIMIT 5);
```

## Exercici 29:

Es volen donar de baixa els productes dels que no tenen estoc i alhora no se n'ha venut cap des de l'any 89.

```
DELETE FROM productes 
 WHERE estoc = 0 
   AND id_producte NOT IN (SELECT producte 
                             FROM comandes 
                            WHERE data >= '1989-01-01');

DELETE FROM productes
 WHERE estoc = 0
   AND NOT EXISTS
       (SELECT * 
          FROM comandes 
         WHERE id_fabricant = fabricant
           AND id_producte = producte
           AND DATE_PART('year', data) >= 1990);
```

## Exercici 30:

Afegiu una oficina de la ciutat de "San Francisco", regió oest, el cap ha de ser "Larry Fitch", les vendes 0, l'objectiu ha de ser la mitja de l'objectiu de les oficines de l'oest i l'identificador de l'oficina ha de ser el següent valor després del valor més alt.

```
INSERT INTO oficines 
VALUES ((SELECT MAX(oficina)+1 
           FROM oficines), 'San Francisco', 'Oest', (SELECT num_empl 
                                                       FROM rep_vendes 
                                                      WHERE nom = 'Larry Fitch'), (SELECT AVG(objectiu) 
                                                                                     FROM oficines 
                                                                                    WHERE regio = 'Oest'), 0);
```


## BACKUP i RESTORE de la Base de DADES training:

Una manera de fer un backup ràpid des del bash és:

```
pg_dump -Fc training > training.dump
```
Podem eliminar també la base de dades:

```
dropdb training
```
Podem restaurar una base de dades amb:
```
pg_restore -C -d postgres training.dump
```

Una altra manera de fer el mateix:

Backup en fitxer pla
```
pg_dump -f training.dump   training
```

Eliminem la base de dades:

```
dropdb training
```

Creem la base de dades:
```
createdb training
```
Reconstruim l'estructura i dades:
```
psql training < training.dump
```
 






