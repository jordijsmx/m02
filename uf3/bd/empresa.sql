DROP DATABASE IF EXISTS empresa;
CREATE DATABASE empresa;

\c empresa

-- Taula
CREATE TABLE vehicles (
    id_vehicle      INT NOT NULL UNIQUE,
    data_compra     DATE NOT NULL DEFAULT CURRENT_DATE,
    disponibilitat  VARCHAR(20) NOT NULL DEFAULT 'Disponible',
    PRIMARY KEY(id_vehicle),
    CHECK(disponibilitat = 'Disponible' or disponibilitat = 'Taller')
);

-- Dades
INSERT INTO vehicles VALUES(1,DEFAULT,DEFAULT);
INSERT INTO vehicles VALUES(2,'2023-01-01',DEFAULT);
INSERT INTO vehicles VALUES(3,'2022-11-17','Taller');