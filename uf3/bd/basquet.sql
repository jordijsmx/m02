DROP DATABASE IF EXISTS basquet;
CREATE DATABASE basquet;
\c basquet


CREATE TABLE clubs (
    id_club      SERIAL,
    nom          VARCHAR(20) NOT NULL UNIQUE,
    data_creacio DATE NOT NULL DEFAULT CURRENT_DATE,
    localitat    VARCHAR(20) NOT NULL,
    PRIMARY KEY(id_club)
);

INSERT INTO clubs VALUES(DEFAULT, 'Club Jordi', DEFAULT, 'Barcelona');
INSERT INTO clubs VALUES(DEFAULT, 'Club Pere', '2023-12-01', 'Tarragona');


CREATE TABLE equips (
    id_equip        SERIAL,
    nom_equip       VARCHAR(50) NOT NULL,
    id_club_eq      INT NOT NULL,
    id_comp_equip   INT NOT NULL,
    PRIMARY KEY(id_equip),
    FOREIGN KEY(id_club_eq) REFERENCES clubs ON DELETE RESTRICT ON UPDATE RESTRICT
);

INSERT INTO equips VALUES(DEFAULT,'Jordi A', 1, 1);
INSERT INTO equips VALUES(DEFAULT,'Jordi B', 1, 1);
INSERT INTO equips VALUES(DEFAULT,'Pere A', 2, 1);
INSERT INTO equips VALUES(DEFAULT,'Pere B', 2, 1);

CREATE TABLE competicions (
    id_comp         SERIAL,
    nom_comp        VARCHAR(20) NOT NULL,
    descripcio      VARCHAR(50) NOT NULL,
    PRIMARY KEY(id_comp)
);

INSERT INTO competicions VALUES(DEFAULT, 'comp1', 'TEXT');

CREATE TABLE partits (
    id_partit           SERIAL,
    id_equiplocal       INT NOT NULL,    
    id_equipvisitant    INT NOT NULL,
    data_partit         DATE NOT NULL DEFAULT CURRENT_DATE,
    hora_partit         TIME NOT NULL DEFAULT CURRENT_TIME,
    punts_eqlocal       INT NOT NULL,
    punts_eqvisitant    INT NOT NULL,
    id_competicio       INT NOT NULL,
    PRIMARY KEY(id_partit),
    FOREIGN KEY(id_equiplocal) REFERENCES equips ON DELETE RESTRICT ON UPDATE RESTRICT,
    FOREIGN KEY(id_equipvisitant) REFERENCES equips ON DELETE RESTRICT ON UPDATE RESTRICT,
    FOREIGN KEY(id_competicio) REFERENCES competicions ON DELETE RESTRICT ON UPDATE RESTRICT
);

ALTER TABLE partits ADD CONSTRAINT UNIQUE (id_equiplocal, id_equipvisitant)

INSERT INTO partits VALUES (DEFAULT, 1,3, DEFAULT, DEFAULT, 20, 70, 1);
INSERT INTO partits VALUES (DEFAULT, 1,4, DEFAULT, DEFAULT, 110, 68, 2);
INSERT INTO partits VALUES (DEFAULT, 4,3, DEFAULT, DEFAULT, 20, 90, 1);
INSERT INTO partits VALUES (DEFAULT, 3,1, DEFAULT, DEFAULT, 110, 88, 2);
INSERT INTO partits VALUES (DEFAULT, 2,1, DEFAULT, DEFAULT, 110, 88, 2);
INSERT INTO partits VALUES (DEFAULT, 1,2, DEFAULT, DEFAULT, 110, 88, 2);

ALTER TABLE equips ADD FOREIGN KEY(id_comp_equip) REFERENCES competicions ON DELETE RESTRICT ON UPDATE RESTRICT;

CREATE TABLE classificacio (
    id_class            SERIAL,
    id_comp_class       INT NOT NULL,
    id_equip_class      INT NOT NULL,
    punts               INT NOT NULL,
    partits_jugats      INT NOT NULL,
    partits_guanyats    INT NOT NULL,
    PRIMARY KEY(id_class),
    FOREIGN KEY(id_comp_class) REFERENCES competicions ON DELETE RESTRICT,
    FOREIGN KEY(id_equip_class) REFERENCES equip ON DELETE RESTRICT
);

INSERT INTO classificacio VALUES(1,1,1,1,1,1);
INSERT INTO classificacio VALUES(2,1,2,1,1,0);

/*
-- Quants partits jugats dels equips:

SELECT id_equip, nom_equip, (SELECT COUNT(*) FROM partits WHERE (id_equiplocal=id_equip) OR (id_equipvisitant=id_equip)) FROM equips;

Quants partits guanyats:

SELECT id_equip, nom_equip, (SELECT COUNT(*) FROM partits WHERE (id_equiplocal=id_equip) OR (id_equipvisitant=id_equip)) AS jugats, 
                            
                            ((SELECT COUNT(*) FROM partits WHERE id_equiplocal=id_equip AND punts_eqlocal > punts_eqvisitant) + 
                            (SELECT COUNT(*) FROM partits WHERE id_equipvisitant=id_equip AND punts_eqvisitant > punts_eqlocal)) AS guanyats, 
                            
                            (SELECT COUNT(*) FROM partits WHERE id_equiplocal=id_equip AND punts_eqlocal < punts_eqvisitant) + 
                            (SELECT COUNT(*) FROM partits WHERE id_equipvisitant=id_equip AND punts_eqvisitant < punts_eqlocal) AS perduts,
                            
                            (SELECT COUNT(*) FROM partits WHERE id_equip = id_equiplocal OR id_equip = id_equipvisitant) + 
                            (SELECT COUNT(*) FROM partits WHERE id_equip = id_equiplocal AND punts_eqlocal > punts_eqvisitant) +
                            (SELECT COUNT(*) FROM partits WHERE id_equip = id_equipvisitant AND punts_eqlocal < punts_eqvisitant),

                            (SELECT SUM(punts_eqlocal) FROM partits WHERE id_equip = id_equiplocal) AS prova,

                            ((SELECT SUM(punts_eqlocal) FROM partits WHERE id_equip = id_equiplocal) + 
                             (SELECT SUM(punts_eqvisitant) FROM partits WHERE id_equip = id_equipvisitant)) AS num_punts_a_favor,

                            ((SELECT SUM(punts_eqvisitant) FROM partits WHERE id_equip = id_equipvisitant) + 
                            (SELECT SUM(punts_eqlocal) FROM partits WHERE id_equip = id_equiplocal)) AS num_punts_en_contra
  FROM equips;
*/