'Exercici 1'

CREATE TABLE autors (
        autor           INT,
        nom             VARCHAR(20) NOT NULL
                                    DEFAULT 'Anònim',
        nacionalitat    VARCHAR(20),
        PRIMARY KEY(autor)     
);

CREATE TABLE llibres (
        ref             SERIAL,
        titol           VARCHAR(50) NOT NULL,
        editorial       VARCHAR(20),
        autor           INT         NOT NULL
                                    DEFAULT 1,
        PRIMARY KEY(ref)
        FOREIGN KEY (autor) REFERENCES autors ON DELETE SET DEFAULT ON UPDATE RESTRICT
);

INSERT INTO autors VALUES(2, 'Anna Roca', NULL);
INSERT INTO autors VALUES(3, 'Vicent Estel', NULL);

INSERT INTO llibres (titol, editorial, autor) VALUES ('Primer llibre', 'Ed1',1);
INSERT INTO llibres (titol, editorial, autor) VALUES ('Segon llibre', 'Ed2',2);
INSERT INTO llibres (titol, editorial, autor) VALUES ('Tercer llibre', 'Ed3',4);
INSERT INTO llibres (titol, editorial, autor) VALUES ('Quart llibre', 'Ed1',3);

'Exercici 2'

CREATE TABLE socis (
        num_soci        SERIAL,
        nom             VARCHAR(20) NOT NULL,
        dni             VARCHAR(9) NOT NULL,
        PRIMARY KEY(num_soci)
);

CREATE TABLE prestecs (
        ref             INT NOT NULL,
        soci            INT NOT NULL,
        data_prestec    DATE NOT NULL,
        FOREIGN KEY(ref) REFERENCES llibres ON DELETE CASCADE,
        FOREIGN KEY(soci) REFERENCES socis ON DELETE RESTRICT  
);

INSERT INTO socis VALUES (7, 'Jordi', '47717874G');
INSERT INTO prestecs VALUES (6, '7','2023-01-01');

'Exercici 3'

CREATE TABLE rep_vendes_baixa AS (SELECT * FROM rep_vendes);
ALTER TABLE rep_vendes_baixa ADD baixa DATE;
'o també'
CREATE TABLE rep_vendes_baixa (baixa DATE) INHERITS (rep_vendes);