DROP DATABASE IF EXISTS instagram;
CREATE DATABASE instagram;
\c instagram

CREATE TABLE usuaris(
    id_usuari   SERIAL,
    alias       VARCHAR(50) NOT NULL UNIQUE,
    nom         VARCHAR(20) NOT NULL,
    mail        VARCHAR(20) NOT NULL UNIQUE,
    data_alta   DATE NOT NULL DEFAULT CURRENT_DATE,
    PRIMARY KEY(id_usuari)
);


CREATE TABLE posts(
    id_post         SERIAL,
    id_usuari_post  INT NOT NULL,
    data_post       DATE NOT NULL DEFAULT CURRENT_DATE,
    text_post       VARCHAR(100),
    imatge_post     VARCHAR(100),
    likes           SERIAL,
    PRIMARY KEY(id_post),
    FOREIGN KEY(id_usuari_post) REFERENCES usuaris ON DELETE RESTRICT ON UPDATE RESTRICT 
);

CREATE TABLE likes(
    id_like         INT NOT NULL,     
    data_like       DATE NOT NULL DEFAULT CURRENT_DATE,
    hora_like       TIME NOT NULL DEFAULT CURRENT_TIME,
    id_usuari_like  INT NOT NULL,
    id_publicacio   INT NOT NULL,
    PRIMARY KEY(id_like),
    FOREIGN KEY(id_usuari_like) REFERENCES usuaris ON DELETE RESTRICT ON UPDATE RESTRICT,
    FOREIGN KEY(id_publicacio) REFERENCES posts ON DELETE RESTRICT ON UPDATE RESTRICT
);

ALTER TABLE likes ADD UNIQUE(id_usuari_like, id_publicacio);


CREATE TABLE seguidors (
    id_seguiment    INT,
    id_seguit       INT NOT NULL,
    id_seguidor     INT NOT NULL,
    data_seg        DATE NOT NULL DEFAULT CURRENT_DATE,
    hora_seg        TIME NOT NULL DEFAULT CURRENT_TIME,
    actiu           BOOL DEFAULT 'True',
    PRIMARY KEY(id_seguiment),
    FOREIGN KEY(id_seguit) REFERENCES usuaris ON DELETE RESTRICT ON UPDATE RESTRICT,
    FOREIGN KEY(id_seguidor) REFERENCES usuaris ON DELETE RESTRICT ON UPDATE RESTRICT
);

ALTER TABLE seguidors ADD UNIQUE(id_seguidor, id_seguit);

CREATE TABLE comentaris (
    id_comentari        SERIAL,
    id_post_com         INT NOT NULL,
    id_usuari_com       INT NOT NULL,
    text_comentari      VARCHAR(100),
    data_com            TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    actiu               BOOL DEFAULT 'True',
    PRIMARY KEY(id_comentari),
    FOREIGN KEY(id_post_com) REFERENCES posts ON DELETE RESTRICT ON UPDATE RESTRICT,
    FOREIGN KEY(id_usuari_com) REFERENCES usuaris ON DELETE RESTRICT ON UPDATE RESTRICT
);

-- usuaris
INSERT INTO usuaris (alias, nom, mail) VALUES('Bloodrooted', 'Jordi', 'jordi@mail.com');
INSERT INTO usuaris (alias, nom, mail) VALUES('XxARSPlayxX', 'Aleix', 'aleix@mail.com');

-- posts
INSERT INTO posts (id_post, id_usuari_post, text_post, imatge_post, likes) VALUES (1,1, 'Hola', 'imatge.jpg', 3);

-- likes
INSERT INTO likes (id_like, id_usuari_like, id_publicacio) VALUES (1,2,1);

-- seguidors
INSERT INTO seguidors (id_seguiment, id_seguit, id_seguidor) VALUES (1,1,2);

-- comentaris
INSERT INTO comentaris VALUES(DEFAULT,1,1,'Hola!!',DEFAULT,DEFAULT);

/*
Errors
-------
-FK:
INSERT INTO likes VALUES(2, DEFAULT, DEFAULT, 3, 1);
ERROR:  insert or update on table "likes" violates foreign key constraint "likes_id_usuari_like_fkey"
DETAIL:  Key (id_usuari_like)=(3) is not present in table "usuaris".

-PK:
INSERT INTO usuaris VALUES(1, 'Marc', 'Marc', 'marc@mail.com', DEFAULT);
ERROR:  duplicate key value violates unique constraint "usuaris_pkey"
DETAIL:  Key (id_usuari)=(1) already exists.
*/