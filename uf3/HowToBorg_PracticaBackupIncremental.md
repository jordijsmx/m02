# BACKUPS INCREMENTALS 

# Guia d'instal·lació i configuració bàsica de Borg a Debian

## Què és Borg?

+ Borg és un programa de backups (còpias de seguretat) que elimina dades
redundants (duplicades). 

+ Aquesta darrera característica fa de Borg una eina molt adient pels backups
diaris ja que només s'emmagatzemen els canvis respecte a la versió anterior.

+ Borg també permet utilitzar compressió i xifratge autenticat.

[Guia d'ús Borg](https://linuxconfig.org/introduction-to-borg-backup)

## Instal·lació del paquet borg a Debian stable

El paquet es troba al nostre repositori, podem fer un `apt-get update` abans si ho considerem necessari: 


```
sudo apt-get install borgbackup  # si volem podem afegir el paquet borgbackup-doc
```

## Com utilitzar borg :

1. Crear un repositori 
2. Crear un arxiu de backup base
3. Crear el següent arxiu de backup incremental
4. Crear n arxius de backup incrementals
5. Restaurar l'arxiu de backup que interessi 

## Creació d'un repositori 

###  Inicialització d'un repositori


Inicialitzem el repositori:

```
borg init --encription=repokey /tmp/repo1
```


+ el valor d'encriptació repokey és el recomanat si volem compatibilitat amb
  versions antigues. Altrament farem servir_authenticated_ per exemple. [Més
info.](file:///usr/share/doc/borgbackup-doc/html/usage/init.html#borg-init) 

+ La repokey, es desa al fitxer `config` que hi ha dintre del repositori, com
  que no només necessitarem el password sinó també aquesta key, no oblidem de
copiar-la i guardar-la a un lloc segur. Si ens oblidem de copiar-la fora del
repositori, ens podem trobar amb una situació equivalent a quedar-nos fora del
cotxe tancat amb la clau a dins.

	Fem un exemple de còpia de la clau, o sigui exportació,:

	```
	borg key export /tmp/repo1  ~/copia_clau_repo1 
	```

	[Més opcions en aquest enllaç](https://borgbackup.readthedocs.io/en/stable/usage/key.html#borg-key-export)

	_No feu servir el format d'exportació de redirecció_ 


+ Si el repositori fos remot anàlogament faríem:

	```
	borg init ... user@hostname:/path/to/repo2
	```
## Creació de l'arxiu backup

##### Creem abans un directori amb dades de prova a guardar :
```
cd /tmp
mkdir dir_a_copiar
cd dir_a_copiar
ls -la > fitxer1
ls -la > fitxer2
ls -la > fitxer3
```
### Creació de l'arxiu de backup base utilitzant el repositori anterior
```
sudo borg create -v --stats /tmp/repo1::primera_copia  /tmp/dir_a_copiar
```
Llistem les dades del repositori :
```
sudo borg list /tmp/repo1
```
##### Afegim dades a /tmp/dir_a_copiar
```
cd /tmp/dir_a_copiar
ls -la > fitxer4
```
### Creació del segon arxiu de backup

```
	sudo borg create -v --stats /tmp/repo1::segona_copia  /tmp/dir_a_copiar
	sudo borg list /tmp/repo1
```
##### Modifiquem dades de /tmp/dir_a_copiar
```
cd /tmp/dir_a_copiar
ls -la >> fitxer1
```


### Creació del tercer arxiu de backup

```
sudo borg create -v --stats /tmp/repo1::tercera_copia  /tmp/dir_a_copiar
sudo borg list /tmp/repo1
```

### Restaurem la segona còpia
```
cd /
mv /tmp/dir_a_copiar /tmp/dir_a_copiar_antic

sudo borg extract /tmp/repo1::segona_copia

```

-------------------------

# CÒPIA DE TOTES LES DADES DE POSTGRES
 
### Creació de repositori per totes les dades postgres

```
borg init --encription=repokey /tmp/repo_pg
```

### Creació de l'arxiu backup

Si fem un backup totalment físic\*, parem primer el servei, sinó podríem estem
jugant a la loteria per obtenir un backup corrupte. 

```
sudo systemctl stop postgresql
```

+ De què volem fer backup?
	Dades de les bases de dades? Configuració?

+ On es troba aquesta info?

	Hint: com a administrador de la base de dades, puc fer servir l'ordre `show
...` per mostrar tot el que vulgui. Em puc ajudar del tabulador


	```
	training=# show data_directory;
		   data_directory        
	-----------------------------
	 /var/lib/postgresql/13/main
	(1 row)

	training=# show config_file ;
				   config_file               
	-----------------------------------------
	 /etc/postgresql/13/main/postgresql.conf
	(1 row)
	```

	```
	sudo borg create -v --stats --list /tmp/repo_pg::primera  /etc/postgresql/13/main/  /var/lib/postgresql/13/main
	```


### Restauració (utilitzant l'arxiu de backup) 


Ens carreguem primer els dos directoris `/etc/postgresql/13/main/`  `/var/lib/postgresql/13/main`:

Mirem de parar el servei de postgresl

```
sudo rm -rf /etc/postgresql/13/main/  /var/lib/postgresql/13/main
```

Comprovem que ja no tenim accés a les bases de dades template1, training ...


Si no diem res el backup es farà al directori on ens trobem:



```
cd /
sudo borg extract /tmp/repo_pg::primera
```

Ens demana la contrasenya i fa la restauració

Engeguem el servei de postgresl i ja ens hauria de funcionar tot un altre cop


\* = parlem de backup totalment físic perquè hi ha una altra opció molt interessant que és un híbrid: fer un backup lògic amb pg_dumpall i després un backup físic d'aquest fitxer.

--------------------------------------------------------------

# CÒPIA INCREMENTAL DE TOtES LES DADES DE POSTGRES


### Restauració a un dels diferents dies de Backup

+ Creem un primer backup, li direm Dilluns


	Parem el servei:

	```
		sudo systemctl stop postgresql
	```

	Inicialitzem el repo:
	```
		borg init --encription=repokey ~/repojordi
	```
	
	Exportem la clau per si de cas:

	```
		borg key export ~/repojordi  ~/copia_clau_repojordi
	```

	Creem el primer backup (Dilluns)
	```
		sudo borg create -v --stats /tmp/repojordi::Dilluns /etc/postgresql/13/main/postgresql.conf /etc/postgresql/13/main/postgresql.conf
	```

+ Eliminem un registre d'una taula d'una base de dades i un altre registre
  d'una taula d'una altra base de dades

	```
	psql training

		DELETE FROM rep_vendes
		WHERE num_empl = 105
	
	psql training2

		DELETE FROM comandes
		WHERE num_comanda = 112961
	```


+ Creem un segon backup, li direm Dimarts

	Parem el servei
	```
		sudo systemctl stop
	```

	Creem el segon backup (Dimarts)
    ```
    	sudo borg create -v --stats /tmp/repojordi::Dimarts /etc/postgresql/13/main/postgresql.conf /etc/postgresql/13/main/postgresql.conf
    ```
	

+ Eliminem un registre més

	```
	psql training

		DELETE FROM rep_vendes
		WHERE num_empl = 107
	
	psql training2

		DELETE FROM comandes
		WHERE num_comanda = 113012
	```

+ Restaurem a Dimarts

	Parem el servei:
	
	``` 
		sudo systemctl stop postgresql
	``` 
	
	Ens col·loquem a `/` i restaurem:

	```
		cd /
		sudo borg extract /tmp/repojordi::Dimarts
	```
	
	Engeguem el servei:

	```
		sudo systemctl start postgresql
	``` 

	Comprovem que el registre que havíem eliminat al pas anterior hi és, però els que havíem eliminat abans de la còpia de Dilluns no.


+ [Manual de Borg](https://borgbackup.readthedocs.io/en/stable/)