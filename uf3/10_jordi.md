# Exercicis de transaccions


## Exercici 1

```sql
CREATE TABLE punts (
    id  INT  NOT NULL,
    valor INT NOT NULL,
    PRIMARY KEY(id)
);
```

Analitzant les següents sentències explica quins canvis es realitzen i on es
realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.

```sql
INSERT INTO punts (id, valor)
VALUES (10, 5);
BEGIN;
UPDATE punts
   SET valor = 4
 WHERE id = 10;
ROLLBACK;
SELECT valor
  FROM punts
 WHERE id = 10;   
```

```
Realitza el INSERT a la taula punts, al camp "id" el valor 10 i al camp "valor" el valor 5. 

No es realitza cap canvi perque es desfan els canvis en la transacció.

Al SELECT s'obtindrà el valor 5 del camp "valor" amb "id" 10.
```

## Exercici 2

Analitzant les següents sentències explica quins canvis es realitzen i on es
realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.

```sql
INSERT INTO punts (id, valor)
VALUES (20,5);
BEGIN;
UPDATE punts
   SET valor = 4
 WHERE id = 20;
COMMIT;
SELECT valor
  FROM punts
 WHERE id = 20;

```

```
Es realitza el INSERT dels valors indicats i a la transacció s'actualitza el camp valor amb el valor 4, on abans era un 5, a la fila on el id és 20.

Per tant, al realitzar la consulta, es mostra el valor 4.
```

## Exercici 3

Analitzant les següents sentències explica quins canvis es realitzen i on es
realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.

```sql
INSERT INTO punts (id, valor)
VALUES (30,5);
BEGIN;
UPDATE punts
   SET valor = 4
 WHERE id = 30;
SAVEPOINT a;
INSERT INTO punts (id, valor)
VALUES (31,7);
ROLLBACK;
SELECT valor
  FROM punts
 WHERE id = 30;
```

```
Inserex els valors indicats, comença la transacció i es canvia el valor de la fila id 30 a 4.

S'estableix una marca en la transacció amb SAVEPOINT on es realitzen uns canvis, però aquests canvis són desfets amb un ROLLBACK, per tant, tota la transacció es desfa, inclòs la d'abans del SAVEPOINT.

La consulta mostra el valor 5.
```

## Exercici 4

Analitzant les següents sentències explica quins canvis es realitzen i on es
realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.

```sql
DELETE FROM punts;
INSERT INTO punts (id, valor)
VALUES (40,5);
BEGIN;
UPDATE punts 
   SET valor = 4
 WHERE id = 40;
SAVEPOINT a;
INSERT INTO punts (id, valor)
VALUES (41,7);
ROLLBACK TO a;
SELECT COUNT(*)
  FROM punts;
```

```
S'esborren tots els valors de la taula.

S'insereixen els valors 40 i 5 als camps id i valor, respectivament.

Comença la transacció i 'sactualitza el valor de 40 a 4. Es crea una marca a la transacció.

S'insereixen nou valors a la taula però es fa un rollback fins a la marca, per tant, l'últim INSERT no es realitza.

La consulta mostra el número de valors actuals, que és 1.
```

## Exercici 5

Analitzant les següents sentències explica quins canvis es realitzen i on es
realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.

```sql
INSERT INTO punts (id, valor)
VALUES (50,5);
BEGIN;
SELECT id, valor
  FROM punts;
UPDATE punts
   SET valor = 4
 WHERE id = 50;
COMMIT;
SELECT valor
  FROM punts
 WHERE id = 50;
```

```
S'insereixen els valors indicats a la taula. Comença transacció.

Es realitza una consulta on es mostren tots els camps i s'actualitza la fila 50 amb el valor 4, es realitzen els canvis amb COMMIT.

La consulta mostra el valor 4.
```

## Exercici 6

Analitzant les següents sentències explica quins canvis es realitzen i on es
realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.

```
DELETE FROM punts;
INSERT INTO punts (id, valor)
VALUES (60,5);
BEGIN;
UPDATE punts
   SET valor = 4
 WHERE id = 60;
SAVEPOINT a;
INSERT INTO punts (id, valor)
VALUES (61,8);
SAVEPOINT b;
INSERT INTO punts (id, valor)
VALUES (62,9);
ROLLBACK TO b;
COMMIT;
SELECT SUM(valor)
  FROM punts;
```

```
S'introdueixen els valors indicats fins al SAVEPOINT "b", és a dir, després d'aquest punt, no es realitza cap canvi ja que es desfan amb ROLLBACK TO "b".

La consulta mostra la suma de la columna valor, que és 8 + 4 = 12.
```


## Exercici 7

Analitzant les següents sentències explica quins canvis es realitzen i on es
realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en
compte que cada sentència s'executa en una connexió determinada.

```
DELETE FROM punts; -- Connexió 0
INSERT INTO punts (id, valor)
VALUES (70,5); -- Connexió 0

BEGIN; -- Connexió 1
DELETE FROM punts; -- Connexió 1

SELECT COUNT(*) 
  FROM punts; -- Connexió 2
```

```
Hi han 3 usuaris diferents a la taula.

Usuari 0 esborra les dades de la taula i insereix els nous valors indicats.

Usuari 1 comença una transacció on vol esborrar la taula però no acaba la transacció. Es queda "penjat"

Usuari 2 realitza una consulta on es mostra quants valors hi ha a la taula. No s'han esborrat ja que la transacció de l'usuari anterior no ha estat finalitzada.

```

## Exercici 8

Analitzant les següents sentències explica quins canvis es realitzen i on es
realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en
compte que cada sentència s'executa en una connexió determinada.

```
INSERT INTO punts (id, valor)
VALUES (80,5); -- Connexió 0
INSERT INTO punts (id, valor)
VALUES (81,9); -- Connexió 0

BEGIN; -- Connexió 1
UPDATE punts 
   SET valor = 4
 WHERE id = 80; -- Connexió 1

BEGIN; -- Connexió 2
UPDATE punts 
   SET valor = 8
 WHERE id = 81; -- Connexió 2

UPDATE punts
   SET valor = 10
 WHERE id = 81; -- Connexió 1

UPDATE punts
   SET valor = 6
 WHERE id = 80; -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor
  FROM punts
 WHERE id = 80; -- Connexió 0
```

```
Usuari 0 insereix els valors indicats a la taula.

Usiari 1 comença una transacció on vol actualitzar el valor del camp amb id 80.

Usuari 2 comença una altra transacció on vol canviar el valor del camp amb id 81.

Usuari 1, dins de la seva transacció anterior, intenta actualitzar el valor del id 81 però es queda "penjat" ja que l'usuari 2 te bloquejada la fila.

Usuari 2, torna a fer un intent de actualització de la fila 80, pero es queda "penjat" també ja que l'usuari 1 té bloquejada la fila 80 amb el seu primer canvi a la transacció.

Surt un error de "sharelock" a l'usuari 2.

Usuari 2 realitza COMMIT però a la sortida es mostra ROLLBACK.

Usuari 1 realitza COMMIT i a la sortida es mostra COMMIT.

La sortida de la consulta de l'usuari 0 és 4, la primera actualització que realitza l'usuari 1.
```

## Exercici 9

Analitzant les següents sentències explica quins canvis es realitzen i on es
realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en
compte que cada sentència s'executa en una connexió determinada.

```
INSERT INTO punts (id, valor)
VALUES (90,5); -- Connexió 0

BEGIN; -- Connexió 1
DELETE FROM punts; -- Connexió 1

BEGIN; -- Connexió 2
INSERT INTO punts (id, valor)
VALUES (91,9); -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor
  FROM punts
 WHERE id = 91; -- Connexió 0
```

```
Usuari 0 insereix els valors indicats.

Usuari 1 comença transacció i esborra la taula.

Usuari 2 comença transacció i insereix nous valors i realitza COMMIT.

Usuari 1 realitza COMMIT i esborra els valors que s'han inserit al primer INSERT, de l'usuari 0.

Per tant, la consulta mostra els valors inserits per l'usuari 2, 91, 9.
```

## Exercici 10

Analitzant les següents sentències explica quins canvis es realitzen i on es
realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en
compte que cada sentència s'executa en una connexió determinada.

```
INSERT INTO punts (id, valor)
VALUES (100,5); -- Connexió 0

BEGIN; -- Connexió 1
UPDATE punts
   SET valor = 6
 WHERE id = 100; -- Connexió 1

BEGIN; -- Connexió 2
UPDATE punts
   SET valor = 7
 WHERE id = 100; -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts
 WHERE id = 100; -- Connexió 0
```

```
Usuari 0 insereix els valors indicats.

Usuari 1 comença transacció on preten aplicar uns canvis als valors anteriors.

Usuari 2 comença transacció on també preten aplicar uns canvis als valors inicials. Es queda "penjat" ja que usuari 1 te bloquejada la fila amb id 100. El COMMIT es queda penjat també.

Usuari 1 realitza COMMIT. Usuari 2 finalitza el COMMIT també, ja que es desbloqueja el bloqueig.

La consulta mostra els canvis realitzats per l'usuari 2 (100,7) ja que ha sigut el primer en realitzar COMMIT;
```

## Exercici 11

Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.

```
INSERT INTO punts (id, valor)
VALUES (110,5); -- Connexió 0
INSERT INTO punts (id, valor)
VALUES (111,5); -- Connexió 0

BEGIN; -- Connexió 1
UPDATE punts
   SET valor = 6
 WHERE id = 110; -- Connexió 1

BEGIN; -- Connexió 2
UPDATE punts
   SET valor = 7
 WHERE id = 110; -- Connexió 2
UPDATE punts
   SET valor = 7
 WHERE id = 111; -- Connexió 2
SAVEPOINT a; -- Connexió 2
UPDATE punts
   SET valor = 8
 WHERE id = 110; -- Connexió 2
ROLLBACK TO a; -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor 
  FROM punts 
 WHERE id = 111; -- Connexió 0
```

```
Usuari 0 insereix els valors indicats.

Usuari 1 comença transacció on preten realitzar uns canvis.

Usuari 2 comença transacció on també pretèn realitzar uns canvis pero es queda "penjat" ja que l'usuari 1 té la fila bloquejada. Insereix la nova fila 111. Estableix una marca amb SAVEPOINT. Insereix un nou canvi a la fila 110 i ROLLBACK a SAVEPOINT. Realitza COMMIT.

Usuari 1 realitza COMMIT.

Tots els canvis realitzats per usuari 2 es realitzen menys el que fa després del SAVEPOINT.

Per tant, la consulta mostra 111, 7.
```

## Exercici 12

Analitzant les següents sentències explica quins canvis es realitzen i on es
realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en
compte que cada sentència s'executa en una connexió determinada.

```
INSERT INTO punts (id, valor)
VALUES (120,5); -- Connexió 0
INSERT INTO punts (id, valor) 
VALUES (121,5); -- Connexió 0

BEGIN; -- Connexió 1
UPDATE punts
   SET valor = 6
 WHERE id = 121; -- Connexió 1
SAVEPOINT a;
UPDATE punts
   SET valor = 9
 WHERE id = 120; -- Connexió 1

BEGIN; -- Connexió 2
UPDATE punts 
   SET valor = 7
 WHERE id = 120; -- Connexió 2

ROLLBACK TO a; -- Connexió 1

SAVEPOINT a; -- Connexió 2
UPDATE punts
   SET valor = 8
 WHERE id = 120; -- Connexió 2
ROLLBACK TO a; -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor
  FROM punts
 WHERE id = 121; -- Connexió 0
```

```
Usuari 0 insereix els valors indicats.

Usuari 1 comença transacció on realitza uns canvis a la fila 121, estableix una marca amb SAVEPOINT i torna a fer canvis a la fila 120.

Usuari 2 comença transacció, realitza canvis a la fila 120. Es queda "penjat" ja que usuari 1 té la fila 120 bloquejada.

Usuari 1 fa ROLLBACK a la marca SAVEPOINT. Per tant, els seus canvis després del SAVEPOINT són descartats.

Usuari 2 estableix una nova marca amb SAVEPOINT i realitza canvi a la fila 120. Fa ROLLBACK al SAVEPOINT i a continuació COMMIT, per tant, es descarten els canvis després de SAVEPOINT.

Usuari 1 realitza COMMMIT dels seus canvis realitzats.

La consulta mostra 121,6. Només s'han realitzat els canvis fets abans de que els usuaris establéssin els SAVEPOINTS, ja que tots els canvis fets després queden descartats.
```