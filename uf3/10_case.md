# Activitat 4. CASE. 

## Estructura de control - Exercicis

1.- Llistar l'identificador i el nom dels representants de vendes. Mostrar un camp anomenat "result" que mostri 0 si la quota és inferior a les vendes, en cas contrari ha de mostrar 1 a no ser que sigui director d'oficina, en aquest cas ha de mostrar 2.
```sql
SELECT num_empl, nom, 
  CASE 
  WHEN quota < vendes THEN '0' 
  WHEN num_empl IN (SELECT cap FROM oficines) THEN '2' 
  ELSE '1' 
   END AS result 
  FROM rep_vendes;
```
2.- Llistar tots els productes amb totes les seves dades afegint un nou camp anomenat "div". El camp div ha de contenir el resultat de la divisió entre el preu i les existències. En cas de divisió per zero, es canviarà el resultat a 0.
```sql
SELECT *, 
  CASE 
  WHEN estoc = 0 THEN '0' 
  ELSE ROUND(preu / estoc,2) 
   END AS div 
  FROM productes;
```
3.- Afegir una condició a la sentència de l'exercici anterior per tal de nomès mostrar aquells productes que el valor del camp div és menor a 1.
```sql
SELECT *, 
  CASE 
  WHEN estoc = 0 THEN '0' 
  ELSE ROUND(preu / estoc,2) 
   END AS div 
  FROM productes;


SELECT *, 
  CASE 
  WHEN estoc = 0 THEN '0' 
  ELSE ROUND(preu / estoc,2) 
   END AS div 
  FROM productes 
 WHERE CASE 
  WHEN estoc = 0 THEN '0' 
  ELSE ROUND(preu / estoc,2) 
   END < 1;
```
4.- Amb una única sentència SQL, i fent servir CASE, establiu la quota a només 1 (1€) als treballadors menors de 35 anys i als treballadors de New York i pugeu un 5% a la resta.
```sql
UPDATE rep_vendes 
   SET quota = 
  CASE 
  WHEN edat < 35 
    OR oficina_rep IN (SELECT oficina 
                         FROM oficines 
                        WHERE ciutat = 'New York') 
  THEN 1 
  ELSE quota * 1.05 
  END;
```