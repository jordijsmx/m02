/* 2- Crear una VIEW per visualitzar, per cada equip , NUM_PARTITS_JUGATS, 
NUM_PARTITS_GUANYATS, NUM_PARTIS_PERDUTS */

-- Partits jugats

CREATE VIEW num_partits_jugats AS 
(SELECT id_equip, nom, (SELECT COUNT(*) FROM partits WHERE (equiplocal=id_equip) OR (equipvisitant=id_equip)) FROM equips);

-- Partits guanyats

CREATE VIEW  num_partits_guanyats AS
(SELECT id_equip, nom, 
                            
                            ((SELECT COUNT(*) FROM partits WHERE equiplocal=id_equip AND puntsequiplocal > puntsequipvisitant) + 
                            (SELECT COUNT(*) FROM partits WHERE equipvisitant=id_equip AND puntsequipvisitant > puntsequiplocal)) AS guanyats 
 FROM equips;)

-- Partits perduts

CREATE VIEW num_partits_perduts AS
(SELECT id_equip, nom, 
                                                                                                                           
                            ((SELECT COUNT(*) FROM partits WHERE equiplocal=id_equip AND puntsequiplocal < puntsequipvisitant) + 
                            (SELECT COUNT(*) FROM partits WHERE equipvisitant=id_equip AND puntsequipvisitant < puntsequiplocal)) AS perduts 
FROM equips;)


/* 3- Crear una VIEW per visualitzar els punts que porta acumulats cada equip,
mostrant-los ordenats del primer a l'últim de la classificació.*/

CREATE VIEW punts AS (SELECT id_equip, nom, (SELECT COUNT(*) FROM partits WHERE id_equip = equiplocal OR id_equip = equipvisitant )+
                      (SELECT COUNT(*) FROM partits WHERE id_equip = equiplocal AND puntsequiplocal > puntsequipvisitant)+
                      (SELECT COUNT(*) FROM partits WHERE id_equip = equipvisitant AND puntsequiplocal < puntsequipvisitant) AS total_punts,
                      ((SELECT SUM(puntsequiplocal) FROM partits WHERE id_equip = equiplocal)+
                      (SELECT SUM(puntsequipvisitant) FROM partits WHERE id_equip = equipvisitant)) AS num_punts_a_favor,
                      ((SELECT SUM(puntsequipvisitant) FROM partits WHERE id_equip = equiplocal)+
                      (SELECT SUM(puntsequiplocal) FROM partits WHERE id_equip = equipvisitant)) AS num_punts_en_contra
 FROM equips
 ORDER BY total_punts DESC, num_punts_a_favor DESC); 

/* 4- Amb la Base de dades INSTA crear una Materialized VIEW per cadascun d'aquests comptadors :

- comptabilitzar el número de likes de cada post */

CREATE MATERIALIZED VIEW num_likes AS
    (SELECT id_post, id_usuari_post, COUNT(id_usuari_like) 
       FROM likes 
       JOIN posts 
         ON id_post = id_publicacio 
      GROUP BY 1,2);

REFRESH MATERIALIZED VIEW num_likes;

/* - comptabilitzar el número de seguidors de cada usuari */

INSERT INTO seguidors VALUES(1,1,2,DEFAULT,DEFAULT,DEFAULT);
INSERT INTO seguidors VALUES(2,2,1,DEFAULT,DEFAULT,DEFAULT);
INSERT INTO seguidors VALUES(3,2,3,DEFAULT,DEFAULT,DEFAULT);

CREATE MATERIALIZED VIEW num_usuaris_seguidors AS 
    (SELECT id_usuari, COUNT(id_seguiment) 
       FROM usuaris 
       LEFT JOIN seguidors 
         ON id_seguit = id_usuari 
      GROUP BY 1);

/* - comptabilitzar el número d'usuaris que segueix cada usuari */

CREATE MATERIALIZED VIEW num_usuaris_seguits AS
    (SELECT id_usuari, COUNT(id_seguiment) 
       FROM usuaris 
       LEFT JOIN seguidors 
         ON id_usuari = id_seguidor 
      GROUP BY 1);

/* - comptabilitzar el número de comentaris de cada post */

 INSERT INTO comentaris VALUES(1,2,2,'Hola Jordi!!!', DEFAULT, DEFAULT);

CREATE MATERIALIZED VIEW num_comentaris_post AS 
    (SELECT id_post, COUNT(id_comentari) 
       FROM posts 
       JOIN comentaris 
         ON id_post = id_post_com 
      GROUP BY 1);

 id_post | count 
---------+-------
       2 |     1
       1 |     1
(2 rows)

INSERT INTO comentaris VALUES(3,1,3,'Hola Jordi, magrada el teu post', DEFAULT, DEFAULT);

REFRESH MATERIALIZED VIEW num_comentaris_post ;

 id_post | count 
---------+-------
       2 |     1
       1 |     2
(2 rows)

/* 5- Comprova com no s'actualitzen les VIEWS a l'actualitzar les dades de l'INSTA */

SELECT * FROM num_likes ;
 id_post | id_usuari_post | count 
---------+----------------+-------
       2 |              2 |     1
       1 |              1 |     2
(2 rows)

INSERT INTO likes VALUES(4,DEFAULT,DEFAULT,3,1);

SELECT * FROM num_likes ;
 id_post | id_usuari_post | count 
---------+----------------+-------
       2 |              2 |     1
       1 |              1 |     2
(2 rows)

/*  7) Crear una VIEW amb el SELECT que retorni, per cada usuari, el número de seguidors i 
el número d'usuaris a qui segueix. Fer la consulta a les VIEWs creades a l'activitat anterior */

CREATE MATERIALIZED VIEW calculs AS
SELECT id_usuari, nom, (SELECT 2 FROM num_usuaris_seguits
                              WHERE usuaris.id_usuari = num_usuaris_seguits.id_usuari),
                              (SELECT 2 FROM num_usuaris_seguidors
                              WHERE usuaris.id_usuari = num_usuaris_seguidors.id_usuari),
                              (SELECT COUNT(id_usuari_post) FROM posts
                              WHERE usuaris.id_usuari = id_usuari_post)
   FROM usuaris;

/* 8) Crear una VIEW amb el SELECT que retorni, per cada usuari, el número de seguidors i 
el número d'usuaris a qui segueix, el número de publicacions i el número total de LIKES rebuts */

CREATE MATERIALIZED VIEW calculs AS
SELECT id_usuari, nom, (SELECT 2 FROM num_usuaris_seguits
                              WHERE usuaris.id_usuari = num_usuaris_seguits.id_usuari),
                              (SELECT 2 FROM num_usuaris_seguidors
                              WHERE usuaris.id_usuari = num_usuaris_seguidors.id_usuari),
                              (SELECT COUNT(id_usuari_post) FROM posts
                              WHERE usuaris.id_usuari = id_usuari_post),
                              (SELECT COUNT(*) FROM posts
                                 JOIN likes
                                   ON posts.id_post = likes.id_publicacio
                                WHERE usuaris.id_usuari = id_usuari_post)
   FROM usuaris;