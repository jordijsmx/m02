# Backup lògic

![diagrama backup i restauració lògica amb Postgresql](PostgreSQL_dump_restore.svg.png)

## Eines de PostgreSQL

Quan el sistema gestor de Base de Dades Relacional (SGBDR o RDBMS en anglès) és
PostgreSQL, les ordres clàssiques per fer la còpia de base de dades són:

+ `pg_dump` per copiar una base de dades del SGBDR a un fitxer script o de
  tipus arxiu 

+ `pg_dumpall` per copiar totes les bases de dades del SGBDR a un fitxer script
  SQL


Anàlogament, quan volem restaurar una base de dades si el fitxer destí ha estat un script SQL farem la restauració amb l'ordre `pg_restore`, mentre que si el fitxer destí és de tipus binari l'ordre a utilitzar serà `______`.

Respon i després *executa les
instruccions comprovant empíricament que són correctes*.


## Pregunta 1. 

Quina instrucció ens fa una còpia de seguretat lògica de la base de dades training (de tipus script SQL) ?
```bash
pg_dump training > fitxer.sql
```
## Pregunta 2.

El mateix d'abans però ara el destí és un directori.
```bash
pg_dump -Fd training -f /directori/
```
## Pregunta 3.

El mateix d'abans però ara el destí és un fitxer tar.
```bash
pg_dump -Ft training > fitxer.tar
```
## Pregunta 4. 

El mateix d'abans però ara el destí és un fitxer tar però la base de dades és mooolt gran.

I com restauraries la base de dades a partir del fitxer obtingut?
```bash
pg_dump -Ft training > fitxer.tar | gzip fitxer.tgz

gunzip fitxer.tgz
```
## Pregunta 5

Imagina que vols fer la mateixa ordre d'abans però vols optimitzar el temps
d'execució. No pots esperar tant de temps en fer el backup. Quina ordre
aconseguirà treballar en paral·lel? Fes la mateixa ordre d'abans però atacant
la info de les 5 taules de la base de dades al mateix temps.
```bash
pg_dump -j 5 -Fd training -f /directori
```
## Pregunta 6

Si no indiquem usuari ni tipus de fitxer quin és el valor per defecte?
```bash
pg_dump training

Mostra per sortida estàndar l'estructura de la base de dades de l'usuari que utilitza l'ordre.
```
## Pregunta 7

Fes una còpia de seguretat lògica només de la taula _comandes_ de tipus script SQL.
```bash
pg_dump -t comandes training > comandes.sql
```
## Pregunta 8

Fes una còpia de seguretat lògica només de la taula _rep_vendes_ de tipus binari.
```bash
pg_dump -Fc -t rep_vendes training > rep_vendes.dump
```
## Pregunta 9 

Elimina la taula _comandes_ de la base de dades _training_. 

Quina ordre restaura la taula _comandes_? (recorda que el fitxer és un script SQL) 
```bash
DROP TABLE comandes CASCADE;

psql training < comandes.sql
```
## Pregunta 10

Fes un cop d'ull al backup tar creat a l'exercici 3 (per exemple amb l'ordre `tar xvf` ). Creus que a partir d'aquest fitxer es podria restaurar només una taula? Si és així, escriu i comprova-ho amb rep_vendes (abans hauràs d'eliminar la taula amb `DROP`)
```bash
pg_dump -Ft -t rep_vendes training > rep_vendes.tar

DROP TABLE rep_vendes CASCADE;

pg_restore -d training rep_vendes.tar
```
## Pregunta 11

Elimina la taula *rep_vendes* de la base de dades training. 

Quina ordre restaura la taula *rep_vendes* (recorda que el fitxer és binari)
```bash
pg_dump -Fc -t rep_vendes training > rep_vendes.bin

DROP TABLE rep_vendes CASCADE;

pg_restore -d training rep_vendes.dat (o < rep_vendes.bin)
```
## Pregunta 12

Després de fer una còpia de tota la base de dades training en format binari, elimina-la. 

Hi ha l'ordre de bash `dropdb` (en realitat és un wrapper de DROP DATABASE). I de manera anàloga també n'hi ha l'ordre de bash `createdb`.
Sense utilitzar aquesta última i amb una única ordre restaura la base de dades training.
```bash
pg_dump -Fc training > training.bin

dropdb training

pg_restore -C -d postgres training.bin
```
## Pregunta 13

Per defecte, si n'hi ha un error quan fem la restauració `psql` ignora l'error i continuarà executant-se. Si volem que pari just quan hi hagi l'errada quina opció afegirem a l'ordre `psql` ?
```bash
psql -v ON_ERROR_STOP=1 training < fitxer.sql
```
## Pregunta 14

Si es vol que la restauració es faci en mode transacció, és a dir o es fa tot o no es fa res, com seria l'ordre?
```bash
psql --single-transaction -d training < fitxer.sql
```
## Pregunta 15

Quina ordre em permet fer un backup de **totes** les bases de dades d'un cluster? Es necessita ser l'administrador de la base de dades?
```bash
pg_dumpall -S postgres > fitxer2.sql

Si, es necessari ser l'administrador si no dona error de permisos.
```
## Pregunta 16

Quina ordre em permet restaurar **totes** les bases de dades d'un cluster a partir del fitxer `backup_all_db.sql`? Es necessita ser l'administrador de la base de dades?
```bash
psql -f < backup_all_db.sql
```
## Pregunta 17

Quina ordre em permet fer una còpia de seguretat de tipus `tar` la base de dades `training` que es troba a 192.168.0.123 ? Canvia la IP per la de l'ordinador del teu company.
```bash
pg_dump -h 10.200.243.207 -F t training > fitxer.tar

pg_dump -h 10.200.243.207 -U xxxxxxx -F t training > fitxer.tar
```
## Pregunta 18

Es pot fer un backup de la base de dades training del server1 i que es restauri _on the fly_ a server2?
```bash
pg_dump -h server1 training > fitxer.sql | psql -h server2 training < fitxer.sql
```