# Examen DML, DDL (Views) CASE

## Pregunta 1

Amb una única sentència SQL, establiu la quota a només 1 (1€) als treballadors menors de 35 anys i als treballadors de New York i pugeu un 5% a la resta. Pista: feu servir CASE
```sql
UPDATE rep_vendes
SET quota =
CASE
WHEN edat < 35 OR oficina_rep IN (SELECT oficina
                                    FROM oficines
                                   WHERE ciutat = 'New York')
THEN 1
ELSE quota * 1.05
END;
```
## Pregunta 2

Esborreu els representants de vendes que tenen unes vendes inferiors a 300000 i no són directors d'oficina ni caps d'empleats.
```sql
DELETE FROM rep_vendes
WHERE vendes < 300000
AND num_empl NOT IN (SELECT director 
                       FROM oficines)
AND num_empl NOT IN (SELECT cap
                       FROM rep_vendes);
```

## Pregunta 4

Creeu una vista anomenada "rep_top_ofi" que llisti totes les dades dels representants de vendes que estan assignats a l'oficina que té més vendes.
```sql
CREATE VIEW rep_top_ofi
AS SELECT * 
     FROM rep_vendes
    WHERE oficina_rep = (SELECT oficina
                           FROM oficines
                          ORDER BY vendes DESC
                          LIMIT 1);
```