# Exercicis de transaccions


## Exercici 1

Analitzant les següents sentències explica quins canvis es realitzen i on es
realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.

```sql
INSERT INTO punts (id, valor)
VALUES (10, 5);
BEGIN;
UPDATE punts
   SET valor = 4
 WHERE id = 10;
ROLLBACK;
SELECT valor
  FROM punts
 WHERE id = 10;
```

- Realitza INSERT
- Comença transacció, es realitza UPDATE i ROLLBACK. No es fa cap canvi.
- Té el valor del primer INSERT. **id=10 valor=5**

## Exercici 2

Analitzant les següents sentències explica quins canvis es realitzen i on es
realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.

```sql
INSERT INTO punts (id, valor)
VALUES (20,5);
BEGIN;
UPDATE punts
   SET valor = 4
 WHERE id = 20;
COMMIT;
SELECT valor
  FROM punts
 WHERE id = 20;
```

- Es fa INSERT **id=20 valor=5**
- Comença transacció
- Realitza UPDATE i COMMIT. Es fan els canvis.
- **id=20 valor=5**

## Exercici 3

Analitzant les següents sentències explica quins canvis es realitzen i on es
realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.

```sql
INSERT INTO punts (id, valor)
VALUES (30,5);
BEGIN;
UPDATE punts
   SET valor = 4
 WHERE id = 30;
SAVEPOINT a;
INSERT INTO punts (id, valor)
VALUES (31,7);
ROLLBACK;
SELECT valor
  FROM punts
 WHERE id = 30;
```



## Exercici 4

Analitzant les següents sentències explica quins canvis es realitzen i on es
realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.

```sql
DELETE FROM punts;
INSERT INTO punts (id, valor)
VALUES (40,5);
BEGIN;
UPDATE punts 
   SET valor = 4
 WHERE id = 40;
SAVEPOINT a;
INSERT INTO punts (id, valor)
VALUES (41,7);
ROLLBACK TO a;
SELECT COUNT(*)
  FROM punts;
```

## Exercici 5

Analitzant les següents sentències explica quins canvis es realitzen i on es
realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.

```sql
INSERT INTO punts (id, valor)
VALUES (50,5);
BEGIN;
SELECT id, valor
 WHERE punts;
UPDATE puntsselec
   SET valor = 4
 WHERE id = 50;
COMMIT;
SELECT valor
  FROM punts
 WHERE id = 50;
```

- Es realitza un INSERT **id=50 valor=4** 
- Comença transacció
- Es fa una consulta incorrecta, dona error, per tant la transacció falla.
- El UPDATE no es realitza.
- El COMMIT serà un ROLLBACK per l'error.
- La última consulta mostra 5.

## Exercici 6

Analitzant les següents sentències explica quins canvis es realitzen i on es
realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.

```sql
DELETE FROM punts;
INSERT INTO punts (id, valor)
VALUES (60,5);
BEGIN;
UPDATE punts
   SET valor = 4
 WHERE id = 60;
SAVEPOINT a;
INSERT INTO punts (id, valor)
VALUES (61,8);
SAVEPOINT b;
INSERT punts (id, valor)
VALUES (62,9);
ROLLBACK TO b;
COMMIT;
SELECT SUM(valor)
  FROM punts;
```
- S'esborren les dades i es realitza un INSERT amb els valors indicats.
- Comença transacció.
- UPDATE **id=60 valor=4**
- Es crea SAVEPOINT a
- Es crea el **id=61 valor=8**
- Es crea SAVEPOINT b
- Es fa un INSERT incorrecte, hi ha error a la transacció.
- ROLLBACK a "b", l'error es cancela.
- Es fa COMMIT i mostra la suma dels valors inserits **8+4=12**

## Exercici 7

Analitzant les següents sentències explica quins canvis es realitzen i on es
realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en
compte que cada sentència s'executa en una connexió determinada.

```sql
DELETE FROM punts; -- Connexió 0
INSERT INTO punts (id, valor)
VALUES (70,5); -- Connexió 0

BEGIN; -- Connexió 1
DELETE FROM punts; -- Connexió 1

SELECT COUNT(*) 
  FROM punts; -- Connexió 2
```

- INSERT de **id=70 valor=5**
- Connexió1: Comença transacció, esborra dades.
- Connexió2: Apareix la dada, no s'ha esborrat res.

## Exercici 8

Analitzant les següents sentències explica quins canvis es realitzen i on es
realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en
compte que cada sentència s'executa en una connexió determinada.

```sql
INSERT INTO punts (id, valor)
VALUES (80,5); -- Connexió 0
INSERT INTO punts (id, valor)
VALUES (81,9); -- Connexió 0

BEGIN; -- Connexió 1
UPDATE punts 
   SET valor = 4
 WHERE id = 80; -- Connexió 1

BEGIN; -- Connexió 2
UPDATE punts 
   SET valor = 8
 WHERE id = 81; -- Connexió 2

UPDATE punts
   SET valor = 10
 WHERE id = 81; -- Connexió 1

UPDATE punts
   SET valor = 6
 WHERE id = 80; -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor
  FROM punts
 WHERE id = 80; -- Connexió 0
```

- INSERT amb **id=80 valor=5**
- INSERT amb **id=81 valor=9**
- Con1: Comença transacció. UPDATE de **id=80 a valor=4**
- Con2: Comença transacció. UPDATE de **id=81 a valor=8**
- Con1: UPDATE de **id=81 a valor=10**. NO s'actualitza
- Con2: UPDATE de **id=80 a valor=6**.
- Con2: COMMIT, es queda bloquejat, esperant a Con1.
- Con1: COMMIT, es realitzen els canvis

## Exercici 9

Analitzant les següents sentències explica quins canvis es realitzen i on es
realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en
compte que cada sentència s'executa en una connexió determinada.

```sql
INSERT INTO punts (id, valor)
VALUES (90,5); -- Connexió 0

BEGIN; -- Connexió 1
DELETE FROM punts; -- Connexió 1

BEGIN; -- Connexió 2
INSERT INTO punts (id, valor)
VALUES (91,9); -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor
  FROM punts
 WHERE id = 91; -- Connexió 0
```

- INSERT dels primers valors.
- Con1: Comença transacció i realitza DELETE de punts.
- Con2: Comença transacció i realitza INSERT dels valors introduits. COMMIT.
- Con1: COMMIT.
- S'ha esborrat les dades del primer INSERT, però NO del segon.

## Exercici 10

Analitzant les següents sentències explica quins canvis es realitzen i on es
realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en
compte que cada sentència s'executa en una connexió determinada.

```sql
INSERT INTO punts (id, valor)
VALUES (100,5); -- Connexió 0

BEGIN; -- Connexió 1
UPDATE punts
   SET valor = 6
 WHERE id = 100; -- Connexió 1

BEGIN; -- Connexió 2
UPDATE punts
   SET valor = 7
 WHERE id = 100; -- Connexió 2  -- DEADLOCK
COMMIT; -- Connexió 2 -- BLOQUEJAT, ESPERANT EL SEGÜENT COMMIT

COMMIT; -- Connexió 1

SELECT valor FROM punts
 WHERE id = 100; -- Connexió 0  -- VALOR 7
```


## Exercici 11

Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.

```sql
INSERT INTO punts (id, valor)
VALUES (110,5); -- Connexió 0
INSERT INTO punts (id, valor)
VALUES (111,5); -- Connexió 0

BEGIN; -- Connexió 1
UPDATE punts
   SET valor = 6
 WHERE id = 110; -- Connexió 1

BEGIN; -- Connexió 2
UPDATE punts
   SET valor = 7
 WHERE id = 110; -- Connexió 2 -- BLOQUEJAT, CON 1 TE CANVIS PER REALITZAR
UPDATE punts
   SET valor = 7
 WHERE id = 111; -- Connexió 2
SAVEPOINT a; -- Connexió 2
UPDATE punts
   SET valor = 8
 WHERE id = 110; -- Connexió 2 -- NO ES REALITZEN AQUESTS CANVIS
ROLLBACK TO a; -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1 -- DESBLOQUEJAT

SELECT valor 
  FROM punts 
 WHERE id = 111; -- Connexió 0 -- VALOR 7
```

## Exercici 12

Analitzant les següents sentències explica quins canvis es realitzen i on es
realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en
compte que cada sentència s'executa en una connexió determinada.

```sql
INSERT INTO punts (id, valor)
VALUES (120,5); -- Connexió 0
INSERT INTO punts (id, valor) 
VALUES (121,5); -- Connexió 0

BEGIN; -- Connexió 1
UPDATE punts
   SET valor = 6
 WHERE id = 121; -- Connexió 1
SAVEPOINT a;
UPDATE punts
   SET valor = 9
 WHERE id = 120; -- Connexió 1

BEGIN; -- Connexió 2
UPDATE punts 
   SET valor = 7
 WHERE id = 120; -- Connexió 2 -- BLOQUEIG, CON 1 TE CANVIS PER REALITZAR

ROLLBACK TO a; -- Connexió 1 -- ES DESBLOQUEJA

SAVEPOINT a; -- Connexió 2
UPDATE punts
   SET valor = 8
 WHERE id = 120; -- Connexió 2
ROLLBACK TO a; -- Connexió 2 -- NO ES REALITZA EL UPDATE ANTERIOR
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor
  FROM punts
 WHERE id = 121; -- Connexió 0 -- VALOR 6
```