# Accés i permisos


## Exercici 1

Explica quina configuració ha de tenir el postgresql i quines sentències s'han
d'executar per permetre la connexió des de qualsevol ordinador de l'aula i que
un company des del seu ordinador pugui accedir al postgresql del teu ordinador
usant varis usuaris amb les seves contrasenya emmagatzemades al postgresql.
Explica com comprovar el correcte funcionament.

- Fitxer **/etc/postgresql/13/main/postgresql.conf**
```vim
listen_addresses = '*' 
```
- Fitxer **/etc/postgresql/13/main/pg_hba.conf**
```vim
# TYPE  DATABASE        USER            ADDRESS                 METHOD
  host 	  all	        all		     10.200.243.0/24			 md5
```
- Comandes **psql**
```sql
CREATE USER usuari_bd PASSWORD 'password';
```
- Connexió a **psql** _remot_
```
psql -h <HOST REMOT> -U <USUARI_BD> <BD> 
```
- Connexió a **psql** _local_
```
psql -U <USUARI_BD> <BD> 
```
## Exercici 2

Explica quina configuració ha de tenir el postgresql i quines sentències s'han
executat per tal que cap usuari es pugui connectar a cap base de dades des d'un
ordinador concret de l'aula, però s'ha d'acceptar connexions des de tots els
altres ordinadors de l'aula. Explica com comprovar el correcte funcionament.

- Hem d'afegir la IP que volem rebutjar al fitxer **pg_hba.conf**
```sql
# TYPE  DATABASE        USER            ADDRESS                 METHOD
  host 	  all	        all		   10.200.243.[IP]/32		    reject
  host 	  all	        all		     10.200.243.0/24			 md5
```

## Exercici 3

Explica quina configuració ha de tenir el postgresql i quines sentències s'han
executat per tal que un usuari anomenat "con_rem" només pugui accedir a la base de
dades amb el mateix nom des d'un ordinador concret de l'aula sense usar contrasenya.
No s'ha de permetre la connexió amb aquest usuari des d'altres ordinadors, però si
amb altres usuaris. Explica com comprovar el correcte funcionament.
- Fitxer **pg_hba.conf**
```vim
# TYPE  DATABASE        USER            ADDRESS                 METHOD
  host 	 con_rem	   con_rem		10.200.243.[IP]/32			 trust
  host	 con_rem	   con_rem		 10.200.243.0/24			reject
  host	 con_rem		all			 10.200.243.0/24			  md5
``` 
## Exercici 4

Explica quina configuració ha de tenir el postgresql i quines sentències s'han
executat per tal que un usuari pugui accedir a totes les bases de dades amb
poders absoluts des d'un ordinador en concret. No s'ha de permetre la connexió
amb aquest usuari des d'altres ordinadors, però si amb altres usuaris. L'usuari
s'ha d'anomenar "rem_admin" i la contrasenya emmagatzemada al postgresql ha de
ser "ra". Explica com comprovar el correcte funcionament.
- Comandes **psql**
```sql
CREATE USER rem_admin SUPERUSER PASSWORD 'ra';
```
- Fitxer **pg_hba.conf**
```vim
# TYPE  DATABASE        USER            ADDRESS                 METHOD
  host 	  all	      rem_admin		 10.200.243.[IP]/32			 md5
  host	  all		  rem_admin		 10.200.243.0/24	   	   reject
  host	  all		    all			 10.200.243.0/24			 md5
```
## Exercici 5

Explica quina configuració ha de tenir el postgresql i quines sentències s'han
executat per tal que un usuari pugui accedir a totes les bases de dades des de
qualsevol ordinador de l'aula. L'usuari s'ha d'anomenar "semi_admin" i la
contrasenya, emmagatzemada al postgresql, ha de ser "sa". Aquest usuari ha de
tenir permisos per a poder crear bases de dades i nous usuaris. Explica com
comprovar el correcte funcionament.
- Comandes **psql**
```sql
CREATE USER semi_admin PASSWORD 'sa' CREATEDB CREATEROLE;
```
- Fitxer **pg_hba.conf**
```vim
# TYPE  DATABASE        USER            ADDRESS                 METHOD
  host 	  all	      semi_admin	 10.200.243.0/24			 md5
```
## Exercici 6

Explica quina configuració ha de tenir el postgresql i quines sentències s'han
executat per tal que un usuari només pugui accedir a la base de dades "db123"
des de qualsevol ordinador. L'usuari s'ha d'anomenar "us123" i la contrasenya,
emmagatzemada al postgresql, ha de ser "123". L'usuari només pot tenir 2
connexions simultànies i només s'ha de poder connectar fins el 31-12-2022,
inclòs. Explica com comprovar el correcte funcionament.
- Comandes **psql**
```sql
CREATE USER us123 PASSWORD '123' CONNECTION LIMIT 2 VALID UNTIL '31-12-2022';
```
- Fitxer **pg_hba.conf**
```vim
# TYPE  DATABASE        USER            ADDRESS                 METHOD
  host 	  db123	      us123			 10.200.243.0/24			 md5
```

## Exercici 7

Explica quines sentències s'han d'executar per tal que un usuari anomenat
"magatzem" només pugui modificar les dades de la col·lumna "estoc" de la taula
"productes" de la base de dades "training" propietat del vostre usuari. La
contrasenya de l'usuari s'ha d'emmagatzemar al sistema gestor de base de dades.
Explica com comprovar el correcte funcionament.
- Comandes **psql**
```sql
CREATE USER magatzem PASSWORD 'contrasenya';

GRANT INSERT, UPDATE, DELETE (estoc) ON productes TO magatzem;
```
- Fitxer **pg_hba.conf**
```vim
# TYPE  DATABASE        USER            ADDRESS                 METHOD
  host 	training	   magatzem		10.200.243.0/24			     md5
```
## Exercici 8

Explica quines sentències s'han d'executar per tal que un usuari anomenat "rrhh"
només pugui modificar les dades de la taula "rep_vendes" de la base de dades "training"
propietat del vostre usuari. La contrasenya de l'usuari s'ha d'emmagatzemar al
sistema gestor de base de dades. Explica com comprovar el correcte funcionament.
- Comandes **psql**
```sql
CREATE USER rrhh PASSWORD 'contrasenya';

GRANT INSERT, UPDATE, DELETE ON rep_vendes TO rrhh;
```
```vim
# TYPE  DATABASE        USER            ADDRESS                 METHOD
  host 	training	    rrhh		10.200.243.0/24			     md5
```
## Exercici 9

Explica quines sentències s'han d'executar per tal que un usuari anomenat
"lectura" no pugui modificar les dades ni l'estructura de la base de dades "training"
propietat del vostre usuari. La contrasenya de l'usuari s'ha d'emmagatzemar al
sistema gestor de base de dades. Explica com comprovar el correcte funcionament.
- Comandes **psql**
```sql
CREATE USER lectura PASSWORD 'contrasenya';

GRANT SELECT ON ALL TABLES IN SCHEMA public TO lectura;
```
```vim
# TYPE  DATABASE        USER            ADDRESS                 METHOD
  host 	training	    lectura		10.200.243.0/24			     md5
```
## Exercici 10

Explica quines sentències s'han d'executar per tal que un usuari anomenat
"gestor" pugui modificar les dades i l'estructura de la base de dades training
propietat del vostre usuari. La contrasenya de l'usuari s'ha d'emmagatzemar al
sistema gestor de base de dades. Aquest usuari no pot tenir permisos de
superusuari.  Explica com comprovar el correcte funcionament.
- Comandes **psql**
```sql
CREATE USER gestor PASSWORD 'contrasenya';

GRANT a191545jj TO gestor;
```
```vim
# TYPE  DATABASE        USER            ADDRESS                 METHOD
  host 	training	    gestor		10.200.243.0/24			     md5
```
## Exercici 11

Explica quina configuració ha de tenir el postgresql i quines sentències s'han
executat per obtenir el següent escenari:

+ Els noms assignats han de ser descriptius.

+ Una base de dades d'una empresa de transport que te una taula per
  emmagatzemar els vehicles amb els següents camps:

	- Camp que identifica el vehicle.

	- Camp que indica la data de compra del vehicle.

	- Camp que indica si el vehicle està disponible o bé per algun motiu està
	  al taller, quan s'afegeix un nou vehicle a la taula aquest camp ha de dir 
	  que el vehicle està disponible.
```sql
DROP DATABASE IF EXISTS empresa;
CREATE DATABASE empresa;

\c empresa

-- Taula
CREATE TABLE vehicles (
    id_vehicle      INT NOT NULL UNIQUE,
    data_compra     DATE NOT NULL DEFAULT CURRENT_DATE,
    estat  		    VARCHAR(10) NOT NULL DEFAULT 'Disponible',
    PRIMARY KEY(id_vehicle),
    CHECK(estat = 'Disponible' or estat = 'Taller')
);

-- Dades
INSERT INTO vehicles VALUES(1,DEFAULT,DEFAULT);
INSERT INTO vehicles VALUES(2,'2023-01-01',DEFAULT);
INSERT INTO vehicles VALUES(3,'2022-11-17','Taller');
```
+ Els usuaris guarden les seves contrasenyes al postgresql.

+ L'usuari administrador:

	- Ha de ser un superusuari del postgresql

	- Només s'ha de poder connectar des d'un ordinador concret. 

- Comandes **psql**
```sql
CREATE USER admin SUPERUSER PASSWORD 'admin';
```
- Fitxer **pg_hba.conf**
```vim
# TYPE  DATABASE        USER            ADDRESS                 METHOD
  host 	  empresa	      admin	 	  192.168.1.2/32			 md5
```
+ L'usuari del taller:
	
	- Només ha de poder modificar les dades referents a l'estat del vehicle.
	
	- Només s'ha de poder connectar des d'un ordinador en concret. 
```sql
CREATE USER taller PASSWORD 'taller';

GRANT UPDATE (estat) ON vehicles TO taller;
```
- Fitxer **pg_hba.conf**
```vim
# TYPE  DATABASE        USER            ADDRESS                 METHOD
  host 	 empresa	    taller	 	  192.168.1.3/32			 md5
```
+ L'usuari de compres:

	- Ha de poder afegir vehicles a la taula dels vehicles, però no ha de poder
	  modificar les dades referents a l'estat del vehicle.

	- Només s'ha de poder connectar des de la xarxa local.
```sql
CREATE USER compres PASSWORD 'compres';

GRANT INSERT, UPDATE ON vehicles TO compres;
REVOKE UPDATE (estat) ON vehicles FROM compres;
```
- Fitxer **pg_hba.conf**
```vim
# TYPE  DATABASE        USER            ADDRESS                 METHOD
 local 	 empresa	    compres	 	  							 md5
```
+ Explica com comprovar el correcte funcionament.

## Exercici 12

Explica quina configuració ha de tenir el postgresql i quines sentències s'han executat per obtenir el següent escenari:

+ Els noms assignats han de ser descriptius.

+ Una base de dades d'una pàgina web amb notícies breus i articles.

+ La base de dades ha de tenir dues taules, una per emmagatzemar les notícies breus i l'altra per emmagatzemar els articles.
```sql
DROP DATABASE IF EXISTS web;
CREATE DATABASE web;
\c web

CREATE TABLE breus (
	id_noticia		SERIAL,
	data_noticia	DATE NOT NULL DEFAULT CURRENT_DATE,
	cos				VARCHAR(100) NOT NULL,
	autor			VARCHAR(20) NOT NULL,
	PRIMARY KEY(id_noticia)
);

CREATE TABLE articles (
	id_article		SERIAL,
	data_article	DATE NOT NULL DEFAULT CURRENT_DATE,
	cos				VARCHAR(100) NOT NULL,
	autor			VARCHAR(20) NOT NULL,
	PRIMARY KEY(id_article)
);
```
+ Els usuaris guarden les seves contrasenyes al postgresql.

+ L'usuari administrador:

	- Només ha de poder tenir una connexió activa.

	- Ha de poder veure i modificar l'estructura i les dades de la base de dades.

	- Ha de poder accedir des de qualsevol lloc.

	- Ha de poder crear nous usuaris i assingar-los els permisos corresponents.

	- No pot tenir permisos de superusuari.
 - Comandes **psql**
```sql
CREATE USER admin NOSUPERUSER PASSWORD 'admin' CREATEROLE CONNECTION LIMIT 1;

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO admin;

GRANT SELECT, INSERT, UPDATE, DELETE IN ALL TABLES ON SCHEMA public TO admin;
```
- Fitxer **pg_hba.conf**
```vim
# TYPE  DATABASE        USER            ADDRESS                 METHOD
  host 	  web	       admin	 	  192.168.1.0/24			 md5
```
+ L'usuari de l'aplicació web:
	
	- Només ha de poder llegir la base de dades.

	- Només s'ha de poder connectar des de l'ordinador que te el servidor web.
- Comandes **psql**
```sql
CREATE USER appweb PASSWORD 'appweb';

GRANT SELECT ON ALL TABLES IN SCHEMA public TO appweb;
```
- Fitxer **pg_hba.conf**
```vim
# TYPE  DATABASE        USER            ADDRESS                 METHOD
  host 	  web	       appweb	 	  192.168.1.2/32			 md5
```
+ L'usuari de gestió de la web:

	- Ha de poder veure i modificar les dades de la base de dades.

	- S'ha de poder connectar des de qualsevol ordinador de la xarxa local.
- Comandes **psql**
```sql
CREATE USER gestio PASSWORD 'gestio';

GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO gestio;
```
- Fitxer **pg_hba.conf**
```vim
# TYPE  DATABASE        USER            ADDRESS                 METHOD
  local    web	       gestio	 	  		        			 md5
```
+ L'usuari de notícies:

	- Ha de poder veure les taules però només ha de poder modificar la taula de notícies breus.

	- S'ha de poder connectar des de qualsevol lloc.
- Comandes **psql**
```sql
CREATE USER noticies PASSWORD 'noticies';

GRANT SELECT ON ALL TABLES IN SCHEMA public TO noticies;
GRANT INSERT, UPDATE, DELETE (breus) ON web TO noticies;
```
- Fitxer **pg_hba.conf**
```vim
# TYPE  DATABASE        USER            ADDRESS                 METHOD
  host    web	       noticies	 	  192.168.1.0/24    		 md5
```
+ L'usuari d'articles:

	- Ha de poder veure les taules però només ha de poder modificar la taula d'articles.

	- Només s'ha de poder connectar des de la xarxa local. 
- Comandes **psql**
```sql
CREATE USER articles PASSWORD 'articles';

GRANT SELECT ON ALL TABLES IN SCHEMA public TO articles;
GRANT INSERT, UPDATE, DELETE (articles) ON web TO articles;
```
- Fitxer **pg_hba.conf**
```vim
# TYPE  DATABASE        USER            ADDRESS                 METHOD
  local   web	       articles	 	  					   		 md5
```
+ Explica com comprovar el correcte funcionament.