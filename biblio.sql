\c template1
DROP DATABASE IF EXISTS biblioteca;

CREATE DATABASE biblioteca;
\c biblioteca

CREATE TABLE autors (
        PRIMARY KEY(autor),
        autor         SMALLINT       DEFAULT 1,
        nom           varchar(20),
        nacionalitat  varchar(15)
);

INSERT INTO autors
VALUES (1, 'anònim', 'espanyola');

Insert into autors  
values (2, 'Almudena Grandes', 'espanyola');

CREATE TABLE llibres (
        PRIMARY KEY(ref),
        ref        SERIAL,
        titol      VARCHAR(30),
        editorial  VARCHAR(30),
        id_autor   SMALLINT     DEFAULT 1,
                   CONSTRAINT llibres_id_autor_fk FOREIGN KEY(id_autor)
                   REFERENCES autors
                   ON DELETE SET DEFAULT
                   ON UPDATE RESTRICT
);

INSERT INTO llibres 
VALUES (11, 'llibertat', 'catalana', 1);

INSERT INTO llibres 
VALUES (12, 'La fageda', 'vapor', 2);

CREATE TABLE socis (
PRIMARY KEY (num_soci),
num_soci   SMALLINT,
nom        VARCHAR(20),
dni        INT

);

INSERT INTO socis 
VALUES (21, 'oscar', 24567869);

INSERT INTO socis 
VALUES (22, 'marta', 45678923);

CREATE TABLE prestecs (
ref           SMALLINT,
soci          SMALLINT,
data_prestec  DATE,

CONSTRAINT prestecs_ref_fk FOREIGN KEY (ref) REFERENCES llibres(ref)
ON DELETE CASCADE,
CONSTRAINT prestecs_soci_fk FOREIGN KEY (soci) REFERENCES socis(num_soci)
ON DELETE RESTRICT

);

INSERT INTO prestecs
VALUES (11, '21', '2021-2-20');

INSERT INTO prestecs
VALUES (12, '22', '2021-7-17');