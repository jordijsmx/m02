## Exercici 1

Doneu data i hora actual del sistema en format dd/MM/aaaa-hh:mm:ss
```sql
SELECT to_char(CURRENT_TIMESTAMP, 'DD/MM/YYYY-HH24:MI:SS');
```
```SQL
       to_char       
---------------------
 19/04/2023-18:17:02
(1 row)
```
## Exercici 2

Doneu hora actual en format hh:mm:ss
```SQL
SELECT to_char(CURRENT_TIMESTAMP, 'HH24:MI:SS');
```
```SQL
 to_char  
----------
 18:23:07
(1 row)

```
## Exercici 3

Determineu la durada de cada contracte fins avui en dies. S'ha de mostrar el
nom del treballador, la data del contracte i la durada.
```sql
SELECT nom, data_contracte, CURRENT_DATE - data_contracte AS dies 
  FROM rep_vendes ;
```
```sql
      nom      | data_contracte | dies  
---------------+----------------+-------
 Bill Adams    | 1988-02-12     | 12850
 Mary Jones    | 1989-10-12     | 12242
 Sue Smith     | 1986-12-10     | 13279
 Sam Clark     | 1988-06-14     | 12727
 Bob Smith     | 1987-05-19     | 13119
 Dan Roberts   | 1986-10-20     | 13330
 Tom Snyder    | 1990-01-13     | 12149
 Larry Fitch   | 1989-10-12     | 12242
 Paul Cruz     | 1987-03-01     | 13198
 Nancy Angelli | 1988-11-14     | 12574
(10 rows)
```
## Exercici 4

Determineu la suma total de les durades de tots els contractes dels
treballadors en dies fins avui.
```sql
SELECT SUM(CURRENT_DATE - data_contracte) AS dies FROM rep_vendes;
```
```sql
  dies  
--------
 127710
(1 row)
```
## Exercici 5

Determineu la diferència de temps treballat entre cadascun dels treballadors
amb el treballador més antic. S'ha de mostrar el nom del treballador, la data
del contracte i la diferència.
```sql
SELECT nom,  data_contracte, data_contracte - (SELECT MIN(data_contracte) 
                                                 FROM rep_vendes) AS diferencia 
  FROM rep_vendes ;
```
```sql
      nom      | data_contracte | diferencia 
---------------+----------------+------------
 Bill Adams    | 1988-02-12     |        480
 Mary Jones    | 1989-10-12     |       1088
 Sue Smith     | 1986-12-10     |         51
 Sam Clark     | 1988-06-14     |        603
 Bob Smith     | 1987-05-19     |        211
 Dan Roberts   | 1986-10-20     |          0
 Tom Snyder    | 1990-01-13     |       1181
 Larry Fitch   | 1989-10-12     |       1088
 Paul Cruz     | 1987-03-01     |        132
 Nancy Angelli | 1988-11-14     |        756
(10 rows)
```
## Exercici 6

Calculeu el nombre de comandes fetes el desembre pels representants de vendes contractats el mes de febrer.
```SQL
SELECT COUNT(*) 
  FROM comandes 
 WHERE EXTRACT(MONTH FROM data) = 12 
   AND rep IN (SELECT num_empl 
                 FROM rep_vendes 
                WHERE EXTRACT(MONTH FROM data_contracte) = 2);
```
```SQL
 count 
-------
     3
(1 row)
```
## Exercici 7

Llisteu el número de treballadors que s'han contractat per a cada mes de l'any. El llistat ha d'estar ordenat pel mes. Ha de tenir el següent format:

```
 mes_de_contractacio | nombre_de_contractes
---------------------+----------------------
                   1 |                    1
                   2 |                    1
                   3 |                    1
...
```
```sql
SELECT EXTRACT(MONTH FROM data_contracte) AS mes_de_contractacio, 
                          COUNT(num_empl) AS nombre_de_contractes 
  FROM rep_vendes 
 GROUP BY 1 
 ORDER BY 1;
```
```sql
 mes_de_contractacio | nombre_de_contractes 
---------------------+----------------------
                   1 |                    1
                   2 |                    1
                   3 |                    1
                   5 |                    1
                   6 |                    1
                  10 |                    3
                  11 |                    1
                  12 |                    1
(8 rows)
```