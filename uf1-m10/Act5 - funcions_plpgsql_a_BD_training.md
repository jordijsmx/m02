# Activitat 5 - Funcions Plpgsql a BD training

## Exercici 1
Crea una funció que canvïi les dates de les comandes, canviant el 1989 per 2022 i el 1990 per 2023
```sql
CREATE OR REPLACE FUNCTION canvi_data_comandes() RETURNS INTEGER AS $$
DECLARE
    num_canvis INT:=0;
    searchsql TEXT:='';
    searchsql1 TEXT:='';
    res_select RECORD; /* se li pot posar qualsevol registre de qualsevol comanda */
    dia TEXT;
    mes TEXT;
    nova_data TEXT;

BEGIN
    searchsql:= 'SELECT *, DATE_PART(''year'',data) AS any_data FROM comandes;';
    FOR res_select IN EXECUTE(searchsql) LOOP

        dia:=SUBSTR(res_select.data::TEXT, 9,2);
        mes:=SUBSTR(res_select.data::TEXT, 6,2);
        IF res_select.any_data = '1989' THEN
            nova_data:=E'2022' || '-' || mes || '-' || dia;
            searchsql1:='UPDATE comandes SET data = ''' || nova_data || ''' WHERE num_comanda =' || res_select.num_comanda ||';';
            num_canvis := num_canvis+1;
            EXECUTE (searchsql1);
            
        ELSE
            IF res_select.any_data = '1990' THEN
            nova_data:=E'2023' || '-' || mes || '-' || dia;
            searchsql1:='UPDATE comandes SET data = ''' || nova_data || ''' WHERE num_comanda = ' || res_select.num_comanda || ';';
            num_canvis := num_canvis+1;
            EXECUTE (searchsql1);
            END IF;
        END IF;
    END LOOP;
    RETURN num_canvis;
    EXCEPTION
    WHEN others THEN RETURN '5';
END;
$$ LANGUAGE PLPGSQL;
```
## Exercici 2
Crea una taula buida amb la mateixa estructura que la taula comades que es digui historic_comandes. Crea una funció que busqui els productes (fabricant + producte) que no existeixen a la taula productes, que els copïi a la taula nova historic_comandes i els esborri de comandes. Després crea la clau forana correcte de comandes a productes.
```sql
CREATE TABLE comandes_historic AS SELECT * FROM comandes;
DELETE FROM comandes_historic ;

CREATE OR REPLACE FUNCTION insert_historic (a INTEGER, 
                                            b TEXT, 
                                            c INTEGER, 
                                            d INTEGER, 
                                            e TEXT, 
                                            f TEXT, 
                                            g INTEGER, 
                                            h INTEGER) RETURNS INTEGER AS $$

DECLARE
    sql TEXT;
BEGIN
    EXECUTE 'INSERT INTO comandes_historic VALUES($1,$2,$3,$4,$5,$6,$7,$8)' 
      USING a,b,c,d,e,f,g,h;
     RETURN '1';

--EXCEPTION
--    WHEN unique_violation THEN RETURN '5';
--   WHEN not_null_violation THEN RETURN '4';
--    WHEN foreign_key_violation THEN RETURN '3';
END;
$$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION busqui_productes() RETURNS TEXT AS $$
DECLARE
    result TEXT;
    num_canvis TEXT:= '0';
    res_select record;
    searchsql TEXT := '';
    searchsql1 TEXT := '';
BEGIN
   searchsql := 'SELECT * FROM comandes WHERE (fabricant, producte) NOT IN
                 (SELECT id_fabricant, id_producte FROM productes);';
   
   FOR res_select IN EXECUTE(searchsql) LOOP
       EXECUTE(searchsql);
       IF NOT FOUND THEN
          result := insert_historic(res_select.num_comanda::INTEGER, 
                                    res_select.data::DATE,
                                    res_select.clie::INTEGER, 
                                    res_select.rep::INTEGER, 
                                    res_select.fabricant::TEXT,
                                    res_select.producte::TEXT, 
                                    res_select.quantitat::INTEGER, 
                                    res_select.import::INTEGER);
            num_canvis := '1';
                 
            IF num_canvis = '1' THEN
                searchsql1 := 'DELETE FROM comandes WHERE num_comanda= ' || res_select.num_comanda || ';';
                EXECUTE(searchsql1);
                END IF;
       END IF;
   END LOOP;
   RETURN num_canvis;
END;
$$ LANGUAGE PLPGSQL;
```
## Exercici 3
Afegeix un camp -data_vigencia- timestamp amb default current_timestamp, i un camp id_producte_historic integer  a la taula de productes. Posa el current_timestamp de valor al camp data_vigencia de tots els registres.  Crea una funció que ompli el camp id_producte_historic amb números seqüencials. Posa coma a clau primària l'id_producte_històric i com a clau única la parella de camps (id_fabricant, id_producte).

D'aquesta manera, quan es faci una modificació, com el preu, a la taula productes, es crearà un registre nou, amb una nova data de vigència, permetent així guardar la informació de producte corresponent a factures on aquella informació era vigent
```SQL
ALTER TABLE productes 
        ADD data_vigencia TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
        ADD id_producte_historic BIGINT;

CREATE OR REPLACE FUNCTION update_sequencial() RETURNS INTEGER AS $$
DECLARE
    searchsql TEXT:= '';
    sql TEXT :='';
    res_select RECORD;
    id_seq INT := 1;
    num_canvis INT := 0;

BEGIN
    searchsql := 'SELECT * FROM productes;';
    FOR res_select IN EXECUTE(searchsql) LOOP
        sql := 'UPDATE productes SET id_producte_historic =' || id_seq || '
        WHERE id_fabricant = ''' || res_select.id_fabricant || ''' AND id_producte = '''
        || res_select.id_producte || ''';';

        RAISE NOTICE '%',sql;
        EXECUTE(sql);
        id_seq := id_seq+1;
        num_canvis := num_canvis+1;
    END LOOP;
    RETURN num_canvis;
END;
$$ LANGUAGE PLPGSQL;

-- Esborrem clau primària actual
ALTER TABLE productes DROP CONSTRAINT pk_producte_fp;

-- Afegim clau única als 2 camps
ALTER TABLE productes ADD UNIQUE(id_fabricant,id_producte);

-- Afegim clau primària al nou camp
ALTER TABLE productes ADD PRIMARY KEY (id_producte_historic);

-- Afegim NOT NULL al nou camp
ALTER TABLE productes ALTER COLUMN id_producte_historic SET NOT NULL;
```
## Exercici 4
El proper pas serà modificar la taula comandes per tal que es relacioni amb productes a través del nou id, per tant caldrà afegir aquest nou camp a comandes, escriure una funció per posar el nou id corresponent a la parella (fabricant, producte), esborrar la clau forana actual i crear la nova clau forana
```sql
ALTER TABLE comandes ADD id_producte_historic BIGINT;

CREATE OR REPLACE FUNCTION id_historic_comandes() RETURNS INTEGER AS $$
DECLARE
    searchsql TEXT :='';
    sql TEXT :='';
    num_canvis INTEGER := 0;
    res_select RECORD;
BEGIN
    searchsql := 'SELECT * FROM productes;';
    FOR res_select IN EXECUTE(searchsql) LOOP
    sql := 'UPDATE comandes SET id_producte_historic =' || res_select.id_producte_historic ||
    'WHERE fabricant  = ''' || res_select.id_fabricant || ''' AND producte = ''' || 
    res_select.id_producte || ''';';
    EXECUTE(sql);
    num_canvis := num_canvis+1;
    END LOOP;
    RETURN num_canvis;
END;
$$ LANGUAGE PLPGSQL;
```