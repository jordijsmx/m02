#  Funcions predefinides

Si no és diu el contrari són funcions estàndars (ANSI SQL 2016)


## Exercici 1

Mostreu la longitud de la cadena "hola que tal"
```sql
SELECT LENGTH('hola que tal');
```
```
 char_length 
-------------
          12
(1 row)
```
## Exercici 2

Mostreu la longitud dels valors del camp "id_producte".
```sql
SELECT LENGTH(id_producte)
  FROM productes;
```
```
 char_length 
-------------
           5
           5
           4
           5
           4
           5
           5
           5
           4
           4
           5
           3
           4
           5
           5
           4
           5
           5
           5
           5
           4
           5
           3
           4
           5
(25 rows)
```
## Exercici 3

Mostrar la longitud dels valors del camp "descripcio".
```sql
SELECT LENGTH(descripcio)
  FROM productes;
```
```
 length 
--------
     16
      9
      8
      5
     11
     15
     15
      9
     11
      8
     14
      7
     13
      4
     15
     11
      8
      8
     15
     14
     13
     10
     13
     16
     18
(25 rows)
```
## Exercici 4

Mostreu els noms dels venedors en majúscules.
```sql
SELECT UPPER(nom)
  FROM rep_vendes;
```
```
     upper     
---------------
 BILL ADAMS
 MARY JONES
 SUE SMITH
 SAM CLARK
 BOB SMITH
 DAN ROBERTS
 TOM SNYDER
 LARRY FITCH
 PAUL CRUZ
 NANCY ANGELLI
(10 rows)
```
## Exercici 5

Mostreu els noms dels venedors en minúscules.
```sql
SELECT LOWER(nom)
  FROM rep_vendes;
```
```
     lower     
---------------
 bill adams
 mary jones
 sue smith
 sam clark
 bob smith
 dan roberts
 tom snyder
 larry fitch
 paul cruz
 nancy angelli
(10 rows)
```
## Exercici 6

Trobeu on és la posició de l'espai en blanc de la cadena 'potser 7'.
```sql
SELECT POSITION('potser 7',' ');
```
```
 strpos 
--------
      7
(1 row)
```
## Exercici 7

Volem mostrar el nom, només el nom dels venedors sense el cognom, en
majúscules.
```SQL
SELECT UPPER(SUBSTRING(nom,1,POSITION(' ' IN nom))) 
  FROM rep_vendes ;
```
```sql
 upper  
--------
 BILL 
 MARY 
 SUE 
 SAM 
 BOB 
 DAN 
 TOM 
 LARRY 
 PAUL 
 NANCY 
(10 rows)
```
## Exercici 8

Crear una vista que mostri l'identificador dels representants de vendes i en
columnes separades el nom i el cognom.
```sql
CREATE VIEW vista AS 
  SELECT num_empl, SUBSTRING(nom,1,POSITION(' ' IN nom)) AS "Nom", SUBSTRING(nom,POSITION(' ' IN nom)) AS "Cognom" 
    FROM rep_vendes ;
```
```sql
 num_empl |  Nom   |  Cognom  
----------+--------+----------
      105 | Bill   |  Adams
      109 | Mary   |  Jones
      102 | Sue    |  Smith
      106 | Sam    |  Clark
      104 | Bob    |  Smith
      101 | Dan    |  Roberts
      110 | Tom    |  Snyder
      108 | Larry  |  Fitch
      103 | Paul   |  Cruz
      107 | Nancy  |  Angelli
(10 rows)
```
## Exercici 9

Mostreu els valors del camp nombre de manera que 'Bill Adams' sorti com 'B.
Adams'.
```SQL
SELECT SUBSTRING(nom,1,1) || '.' || SUBSTRING(nom, POSITION(' ' IN nom))
  FROM rep_vendes;
```
```sql
  ?column?  
------------
 B. Adams
 M. Jones
 S. Smith
 S. Clark
 B. Smith
 D. Roberts
 T. Snyder
 L. Fitch
 P. Cruz
 N. Angelli
(10 rows)
```
## Exercici 10

Mostreu els valors del camp nombre de manera que 'Bill Adams' sorti com 'Adams,
Bill'.
```sql
SELECT SUBSTRING(nom, POSITION(' ' IN nom)) || ',' || SUBSTRING(nom, 1, POSITION(' ' IN nom))
  FROM rep_vendes;
```
```sql
    ?column?    
----------------
  Adams,Bill
  Jones,Mary
  Smith,Sue
  Clark,Sam
  Smith,Bob
  Roberts,Dan
  Snyder,Tom
  Fitch,Larry
  Cruz,Paul
  Angelli,Nancy
(10 rows)
```
## Exercici 11

Volem mostrar el camp descripcion de la taula productos però que en comptes de
sortir espais en blanc, volem subratllats ('_').
```sql
SELECT REPLACE(descripcio, ' ', '_') FROM productes ;
```
```sql
      replace       
--------------------
 V_Stago_Trinquet
 Extractor
 Reductor
 Plate
 Riosta_2-Tm
 Article_Tipus_3
 Article_Tipus_4
 Manovella
 Pern_Riosta
 Reductor
 Frontissa_Esq.
 Coberta
 Suport_Riosta
 Retn
 Article_Tipus_1
 Riosta_1-Tm
 Muntador
 Reductor
 Article_Tipus_2
 Frontissa_Dta.
 Riosta_1/2-Tm
 Peu_de_rei
 Bancada_Motor
 Retenidor_Riosta
 Passador_Frontissa
(25 rows)
```
## Exercici 12

Volem treure per pantalla una columna, que conté el nom i les vendes, amb els
següent estil:
```sql
   vendes dels empleats
 ---------------------------
  Bill Adams..... 367911,00
  Mary Jones..... 392725,00
  Sue Smith...... 474050,00
  Sam Clark...... 299912,00
  Bob Smith...... 142594,00
  Dan Roberts.... 305673,00
  Tom Snyder.....  75985,00
  Larry Fitch.... 361865,00
  Paul Cruz...... 286775,00
  Nancy Angelli.. 186042,00
 (10 rows)
```
```sql
SELECT RPAD(nom,15,'.') || vendes 
  FROM rep_vendes ;
```
## Exercici 13

Treieu per pantalla el temps total que fa que estan contractats els
treballadors, ordenat pels cognoms dels treballadors amb un estil semblant al
següent:
```sql
        nom    |     temps_treballant
    -----------+-------------------------
    Mary Jones | 13 years 4 months 6 days
```
```sql
SELECT nom, age(data_contracte) AS temps_treballant FROM rep_vendes;
```
## Exercici 14

Cal fer un llistat dels productes dels quals l'estoc és inferior al
total d'unitats venudes d'aquell producte els darrers 60 dies, a comptar des de
la data actual. Cal mostrar els codis de fabricant i de producte, l'estoc i les
unitats totals venudes dels darrers 60 dies.
```sql
SELECT id_producte, id_fabricant, estoc
  FROM productes
 WHERE estoc < (SELECT SUM(quantitat)
                  FROM comandes
                 WHERE (id_producte, id_fabricant) = (producte, fabricant)
                   AND data < CURRENT_DATE - 60);
```
```sql
 id_producte | id_fabricant | estoc 
-------------+--------------+-------
 775c        | imm          |     5
 2a44r       | rei          |    12
 114         | fea          |    15
(3 rows)
```
## Exercici 15

Com l'exercici anterior però en comptes de 60 dies ara es volen aquells
productes venuts durant el mes actual o durant l'anterior.
```

```
## Exercici 16

Per fer un estudi previ de la campanya de Nadal es vol un llistat on, per cada
any del qual hi hagi comandes a la base de dades, es mostri el nombre de
clients diferents que hagin fet comandes en el mes de desembre d'aquell any.
Cal mostrar l'any i el número de clients, ordenat ascendent per anys.
```sql
SELECT EXTRACT(year FROM data) AS year, COUNT(DISTINCT clie)                 
FROM comandes
WHERE EXTRACT(month FROM data) = 12
GROUP BY year;
```
```sql
 year | count 
------+-------
 1989 |     3
(1 row)

```
## Exercici 17

Llisteu codi(s) i descripció dels productes. La descripció ha d'aparèixer en
majúscules. Ha d'estar ordenat per la longitud de les descripcions (les més
curtes primer).
```sql
SELECT id_producte, id_fabricant, UPPER(descripcio)
  FROM productes 
 ORDER BY LENGTH(descripcio);
```
## Exercici 18

LListar el nom dels treballadors i la data del seu contracte mostrant-la amb el següent format:

*Dia_de_la_setmana dia_mes_numeric, mes_text del any_4digits*

Per exemple:

```
...
Bill Adams    | Friday    12, February del 1988
...
```
```sql
SELECT nom, TO_CHAR(data_contracte, 'Day DD, Month "del" YYYY')
  FROM rep_vendes;
```

## Exercici 19

Modificar l'import de les comandes que s'han realitzat durant la tardor
augmentant-lo un 20% i arrodonint a l'alça el resultat.
```

```
## Exercici 20

Mostar les dades de les oficines llistant en primera instància aquelles
oficines que tenen una desviació entre vendes i objectius més gran.
```sql
SELECT *, ABS(vendes - objectiu) AS desviacio 
  FROM oficines 
 ORDER BY desviacio DESC;
```
```sql
 oficina |   ciutat    | regio | director | objectiu  |  vendes   | desviacio 
---------+-------------+-------+----------+-----------+-----------+-----------
      11 | New York    | Est   |      106 | 575000.00 | 692637.00 | 117637.00
      22 | Denver      | Oest  |      108 | 300000.00 | 186042.00 | 113958.00
      21 | Los Angeles | Oest  |      108 | 725000.00 | 835915.00 | 110915.00
      12 | Chicago     | Est   |      104 | 800000.00 | 735042.00 |  64958.00
      13 | Atlanta     | Est   |      105 | 350000.00 | 367911.00 |  17911.00
(5 rows)
```
## Exercici 21

Llistar les dades d'aquells representants de vendes que tenen un identificador
senar i són directors d'algun representant de vendes.
```sql
SELECT num_empl
  FROM rep_vendes
 WHERE MOD(num_empl, 2) = 1
   AND num_empl IN 
       (SELECT cap 
          FROM rep_vendes);
```
```sql
 num_empl 
----------
      101
(1 row)
```