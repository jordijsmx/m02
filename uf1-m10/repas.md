- Exàmen
    - Funcions predefinides
    - Funcions SQL
    - Funcions PLPGSQL
        - IF -END-IF

- RETURNS
  - valor simple
      - text
      - smallint
      - int
      - numeric
      - date
  - SETOF
  - més d'un valor o valor complex
  - SETOF més d'un valor
  - TABLE

- funcions predefinides
```sql
DATE_PART()
STRPOS()
SUBSTR()
RIGHT()
LEFT()
LENGTH()
CURRENT_DATE
CURRENT_TIME
CURRENT_TIMESTAMP
TO CHAR()

    DATE_PART('day', data::DATE);

    SELECT DATE_PART('day',CURRENT_DATE) || '/' || DATE_PART('month', CURRENT_DATE) || '/' || EXTRACT('year',CURRENT_DATE);

    SELECT DATE_PART('day',CURRENT_DATE) || '/' || SUBSTR(CURRENT_DATE::TEXT 6,2) || '/' || EXTRACT('year' FROM CURRENT_DATE);
```
- Funció SQL que torni les dades del producte del qual s'ha venut més en import
- Retorn tipus atula o view: productes
```sql
SELECT * 
  FROM comandes 
 GROUP BY 1,2,3 
 ORDER BY SUM(import) DESC
 LIMIT 1;

 SELECT fabricant, producte, import 
   FROM productes 
   JOIN comandes 
     ON (id_fabricant, id_producte) = (fabricant, producte) 
  GROUP BY 1,2,3
   ORDER BY SUM(import) DESC
   LIMIT 1;


CREATE OR REPLACE FUNCTION max_import() RETURNS productes AS $$
    SELECT productes.*
    FROM productes 
    JOIN comandes 
      ON (id_fabricant, id_producte) = (fabricant, producte) 
   GROUP BY 1,2,3
   ORDER BY SUM(import) DESC
   LIMIT 1;
$$ LANGUAGE SQL;
```
- RETURN tipus SETOF
```sql
CREATE OR REPLACE FUNCTION max_import() RETURNS SETOF productes AS $$
    SELECT productes.*
    FROM productes 
    JOIN comandes 
      ON (id_fabricant, id_producte) = (fabricant, producte) 
   GROUP BY 1,2,3
   ORDER BY SUM(import) DESC
   OFFSET 1;
$$ LANGUAGE SQL;
```
- RETURN tipus nou
```sql
CREATE TYPE producte_dos_camps AS (fab CHAR(3), pro VARCHAR(5));

CREATE OR REPLACE FUNCTION producte_max3() RETURNS SETOF producte_dos_camps AS $$
  SELECT id_fabricant, id_producte
    FROM productes
    WHERE (id_fabricant,id_producte) = (SELECT fabricant, producte
                                        FROM comandes)
    GROUP BY 1,2;
$$ LANGUAGE SQL;
```
- Funció que donat un producte (id_fabricant, id_producte), i digui si existeix o no:
  - si no existeix, retorna: 'El producte
```SQL
CREATE OR REPLACE FUNCTION info_producte(fab TEXT, prod TEXT) RETURNS TEXT AS $$
DECLARE
    result TEXT :='';
    res_select RECORD;
    search TEXT:='';
BEGIN
    SELECT * INTO res_select
     FROM productes
    WHERE id_fabricant=$1
      AND id_producte=$2;

    IF res_select.id_fabricant > '' THEN -- Tambe IF FOUND
        result := 'El producte ' || res_select.id_fabricant || '-' || res_select.id_producte || ' existeix.';

        -- Busquem dades de les comandes
        SELECT COUNT(*) AS num_comandes,
            SUM(import) AS total_import,
            COUNT(DISTINCT(clie)) AS num_clients
            FROM comandes INTO res_select
        WHERE fabricant = $1
        AND producte = $2;

        IF res_select.num_comandes > 0 THEN
            result := result || 'Se n''han fet ' || res_select.num_comandes || ' comandes, per un valor de ' || res_select.total_import || '€. Ens l''han comprat ' || res_select.num_clients || ' clients diferents.' || E'\n';  
        
        -- Busquen quins clients l'han comprat
        search := 'SELECT *
          FROM clients
         WHERE num_clie IN (SELECT clie 
                              FROM comandes
                             WHERE fabricant = ''' || $1 || '''
                               AND producte = ''' || $2 || ''');';

        FOR res_select IN EXECUTE(search) LOOP
            IF FOUND THEN
                result := result || 'Ens l''ha comprat el client: ' || res_select.num_clie || ' ' || res_select.empresa || E'\n';  

            END IF;
        END LOOP;        
        
        ELSE
            result := result || 'No se n''ha fet cap comanda' || E'\n';
        END IF;
    ELSE
        result := 'El producte no existeix.';
    END IF;

    RETURN result;
END;
$$ LANGUAGE PLPGSQL;
```