-- Exercici 1

CREATE OR REPLACE FUNCTION info_any(INTEGER) RETURNS TABLE(a TEXT, b TEXT, c INTEGER, d INTEGER) AS $$
    SELECT COUNT(DISTINCT(clie)),
            COUNT(DISTINCT(rep)),
            COUNT(num_comanda),
            SUM(import)
      FROM comandes
     WHERE DATE_PART('year',data) = $1; 
$$ LANGUAGE SQL;

--Sortida
SELECT info_any(1989);

   info_any    
---------------
 (7,6,9,86712)
(1 row)

-- Exercici 2
CREATE OR REPLACE FUNCTION info_any_mes(INTEGER) RETURNS TABLE(a INTEGER, b INTEGER, c INTEGER, d INTEGER, e INTEGER) AS $$
    SELECT DATE_PART('month',data), 
            COUNT(DISTINCT(clie)),
                COUNT(DISTINCT(rep)),
                COUNT(num_comanda),
                SUM(import)
        FROM comandes
        WHERE DATE_PART('year',data) = $1
        GROUP BY DATE_PART('month',data); 
$$ LANGUAGE SQL;

-- Sortida
SELECT info_any_mes(1989);
     
   info_any_mes   
------------------
 (1,1,1,1,1896)
 (10,2,2,2,18978)
 (11,1,1,1,760)
 (12,3,3,5,65078)
(4 rows)


-- Exercici 3
CREATE OR REPLACE FUNCTION info_client(TEXT) RETURNS TEXT AS $$
DECLARE
    result TEXT :='';
    res_select RECORD;
BEGIN
    SELECT * 
      FROM clients INTO res_select
     WHERE empresa = $1;
    
    IF res_select.empresa > '' THEN
        result := 'El client '|| res_select.empresa || ' té com a identificador ' || res_select.num_clie || E'\n';

        SELECT COUNT(*) AS num_comanda,
             SUM(import) AS total_import,
            COUNT(DISTINCT(id_fabricant)) AS productes
                FROM clients
                JOIN comandes
                  ON num_clie = clie 
                JOIN productes INTO res_select
                ON (fabricant, producte) = (id_fabricant,id_producte)
                WHERE empresa = $1;

        IF res_select.num_comanda > 0 THEN
            result := result || 'Ha fet ' || res_select.num_comanda || ' comandes per un valor de ' || 
            res_select.total_import || ' €, i ens ha comprat ' || res_select.productes || ' productes diferents.';
        
        
        
        ELSE
            result := result || 'No ha fet cap comanda.' || E'\n';
        END IF;
    ELSE
        result := 'No existeix cap empresa amb aquest nom.';
    END IF;
    RETURN result;
END;
$$ LANGUAGE PLPGSQL;

SELECT info_client('Corp');
                
               info_client               
-----------------------------------------
 No existeix cap empresa amb aquest nom.
(1 row)

SELECT info_client('JCP Inc.');
                                       
                                     info_client                                      
--------------------------------------------------------------------------------------
 El client JCP Inc. té com a identificador 2111                                      +
 Ha fet 3 comandes per un valor de 6445.00 €, i ens ha comprat 2 productes diferents.
(1 row)



-- Exercici 4

CREATE OR REPLACE FUNCTION info_client_cadena(TEXT) RETURNS TEXT AS $$
DECLARE
    result TEXT :='';
    res_select RECORD;
    emp_entrada TEXT :='';
    clients INTEGER;
    sql TEXT;
    linia_producte TEXT := '';
    num_clients RECORD;
BEGIN
    emp_entrada := '%' || $1 || '%';

      SELECT * 
      FROM clients INTO res_select
     WHERE empresa LIKE emp_entrada;


      SELECT COUNT(*) 
      FROM clients INTO clients
     WHERE empresa LIKE emp_entrada;

    IF res_select.empresa > '' THEN
        result := 'El client '|| res_select.empresa || ' té com a identificador ' || res_select.num_clie || E'\n';
        IF clients = 1 THEN
            SELECT COUNT(*) AS num_comanda,
                SUM(import) AS total_import,
                COUNT(DISTINCT(id_fabricant)) AS productes,
                num_clie
                    FROM clients
                    JOIN comandes
                    ON num_clie = clie 
                    JOIN productes INTO res_select
                    ON (fabricant, producte) = (id_fabricant,id_producte)
                    WHERE empresa LIKE emp_entrada
                    GROUP BY 4;

            IF res_select.num_comanda > 0 THEN
                result := result || 'Ha fet ' || res_select.num_comanda || ' comandes per un valor de ' || 
                res_select.total_import || ' €, i ens ha comprat ' || res_select.productes || ' productes diferents.';
            
                SELECT clie FROM comandes INTO num_clients;
                 RAISE NOTICE 'sql = %', num_clients;
                sql := 'SELECT *
                          FROM productes
                          WHERE id_fabricant || id_producte
                            IN (SELECT fabricant || producte
                                  FROM comandes
                                 WHERE clie = ' || num_clients || ');';
               

                FOR res_select IN EXECUTE(sql) LOOP
                    linia_producte := linia_producte || '--' || res_select.id_fabricant 
                                                     || '--' || res_select.id_producte 
                                                     || '--' || res_select.descripcio || E'\n';

                END LOOP;
                result := result || 'Els productes que ha comprat són: ' || E'\n' || linia_producte;
            ELSE
                result := result || 'No ha fet cap comanda.' || E'\n';
            END IF;
        ELSE
            result := 'Hi ha més d''un client amb aquesta cadena.';
        END IF;
    ELSE
        result := 'No hi ha cap client amb aquesta cadena.';
    END IF;


    RETURN result;
END;
$$ LANGUAGE PLPGSQL;

SELECT info_client_cadena('rr');
                 
            info_client_cadena            
------------------------------------------
 No hi ha cap empresa amb aquesta cadena.
(1 row)

SELECT info_client_cadena('AAA');
                       
                  info_client_cadena                   
-------------------------------------------------------
 El client AAA Investments té com a identificador 2105+
 No ha fet cap comanda.                               +
 
(1 row)

SELECT info_client_cadena('Mid');
                                      
                                  info_client_cadena                                  
--------------------------------------------------------------------------------------
 El client Midwest Systems té com a identificador 2118                               +
 Ha fet 3 comandes per un valor de 2188.00 €, i ens ha comprat 3 productes diferents.
(1 row)