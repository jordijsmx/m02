# Funcions d'usuari

## Exercici 1

Donat l'identificador d'un client retorneu la importància del client, és a
dir, el percentatge dels imports de les comandes del client respecte al total
dels imports de les comandes.
```sql
CREATE FUNCTION importancia_client(INTEGER) RETURNS NUMERIC AS $$
    SELECT ROUND(SUM(import) / (SELECT SUM(import) FROM comandes)*100,2) 
    FROM comandes 
    WHERE clie = $1
    GROUP BY clie;
$$ LANGUAGE SQL;

SELECT clie, ROUND(SUM(import) / (SELECT SUM(import) FROM comandes) * 100,2) AS importancia 
  FROM comandes 
 GROUP BY clie 
 ORDER BY 1;
```
## Exercici 2

Calculeu el que s'ha deixat de cobrar per a un producte determinat.
És a dir, la diferència que hi ha entre el que val i el que li hem cobrat al total de clients.
```sql
CREATE OR REPLACE FUNCTION diferencia_pagament(CHARACTER(3), CHARACTER(5)) RETURNS NUMERIC AS $$
    SELECT SUM(preu * quantitat - import)
    FROM comandes
    JOIN productes
      ON (fabricant, producte) = (id_fabricant, id_producte)
   WHERE fabricant = $1
     AND producte = $2
   GROUP BY fabricant, producte;
$$ LANGUAGE SQL;
```
## Exercici 3

Creeu una funció que si li passem les columnes vendes i quota ens retorni una
columna amb el valor de vendes - quota.
```sql
CREATE FUNCTION dif_vendes_quota(rep_vendes.vendes%TYPE, rep_vendes.quota%TYPE) RETURNS NUMERIC AS $$
  SELECT $1 - $2;
$$ LANGUAGE SQL;
```
## Exercici 4

Feu una funció que donat un identificador de representant de vendes retorni
l'identificador dels clients que té assignats amb el seu límit de crèdit.
```sql
CREATE VIEW client_credit_view AS 
  SELECT numclie, limit_credit
    FROM clients;
```
```sql
CREATE FUNCTION clients_rep(SMALLINT) RETURNS SETOF client_credit AS $$
  SELECT num_clie, limit_credit
    FROM clients
   WHERE rep_clie = $1;
$$ LANGUAGE SQL;
```
## Exercici 5

Crear una funció promig_anual(venedor, any) que retorni el promig d'imports de
comandes del venedor durant aquell any.
```sql
CREATE OR REPLACE FUNCTION promig_anual(INTEGER, INTEGER) RETURNS NUMERIC AS $$
  SELECT ROUND(AVG(import),2)
    FROM comandes
   WHERE rep = $1
     AND DATE_PART('year', data) = $2;
$$ LANGUAGE SQL;  
```
## Exercici 6

Creeu una funció max_promig_anual(anyo) que retorni el màxim dels promitjos
anuals de tots els venedors. Useu la funció de l'exercici anterior.
```sql
CREATE FUNCTION max_promig_anual(INTEGER) RETURNS NUMERIC AS $$
  SELECT MAX(promig_anual(num_empl,$1))
    FROM rep_vendes;
$$ LANGUAGE SQL;
```
## Exercici 7

Creeu una funció promig_anual_tots(anyo) que retorni el promig anual de cada
venedor durant l'any indicat. Useu funcions creades en els exercicis anteriors.
```sql
CREATE OR REPLACE FUNCTION promig_anual_tots(INTEGER) RETURNS SETOF NUMERIC AS $$
  SELECT COALESCE(promig_anual(num_empl,$1),0)
    FROM rep_vendes;
$$ LANGUAGE SQL;
```
## Exercici 8

Feu una funció que retorni tots els codis dels clients que no hagin comprat res durant el mes introduït.
```sql
CREATE FUNCTION clients_sense_compra(mes INTEGER) RETURNS SETOF SMALLINT AS $$
  SELECT DISTINCT num_clie
    FROM clients
    WHERE num_clie NOT IN (SELECT clie
                             FROM comandes
                             WHERE DATE_PART('month', data)=$1);
$$ LANGUAGE SQL;
```
## Exercici 9

Funció anomenada maxim_mes a la que se li passa un any i retorna el mes en el
que hi ha hagut les màximes vendes (imports totals del mes).
```SQL
CREATE FUNCTION maxim_mes(anyo INTEGER) RETURNS INTEGER AS $$
  SELECT DATE_PART('month', data)
    FROM comandes
   WHERE DATE_PART('year', data)=$1
   GROUP BY 1
   ORDER BY SUM(import) DESC
   LIMIT 1;
$$ LANGUAGE SQL;
```
## Exercici 10

1. Creeu una funció baixa_rep que doni de baixa el venedor que se li passa per paràmetre i reassigni tots els seus clients al venedor que tingui menys clients assignats (si hi ha empat, a qualsevol d'ells).
```sql
CREATE OR REPLACE FUNCTION baixa_rep(INTEGER) RETURNS VOID AS $$
  UPDATE clients 
     SET rep_clie = (SELECT rep_clie 
                       FROM clients 
                      GROUP BY rep_clie 
                      ORDER BY COUNT(num_clie) 
                      LIMIT 1) 
   WHERE rep_clie = $1;
$$ LANGUAGE SQL;
```

2. Com usarieu la funció per donar de baixa els 3 venedors que fa més temps que no han fet cap venda?
```sql
CREATE OR REPLACE FUNCTION baixa_rep2() RETURNS VOID AS $$ 
 DELETE FROM rep_vendes
 WHERE num_empl IN (SELECT rep
                      FROM comandes 
                     GROUP BY rep
                     ORDER BY MAX(data) ASC 
                     LIMIT 3);
$$ LANGUAGE SQL;
```
## Exercici 11

Creeu una funció anomenada *n_clients* que donat un identificador d'un
representant de vendes ens retorni el nombre de clients que te assignats. Si
l'entrada és nul·la s'ha de retornar un valor nul. 
```sql
CREATE FUNCTION n_clients(INTEGER) RETURNS INTEGER AS $$
  SELECT COUNT(num_clie) 
    FROM clients 
   WHERE rep_clie = $1;
$$ LANGUAGE SQL RETURNS NULL ON NULL INPUT;
```
## Exercici 12

Creeu una funció anomenada *n_atesos* que donat un identificador d'un
representant de vendes ens retorni el nombre de clients diferents que ha atès.
Si l'entrada és nul·la s'ha de retornar un valor nul.
```sql
CREATE FUNCTION n_atesos(INTEGER) RETURNS INTEGER AS $$
  SELECT COUNT(DISTINCT(clie)) 
    FROM comandes 
   WHERE rep = $1;
$$ LANGUAGE SQL RETURNS NULL ON NULL INPUT;
```
## Exercici 13

Creeu una funció anomenada *total_imports* que donat un identificador d'un
representant de vendes ens retorni la suma dels imports de les seves comandes.
Si l'entrada és nul·la s'ha de retornar un valor nul.
```SQL
CREATE FUNCTION total_imports(INTEGER) RETURNS INTEGER AS $$
  SELECT SUM(import)
    FROM comandes
   WHERE rep = $1;
$$ LANGUAGE SQL RETURNS NULL ON NULL INPUT;
```
## Exercici 14

Creeu una funció anomenada "informe_rep" que ens retorni una taula amb
l'identificador del representant de vendes, el resultat de *n_clients*,
*n_atesos* i "total_imports*. Si a la funció se li passa un identificador de
representant de vendes només ha de retornar la informació relativa a aquest
representant de vendes. En cas de passar un valor nul a la funció, aquesta ha
de donar la informació de tots els representants de vendes.
```SQL
CREATE FUNCTION informe_rep(INT DEFAULT NULL) RETURNS TABLE (a INT, 
                                                             b INT, 
                                                             c INT) AS $$
  SELECT n_clients(num_empl), n_atesos(num_empl), total_imports(num_empl)
    FROM rep_vendes 
    WHERE CASE
          WHEN $1 IS NULL THEN TRUE
          ELSE num_empl = $1
           END;
$$ LANGUAGE SQL;
```
## Exercici 15

Creeu una funció, i les funcions auxiliars convenients, que rebi l'identificador
d'un producte i retorni una taula amb les següents dades:

+ Identificador de producte i fabricant.

+ Nombre de representants de vendes que han venut aquest producte.
```sql
CREATE FUNCTION num_rep(fabricant VARCHAR(3), producte VARCHAR(5)) RETURNS INTEGER AS $$
       SELECT COUNT(rep)
         FROM comandes
        WHERE fabricant = $1
          AND producte = $2;
$$ LANGUAGE SQL;
```
+ Nombre de clients que han comprat el producte.
```sql
CREATE FUNCTION num_clie(fabricant VARCHAR(3), producte VARCHAR(5)) RETURNS INTEGER AS $$
       SELECT COUNT(clie)
         FROM comandes
        WHERE fabricant = $1
          AND producte = $2;
$$ LANGUAGE SQL;
```
+ Mitjana de l'import de les comandes d'aquest producte.
```sql
CREATE FUNCTION avg_import(fabricant VARCHAR(3), producte VARCHAR(5)) RETURNS INTEGER AS $$
       SELECT AVG(import)
         FROM comandes
        WHERE fabricant = $1
          AND producte = $2;
$$ LANGUAGE SQL;
```
+ Quantitat mínima i quantitat màxima que s'ha demanat del producte en una sola comanda.
```sql
CREATE FUNCTION min_max(fabricant VARCHAR(3), producte VARCHAR(5)) RETURNS TABLE (a INTEGER,b INTEGER) AS $$
       SELECT MIN(quantitat), MAX(quantitat)
         FROM comandes
        WHERE fabricant = $1
          AND producte = $2;
$$ LANGUAGE SQL;
```

```sql
CREATE OR REPLACE FUNCTION total(fabricant VARCHAR(3), producte VARCHAR(5)) RETURNS TABLE(a VARCHAR(3), 
              b VARCHAR(5), 
              c INTEGER, 
              d INTEGER, 
              e INTEGER, 
              f RECORD) AS $$
  SELECT fabricant, producte, 
         num_rep(fabricant,producte), 
         num_clie(fabricant,producte),
         avg_import(fabricant,producte),
         min_max(fabricant,producte);
$$ LANGUAGE SQL;
```
## Exercici 16

Creeu una funció que donada una oficina ens retorni una taula identificant els
productes i mostrant la quantitat d'aquest producte que s'ha venut a l'oficina.
```sql
CREATE OR REPLACE FUNCTION ofi(INTEGER) RETURNS TABLE (a SMALLINT, b CHAR(3), c VARCHAR(5), e INTEGER) AS $$
  SELECT oficina, fabricant, producte, SUM(quantitat) 
    FROM oficines 
    JOIN rep_vendes 
      ON oficina_rep = oficina 
    JOIN comandes 
      ON num_empl = rep
   WHERE oficina = $1 
   GROUP BY 1,2,3;
$$ LANGUAGE SQL;
```
## Exercici 17

Creeu les funcions necessàries per aconseguir el següent resultat:

+ Cridant *resum_client()* ha de retornar una taula amb els identificadors dels clients, la suma de les seves compres, el nombre de comandes realitzades i el nombre de representants de vendes que l'han atès.
```SQL
CREATE OR REPLACE FUNCTION resum_client() RETURNS TABLE (a INTEGER, b INTEGER, c INTEGER, d INTEGER) AS $$
  SELECT clie, SUM(import), COUNT(num_comanda), COUNT(rep) 
    FROM comandes 
   GROUP BY 1;
$$ LANGUAGE SQL;
```
+ Cridant *resum_client(num_clie)* ha de retornar una taula amb els identificador dels representants de vendes i el nombre de comandes que ha realitzat el client amb aquest representant de vendes
```SQL
CREATE OR REPLACE FUNCTION resum_client(num_clie INTEGER) RETURNS TABLE (a INTEGER, b INTEGER) as $$
  SELECT rep, COUNT(num_comanda)
    FROM comandes
  WHERE clie = num_clie 
  GROUP BY clie,rep;
$$ LANGUAGE SQL;
```
+ Cridant *resum_client(num_clie, num_empl)* ha de retornar una taula amb el nombre de productes diferents que ha demanat el client especificat al representant de vendes especificat i la mitja de l'import de les comandes que ha realitzat el client especificat al representant de vendes especificat.
```SQL
CREATE OR REPLACE FUNCTION resum_client(num_clie INTEGER, num_empl INTEGER) RETURNS TABLE (a INTEGER, b INTEGER) AS $$
  SELECT COUNT(DISTINCT(producte)), ROUND(AVG(import),2) 
    FROM comandes 
   WHERE clie = num_clie 
     AND rep = num_empl;
$$ LANGUAGE SQL;
```
## Exercici 18

Creeu la funció *millor_venedor()* que retorni totes les dades del venedor que ha
venut més (major total d'imports) durant l'any en curs.
```SQL
CREATE OR REPLACE FUNCTION millor_venedor() RETURNS rep_vendes AS $$
  SELECT * 
    FROM rep_vendes 
   WHERE num_empl = (SELECT rep 
                       FROM comandes 
                      WHERE DATE_PART('year',data) = (SELECT MAX(DATE_PART('year',data)) 
                                                        FROM comandes) 
                      GROUP BY rep 
                      ORDER BY SUM(import) DESC 
                      LIMIT 1);
$$ LANGUAGE SQL;
```
## Exercici 19

Calcular el descompte fet a un client concret, respecte totes les comandes del client.

Cal crear dos funcions auxiliars:

+ La primera obtindrà el total dels imports de les comandes d'un client determinat.
```SQL
CREATE OR REPLACE FUNCTION total_imports(INTEGER) RETURNS INTEGER AS $$
  SELECT SUM(import) 
    FROM comandes 
   WHERE clie = $1;
$$ LANGUAGE SQL;
```
+ La segona serà una funció preu de "comanda abstracta" que necessitarà el producte en qüestió i la quantitat de productes demanats.
```SQL
CREATE OR REPLACE FUNCTION comanda_abstracta(VARCHAR(5),CHAR(3),INTEGER) RETURNS INTEGER AS $$
  SELECT preu * $3 
    FROM productes 
   WHERE id_producte = $1
     AND id_fabricant = $2;
$$ LANGUAGE SQL;
```