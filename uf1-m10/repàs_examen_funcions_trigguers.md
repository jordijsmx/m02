# Funcions amb trigguers

## Exercici 1

Desenvolupar una funció plpgsql que rebi un client i compari les compres d'aquest client en el mes d'abril de l'any acutal i en el mes de març de l'any actual
La funció tornarà '1' si les vendes d'abril son superiors o iguals a les de març
La funció tornarà '2' si les vendes d'abril son inferiors a les de març
```SQL
CREATE OR REPLACE FUNCTION client(INTEGER) RETURNS TEXT AS $$
DECLARE
    marc INTEGER;
    abril INTEGER;
BEGIN
    SELECT COALESCE(SUM(import),0) INTO marc 
      FROM comandes 
     WHERE DATE_PART('month',data) = 03 
       AND DATE_PART('year',data) = DATE_PART('year',CURRENT_DATE);
       AND clie = $1;

    SELECT COALESCE(SUM(import),0) INTO abril
      FROM comandes 
     WHERE DATE_PART('month',data) = 04 
       AND DATE_PART('year',data) = DATE_PART('year',CURRENT_DATE);
       AND clie = $1;

        IF abril >= marc THEN RETURN'1';
        ELSE RETURN '2';
        END IF;
END;
$$ LANGUAGE PLPGSQL;
```
## Exercici 2

Desenvolupar una funció que per cada client de la taula clients, retorni el seu nom i un '1' o un '2' depenent de si les vendes han pujat o baixat, fent servir la funció d'exercici1
```sql
CREATE OR REPLACE FUNCTION clients() RETURNS TEXT AS $$
DECLARE
    result TEXT := '';
    res_select RECORD;
    sql TEXT;
    num_return INTEGER;
BEGIN
    sql := 'SELECT * FROM clients;';

    FOR res_select IN EXECUTE(sql) LOOP
        SELECT client(res_select.num_clie) INTO num_return;
        result := result || res_select.empresa || ' ' || FORMAT('%10s', num_return) || E'\n';
    END LOOP;
    RETURN result;
END;
$$ LANGUAGE PLPGSQL;
```
## Exercici 3

Després de guardar una nova comanda, mirarem si les vendes del client estan pujant o baixant, a través d'un triger, fent servir la funció d'exercici1. 

Si les vendes del client han baixat guardarem el codi de client en una taula : alarmes_client

La taula alarmes_client tindrà  camps :

num_clie int
dat_hora de registre timestamp
```SQL
CREATE TABLE alarmes_client(
    num_clie INTEGER,
    data_hora TIMESTAMP
);

CREATE OR REPLACE FUNCTION ex3() RETURNS TRIGGER AS $$
DECLARE
num INTEGER;
res_select INTEGER;
BEGIN
    num := clients(NEW.num_clie);
    IF num = 2 THEN
        INSERT INTO alarmes_client VALUES(NEW.clie,CURRENT_TIMESTAMP);
    END IF;
RETURN NULL;
END;
$$ LANGUAGE PLPGSQL;

CREATE TRIGGER trigger3 AFTER INSERT ON comandes
FOR EACH ROW EXECUTE PROCEDURE ex3();
```