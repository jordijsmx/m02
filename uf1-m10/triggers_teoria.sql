--TRIGGERs (disparadors)

/*
Creem un TRIGGER quan volem que la B.D. executi una funció ABANS d'un INSERT o d'un UPDATE o d'un DELETE, o DESPRÉS d'un INSERT o d'un UPDATE o d'un DELETE, per una taula especificada.

La funció que crida el TRIGGER pot tornar :
    • NULL : En aquest cas, i si el TRIGGER és -BEFORE-, no s'executarà l'INSERT o l'UPDATE o el DELETE posterior
    • NEW : ÉS el registre tal com s'insertarà o modificarà  si el TRIGGER és -BEFORE- per INSERT o l'UPDATE
    • OLD : El registre afectat si el TRIGGER és -BEFORE- per DELETE


Primer exemple : TRIGGER ABANS de l'INSERT que posa majúscules al nom.
*/

CREATE TABLE emp
 (  empname text,
    salary integer,
    last_date timestamp,
    last_user text
);


--Funció que cridarà el TRIGGER, ha d'existir abans de crear el TRIGGER:
CREATE FUNCTION emp_nom_upper() RETURNS trigger 
AS $emp_stamp$
   	BEGIN
		NEW.empname = upper(NEW.empname);
		RETURN NEW;
	END;
 $emp_stamp$ 
LANGUAGE plpgsql;

--Creació del TRIGGER:
CREATE TRIGGER emp_stamp BEFORE INSERT OR UPDATE ON emp
    FOR EACH ROW EXECUTE PROCEDURE emp_nom_upper();

--Prova del TRIGGER:
INSERT INTO emp VALUES ('Sara Plans Tries', 1000);

SELECT * FROM emp;

--Visualitzar la definició de la taula amb el TRIGGER incorporat:
\d emp;


--Segon exemple : TRIGGER ABANS de l'INSERT que controla que el nom i el salari no siguin NULL o negatiu i posa data-timestamp i usuari.

CREATE OR REPLACE FUNCTION emp_stamp() RETURNS trigger AS $emp_stamp$
    BEGIN
        IF NEW.empname IS NULL OR NEW.empname = '' OR NEW.empname = ' az' THEN
            RAISE EXCEPTION 'empname cannot be null';
        END IF;
        IF NEW.salary IS NULL THEN
            RAISE EXCEPTION '% cannot have null salary', NEW.empname;
        END IF;
        IF NEW.salary < 0 THEN
            RETURN NULL;
        END IF;

        NEW.last_date := current_timestamp;
        NEW.last_user := current_user;
        RETURN NEW;
    END;
$emp_stamp$ LANGUAGE plpgsql;

--–> esborrar TRIGGER anterior : DROP TRIGGER emp_stamp ON emp;

CREATE TRIGGER emp_stamp BEFORE INSERT OR UPDATE ON emp
    FOR EACH ROW EXECUTE PROCEDURE emp_stamp();

--Prova del TRIGGER:
INSERT INTO emp VALUES ('Carles Saus Blanc', 3000);
INSERT INTO emp VALUES ('Carles Saus Blanc', -3000);
INSERT INTO emp VALUES ('Carles Saus Blanc');
INSERT INTO emp VALUES ('');

SELECT * FROM emp;

--Buscar els logs amb l’error a : /var/log/postgresql/postgresql-nn-main.log

--Visualitzar la definició de la taula amb el TRIGGER incorporat:
\d emp;

--Tercer exemple : TRIGGER ABANS del DELETE que només deixarà esborrar els registres amb salari més gran de 1000.

--Funció que cridarà el TRIGGER, ha d'existir abans de crear el TRIGGER:
CREATE FUNCTION emp_no_esborrar() RETURNS trigger 
AS $emp_stamp$
   	BEGIN
		IF OLD.salary > 1000 THEN
			RETURN OLD;
		ELSE
			RETURN NULL;
		END IF;
	END;
 $emp_stamp$ 
LANGUAGE plpgsql;

--Creació del TRIGGER:
CREATE TRIGGER emp_stamp BEFORE DELETE ON emp
    FOR EACH ROW EXECUTE PROCEDURE emp_no_esborrar();

--Prova del TRIGGER:
DELETE FROM emp;

SELECT * FROM emp;

--Visualitzar la definició de la taula amb el TRIGGER incorporat:
\d emp;


/*
2 tipus de TRIGGER :

    1. FOR EACH ROW : S'executa per cada registre afectat per la sentència SQL. Els exemples anteriors són  row-level.
       
    2. FOR EACH STATEMENT : S'executa només un cop, tant si la sentècia SQL afecta només a un registre com si n'afecta a més d'un. En els exemples anteriors el TRIGGER BEFORE DELETE afecta a varis registres, si hagués sigut statement-level només s'hagués executat un cop la funció.
Regles a tenir en compte al treballar amb TRIGGERs:
    1. El procedimiento almacenado que se vaya a utilizar por el disparador debe de definirse e instalarse antes de definir el propio disparador.

    2. Un procedimiento que se vaya a utilizar por un disparador no puede tener argumentos y tiene que devolver el tipo "trigger".

    3. Un mismo procedimiento almacenado se puede utilizar por múltiples disparadores en diferentes tablas.

    4. Procedimientos almacenados utilizados por disparadores que se ejecutan una sola vez per comando SQL (statement-level) tienen que devolver siempre NULL.

    5. Procedimientos almacenados utilizados por disparadores que se ejecutan una vez per linea afectada por el comando SQL (row-level) pueden devolver una fila de tabla.

    6. Procedimientos almacenados utilizados por disparadores que se ejecutan una vez per fila afectada por el comando SQL (row-level) y ANTES de ejecutar el comando SQL que lo lanzó, pueden:
        1. Retornar NULL para saltarse la operación en la fila afectada.
        2. Ó devolver una fila de tabla (RECORD)

    7. Procedimientos almacenados utilizados por disparadores que se ejecutan DESPUES de ejecutar el comando SQL que lo lanzó, ignoran el valor de retorno, asi que pueden retornar NULL sin problemas.

    8. En resumen, independendientemente de como se defina un disparador, el procedimiento almacenado utilizado por dicho disparador tiene que devolver ó bien NULL, ó bien un valor RECRD con la misma estructura que la tabla que lanzó dicho disparador.

    9. Si una tabla tiene más de un disparador definido para un mismo evento (INSERT,UPDATE,DELETE), estos se ejecutarán en orden alfabético por el nombre del disparador. En el caso de disparadores del tipo ANTES / row-level, la file retornada por cada disparador, se convierte en la entrada del siguiente. Si alguno de ellos retorna NULL, la operación será anulada para la fila afectada.

    10. Procedimientos almacenados utilizados por disparadores pueden ejecutar sentencias SQL que a su vez pueden activar otros disparadores. Esto se conoce como disparadores en cascada. No existe límite para el número de disparadores que se pueden llamar pero es responsabilidad del programador el evitar una recursión infinita de llamadas en la que un disparador se llame asi mismo de manera recursiva.

Otra cosa que tenemos que tener en cuenta es que, por cada disparador que definamos en una tabla, nuestra base de datos tendrá que ejecutar la función asociada a dicho disparador. El uso de disparadores de manera incorrecta ó inefectiva puede afectar significativamente al rendimiento de nuestra base de datos. 

Variables especiales en PL/pgSQL
Cuando una función escrita en PL/pgSQL es llamada por un disparador tenemos ciertas variableS especiales disponibles en dicha función. Estas variables son las siguientes:
NEW
Tipo de dato RECORD; Variable que contiene la nueva fila de la tabla para las operaciones INSERT/UPDATE en disparadores del tipo row-level. Esta variable es NULL en disparadores del tipo statement-level.
OLD
Tipo de dato RECORD; Variable que contiene la antigua fila de la tabla para las operaciones UPDATE/DELETE en disparadores del tipo row-level. Esta variable es NULL en disparadores del tipo statement-level.
TG_NAME
Tipo de dato name; variable que contiene el nombre del disparador que está usando la función actualmente.
TG_WHEN
Tipo de dato text; una cadena de texto con el valor BEFORE o AFTER dependiendo de como el disparador que está usando la función actualmente ha sido definido
TG_LEVEL
Tipo de dato text; una cadena de texto con el valor ROW o STATEMENT dependiendo de como el disparador que está usando la función actualmente ha sido definido
TG_OP
Tipo de dato text; una cadena de texto con el valor INSERT, UPDATE o DELETE dependiendo de la operación que ha activado el disparador que está usando la función actualmente.
TG_RELID
Tipo de dato oid; el identificador de objeto de la tabla que ha activado el disparador que está usando la función actualmente.
TG_RELNAME
Tipo de dato name; el nombre de la tabla que ha activado el disparador que está usando la función actualmente. Esta variable es obsoleta y puede desaparacer en el futuro. Usar TG_TABLE_NAME.
TG_TABLE_NAME
Tipo de dato name; el nombre de la tabla que ha activado el disparador que está usando la función actualmente.
TG_TABLE_SCHEMA
Tipo de dato name; el nombre de la schema de la tabla que ha activado el disparador que está usando la función actualmente.
TG_NARGS
Tipo de dato integer; el número de argumentos dados al procedimiento en la sentencia CREATE TRIGGER.
TG_ARGV[]
Tipo de dato text array; los argumentos de la sentencia CREATE TRIGGER. El índice empieza a contar desde 0. Indices inválidos (menores que 0 ó mayores/iguales que tg_nargs) resultan en valores nulos.
*/

--Quart exemple : TRIGGER que grava en una nova taula quines operacions es fan sobre la taula emp, quan es fan les operacions, qui les fa i els valors anteriors dels camps en cas d'UPDATE o DELETE.

CREATE TABLE emp_audit(
operation char(1) NOT NULL,
stamp timestamp NOT NULL,
userid text NOT NULL,
empname text NOT NULL,
salary integer); 

CREATE OR REPLACE FUNCTION emp_auditar() RETURNS trigger AS
$$
BEGIN
IF (TG_OP = 'DELETE') THEN
	INSERT INTO emp_audit SELECT 'D', now(), user, OLD.empname,OLD.salary;
	RETURN OLD;
ELSIF (TG_OP = 'UPDATE') THEN
	INSERT INTO emp_audit SELECT 'U', now(), user,OLD.empname,OLD.salary;
	RETURN NEW;
ELSIF (TG_OP = 'INSERT') THEN
	INSERT INTO emp_audit SELECT 'I', now(), user, NEW.empname,NEW.salary;
	RETURN NEW;
END IF;
RETURN NULL;
END;
$$ LANGUAGE plpgsql;
 
CREATE TRIGGER emp_auditar BEFORE INSERT OR UPDATE OR DELETE ON emp
FOR EACH ROW EXECUTE PROCEDURE emp_auditar();