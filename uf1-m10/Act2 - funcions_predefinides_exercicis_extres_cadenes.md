# EXTRES funcions predefinides cadena

> Heu de fer els següents exercicis amb una cadena d'exemple:

## Exercici 1 (reverse)

Donada una cadena, crea una altra cadena amb els caràcters de la cadena donada invertits.
```sql
SELECT REVERSE('cadena');
```
```sql
 reverse 
---------
 anedac
(1 row)
```
## Exercici 2. (removeSpaces) 

Donada una cadena, crea una altra cadena igual a la primera sense espais en blanc.
```sql
SELECT REPLACE('cadena amb espais',' ','');
```
```sql
     replace     
-----------------
 cadenaambespais
(1 row)
```
## Exercici 3. (withoutString)

Donades dues cadenes, crea una nova cadena igual a la primera sense l'aparició de la segona. Si apareix més d'un cop, només es suprimirà la primera aparició.
```sql
SELECT REPLACE('Hola que tal','que','');
```
```sql
  replace  
-----------
 Hola  tal
(1 row)
```
## Exercici 4. (toUpper) 

Donada una cadena, crea una segona cadena amb els caràcters que estaven en minúscula passats a majúscula.
```sql
SELECT UPPER('Hola Que Tal');
```
```sql
    upper     
--------------
 HOLA QUE TAL
(1 row)
```
## Exercici 5. (dni2nif) 

Donada una cadena amb el número d'un DNI, genera una cadena amb el NIF.

El NIF s'obté a partir del DNI afegint-li la lletra que s'obté calculant el
residu de la divisió entera del DNI entre 23.

Les lletres són: TRWAGMYFPDXBNJZSQVHLCKE i la transformació de codi és: 0 ->T,
1->R, 2->W, etc.

Exemple: Al DNI  37721039 li correspon el NIF 37721039G
```sql
CREATE OR REPLACE FUNCTION dni_lletra(dni_rebut text) RETURNS TEXT AS $$
    SELECT dni_rebut || SUBSTR('TRWAGMYFPDXBNJZSQVHLCKE',MOD(dni_rebut::INTEGER,23)+1,1)
$$ LANGUAGE SQL;

CREATE FUNCTION dni_lletra1(dni_rebut text) RETURNS TEXT AS $$
    SELECT dni_rebut || SUBSTRING('TRWAGMYFPDXBNJZSQVHLCKE' FROM (MOD(dni_rebut::INTEGER,23)+1) FOR 1)
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION dni_lletra2(dni_rebut TEXT) RETURNS TEXT AS $$
DECLARE
lletra CHAR :='';
resultat TEXT :='';
modul INTEGER;
cadena_lletres_dni TEXT :='TRWAGMYFPDXBNJZSQVHLCKE';

BEGIN

modul:= MOD(dni_rebut::INTEGER,23);
lletra:= SUBSTR(cadena_lletres_dni,modul+1,1);
resultat:= CONCAT(dni_rebut,lletra);
RETURN resultat;

END;

$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION dni_lletra3(dni_rebut TEXT) RETURNS TEXT AS $$
DECLARE
lletra CHAR :='';
resultat TEXT :='';
modul INTEGER;
cadena_lletres_dni TEXT :='TRWAGMYFPDXBNJZSQVHLCKE';

BEGIN

modul:= MOD(dni_rebut::INTEGER,23);
lletra:= SUBSTRING(cadena_lletres_dni FROM modul+1 FOR 1);
resultat:= dni_rebut || lletra;
RETURN resultat;

END;

$$ LANGUAGE 'plpgsql';
```
