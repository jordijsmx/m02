## Teoria

### Tipus funcions
```sql
CREATE FUNCTION funcio1() RETURNS TEXT AS $$
DECLARE
  nom_empresa TEXT := 'No hi ha cap resultat';

BEGIN
  SELECT empresa INTO nom_empresa
  FROM clients
  WHERE num_clie > 10;
  RETURN nom_empresa;
END;

$$ LANGUAGE PLPGSQL;
```
```sql
CREATE FUNCTION funcio2() RETURNS TEXT AS $$
DECLARE
  nom_empresa RECORD; /* Per recollir tot un registre, més d'un valor */

BEGIN
  SELECT * INTO nom_empresa
  FROM clients
  WHERE num_clie > 10;
  RETURN nom_empresa; /* Retornem un tipus registre, mès d'un valor */
END;

$$ LANGUAGE PLPGSQL;
```
```sql
CREATE FUNCTION funcio3() RETURNS TEXT AS $$
DECLARE
  nom_empresa clients%ROWTYPE; /* Es declara un registre igual al de una taula */

BEGIN
  SELECT * INTO nom_empresa
  FROM clients
  WHERE num_clie > 10;
  RETURN nom_empresa;
END;

$$ LANGUAGE PLPGSQL;
```
```sql
CREATE FUNCTION funcio4() RETURNS SETOF VARCHAR(20) AS $$
BEGIN
  RETURN QUERY
  SELECT empresa
  FROM clients
  WHERE num_clie > 10;
END;

$$ LANGUAGE PLPGSQL;
```
```sql
CREATE FUNCTION funcio5(ofi INT) RETURNS TABLE (id_fabricant CHAR(3), id_producte CHAR(5)) AS $$
BEGIN
  SELECT fabricant, producte
    FROM comandes 
    JOIN rep_vendes 
      ON rep = num_empl 
    JOIN oficines 
      ON oficina_rep = ofi;
END;

$$ LANGUAGE PLPGSQL;
```

### IF ELSIF
```sql
IF condicio THEN
ELSIF condicio THEN
ELSE ...
END IF;
```

```sql
CREATE OR REPLACE FUNCTION funcio6(INT) RETURNS TEXT AS $$
DECLARE
    resultat TEXT;
    var_regio TEXT;
    var_oficina TEXT;
BEGIN
    SELECT regio INTO var_regio
      FROM  oficines
     WHERE oficina = $1;
    
    IF FOUND THEN
      IF var_regio = 'Est' THEN 
        resultat:='oficina Est';
      ELSE 
        resultat:='oficina NO Est';
      END IF;
    ELSE
      resultat:='Aquesta oficina no existeix.';
    END IF;
    RETURN resultat;
END;
$$ LANGUAGE PLPGSQL;
```
### Funció amb sentència SQL construida concatenant text a variable per executar-la amb execute
```sql
CREATE OR REPLACE FUNCTION empl_est() RETURNS SETOF rep_vendes AS $$
DECLARE
  empl RECORD;
  sql text:='SELECT rep_vendes.* 
               FROM rep_vendes
               JOIN oficines
                 ON oficina_rep = oficina
                 WHERE regio = ''Est''';
  sql2 text:='HOLAAAAA';
  BEGIN
    RAISE NOTICE 'sql=%',sql;  /* RAISE NOTICE 'variable=%',variable; */
    RETURN QUERY execute(sql);
    RETURN;
  END;
$$ LANGUAGE PLPGSQL;
```
### Funció amb concatenació de variable a dins de la cadena
```sql
CREATE OR REPLACE FUNCTION prova2(cadena TEXT) RETURNS SETOF clients AS $$
DECLARE
  empl RECORD;
  sql text:='SELECT * 
               FROM clients 
              WHERE empresa LIKE ''%' || cadena || ''''; /* 2 COMETES EQUIVALEN A 1, ESCAPEM AMB '' */
  BEGIN
    RAISE NOTICE 'sql=%',sql;
    RETURN QUERY execute(sql);
    RETURN;
  END;
$$ LANGUAGE PLPGSQL;
```
```SQL
CREATE OR REPLACE FUNCTION busca_client(num_clie TEXT) RETURNS SETOF clients AS $$
DECLARE
    client RECORD;
    sql TEXT := 'SELECT* FROM clients WHERE num_clie =' || num_clie || ';';
BEGIN
    RAISE NOTICE 'sql = %', sql;
    RETURN QUERY execute(sql);
    RETURN;
END;
$$ LANGUAGE PLPGSQL;
```
### funció amb EXCEPTION
```sql
CREATE OR REPLACE FUNCTION pri_insCli(a INTEGER, b TEXT, c INTEGER, d INTEGER) RETURNS TEXT AS $$
DECLARE
  sql text;

BEGIN
  EXECUTE 'INSERT INTO clients VALUES($1,$2,$3,$4)' USING a,b,c,d;
  RETURN '1';
EXCEPTION  /* Control d'errors per no violar regles de la taula */
  WHEN unique_violation THEN RETURN '5';
  WHEN not_null_violation THEN RETURN '4';
  WHEN foreign_key_violation THEN RETURN '3';
END;
$$ LANGUAGE PLPGSQL;
```
```sql
CREATE OR REPLACE FUNCTION insert_ofi(a INTEGER, b TEXT, c TEXT, d INTEGER, e NUMERIC,f NUMERIC) RETURNS TEXT AS $$
DECLARE
  sql text;

BEGIN
  EXECUTE 'INSERT INTO oficines VALUES($1,$2,$3,$4,$5,$6)' USING a,b,c,d,e,f;
  RETURN '1';
EXCEPTION
  WHEN unique_violation THEN RETURN '5';
  WHEN not_null_violation THEN RETURN '4';
  WHEN foreign_key_violation THEN RETURN '3';
END;
$$ LANGUAGE PLPGSQL;
```
### Funció llegir clients i tractar-los dins del FOR,RAISE notice '%',i
```SQL
CREATE OR REPLACE FUNCTION pri_sel(id_cli INTEGER) RETURNS TEXT AS $$
DECLARE
  result TEXT:='';
  searchsql TEXT:='';
  res_select RECORD;
BEGIN
  searchsql:='SELECT * FROM clients WHERE num_clie >= ' || $1;

  RAISE NOTICE '%', searchsql;

  FOR res_select IN EXECUTE(searchsql) LOOP
    IF result > '' THEN
      result:=result || res_select.num_clie || '= ' || res_select || E'\n';
    ELSE
      result:=res_select.num_clie || '= ' || res_select || E'\n';
    END IF;
  END LOOP;
  IF result = '' THEN
    result:='Dades inexistents';
  END IF;

  RETURN searchsql || ': ' || E'\n' || result;
EXCEPTION
  WHEN others THEN RETURN 'Error BD';
END;
$$ LANGUAGE PLPGSQL;
```
```sql
CREATE OR REPLACE FUNCTION pri_sel2(id_cli INTEGER) RETURNS TEXT AS $$
DECLARE
  result TEXT:='';
  searchsql TEXT:='';
  res_select RECORD;
BEGIN
  searchsql:='SELECT * FROM clients WHERE num_clie >= ' || $1;

  RAISE NOTICE '%', searchsql;

  FOR res_select IN EXECUTE(searchsql) LOOP
    result:=result || '---' || res_select.num_clie || '---' || res_select.empresa || E'\n';

  END LOOP;
  IF result = '' THEN
    result:='Dades inexistents';
  END IF;

  RETURN searchsql || ': ' || E'\n' || result;
EXCEPTION
  WHEN others THEN RETURN 'Error BD';
END;
$$ LANGUAGE PLPGSQL;
```
```sql
CREATE OR REPLACE FUNCTION pri_sel2(edat INTEGER) RETURNS TEXT AS $$
DECLARE
  result TEXT:='';
  searchsql TEXT:='';
  res_select RECORD;
BEGIN
  searchsql:='SELECT * FROM rep_vendes WHERE edat >= ' || $1;

  RAISE NOTICE '%', searchsql;

  FOR res_select IN EXECUTE(searchsql) LOOP
    result:=result || '--' || res_select.num_empl || ' ' || res_select.nom || ', ' || res_select.edat || E'\n';

  END LOOP;
  IF result = '' THEN
    result:='Dades inexistents';
  END IF;

  RETURN searchsql || ': ' || E'\n' || result;
EXCEPTION
  WHEN others THEN RETURN 'Error BD';
END;
$$ LANGUAGE PLPGSQL;
```