# PLpgSQL - FOR


## Exercici 1


Crea una funció que donat un identificador de client mostri per pantalla les factures corresponents a les seves comandes.

+ Cada factura ha de contenir el productes que ha demanat el client amb la data en que els ha demanat.

+ S'ha de calcular l'IVA (21% per tots els productes) i el total.

+ Feu servir la funció format per posar un número de caràcters fix que ha d'ocupar cada valor:

```sql
CREATE OR REPLACE FUNCTION factures1(INTEGER) RETURNS TEXT AS $$
DECLARE

BEGIN

END;
$$ LANGUAGE PLPGSQL;
```
```
SELECT FORMAT('|%10s|', 'one');
    format
--------------
 |       one|

SELECT FORMAT('|%-10s|', 'one');
    format
--------------
 |one       |
```

El llistaT de les factures d'un client ha de tenir el següent aspecte. 
```
select factures1(2103);
                                                   factures1                                                    
----------------------------------------------------------------------------------------------------------------
 Factura Num.: 112987       Client (id): Acme Mfg.(2103)                                                       +
 =======================================================                                                       +
 Data: 04/05/2023                                                                                              +
                                                                                                               +
 Descripció del producte (id)            Data de la comanda  Quantitat  Preu Unitari  Descompte         Import +
 --------------------------------------------------------------------------------------------------------------+
 Extractor(aci, 4100y)                           31/12/1989         11       2750.00                  27500.00 +
 Base:          27500.00                                                                                       +
                                                                                   IVA(21%):           5775.00 +
                                                                                      Total:          33275.00 +
 ==============================================================================================================+
                                                                                                               +
                                                                                                               +
                                                                                                               +
                                                                                                               +
                                                                                                               +
 Factura Num.: 112983       Client (id): Acme Mfg.(2103)                                                       +
 =======================================================                                                       +
 Data: 04/05/2023                                                                                              +
                                                                                                               +
 Descripció del producte (id)            Data de la comanda  Quantitat  Preu Unitari  Descompte         Import +
 --------------------------------------------------------------------------------------------------------------+
 Article Tipus 4(aci, 41004)                     27/12/1989          6        117.00                    702.00 +
 Base:            702.00                                                                                       +
                                                                                   IVA(21%):            147.42 +
                                                                                      Total:            849.42 +
 ==============================================================================================================+
                                                                                                               +
                                                                                                               +
                                                                                                               +
                                                                                                               +
                                                                                                               +
 Factura Num.: 112963       Client (id): Acme Mfg.(2103)                                                       +
 =======================================================                                                       +
 Data: 04/05/2023                                                                                              +
                                                                                                               +
 Descripció del producte (id)            Data de la comanda  Quantitat  Preu Unitari  Descompte         Import +
 --------------------------------------------------------------------------------------------------------------+
 Article Tipus 4(aci, 41004)                     17/12/1989         28        117.00                   3276.00 +
 Base:           3276.00                                                                                       +
                                                                                   IVA(21%):            687.96 +
                                                                                      Total:           3963.96 +
 ==============================================================================================================+
                                                                                                               +
                                                                                                               +
                                                                                                               +
                                                                                                               +
                                                                                                               +
 Factura Num.: 113027       Client (id): Acme Mfg.(2103)                                                       +
 =======================================================                                                       +
 Data: 04/05/2023                                                                                              +
                                                                                                               +
 Descripció del producte (id)            Data de la comanda  Quantitat  Preu Unitari  Descompte         Import +
 --------------------------------------------------------------------------------------------------------------+
 Article Tipus 2(aci, 41002)                     22/01/1990         54         76.00                   4104.00 +
 Base:           4104.00                                                                                       +
                                                                                   IVA(21%):            861.84 +
                                                                                      Total:           4965.84 +
 ==============================================================================================================+
                                                                                                               +
                                                                                                               +
                                                                                                               +
                                                                                                               +
                                                                                                               +
 
(1 row)
