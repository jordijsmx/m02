# PLpgSQL - Estructura alternativa

## Exercici 1

(sign) Determina si un nombre real és positiu, negatiu o zero. La funció
retornarà les cadenes "positiu", "negatiu" o "zero", respectivament.
```SQL
CREATE OR REPLACE FUNCTION num(INT) RETURNS TEXT AS $$
DECLARE
    resultat TEXT;
BEGIN
    CASE
      WHEN $1 > 0 THEN 
        resultat := 'Positiu';
      WHEN $1 < 0 THEN 
        resultat := 'Negatiu';
      ELSE 
        resultat := 'Zero';
    END CASE;
    RETURN resultat;
END;
$$ LANGUAGE PLPGSQL;
```
## Exercici 2

(triangle) A partir de les llargades de tres numments, esbrina si formen o no
un triangle i si formen triangle de quin tipus. 

Per tal de que tres numments formin un triangle, un numment qualsevol ha de ser
més petit que la suma de la llargada dels altres dos. 

Si els tres numments formen un triangle, aquest pot ser:

+ equilàter si els tres costats són iguals (del llatí: costats iguals)
	
+ isòsceles si només dos costats són iguals (del grec: dues cames iguals)

+ escalè si tots 3 costat són de diferent mida (del llatí que el va agafar del grec: desigual)

Heu de dissenyar una funció que retorni

+ "No és triangle"

+ "Triangle equilàter"

+ "Triangle isòsceles"

+ "Triangle escalè".
```SQL
CREATE OR REPLACE FUNCTION triangle(num1 INT, num2 INT, num3 INT) RETURNS TEXT AS $$
DECLARE
    tipus TEXT;
BEGIN
   IF (num1 >= num2 + num3) OR (num2 >= num1 + num3) OR (num3 >= num1 + num2) THEN
        tipus := 'No és triangle';
    ELSE
        IF (num1 = num2) AND (num2 = num3) THEN
            tipus := 'Triangle equilàter';
        ELSEIF (num1 = num2) OR (num1 = num3) OR (num2 = num3) THEN
            tipus := 'Triangle isòsceles';
        ELSE
            tipus := 'Triangle escalè';
        END IF;
    END IF;
    
    RETURN tipus;
END;
$$ LANGUAGE PLPGSQL;
```
## Exercici 3

Donada una cadena amb un NIF, determina si és un NIF correcte.

+ S'ha de validar que la llargada de la cadena d'entrada sigui correcta (9
  caràcters) i que la lletra sigui correcta.

+ El NIF s'obté a partir del DNI afegint-li la lletra que s'obté calculant el
  residu de la divisió entera del DNI entre 23.

+ Les lletres són: TRWAGMYFPDXBNJZSQVHLCKE i la transformació de codi és: 0 ->
  T, 1-> R, 2 -> W, etc.

+ Exemple: Al DNI 37721039 li correspon el NIF 37721039G. Si la llargada és
  incorrecta retornarem un 1, si el problema és la lletra retornarem un 2 i si tot és correcte retornarem un 0.
```SQL
CREATE OR REPLACE FUNCTION dni(TEXT) RETURNS TEXT AS $$
DECLARE
  lletres_dni TEXT :='TRWAGMYFPDXBNJZSQVHLCKE';
  modul INTEGER;
  lletra_llista TEXT;
  lletra TEXT;
BEGIN
  modul := MOD(SUBSTR($1,1,8)::integer,23)+1;
  lletra_llista := SUBSTR(lletres_dni,modul,1);
  lletra := SUBSTR($1,9);

  IF LENGTH($1) <> 9 THEN
    RETURN '1';
  ELSEIF lletra_llista <> lletra THEN
    RETURN '2';
  ELSE
    RETURN '0';
  END IF;
END;
$$ LANGUAGE PLPGSQL;
```
## Exercici 4

Donada una comanda, esbrina si es va fer en un any de traspàs. 

+ Si era any de traspàs retorna TRUE, sinó FALSE.

+ Són de traspàs els anys que són múltiples de 400 i els anys que són múltiples
  de 4 però no de 100.
```sql
CREATE OR REPLACE FUNCTION traspas(INTEGER) RETURNS BOOLEAN AS $$
DECLARE
    any_com INTEGER;
BEGIN
    SELECT DATE_PART('year', data) INTO any_com
      FROM comandes 
     WHERE num_comanda = $1;

    IF any_com % 400 = 0 OR (any_com % 4 = 0 AND any_com % 100 <> 0) THEN
        RETURN TRUE;
     ELSE
        RETURN FALSE;
    END IF;
END;
$$ LANGUAGE PLPGSQL;
```
## Exercici 5

Crea una funció que insereixi una oficina a la taula oficines. Per paràmetre
arribaran totes les dades d'una oficina (oficina, ciutat, regio, director,
objectiu, vendes). S'hauran de fer les següents comprovacions:
  
+ El codi d'oficina no existeix a la taula oficines.
```SQL
CREATE OR REPLACE FUNCTION ofi(num INTEGER) RETURNS BOOLEAN AS $$
BEGIN
    RETURN EXISTS(SELECT * FROM oficines WHERE oficina = num);
END;
$$ LANGUAGE PLPGSQL;
```
+ La regió es 'Est' o 'Oest'.
```sql
CREATE OR REPLACE FUNCTION regio(reg TEXT) RETURNS BOOLEAN AS $$
BEGIN
  IF reg IN('Est','Oest') THEN
    RETURN TRUE;
  ELSE
    RETURN FALSE;
  END IF;
END;
$$ LANGUAGE PLPGSQL;
```
+ El codi de director és valid.
```sql
CREATE OR REPLACE FUNCTION director(num INTEGER) RETURNS BOOLEAN AS $$
DECLARE 
  dir INTEGER;
BEGIN
  SELECT director INTO dir
    FROM oficines 
   WHERE director = num;

    IF num IN (dir) THEN
    RETURN TRUE;
  ELSE
    RETURN FALSE;
    END IF;
END;
$$ LANGUAGE PLPGSQL;
```
+ L'objectiu és superior a 0.
```SQL
CREATE OR REPLACE FUNCTION objectiu(obj INTEGER) RETURNS BOOLEAN AS $$
BEGIN
  RETURN obj > 0;
END;
$$ LANGUAGE PLPGSQL;
```
+ Si totes les dades són vàlides, l'oficina s'inserirà i retornarem TRUE.
Altrament no s'inserirà i retornarem FALSE.

IMPORTANT: feu totes les comprovacions en funcions separades per poder-les reutilitzar.
```sql
CREATE OR REPLACE FUNCTION insert(a INTEGER, 
                                  b TEXT, 
                                  c TEXT, 
                                  d INTEGER, 
                                  e INTEGER,
                                  f INTEGER) RETURNS BOOLEAN AS $$
BEGIN
  IF ofi(a) THEN RETURN FALSE;
  ELSIF NOT regio(c) THEN RETURN FALSE;
  ELSIF NOT director(d) THEN RETURN FALSE;
  ELSIF NOT objectiu(e) THEN RETURN FALSE;
  ELSE
    INSERT INTO oficines VALUES(a,b,c,d,e,f);
    RETURN TRUE;
  END IF;
END;
$$ LANGUAGE PLPGSQL;
```

## Exercici 6

Crea una funció que insereixi o modifiqui una oficina a la taula oficines. Per
paràmetre arribaran totes les dades d'una oficina (oficina, ciutat, regio,
director, objectiu, vendes). S'hauran de fer les següents comprovacions:

+ La regio es 'Est' o 'Oest'.

+ El codi de director és vàlid.

+ L'objectiu és superior a 0.

+ Si totes les dades són vàlides i el codi d'oficina no existeix, l'oficina
s'inserirà i retornarem un 1.

+ Si totes les dades són vàlides i el codi d'oficina existeix, l'oficina amb
aquest codi es modificarà i retornarem un 2. 

+ Altrament no es farà res i retornarem un -1.
```SQL
CREATE OR REPLACE FUNCTION insert(a INTEGER, 
                                  b TEXT, 
                                  c TEXT, 
                                  d INTEGER, 
                                  e INTEGER,
                                  f INTEGER) RETURNS INTEGER AS $$
DECLARE
  regio_valida BOOLEAN;
  director_valid BOOLEAN;
  objectiu_valid BOOLEAN;
  oficina_existeix BOOLEAN;
BEGIN
  -- Comprovem si la regió és vàlida
  regio_valida := (c IN ('Est', 'Oest'));
  
  -- Comprovem si el director és vàlid
  SELECT EXISTS (SELECT director FROM oficines WHERE director = d) INTO director_valid;
  
  -- Comprovem si l'objectiu és vàlid
  objectiu_valid := (e > 0);
  
  -- Comprovem si l'oficina ja existeix
  SELECT EXISTS (SELECT oficina FROM oficines WHERE oficina = a) INTO oficina_existeix;
  
  -- Si totes les comprovacions són correctes, insertem o actualitzem l'oficina
  IF regio_valida AND director_valid AND objectiu_valid THEN
    IF NOT oficina_existeix THEN
      INSERT INTO oficines VALUES (a, b, c, d, e, f);
      RETURN 1;
    ELSE
      UPDATE oficines SET ciutat = b, regio = c, director = d, objectiu = e, vendes = f
        WHERE oficina = a;
      RETURN 2;
    END IF;
  ELSE
    RETURN -1;
  END IF;
END;
$$ LANGUAGE PLPGSQL;
```