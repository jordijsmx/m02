# BD w3schools:

https://www.w3schools.com/sql/trysql.asp?filename=trysql_select_all

## PRODUCTS

## WHERE

### Exercici 1

Productes dels SuplplierID 3, 6 i 8
```
SELECT * 
  FROM Products
 WHERE SupplierID IN (3,6,8);
```
### Exercici 2
Productes dels SupplierID 3, 6 i 8 que siguin de les Categories 2, 8 i 4
```
SELECT * 
  FROM Products
 WHERE (SupplierID = 3 OR SupplierID = 6 OR SupplierID = 8) 
   AND (CategoryID = 2 OR CategoryID = 8 OR CategoryID = 4);
```
### Exercici 3
Productes dels SupplierID 3, 6 i 8 que siguin de les Categories 2, 8 i 4 que tinguin un preu superior a 25
```
SELECT * 
  FROM Products
 WHERE (SupplierID = 3 OR SupplierID = 6 OR SupplierID = 8) 
   AND (CategoryID = 2 OR CategoryID = 8 OR CategoryID = 4)
   AND Price > 25;
```
### Exercici 4
Productes dels SupplierID 3, 6 i 8 que siguin de les Categories 2, 8 i 4 que tinguin un preu no superior a 25
```
SELECT * 
  FROM Products
 WHERE (SupplierID = 3 OR SupplierID = 6 OR SupplierID = 8) 
   AND (CategoryID = 2 OR CategoryID = 8 OR CategoryID = 4)
   AND Price <= 25;
```
### Exercici 6
Productes dels SupplierID 3, 6 i 8 que siguin de les Categories 2, 8 i 4 que tinguin un preu superior a 25 que tinguin un preu inferior a 40
```
SELECT * 
  FROM Products
 WHERE (SupplierID = 3 OR SupplierID = 6 OR SupplierID = 8) 
   AND (CategoryID = 2 OR CategoryID = 8 OR CategoryID = 4)
   AND Price < 40;
```
### Exercici 7
Productes dels SupplierID 3, 6 i 8 que siguin de les Categories 2, 8 i 4 que tinguin un preu no superior a 25 i no inferior a 10
```
SELECT * 
  FROM Products
 WHERE NOT (SupplierID = 3 AND SupplierID = 6 AND SupplierID = 8);
```

### Exercici 8
Productes dels SupplierID que no siguin ni 3, ni 6, ni 8 
```
```

```
```
### Exercici 9
Productes dels SupplierID que no siguin ni 3, ni 6, ni 8 i que no siguin de les Categories 2, 8 i 4
```
```

```
```
### Exercici 10
Productes dels SuplplierID 3, 6 i 8 que siguin de les Categories 2, 8 i 4 i que continguin al seu nom les lletres 'w' i 'g'
```
```

```
```
## HAVING
### Exercici 11
Quants productes hi ha de cada Supplier
```
```

```
```
### Exercici 12
Per cada Supplier, quants productes hi ha i quantes Categories diferents té
```
```

```
```
### Exercici 13
Per cada preu diferent, quants productes tenim
```
```

```
```

## PRODUCTS + ORDERS + ORDERSDETAILS

## HAVING i JOIN
### Exercici 1
Per cada producte diferent de PRODUCTS quantes orders s'han fet
```
```

```
```
### Exercici 2
Per cada producte diferent quantes unitats s'han venut
```
```

```
```
### Exercici 3
Per cada CustomerID, quantes Orders tenim?
```
```

```
```
### Exercici 4
Per cada producte, a quants clients diferents s'ha venut?
```
```

```
```
### Exercici 5
Cada client, quantes Orders ha fet
```
```

```
```
### Exercici 6
Cada clients, quants cops ha comprat cada producte?
```
```

```
```
### Exercici 7
Cada EmployeeID, quantes Orders ha fet
```
```

```
```
### Exercici 8
Cada EmployeeId, a quants clients diferents ha venut?
```
```

```
```
### Exercici 9
Cada EmployeeId, quin import ha facturat per cada producte?
```
```

```
```
### Exercici 10
Cada EmployeeId, quin import ha facturat per cada producte a cada client?
```
```

```
```
### Exercici 11
Cada EmployeeId, quin és el preu més alt que ha venut, i quin és el preu més baix?
```
```

```
```
### Exercici 12
Per cada Supplier quant s'ha facturat?
```
```

```
```
### Exercici 13
Per cada Supplier, quant ha facturat cada EmployeeId?
```
```

```
```
### Exercici 14
Per cada Supplier, quant ha facturat cada EmployeeId de cada categoria?
```
```

```
```