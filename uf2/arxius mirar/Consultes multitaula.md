# Consultes multitaula



## Exercici 1

Llista la ciutat de les oficines, i el nom i títol dels directors de cada oficina.

```
SELECT ciutat, nom, carrec
  FROM oficines 
  JOIN rep_vendes 
    ON oficines.director = rep_vendes.num_empl;
```

```
   ciutat    |     nom     |       carrec        
-------------+-------------+---------------------
 Denver      | Larry Fitch | Dir Vendes
 New York    | Sam Clark   | VP Vendes
 Chicago     | Bob Smith   | Dir Vendes
 Atlanta     | Bill Adams  | Representant Vendes
 Los Angeles | Larry Fitch | Dir Vendes
(5 rows)
```

## Exercici 2

Llista totes les comandes mostrant el seu número, import, número de client i límit de crèdit.

```
SELECT num_comanda, descripcio 
  FROM comandes 
  JOIN productes 
    ON (comandes.producte, comandes.fabricant) = (productes.id_producte, productes.id_fabricant);
```

``` 
 num_comanda |     descripcio     
-------------+--------------------
      112961 | Frontissa Esq.
      113012 | Article Tipus 3
      112989 | Bancada Motor
      112968 | Article Tipus 4
      110036 | Muntador
      113045 | Frontissa Dta.
      112963 | Article Tipus 4
      113013 | Manovella
      113058 | Coberta
      112997 | Manovella
      112983 | Article Tipus 4
      113024 | Reductor
      113062 | Bancada Motor
      112979 | Muntador
      113027 | Article Tipus 2
      113007 | Riosta 1/2-Tm
      113069 | Riosta 1-Tm
      113034 | V Stago Trinquet
      112992 | Article Tipus 2
      112975 | Passador Frontissa
      113055 | Peu de rei
      113048 | Riosta 2-Tm
      112993 | V Stago Trinquet
      113065 | Reductor
      113003 | Riosta 2-Tm
      113049 | Reductor
      112987 | Extractor
      113057 | Peu de rei
      113042 | Frontissa Dta.
(29 rows)
```

## Exercici 3

Llista el número de totes les comandes amb la descripció del producte demanat.

```
SELECT num_comanda, descripcio 
  FROM comandes 
  JOIN productes 
    ON comandes.producte = productes.id_producte;
```

```
 num_comanda |     descripcio     
-------------+--------------------
      112961 | Frontissa Esq.
      113012 | Manovella
      113012 | Article Tipus 3
      112989 | Bancada Motor
      112968 | Article Tipus 4
      110036 | Muntador
      113045 | Frontissa Dta.
      112963 | Article Tipus 4
      113013 | Manovella
      113013 | Article Tipus 3
      113058 | Coberta
      112997 | Manovella
      112997 | Article Tipus 3
      112983 | Article Tipus 4
      113024 | Reductor
      113062 | Bancada Motor
      112979 | Muntador
      113027 | Article Tipus 2
      113007 | Riosta 1/2-Tm
      113069 | Riosta 1-Tm
      113034 | V Stago Trinquet
      112992 | Article Tipus 2
      112975 | Passador Frontissa
      113055 | Peu de rei
      113048 | Riosta 2-Tm
      112993 | V Stago Trinquet
      113065 | Reductor
      113003 | Riosta 2-Tm
      113049 | Reductor
      112987 | Extractor
      113057 | Peu de rei
      113042 | Frontissa Dta.
(32 rows)
```

## Exercici 4

Llista el nom de tots els clients amb el nom del representant de vendes assignat.

```
SELECT empresa, nom 
  FROM clients 
  JOIN rep_vendes 
    ON rep_vendes.num_empl = clients.rep_clie;
```

```
      empresa      |      nom      
-------------------+---------------
 JCP Inc.          | Paul Cruz
 First Corp.       | Dan Roberts
 Acme Mfg.         | Bill Adams
 Carter & Sons     | Sue Smith
 Ace International | Tom Snyder
 Smithson Corp.    | Dan Roberts
 Jones Mfg.        | Sam Clark
 Zetacorp          | Larry Fitch
 QMA Assoc.        | Paul Cruz
 Orion Corp        | Sue Smith
 Peter Brothers    | Nancy Angelli
 Holm & Landis     | Mary Jones
 J.P. Sinclair     | Sam Clark
 Three-Way Lines   | Bill Adams
 Rico Enterprises  | Sue Smith
 Fred Lewis Corp.  | Sue Smith
 Solomon Inc.      | Mary Jones
 Midwest Systems   | Larry Fitch
 Ian & Schmidt     | Bob Smith
 Chen Associates   | Paul Cruz
 AAA Investments   | Dan Roberts
(21 rows)
```

## Exercici 5

Llista la data de totes les comandes amb el numero i nom del client de la comanda.

```
SELECT data, clie, empresa 
  FROM comandes 
  JOIN clients 
    ON comandes.clie = clients.num_clie;
```

```
    data    | clie |      empresa      
------------+------+-------------------
 1989-12-17 | 2117 | J.P. Sinclair
 1990-01-11 | 2111 | JCP Inc.
 1990-01-03 | 2101 | Jones Mfg.
 1990-02-10 | 2118 | Midwest Systems
 1989-10-12 | 2102 | First Corp.
 1990-01-30 | 2107 | Ace International
 1990-02-02 | 2112 | Zetacorp
 1989-12-17 | 2103 | Acme Mfg.
 1990-01-14 | 2118 | Midwest Systems
 1990-02-23 | 2108 | Holm & Landis
 1990-01-08 | 2124 | Peter Brothers
 1989-12-27 | 2103 | Acme Mfg.
 1990-01-20 | 2114 | Orion Corp
 1990-02-24 | 2124 | Peter Brothers
 1989-10-12 | 2114 | Orion Corp
 1990-01-22 | 2103 | Acme Mfg.
 1990-01-08 | 2112 | Zetacorp
 1990-03-02 | 2109 | Chen Associates
 1990-01-29 | 2107 | Ace International
 1989-11-04 | 2118 | Midwest Systems
 1989-12-12 | 2111 | JCP Inc.
 1990-02-15 | 2108 | Holm & Landis
 1990-02-10 | 2120 | Rico Enterprises
 1989-01-04 | 2106 | Fred Lewis Corp.
 1990-02-27 | 2106 | Fred Lewis Corp.
 1990-01-25 | 2108 | Holm & Landis
 1990-02-10 | 2118 | Midwest Systems
 1989-12-31 | 2103 | Acme Mfg.
 1990-02-18 | 2111 | JCP Inc.
 1990-02-02 | 2113 | Ian & Schmidt
(30 rows)
```

## Exercici 6

Llista les oficines, noms i títols del seus directors amb un objectiu superior a 600.000.

```
SELECT oficines.oficina, rep_vendes.nom, rep_vendes.carrec
  FROM oficines 
  JOIN rep_vendes 
    ON oficines.director = rep_vendes.num_empl 
 WHERE objectiu > 600000; 
```

```
 oficina |     nom     |   carrec   
---------+-------------+------------
      12 | Bob Smith   | Dir Vendes
      21 | Larry Fitch | Dir Vendes
(2 rows)
```

## Exercici 7

Llista els venedors de les oficines de la regió est.

```
SELECT rep_vendes.nom 
  FROM rep_vendes 
  JOIN oficines 
    ON rep_vendes.oficina_rep = oficines.oficina 
 WHERE regio = 'Est';
```

```
     nom     
-------------
 Bill Adams
 Mary Jones
 Sam Clark
 Bob Smith
 Dan Roberts
 Paul Cruz
(6 rows)
```

## Exercici 8

Llista les comandes superiors a 25000, incloent el nom del venedor que va servir la comanda i el nom del client que el va sol·licitar.

```
SELECT comandes.import, rep_vendes.nom, clients.empresa
  FROM comandes
  JOIN rep_vendes ON comandes.rep = rep_vendes.num_empl
  JOIN clients 
    ON comandes.clie = clients.num_clie 
 WHERE import > 25000;
```

```
  import  |      nom      |     empresa     
----------+---------------+-----------------
 31500.00 | Sam Clark     | J.P. Sinclair
 45000.00 | Larry Fitch   | Zetacorp
 31350.00 | Nancy Angelli | Chen Associates
 27500.00 | Bill Adams    | Acme Mfg.
(4 rows)
```

## Exercici 9

Llista les comandes superiors a 25000, mostrant el client que va servir la comanda i el nom del venedor que té assignat el client.

```
SELECT comandes.import, clients.empresa, rep_vendes.nom 
  FROM comandes 
  JOIN clients 
    ON comandes.clie = clients.num_clie 
  JOIN rep_vendes 
    ON rep_vendes.num_empl = comandes.rep 
 WHERE import > 25000;
```

```
  import  |     empresa     |      nom      
----------+-----------------+---------------
 31500.00 | J.P. Sinclair   | Sam Clark
 45000.00 | Zetacorp        | Larry Fitch
 31350.00 | Chen Associates | Nancy Angelli
 27500.00 | Acme Mfg.       | Bill Adams
(4 rows)
```

## Exercici 10

Llista les comandes superiors a 25000, mostrant el nom del client que el va ordenar, el venedor associat al client, i l'oficina on el venedor treballa.

```
SELECT comandes.import, clients.empresa, rep_vendes.nom, oficines.oficina
  FROM comandes 
  JOIN clients 
    ON comandes.clie = clients.num_clie
  JOIN rep_vendes 
    ON clients.rep_clie = rep_vendes.num_empl
  JOIN oficines 
    ON oficines.oficina = rep_vendes.oficina_rep
 WHERE import > 25000;
```

```
  import  |     empresa     |     nom     | oficina 
----------+-----------------+-------------+---------
 31500.00 | J.P. Sinclair   | Sam Clark   |      11
 45000.00 | Zetacorp        | Larry Fitch |      21
 31350.00 | Chen Associates | Paul Cruz   |      12
 27500.00 | Acme Mfg.       | Bill Adams  |      13
(4 rows)
```

## Exercici 11

Llista totes les combinacions de venedors i oficines on la quota del venedor és superior a l'objectiu de l'oficina.

```
SELECT rep_vendes.nom, oficines.ciutat
  FROM rep_vendes
  CROSS JOIN oficines
  WHERE rep_vendes.quota > oficines.objectiu;
```

```
     nom     | ciutat 
-------------+--------
 Bill Adams  | Denver
 Sue Smith   | Denver
 Larry Fitch | Denver
(3 rows)
```

## Exercici 12

Informa sobre tots els venedors i les oficines en les que treballen.

```
SELECT rep_vendes.nom, oficines.oficina, oficines.ciutat
  FROM rep_vendes 
  JOIN oficines 
    ON rep_vendes.oficina_rep = oficines.oficina;
```

```
      nom      | oficina |   ciutat    
---------------+---------+-------------
 Bill Adams    |      13 | Atlanta
 Mary Jones    |      11 | New York
 Sue Smith     |      21 | Los Angeles
 Sam Clark     |      11 | New York
 Bob Smith     |      12 | Chicago
 Dan Roberts   |      12 | Chicago
 Larry Fitch   |      21 | Los Angeles
 Paul Cruz     |      12 | Chicago
 Nancy Angelli |      22 | Denver
(9 rows)
```

## Exercici 13

Llista els venedors amb una quota superior a la dels seus directors.

```
SELECT b.num_empl, a.nom
  FROM rep_vendes a 
  JOIN rep_vendes b
    ON b.num_empl = a.cap WHERE a.quota > b.quota;
```

```
 num_empl |     nom     
----------+-------------
      104 | Bill Adams
      106 | Mary Jones
      104 | Dan Roberts
      106 | Larry Fitch
      104 | Paul Cruz
(5 rows)
```

## Exercici 14

Llistar el nom de l'empresa i totes les comandes fetes pel client 2103.

```
SELECT clients.empresa, comandes.* 
  FROM clients
  JOIN comandes 
    ON clients.num_clie = comandes.clie
  WHERE comandes.clie = 2103;
```

```
  empresa  | num_comanda |    data    | clie | rep | fabricant | producte | quantitat |  import  
-----------+-------------+------------+------+-----+-----------+----------+-----------+----------
 Acme Mfg. |      112963 | 1989-12-17 | 2103 | 105 | aci       | 41004    |        28 |  3276.00
 Acme Mfg. |      112983 | 1989-12-27 | 2103 | 105 | aci       | 41004    |         6 |   702.00
 Acme Mfg. |      113027 | 1990-01-22 | 2103 | 105 | aci       | 41002    |        54 |  4104.00
 Acme Mfg. |      112987 | 1989-12-31 | 2103 | 105 | aci       | 4100y    |        11 | 27500.00
(4 rows)
```

## Exercici 15

Llista aquelles comandes que el seu import sigui superior a 10000, mostrant el numero de comanda, els imports i les descripcions del producte.

```
SELECT comandes.num_comanda, comandes.import, productes.descripcio 
  FROM comandes
  JOIN productes 
    ON comandes.producte = productes.id_producte 
 WHERE comandes.import > 10000;
```

```
 num_comanda |  import  |   descripcio   
-------------+----------+----------------
      112987 | 27500.00 | Extractor
      112961 | 31500.00 | Frontissa Esq.
      113069 | 31350.00 | Riosta 1-Tm
      112979 | 15000.00 | Muntador
      110036 | 22500.00 | Muntador
      113042 | 22500.00 | Frontissa Dta.
      113045 | 45000.00 | Frontissa Dta.
(7 rows)
```

## Exercici 16

Llista les comandes superiors a 25000, mostrant el nom del client que la va demanar, el venedor associat al client, i l'oficina on el venedor treballa. També cal mostar la descripció del producte.

```
SELECT clients.empresa, rep_vendes.nom, oficines.oficina, productes.descripcio
  FROM comandes
  JOIN clients 
    ON comandes.clie = clients.num_clie
  JOIN rep_vendes 
    ON clients.rep_clie = rep_vendes.num_empl
  JOIN oficines 
    ON oficines.oficina = rep_vendes.oficina_rep
  JOIN productes 
    ON productes.id_producte = comandes.producte
 WHERE comandes.import > 25000;
```

```
     empresa     |     nom     | oficina |   descripcio   
-----------------+-------------+---------+----------------
 Acme Mfg.       | Bill Adams  |      13 | Extractor
 J.P. Sinclair   | Sam Clark   |      11 | Frontissa Esq.
 Chen Associates | Paul Cruz   |      12 | Riosta 1-Tm
 Zetacorp        | Larry Fitch |      21 | Frontissa Dta.
(4 rows)
```

## Exercici 17

Trobar totes les comandes rebudes en els dies en que un nou venedor va ser contractat. Per cada comanda mostrar un cop el número, import i data de la comanda.

```
SELECT DISTINCT comandes.num_comanda, comandes.import, comandes.data
  FROM comandes
  JOIN rep_vendes 
    ON comandes.data = rep_vendes.data_contracte
 WHERE comandes.data = rep_vendes.data_contracte;
```

```
 num_comanda |  import  |    data    
-------------+----------+------------
      112968 |  3978.00 | 1989-10-12
      112979 | 15000.00 | 1989-10-12
(2 rows)
```

## Exercici 18

Mostra el nom, les vendes dels treballadors que tenen assignada una oficina, amb la ciutat i l'objectiu de l'oficina de cada venedor.

```
SELECT rep_vendes.nom, rep_vendes.vendes, oficines.ciutat, oficines.objectiu
  FROM rep_vendes
  JOIN oficines 
    ON oficines.oficina = rep_vendes.oficina_rep;
```

```
      nom      |  vendes   |   ciutat    | objectiu  
---------------+-----------+-------------+-----------
 Bill Adams    | 367911.00 | Atlanta     | 350000.00
 Mary Jones    | 392725.00 | New York    | 575000.00
 Sue Smith     | 474050.00 | Los Angeles | 725000.00
 Sam Clark     | 299912.00 | New York    | 575000.00
 Bob Smith     | 142594.00 | Chicago     | 800000.00
 Dan Roberts   | 305673.00 | Chicago     | 800000.00
 Larry Fitch   | 361865.00 | Los Angeles | 725000.00
 Paul Cruz     | 286775.00 | Chicago     | 800000.00
 Nancy Angelli | 186042.00 | Denver      | 300000.00
(9 rows)
```

## Exercici 19

Llista el nom de tots els venedors i el del seu director en cas de tenir-ne. El camp que conté el nom del treballador s'ha d'identificar amb "empleado" i el camp que conté el nom del director amb "director".

```
SELECT a.nom AS "empleado", b.nom "director"
  FROM rep_vendes a 
  JOIN rep_vendes b 
    ON b.cap = a.num_empl;
```

```
  empleado   |   director    
-------------+---------------
 Bob Smith   | Bill Adams
 Sam Clark   | Mary Jones
 Larry Fitch | Sue Smith
 Sam Clark   | Bob Smith
 Bob Smith   | Dan Roberts
 Dan Roberts | Tom Snyder
 Sam Clark   | Larry Fitch
 Bob Smith   | Paul Cruz
 Larry Fitch | Nancy Angelli
(9 rows)
```

## Exercici 20

Llista totes les combinacions possibles de venedors i ciutats.

```
SELECT rep_vendes.nom, oficines.ciutat
  FROM rep_vendes 
  CROSS JOIN oficines;
```

```
      nom      |   ciutat    
---------------+-------------
 Bill Adams    | Denver
 Mary Jones    | Denver
 Sue Smith     | Denver
 Sam Clark     | Denver
 Bob Smith     | Denver
 Dan Roberts   | Denver
 Tom Snyder    | Denver
 Larry Fitch   | Denver
 Paul Cruz     | Denver
 Nancy Angelli | Denver
 Bill Adams    | New York
 Mary Jones    | New York
 Sue Smith     | New York
 Sam Clark     | New York
 Bob Smith     | New York
 Dan Roberts   | New York
 Tom Snyder    | New York
 Larry Fitch   | New York
 Paul Cruz     | New York
 Nancy Angelli | New York
 Bill Adams    | Chicago
 Mary Jones    | Chicago
 Sue Smith     | Chicago
 Sam Clark     | Chicago
 Bob Smith     | Chicago
 Dan Roberts   | Chicago
 Tom Snyder    | Chicago
 Larry Fitch   | Chicago
 Paul Cruz     | Chicago
 Nancy Angelli | Chicago
 Bill Adams    | Atlanta
 Mary Jones    | Atlanta
 Sue Smith     | Atlanta
 Sam Clark     | Atlanta
 Bob Smith     | Atlanta
 Dan Roberts   | Atlanta
 Tom Snyder    | Atlanta
 Larry Fitch   | Atlanta
 Paul Cruz     | Atlanta
 Nancy Angelli | Atlanta
 Bill Adams    | Los Angeles
 Mary Jones    | Los Angeles
 Sue Smith     | Los Angeles
 Sam Clark     | Los Angeles
 Bob Smith     | Los Angeles
 Dan Roberts   | Los Angeles
 Tom Snyder    | Los Angeles
 Larry Fitch   | Los Angeles
 Paul Cruz     | Los Angeles
 Nancy Angelli | Los Angeles
(50 rows)
```

## Exercici 21

Per a cada venedor, mostrar el nom, les vendes i la ciutat de l'oficina en cas de tenir-ne una d'assignada.

```
SELECT rep_vendes.nom, rep_vendes.vendes, oficines.ciutat
  FROM rep_vendes 
  JOIN oficines 
    ON rep_vendes.oficina_rep = oficines.oficina;
```

```
      nom      |  vendes   |   ciutat    
---------------+-----------+-------------
 Bill Adams    | 367911.00 | Atlanta
 Mary Jones    | 392725.00 | New York
 Sue Smith     | 474050.00 | Los Angeles
 Sam Clark     | 299912.00 | New York
 Bob Smith     | 142594.00 | Chicago
 Dan Roberts   | 305673.00 | Chicago
 Larry Fitch   | 361865.00 | Los Angeles
 Paul Cruz     | 286775.00 | Chicago
 Nancy Angelli | 186042.00 | Denver
(9 rows)
```

## Exercici 22

Mostra les comandes de productes que tenen unes existències inferiors a 10. Llistar el numero de comanda, la data de la comanda, el nom del client que ha fet la comanda, identificador del fabricant i l'identificador de producte de la comanda.

```
SELECT comandes.num_comanda, comandes.data, clients.empresa, productes.id_fabricant, productes.id_producte
  FROM comandes 
  JOIN clients 
    ON clients.num_clie = comandes.clie
  JOIN productes 
    ON comandes.producte = productes.id_producte
 WHERE estoc < 10;
```

```
 num_comanda |    data    |     empresa      | id_fabricant | id_producte 
-------------+------------+------------------+--------------+-------------
      113012 | 1990-01-11 | JCP Inc.         | bic          | 41003
      113013 | 1990-01-14 | Midwest Systems  | bic          | 41003
      112997 | 1990-01-08 | Peter Brothers   | bic          | 41003
      113069 | 1990-03-02 | Chen Associates  | imm          | 775c 
      113048 | 1990-02-10 | Rico Enterprises | imm          | 779c 
      113003 | 1990-01-25 | Holm & Landis    | imm          | 779c 
(6 rows)
```

## Exercici 23

Llista les 5 comandes amb un import superior. Mostrar l'identificador de la comanda, import de la comanda, preu del producte, nom del client, nom del representant de vendes que va efectuar la comanda i ciutat de l'oficina, en cas de tenir oficina assignada.

```
SELECT comandes.num_comanda, comandes.import, productes.preu, clients.empresa, rep_vendes.nom, oficines.ciutat
  FROM comandes 
  JOIN productes 
    ON productes.id_producte = comandes.producte
  JOIN clients 
    ON clients.num_clie = comandes.clie 
  JOIN rep_vendes 
    ON rep_vendes.num_empl = comandes.rep
  JOIN oficines 
    ON oficines.oficina = rep_vendes.oficina_rep
  ORDER BY import DESC limit 5;
```

```
 num_comanda |  import  |  preu   |     empresa     |      nom      |   ciutat    
-------------+----------+---------+-----------------+---------------+-------------
      113045 | 45000.00 | 4500.00 | Zetacorp        | Larry Fitch   | Los Angeles
      112961 | 31500.00 | 4500.00 | J.P. Sinclair   | Sam Clark     | New York
      113069 | 31350.00 | 1425.00 | Chen Associates | Nancy Angelli | Denver
      112987 | 27500.00 | 2750.00 | Acme Mfg.       | Bill Adams    | Atlanta
      113042 | 22500.00 | 4500.00 | Ian & Schmidt   | Dan Roberts   | Chicago
(5 rows)
```

## Exercici 24

Llista les comandes que han estat preses per un representant de vendes que no és l'actual representant de vendes del client pel que s'ha realitzat la comanda. Mostrar el número de comanda, el nom del client, el nom de l'actual representant de vendes del client com a "rep_cliente" i el nom del representant de vendes que va realitzar la comanda com a "rep_pedido".

```
SELECT c.num_comanda, c2.empresa, a.nom AS "rep_cliente", b.nom AS "rep_pedido" 
    FROM rep_vendes b 
    JOIN comandes c 
      ON c.rep = b.num_empl 
    JOIN clients c2 
      ON c2.num_clie = c.clie 
    JOIN rep_vendes a 
      ON a.num_empl = c2.rep_clie 
   WHERE c2.rep_clie != c.rep;
```

```
 num_comanda |     empresa     | rep_cliente |  rep_pedido   
-------------+-----------------+-------------+---------------
      113012 | JCP Inc.        | Paul Cruz   | Bill Adams
      113024 | Orion Corp      | Sue Smith   | Larry Fitch
      113069 | Chen Associates | Paul Cruz   | Nancy Angelli
      113055 | Holm & Landis   | Mary Jones  | Dan Roberts
      113042 | Ian & Schmidt   | Bob Smith   | Dan Roberts
(5 rows)
```

## Exercici 25

Llista les comandes amb un import superior a 5000 i també aquelles comandes realitzades per un client amb un crèdit inferior a 30000. Mostrar l'identificador de la comanda, el nom del client i el nom del representant de vendes que va prendre la comanda.

```
SELECT comandes.num_comanda, clients.empresa, rep_vendes.nom
  FROM comandes
  JOIN clients 
    ON clients.num_clie = comandes.clie
  JOIN rep_vendes 
    ON rep_vendes.num_empl = clients.rep_clie
 WHERE comandes.import > 5000 
   AND clients.limit_credit < 30000;
```

```
 num_comanda |     empresa     |    nom    
-------------+-----------------+-----------
      113024 | Orion Corp      | Sue Smith
      112979 | Orion Corp      | Sue Smith
      113069 | Chen Associates | Paul Cruz
      113042 | Ian & Schmidt   | Bob Smith
(4 rows)
```
