# Subconsultes

## Exercici 0

Torna a fer l'exercici 4 de les diapositives de teoria de Subconsultes, però
ara fent servir GROUP BY i la funció de group STRING_AGG. Recordem que la
subconsulta era:

_Mostreu els empleats que treballen a la mateixa oficina i que tenen el mateix
càrrec._
  
```
SELECT COUNT(num_empl), STRING_AGG(nom, ', '), oficina_rep, carrec
  FROM rep_vendes 
 GROUP BY oficina_rep, carrec
  HAVING COUNT(num_empl) > 1;
```

## Exercici 1

Llista els venedors que tinguin una quota igual o inferior a l'objectiu de
l'oficina de vendes d'Atlanta.

```
SELECT nom 
  FROM rep_vendes 
 WHERE quota <= 
                ( SELECT objectiu 
                    FROM oficines 
                   WHERE ciutat 
                    LIKE 'Atlanta');

```

```
      nom      
---------------
 Bill Adams
 Mary Jones
 Sue Smith
 Sam Clark
 Bob Smith
 Dan Roberts
 Larry Fitch
 Paul Cruz
 Nancy Angelli
(9 rows)
```

## Exercici 2

Tots els clients, identificador i nom de l'empresa, que han estat atesos per
(que han fet comanda amb) Bill Adams.

```
SELECT num_clie, empresa 
  FROM clients 
 WHERE num_clie IN
                    (SELECT clie 
                       FROM comandes 
                      WHERE rep = 105)
```

```
 num_clie |  empresa  
----------+-----------
     2111 | JCP Inc.
     2103 | Acme Mfg.
(2 rows)
```

## Exercici 3

Venedors amb quotes que siguin iguals o superiors a l'objectiu de llur oficina
de vendes.

```
SELECT nom 
  FROM rep_vendes 
  JOIN (SELECT objectiu, oficina FROM oficines ) AS b 
    ON rep_vendes.oficina_rep = b.oficina 
 WHERE quota >= objectiu;


 SELECT nom 
   FROM rep_vendes 
  WHERE quota >= 
  (SELECT objectiu FROM oficines WHERE oficina_rep = oficina);

```

```
      nom      
---------------
 Bill Adams
 Nancy Angelli
(2 rows)
```

## Exercici 4

Mostrar l'identificador de l'oficina i la ciutat de les oficines on l'objectiu
de vendes de l'oficina excedeix la suma de quotes dels venedors d'aquella
oficina.

```
SELECT oficina, ciutat 
  FROM oficines 
 WHERE objectiu > 
      (SELECT SUM(quota) 
         FROM rep_vendes 
        WHERE oficina_rep = oficina);


```
```
 oficina |   ciutat    
---------+-------------
      12 | Chicago
      21 | Los Angeles
(2 rows)
```

## Exercici 5

Llista dels productes del fabricant amb identificador "aci" que les existències superen les existències del producte amb identificador de producte "41004" i identificador de fabricant "aci".

```
SELECT descripcio 
  FROM productes 
 WHERE id_fabricant 
  LIKE 'aci' 
   AND estoc > 
        (SELECT estoc 
           FROM productes 
          WHERE id_fabricant LIKE 'aci' AND id_producte LIKE '41004');
```

```
   descripcio    
-----------------
 Article Tipus 3
 Article Tipus 1
 Article Tipus 2
(3 rows)
```

## Exercici 6

Llistar els venedors que han acceptat una comanda que representa més del 10% de
la seva quota.

```
SELECT nom 
  FROM rep_vendes 
 WHERE quota * 0.10 < ANY 
      (SELECT import 
         FROM comandes 
        WHERE rep = num_empl);

```

```
      nom      
---------------
 Sam Clark
 Larry Fitch
 Nancy Angelli
(3 rows)
```

## Exercici 7

Llistar el nom i l'edat de totes les persones de l'equip de vendes que no dirigeixen una oficina.

```
SELECT nom, edat 
  FROM rep_vendes 
 WHERE num_empl != ALL 
        (SELECT director 
           FROM oficines);
```

```
      nom      | edat 
---------------+------
 Mary Jones    |   31
 Sue Smith     |   48
 Dan Roberts   |   45
 Tom Snyder    |   41
 Paul Cruz     |   29
 Nancy Angelli |   49
(6 rows)
```

## Exercici 8

Llistar aquelles oficines, i els seus objectius, els venedors de les quals
tenen unes vendes que superen el 50% de l'objectiu de l'oficina.

```
SELECT ciutat, objectiu 
  FROM oficines 
 WHERE objectiu * 0.50 < ALL 
      (SELECT vendes 
         FROM rep_vendes 
        WHERE oficina = oficina_rep);
```

```
  ciutat  | objectiu  
----------+-----------
 Denver   | 300000.00
 New York | 575000.00
 Atlanta  | 350000.00
(3 rows)
```

## Exercici 9

Llistar aquells clients els representants de vendes dels quals estàn assignats
a oficines de la regió Est.

```
SELECT empresa 
  FROM clients 
 WHERE rep_clie = ANY 
      (SELECT num_empl 
         FROM rep_vendes 
        WHERE oficina_rep = ANY 
            (SELECT oficina 
               FROM oficines 
              WHERE regio = 'Est'));
```

```
     empresa     
-----------------
 JCP Inc.
 First Corp.
 Acme Mfg.
 Smithson Corp.
 Jones Mfg.
 QMA Assoc.
 Holm & Landis
 J.P. Sinclair
 Three-Way Lines
 Solomon Inc.
 Ian & Schmidt
 Chen Associates
 AAA Investments
(13 rows)
```

## Exercici 10

Llistar els venedors que treballen en oficines que superen el seu objectiu.

```
SELECT nom 
  FROM rep_vendes 
 WHERE oficina_rep = ANY 
        (SELECT oficina 
           FROM oficines 
          WHERE objectiu < vendes 
            AND oficina = oficina_rep);
```

```
     nom     
-------------
 Bill Adams
 Mary Jones
 Sue Smith
 Sam Clark
 Larry Fitch
(5 rows)
```

## Exercici 11

Llistar els venedors que treballen en oficines que superen el seu objectiu.
Mostrar també les següents dades de l'oficina: ciutat i la diferència entre les
vendes i l'objectiu. Ordenar el resultat per aquest últim valor. Proposa dues
sentències SQL, una amb subconsultes i una sense.

```


```

```


```

## Exercici 12

Llista els venedors que no treballen en oficines dirigides per Larry Fitch, o
que no treballen a cap oficina. Sense usar consultes multitaula.

```
SELECT nom 
  FROM rep_vendes 
 WHERE oficina_rep IS NULL OR oficina_rep <> ALL 
        (SELECT oficina 
           FROM oficines 
          WHERE director = 
                (SELECT num_empl 
                   FROM rep_vendes 
                  WHERE nom = 'Larry Fitch'));
```

```
     nom     
-------------
 Bill Adams
 Mary Jones
 Sam Clark
 Bob Smith
 Dan Roberts
 Tom Snyder
 Paul Cruz
(7 rows)
```

## Exercici 13

Llista els venedors que no treballen en oficines dirigides per Larry Fitch, o
que no treballen a cap oficina. Mostrant també la ciutat de l'oficina on
treballa l'empleat i l'identificador del cap de la oficina. Proposa dues
sentències SQL, una amb subconsultes i una sense.

```


```

```


```

## Exercici 14

Llista tots els clients que han realitzat comandes del productes de la família
ACI Widgets entre gener i juny del 1990. Els productes de la famíla ACI Widgets
són aquells que tenen identificador de fabricant "aci" i que l'identificador
del producte comença per "4100".

```
SELECT empresa
  FROM comandes 
  JOIN clients
    ON clie = num_clie
  JOIN (SELECT id_producte, id_fabricant 
          FROM productes 
         WHERE id_fabricant = 'aci' 
           AND id_producte LIKE '4100%') AS a 
    ON (a.id_producte, a.id_fabricant) = (producte, fabricant)
 WHERE data BETWEEN '1990-01-01' AND '1990-06-01';
```

```
      empresa      
-------------------
 JCP Inc.
 Ace International
 Acme Mfg.
 Holm & Landis
 JCP Inc.
(5 rows)
```

## Exercici 15

Llista els clients que no tenen cap comanda.

```
SELECT num_clie,empresa 
  FROM clients 
 WHERE num_clie NOT IN 
            (SELECT clie 
               FROM comandes);

```

```
 num_clie |     empresa     
----------+-----------------
     2123 | Carter & Sons
     2115 | Smithson Corp.
     2121 | QMA Assoc.
     2122 | Three-Way Lines
     2119 | Solomon Inc.
     2105 | AAA Investments
(6 rows)
```

## Exercici 16

Llista els clients que tenen assignat el venedor que porta més temps contractat.

```
SELECT empresa 
  FROM clients 
  JOIN rep_vendes 
    ON rep_clie = num_empl
 WHERE data_contracte = 
          (SELECT MIN(data_contracte) 
             FROM rep_vendes);
```

```
     empresa     
-----------------
 First Corp.
 Smithson Corp.
 AAA Investments
(3 rows)
```

## Exercici 17

Llista els clients assignats a Sue Smith que no han fet cap comanda amb un import superior a 30000. Proposa una sentència SQL sense usar multitaula i una altre en que s'usi multitaula i subconsultes.

```


```

```


```

## Exercici 18

Llista l'identificador i el nom dels caps d'empleats que tenen més de 40 anys i que dirigeixen un venedor que té unes vendes superiors a la seva pròpia quota.

```
SELECT DISTINCT a.nom
  FROM rep_vendes AS a 
       JOIN (SELECT num_empl, cap 
               FROM rep_vendes 
              WHERE vendes > quota) AS b 
       ON b.cap = a.num_empl;
```

```
     nom     
-------------
 Larry Fitch
 Bob Smith
 Sam Clark
(3 rows)
```

## Exercici 19

Llista d'oficines on hi hagi algun venedor tal que la seva quota representi més
del 50% de l'objectiu de l'oficina

```
SELECT ciutat
  FROM oficines 
 WHERE objectiu * 0.50 < ANY 
          (SELECT quota 
             FROM rep_vendes 
            WHERE oficina_rep = oficina);

```

```
  ciutat  
----------
 Denver
 New York
 Atlanta
(3 rows)
```

## Exercici 20

Llista d'oficines on tots els venedors tinguin la seva quota superior al 55% de
l'objectiu de l'oficina.

```
SELECT ciutat
  FROM oficines 
 WHERE objectiu * 0.55 < ALL 
          (SELECT quota 
             FROM rep_vendes 
            WHERE oficina_rep = oficina);
```

```
 ciutat  
---------
 Denver
 Atlanta
(2 rows)
```

## Exercici 21

Transforma el següent JOIN a una comanda amb subconsultes:

```
SELECT num_comanda, import, clie, num_clie, limit_credit
  FROM comandes JOIN clients
    ON clie = num_clie;
```

```
SELECT num_comanda, import, clie, num_clie, limit_credit
  FROM comandes 
  JOIN (SELECT num_clie, limit_credit 
          FROM clients ) AS clie 
    ON clie.num_clie = clie;
```

## Exercici 22

Transforma el següent JOIN a una comanda amb subconsultes:

```
SELECT empl.nom, empl.quota, dir.nom, dir.quota
  FROM rep_vendes AS empl JOIN rep_vendes AS cap
    ON empl.cap = cap.num_empl
 WHERE empl.quota > cap.quota;
```

```
SELECT empl.nom, empl.quota, dir.nom, dir.quota
  FROM rep_vendes AS empl 
  JOIN (SELECT num_empl, nom, quota 
          FROM rep_vendes ) AS dir 
    ON dir.num_empl = cap 
 WHERE empl.quota > dir.quota;
```

## Exercici 23

Transforma la següent consulta amb un ANY a una consulta amb un EXISTS i també
en una altre consulta amb un ALL:

```
SELECT oficina
  FROM oficines
 WHERE vendes * 0.8 < ANY 
       (SELECT vendes
          FROM rep_vendes
         WHERE oficina_rep = oficina);
```
EXISTS

```
```
ALL
```
SELECT oficina 
  FROM oficines 
 WHERE oficina <> ALL 
          (SELECT oficina 
             FROM oficines 
            WHERE vendes * 0.80 > ANY 
                  (SELECT vendes 
                     FROM rep_vendes 
                    WHERE oficina_rep = oficina));
```
## Exercici 24

Transforma la següent consulta amb un ALL a una consutla amb un EXISTS i també
en una altre consulta amb un ANY:

```
SELECT num_clie
  FROM clients
 WHERE limit_credit < ALL
       (SELECT import
          FROM comandes
         WHERE num_clie = clie);
```

ANY
```
SELECT num_clie 
  FROM clients 
 WHERE limit_credit <> ANY 
      (SELECT import 
         FROM comandes 
        WHERE limit_credit < ALL 
              (SELECT import 
                 FROM comandes 
                WHERE num_clie = clie));
```
                                 
## Exercici 25

Transforma la següent consulta amb un EXISTS a una consulta amb un ALL i també
a una altre consulta amb un ANY:

```
SELECT num_clie, empresa
  FROM clients
 WHERE EXISTS
       (SELECT *
          FROM rep_vendes
         WHERE rep_clie = num_empl
           AND edad BETWEEN 40 AND 50);
```

ALL
```
SELECT num_clie, empresa 
  FROM clients 
 WHERE rep_clie <> ALL 
      (SELECT num_empl 
         FROM rep_vendes 
        WHERE NOT EXISTS 
              (SELECT edat 
                 FROM rep_vendes 
                WHERE rep_clie = num_empl 
                  AND edat BETWEEN 40 AND 50));
```
## Exercici 26

Transforma la següent consulta amb subconsultes a una consulta sense
subconsultes.

```
SELECT *
  FROM productes
 WHERE id_fabricant IN 
       (SELECT fabricant
          FROM comandes
         WHERE quantitat > 30)
   AND id_producte IN 
       (SELECT producte
          FROM comandes
         WHERE quantitat > 30);
```	 

## Exercici 27

Transforma la següent consulta amb subconsultes a una consulta sense
subconsultes.

```
SELECT num_empl, nom
  FROM rep_vendes
 WHERE num_empl = ANY 
       (SELECT rep_clie
          FROM clients
         WHERE empresa LIKE '%Inc.');
```

## Exercici 28

Transforma la següent consulta amb un IN a una consulta amb un EXISTS i també a
una altre consulta amb un ALL.

```
SELECT num_empl, nom
  FROM rep_vendes
 WHERE num_empl IN
       (SELECT cap
          FROM rep_vendes);
```

## Exercici 29

Modifica la següent consulta perquè mostri la ciutat de l'oficina, proposa una
consulta simplificada.

```
SELECT num_comanda
  FROM comandes
 WHERE rep IN
       (SELECT num_empl
	      FROM rep_vendes
		 WHERE vendes >
		       (SELECT avg(vendes)
			      FROM rep_vendes)
           AND oficina_rep IN
		       (SELECT oficina
                  FROM oficines
                 WHERE regio ILIKE 'est') );
```

## Exercici 30

Transforma la següent consulta amb subconsultes a una consulta amb les mínimes
subconsultes possibles.

```
SELECT num_clie, empresa,
       (SELECT nom
          FROM rep_vendes
         WHERE rep_clie = num_empl) AS rep_nom
  FROM clients
 WHERE rep_clie = ANY
       (SELECT num_empl
          FROM rep_vendes
         WHERE vendes >
               (SELECT MAX(quota)
                  FROM rep_vendes));
```

