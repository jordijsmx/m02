# Subconsultes

## Exercici 0

Torna a fer l'exercici 4 de les diapositives de teoria de Subconsultes, però ara fent servir GROUP BY i la funció de group STRING_AGG. Recordem que la subconsulta era:

_Mostreu els empleats que treballen a la mateixa oficina i que tenen el mateix càrrec._
```
  SELECT COUNT(num_empl), STRING_AGG(nom, ', '), oficina_rep, carrec
    FROM rep_vendes 
GROUP BY oficina_rep, carrec
  HAVING COUNT(num_empl) > 1;

 count |       string_agg       | oficina_rep |       carrec        
-------+------------------------+-------------+---------------------
     2 | Dan Roberts, Paul Cruz |          12 | Representant Vendes
(1 row)

```
## Exercici 1

Llista els venedors que tinguin una quota igual o inferior a l'objectiu de l'oficina de vendes d'Atlanta.
```
  SELECT num_empl, nom, quota
    FROM rep_vendes 
   WHERE quota <= (SELECT objectiu 
                     FROM oficines 
                    WHERE ciutat = 'Atlanta')
ORDER BY quota;

 num_empl |      nom      |   quota   
----------+---------------+-----------
      104 | Bob Smith     | 200000.00
      106 | Sam Clark     | 275000.00
      103 | Paul Cruz     | 275000.00
      107 | Nancy Angelli | 300000.00
      109 | Mary Jones    | 300000.00
      101 | Dan Roberts   | 300000.00
      108 | Larry Fitch   | 350000.00
      102 | Sue Smith     | 350000.00
      105 | Bill Adams    | 350000.00
(9 rows)

```
## Exercici 2

Tots els clients, identificador i nom de l'empresa, que han estat atesos per (que han fet comanda amb) Bill Adams.
```
SELECT DISTINCT(clie), empresa, rep
  FROM comandes 
       JOIN clients 
       ON num_clie = clie
 WHERE rep = (SELECT num_empl 
                FROM rep_vendes 
               WHERE nom = 'Bill Adams');
               
 clie |  empresa  | rep 
------+-----------+-----
 2103 | Acme Mfg. | 105
 2111 | JCP Inc.  | 105
(2 rows)

```
## Exercici 3

Venedors amb quotes que siguin iguals o superiors a l'objectiu de llur oficina de vendes.
```
SELECT num_empl, nom, quota, o_ofi.objectiu, oficina_rep
  FROM rep_vendes 
       JOIN (SELECT objectiu, oficina 
             FROM oficines) AS o_ofi 
       ON o_ofi.oficina = oficina_rep
 WHERE quota >= o_ofi.objectiu;       
                    
 num_empl |      nom      |   quota   | objectiu  | oficina_rep 
----------+---------------+-----------+-----------+-------------
      105 | Bill Adams    | 350000.00 | 350000.00 |          13
      107 | Nancy Angelli | 300000.00 | 300000.00 |          22
(2 rows)

```
## Exercici 4

Mostrar l'identificador de l'oficina i la ciutat de les oficines on l'objectiu de vendes de l'oficina excedeix la suma de quotes dels venedors d'aquella oficina.
```
SELECT oficina, ciutat, objectiu, ven.suma_quota
  FROM oficines 
       JOIN ( SELECT SUM(quota) AS suma_quota, oficina_rep 
                FROM rep_vendes 
            GROUP BY oficina_rep) AS ven 
       ON oficina = ven.oficina_rep 
 WHERE objectiu > ven.suma_quota;

 oficina |   ciutat    | objectiu  | suma_quota 
---------+-------------+-----------+------------
      12 | Chicago     | 800000.00 |  775000.00
      21 | Los Angeles | 725000.00 |  700000.00
(2 rows)

```
## Exercici 5

Llista dels productes del fabricant amb identificador "aci" que les existències superen les existències del producte amb identificador de producte "41004" i identificador de fabricant "aci".
```
SELECT id_producte, id_fabricant, estoc
  FROM productes 
 WHERE estoc > (SELECT estoc 
                  FROM productes 
                 WHERE id_producte = '41004' 
                       AND id_fabricant = 'aci') 
       AND id_fabricant = 'aci';

 id_producte | id_fabricant | estoc 
-------------+--------------+-------
 41003       | aci          |   207
 41001       | aci          |   277
 41002       | aci          |   167
(3 rows)

```
## Exercici 6

Llistar els venedors que han acceptat una comanda que representa més del 10% de la seva quota.
```
SELECT rep, nom, quota, import, ven.percentatge
  FROM comandes 
       JOIN (SELECT quota * 0.1 AS percentatge, num_empl, nom, quota 
               FROM rep_vendes) AS ven 
       ON ven.num_empl = rep
 WHERE import > ven.percentatge;

 rep |      nom      |   quota   |  import  | percentatge 
-----+---------------+-----------+----------+-------------
 106 | Sam Clark     | 275000.00 | 31500.00 |   27500.000
 108 | Larry Fitch   | 350000.00 | 45000.00 |   35000.000
 107 | Nancy Angelli | 300000.00 | 31350.00 |   30000.000
(3 rows)

```
## Exercici 7

Llistar el nom i l'edat de totes les persones de l'equip de vendes que no dirigeixen una oficina.
```
SELECT nom, edat
  FROM rep_vendes 
 WHERE num_empl <> ALL (SELECT director 
                          FROM oficines);

      nom      | edat 
---------------+------
 Mary Jones    |   31
 Sue Smith     |   48
 Dan Roberts   |   45
 Tom Snyder    |   41
 Paul Cruz     |   29
 Nancy Angelli |   49
(6 rows)

```
## Exercici 8

Llistar aquelles oficines, i els seus objectius, els venedors de les quals tenen unes vendes que superen el 50% de l'objectiu de l'oficina.
```
SELECT oficina, objectiu, ven.percentatge
  FROM oficines 
       JOIN ( SELECT SUM(vendes) * 0.5 AS percentatge, oficina_rep 
                FROM rep_vendes 
            GROUP BY oficina_rep) AS ven
       ON ven.oficina_rep = oficina 
 WHERE objectiu > ven.percentatge;

 oficina | objectiu  | percentatge 
---------+-----------+-------------
      22 | 300000.00 |   93021.000
      11 | 575000.00 |  346318.500
      12 | 800000.00 |  367521.000
      13 | 350000.00 |  183955.500
      21 | 725000.00 |  417957.500
(5 rows)

```
## Exercici 9

Llistar aquells clients els representants de vendes dels quals estàn assignats a oficines de la regió Est.
```
  SELECT num_clie, empresa, rep_clie, o_ven.regio
    FROM clients 
         JOIN (SELECT num_empl, regio 
                 FROM rep_vendes 
                      JOIN oficines 
                      ON oficina = oficina_rep 
                WHERE regio = 'Est') AS o_ven 
         ON o_ven.num_empl = rep_clie 
ORDER BY rep_clie;
    
 num_clie |     empresa     | rep_clie | regio 
----------+-----------------+----------+-------
     2115 | Smithson Corp.  |      101 | Est
     2102 | First Corp.     |      101 | Est
     2105 | AAA Investments |      101 | Est
     2111 | JCP Inc.        |      103 | Est
     2121 | QMA Assoc.      |      103 | Est
     2109 | Chen Associates |      103 | Est
     2113 | Ian & Schmidt   |      104 | Est
     2103 | Acme Mfg.       |      105 | Est
     2122 | Three-Way Lines |      105 | Est
     2101 | Jones Mfg.      |      106 | Est
     2117 | J.P. Sinclair   |      106 | Est
     2119 | Solomon Inc.    |      109 | Est
     2108 | Holm & Landis   |      109 | Est
(13 rows)

```
## Exercici 10

Llistar els venedors que treballen en oficines que superen el seu objectiu.
```
SELECT rep.num_empl, rep.nom, rep.oficina_rep, s_ven.suma_vendes
  FROM rep_vendes AS rep 
       JOIN (SELECT objectiu, oficina 
               FROM oficines) AS ofi 
       ON ofi.oficina = rep.oficina_rep 
       JOIN ( SELECT SUM(vendes) AS suma_vendes, oficina_rep 
                FROM rep_vendes 
            GROUP BY oficina_rep) AS s_ven 
       ON s_ven.oficina_rep = rep.oficina_rep
 WHERE ofi.objectiu < s_ven.suma_vendes;
 
 num_empl |     nom     | oficina_rep | suma_vendes 
----------+-------------+-------------+-------------
      105 | Bill Adams  |          13 |   367911.00
      109 | Mary Jones  |          11 |   692637.00
      102 | Sue Smith   |          21 |   835915.00
      106 | Sam Clark   |          11 |   692637.00
      108 | Larry Fitch |          21 |   835915.00
(5 rows)

```
## Exercici 11

Llistar els venedors que treballen en oficines que superen el seu objectiu. Mostrar també les següents dades de l'oficina: ciutat i la diferència entre les vendes i l'objectiu. Ordenar el resultat per aquest últim valor. Proposa dues sentències SQL, una amb subconsultes i una sense.
```
  SELECT rep.num_empl, rep.nom, rep.oficina_rep, ofi.ciutat, s_ven.suma_vendes - ofi.objectiu AS diferencia 
    FROM rep_vendes AS rep 
         JOIN (SELECT objectiu, oficina, ciutat 
               FROM oficines) AS ofi 
         ON ofi.oficina = rep.oficina_rep 
         JOIN ( SELECT SUM(vendes) AS suma_vendes, oficina_rep 
                  FROM rep_vendes 
              GROUP BY oficina_rep) AS s_ven 
          ON s_ven.oficina_rep = rep.oficina_rep
    WHERE ofi.objectiu < s_ven.suma_vendes
 ORDER BY diferencia;   

 num_empl |     nom     | oficina_rep |   ciutat    | diferencia 
----------+-------------+-------------+-------------+------------
      105 | Bill Adams  |          13 | Atlanta     |   17911.00
      102 | Sue Smith   |          21 | Los Angeles |  110915.00
      108 | Larry Fitch |          21 | Los Angeles |  110915.00
      109 | Mary Jones  |          11 | New York    |  117637.00
      106 | Sam Clark   |          11 | New York    |  117637.00
(5 rows)

```
```
  SELECT num_empl, nom, oficina_rep, ciutat, (oficines.vendes - oficines.objectiu) AS diferencia
    FROM rep_vendes 
         JOIN oficines 
         ON oficina_rep = oficina 
   WHERE oficines.objectiu < oficines.vendes
GROUP BY oficina_rep, num_empl, oficina
ORDER BY diferencia;

 num_empl |     nom     | oficina_rep |   ciutat    | diferencia 
----------+-------------+-------------+-------------+------------
      105 | Bill Adams  |          13 | Atlanta     |   17911.00
      102 | Sue Smith   |          21 | Los Angeles |  110915.00
      108 | Larry Fitch |          21 | Los Angeles |  110915.00
      109 | Mary Jones  |          11 | New York    |  117637.00
      106 | Sam Clark   |          11 | New York    |  117637.00
(5 rows)

```
## Exercici 12

Llista els venedors que no treballen en oficines dirigides per Larry Fitch, o que no treballen a cap oficina. Sense usar consultes multitaula.
```
SELECT num_empl, nom, oficina_rep 
       FROM rep_vendes 
            LEFT JOIN oficines 
            ON oficina = oficina_rep 
      WHERE director <> (SELECT num_empl 
                           FROM rep_vendes 
                          WHERE nom = 'Larry Fitch') 
            OR oficina_rep IS NULL;
         
 num_empl |     nom     | oficina_rep 
----------+-------------+-------------
      105 | Bill Adams  |          13
      109 | Mary Jones  |          11
      106 | Sam Clark   |          11
      104 | Bob Smith   |          12
      101 | Dan Roberts |          12
      110 | Tom Snyder  |            
      103 | Paul Cruz   |          12
(7 rows)
```
## Exercici 13

Llista els venedors que no treballen en oficines dirigides per Larry Fitch, o que no treballen a cap oficina. Mostrant també la ciutat de l'oficina on
treballa l'empleat i l'identificador del cap de la oficina. Proposa dues sentències SQL, una amb subconsultes i una sense.
```
SELECT num_empl, nom, oficina_rep, director, ciutat 
       FROM rep_vendes 
            LEFT JOIN oficines 
            ON oficina = oficina_rep 
      WHERE director <> (SELECT num_empl 
                           FROM rep_vendes 
                          WHERE nom = 'Larry Fitch') 
         OR oficina_rep IS NULL;
         
 num_empl |     nom     | oficina_rep | director |  ciutat  
----------+-------------+-------------+----------+----------
      105 | Bill Adams  |          13 |      105 | Atlanta
      109 | Mary Jones  |          11 |      106 | New York
      106 | Sam Clark   |          11 |      106 | New York
      104 | Bob Smith   |          12 |      104 | Chicago
      101 | Dan Roberts |          12 |      104 | Chicago
      110 | Tom Snyder  |             |          | 
      103 | Paul Cruz   |          12 |      104 | Chicago
(7 rows)

```
```
SELECT ven.num_empl, ven.nom, ven.oficina_rep, director, ciutat
  FROM rep_vendes AS ven
       LEFT JOIN oficines 
       ON oficina_rep = oficina
       LEFT JOIN rep_vendes AS dir
       ON director = dir.num_empl
 WHERE dir.nom <> 'Larry Fitch' 
       OR ven.oficina_rep IS NULL;
    
 num_empl |     nom     | oficina_rep | director |  ciutat  
----------+-------------+-------------+----------+----------
      105 | Bill Adams  |          13 |      105 | Atlanta
      109 | Mary Jones  |          11 |      106 | New York
      106 | Sam Clark   |          11 |      106 | New York
      104 | Bob Smith   |          12 |      104 | Chicago
      101 | Dan Roberts |          12 |      104 | Chicago
      110 | Tom Snyder  |             |          | 
      103 | Paul Cruz   |          12 |      104 | Chicago
(7 rows)

```
## Exercici 14

Llista tots els clients que han realitzat comandes del productes de la família ACI Widgets entre gener i juny del 1990. Els productes de la famíla ACI Widgets són aquells que tenen identificador de fabricant "aci" i que l'identificador del producte comença per "4100".
```
SELECT DISTINCT clie, empresa, data
  FROM comandes 
       JOIN clients
       ON clie = num_clie
       JOIN (SELECT id_producte, id_fabricant 
               FROM productes 
              WHERE id_fabricant = 'aci' 
                    AND id_producte LIKE '4100%') AS productes 
       ON productes.id_producte = producte 
          AND productes.id_fabricant = fabricant
 WHERE data BETWEEN '1990-01-01' AND '1990-06-01';

 clie |      empresa      |    data    
------+-------------------+------------
 2103 | Acme Mfg.         | 1990-01-22
 2107 | Ace International | 1990-01-30
 2108 | Holm & Landis     | 1990-02-15
 2111 | JCP Inc.          | 1990-01-11
 2111 | JCP Inc.          | 1990-02-18
(5 rows)

```
## Exercici 15

Llista els clients que no tenen cap comanda.
```
SELECT num_clie, empresa
  FROM clients          
 WHERE num_clie NOT IN (SELECT num_clie
                          FROM clients 
                               JOIN comandes 
                               ON clie = num_clie);

 num_clie |     empresa     
----------+-----------------
     2123 | Carter & Sons
     2115 | Smithson Corp.
     2121 | QMA Assoc.
     2122 | Three-Way Lines
     2119 | Solomon Inc.
     2105 | AAA Investments
(6 rows)

```
## Exercici 16

Llista els clients que tenen assignat el venedor que porta més temps contractat.
```
SELECT num_clie, empresa, nom
  FROM clients 
       JOIN rep_vendes 
       ON rep_clie = rep.num_empl
 WHERE rep.data_contracte = (SELECT MIN(data_contracte) 
                               FROM rep_vendes);
                               
 num_clie |     empresa     |     nom     
----------+-----------------+-------------
     2102 | First Corp.     | Dan Roberts
     2115 | Smithson Corp.  | Dan Roberts
     2105 | AAA Investments | Dan Roberts
(3 rows)

```
## Exercici 17

Llista els clients assignats a Sue Smith que no han fet cap comanda amb un import superior a 30000. Proposa una sentència SQL sense usar multitaula i una altre en que s'usi multitaula i subconsultes.
```
SELECT DISTINCT clie, empresa, nom
  FROM comandes 
       JOIN clients 
       ON num_clie = clie
       JOIN rep_vendes 
       ON rep_clie = num_empl
 WHERE import < 30000 
       AND rep_clie = (SELECT num_empl 
                         FROM rep_vendes 
                        WHERE nom = 'Sue Smith');

 clie |     empresa      |    nom    
------+------------------+-----------
 2106 | Fred Lewis Corp. | Sue Smith
 2114 | Orion Corp       | Sue Smith
 2120 | Rico Enterprises | Sue Smith
(3 rows)

```
Primer hem de saber el numero de empleat de Sue Smith, per tant hem de fer aquesta consulta apart.
```
SELECT num_empl 
  FROM rep_vendes 
 WHERE nom = 'Sue Smith'
 
  num_empl 
----------
      102
(1 row)

```
Una vegada tenim el seu numero d'empleat, fem la colsulta.
```
SELECT DISTINCT rep, clie
  FROM comandes 
 WHERE import < 30000 AND rep = 102;
 
 rep | clie 
-----+------
 102 | 2106
 102 | 2114
 102 | 2120
(3 rows)

```
O també..
```
 SELECT DISTINCT rep, clie
  FROM comandes 
 WHERE import < 30000 AND rep = (SELECT num_empl 
                                   FROM rep_vendes 
                                  WHERE nom = 'Sue Smith');
                                  
 rep | clie 
-----+------
 102 | 2106
 102 | 2114
 102 | 2120
(3 rows)
```
## Exercici 18

Llista l'identificador i el nom dels caps d'empleats que tenen més de 40 anys i que dirigeixen un venedor que té unes vendes superiors a la seva pròpia quota.
```
SELECT DISTINCT cap.num_empl, cap.nom
  FROM rep_vendes AS cap 
       JOIN (SELECT num_empl, cap 
               FROM rep_vendes 
              WHERE vendes > quota) AS ven 
       ON ven.cap = cap.num_empl;
       
 num_empl |     nom     
----------+-------------
      106 | Sam Clark
      104 | Bob Smith
      108 | Larry Fitch
(3 rows)

```
## Exercici 19

Llista d'oficines on hi hagi algun venedor tal que la seva quota representi més del 50% de l'objectiu de l'oficina
```
 SELECT oficina, ciutat
   FROM oficines 
        JOIN ( SELECT quota, oficina_rep 
                 FROM rep_vendes 
             GROUP BY oficina_rep, quota) AS ven 
        ON ven.oficina_rep = oficina
  WHERE objectiu * 0.5 < ven.quota;

 oficina |  ciutat  
---------+----------
      11 | New York
      22 | Denver
      13 | Atlanta
(3 rows)

```
```
SELECT oficina, ciutat
  FROM oficines 
 WHERE objectiu * 0.5 < ANY (SELECT quota 
                               FROM rep_vendes 
                              WHERE oficina_rep = oficina);
                              
 oficina |  ciutat  
---------+----------
      22 | Denver
      11 | New York
      13 | Atlanta
(3 rows)

```
## Exercici 20

Llista d'oficines on tots els venedors tinguin la seva quota superior al 55% de l'objectiu de l'oficina.
```
SELECT oficina, ciutat
  FROM oficines 
 WHERE objectiu * 0.55 < ALL (SELECT quota 
                                FROM rep_vendes 
                               WHERE oficina_rep = oficina);

 oficina | ciutat  
---------+---------
      22 | Denver
      13 | Atlanta
(2 rows)

```
## Exercici 21

Transforma el següent JOIN a una comanda amb subconsultes:

```
SELECT num_comanda, import, clie, num_clie, limit_credit
  FROM comandes JOIN clients
    ON clie = num_clie;
```
Amb subconsultes:
```
SELECT num_comanda, import, clie, num_clie, limit_credit
  FROM comandes 
       JOIN (SELECT num_clie, limit_credit 
               FROM clients ) AS clie 
       ON clie.num_clie = clie;
       
```
## Exercici 22

Transforma el següent JOIN a una comanda amb subconsultes:

```
SELECT empl.nom, empl.quota, dir.nom, dir.quota
  FROM rep_vendes AS empl JOIN rep_vendes AS cap
    ON empl.cap = cap.num_empl
 WHERE empl.quota > cap.quota;
```
Amb subconsultes:
```
 SELECT ven.num_empl, ven.quota, dir.nom, dir.quota
   FROM rep_vendes AS ven 
        JOIN (SELECT num_empl, nom, quota 
                FROM rep_vendes ) AS dir 
        ON dir.num_empl = cap 
  WHERE ven.quota > dir.quota;
```
## Exercici 23

Transforma la següent consulta amb un ANY a una consulta amb un EXISTS i també en una altre consulta amb un ALL:

```
SELECT oficina
  FROM oficines
 WHERE vendes * 0.8 < ANY 
       (SELECT vendes
          FROM rep_vendes
         WHERE oficina_rep = oficina);
```
Amb EXISTS:
```
SELECT oficina, ciutat
  FROM oficines 
 WHERE EXISTS (SELECT quota 
                 FROM rep_vendes 
                WHERE oficina_rep = oficina 
                      AND quota IS NOT NULL 
                      AND objectiu * 0.8 < quota);

 oficina | ciutat  
---------+---------
      22 | Denver
      13 | Atlanta
(2 rows)

```
Amb ALL:
```
SELECT oficina, ciutat
  FROM oficines 
 WHERE objectiu * 0.8 < ALL (SELECT quota 
                               FROM rep_vendes 
                              WHERE oficina_rep = oficina 
                                    AND quota IS NOT NULL);

 oficina | ciutat  
---------+---------
      22 | Denver
      13 | Atlanta
(2 rows)
```
## Exercici 24

Transforma la següent consulta amb un ALL a una consutla amb un EXISTS i també en una altre consulta amb un ANY:

```
SELECT num_clie
  FROM clients
 WHERE limit_credit < ALL
       (SELECT import
          FROM comandes
         WHERE num_clie = clie);
```
Amb EXISTS:
```

```        
```
          
## Exercici 25

Transforma la següent consulta amb un EXISTS a una consulta amb un ALL i també a una altre consulta amb un ANY:

```
SELECT num_clie, empresa
  FROM clients
 WHERE EXISTS
       (SELECT *
          FROM rep_vendes
         WHERE rep_clie = num_empl
           AND edad BETWEEN 40 AND 50);
```
```

```
## Exercici 26

Transforma la següent consulta amb subconsultes a una consulta sense subconsultes.

```
SELECT *
  FROM productes
 WHERE id_fabricant IN 
       (SELECT fabricant
          FROM comandes
         WHERE quantitat > 30)
   AND id_producte IN 
       (SELECT producte
          FROM comandes
         WHERE quantitat > 30);
```
```
SELECT productes.*
  FROM productes 
       JOIN comandes 
       ON id_fabricant = fabricant 
          AND id_producte = producte
 WHERE quantitat > 30;	 
 
  id_fabricant | id_producte |   descripcio    |  preu  | estoc 
--------------+-------------+-----------------+--------+-------
 aci          | 41003       | Article Tipus 3 | 107.00 |   207
 aci          | 41004       | Article Tipus 4 | 117.00 |   139
 aci          | 41002       | Article Tipus 2 |  76.00 |   167
(3 rows)

```
## Exercici 27

Transforma la següent consulta amb subconsultes a una consulta sense subconsultes.

```
SELECT num_empl, nom
  FROM rep_vendes
 WHERE num_empl = ANY 
       (SELECT rep_clie
          FROM clients
         WHERE empresa LIKE '%Inc.');
```
```
SELECT num_empl, nom
  FROM rep_vendes 
       JOIN clients 
       ON num_empl = rep_clie
 WHERE empresa LIKE '%Inc.';
 
 num_empl |    nom     
----------+------------
      103 | Paul Cruz
      109 | Mary Jones
(2 rows)

```
## Exercici 28

Transforma la següent consulta amb un IN a una consulta amb un EXISTS i també a una altre consulta amb un ALL.

```
SELECT num_empl, nom
  FROM rep_vendes
 WHERE num_empl IN
       (SELECT cap
          FROM rep_vendes);
```
```

```
## Exercici 29

Modifica la següent consulta perquè mostri la ciutat de l'oficina, proposa una consulta simplificada.

```
SELECT num_comanda
  FROM comandes
 WHERE rep IN
       (SELECT num_empl
	  FROM rep_vendes
         WHERE vendes >
		       (SELECT avg(vendes)
			  FROM rep_vendes)
           AND oficina_rep IN
		       (SELECT oficina
                          FROM oficines
                         WHERE regio ILIKE 'est') );
```
```

```
## Exercici 30

Transforma la següent consulta amb subconsultes a una consulta amb les mínimes subconsultes possibles.

```
SELECT num_clie, empresa,
       (SELECT nom
          FROM rep_vendes
         WHERE rep_clie = num_empl) AS rep_nom
  FROM clients
 WHERE rep_clie = ANY
       (SELECT num_empl
          FROM rep_vendes
         WHERE vendes >
               (SELECT MAX(quota)
                  FROM rep_vendes));
```
```
SELECT num_clie, empresa, nom
  FROM rep_vendes 
       JOIN clients 
       ON rep_clie = num_empl
 WHERE vendes = ANY (SELECT vendes 
                       FROM rep_vendes 
                      WHERE vendes > (SELECT MAX(quota) 
                                        FROM rep_vendes));
```
