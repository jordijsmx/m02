# Exercicis Vistes

## Exercici 1

Defineix una vista anomenada "oficina_est" que contingui únicament les dades de
les oficines de la regió est.

```
CREATE VIEW oficines_est
    AS SELECT oficina, ciutat, regio
         FROM oficines
        WHERE regio = 'Est'
        ORDER BY ciutat;
```


## Exercici 2

Crear una vista de nom "rep_oest" que mostri les dades dels venedors de la
regió oest.

```
CREATE VIEW rep_oest 
    AS SELECT * 
         FROM rep_vendes 
        WHERE oficina_rep IN 
            (SELECT oficina 
               FROM oficines 
              WHERE regio = 'Oest' );
```

## Exercici 3

Crea una vista temporal de nom "comandes_sue" que contingui únicament les
comandes fetes per clients assignats la representant de vendes Sue.

```
CREATE LOCAL TEMP VIEW comandes_sue 
    AS SELECT * 
         FROM comandes 
        WHERE rep IN 
            (SELECT num_empl 
               FROM rep_vendes 
              WHERE nom = 'Sue Smith');
```

## Exercici 4

Crea una vista de nom "clientes_vip" mostri únicament aquells clients que la
suma dels imports de les seves comandes superin 30000.

```
 CREATE VIEW clientes_vip 
     AS SELECT * 
          FROM clients 
         WHERE num_clie IN 
                (SELECT clie 
                   FROM comandes 
                  GROUP BY clie 
                 HAVING SUM(import) > 30000);
```

## Exercici 5

Crear una vista de nom "info_rep" amb les següents dades dels venedors:
num_empl, nombre, oficina_rep.

```
CREATE VIEW info_rep 
    AS SELECT num_empl, nom, oficina_rep 
         FROM rep_vendes ;
```

## Exercici 6

Crear una vista de nom "info_oficina" que mostri les oficines amb
l'identificador de l'oficina, la ciutat i la regió.

```
CREATE VIEW info_oficina 
    AS SELECT oficina, ciutat, regio 
         FROM oficines;
```

## Exercici 7

Crear una vista de nom "info_clie" que contingui el nom de l'empresa dels
clients i l'identificador del venedor que tenen assignat.

```
CREATE VIEW info_clie 
    AS SELECT empresa, rep_clie 
         FROM clients;
```

## Exercici 8

Crea una vista de nom "clie_bill" que conté el número de client, el nom de
empresa i el límit de crèdit de tots els clients assignats al representant de
vendes "Bill Adams".

```
CREATE VIEW clie_bill 
    AS SELECT num_clie, empresa, limit_credit 
         FROM clients 
        WHERE rep_clie IN 
                (SELECT num_empl 
                   FROM rep_vendes 
                  WHERE nom = 'Bill Adams');
```

## Exercici 9

Crea una vista de nom comanda_per_rep que conté les següents dades de les
comandes de cada venedor: id_representant_vendes, quantitat_pedidos,
import_total, import_minim, import_maxim, import_promig.

```
CREATE VIEW comanda_per_rep 
    AS SELECT rep AS id_representant_vendes, 
    COUNT(quantitat) AS quantitat_pedidos, 
    SUM(import) AS import_total, 
    MIN(import) AS import_minim, 
    MAX(import) AS import_maxim, 
    AVG(import) AS import_promig 
    FROM comandes 
    GROUP BY rep;
```

## Exercici 10

De la vista anterior volem una nova vista per mostrar el nom del representant
de vendes, números de comandes, import total de les comandes i el promig de les
comandes per a cada venedor. S'han d'ordenar per tal que primer es mostrin els
que tenen major import total.

```
CREATE VIEW comanda_per_rep2 
  AS SELECT 
(SELECT nom 
   FROM rep_vendes 
  WHERE id_representant_vendes = num_empl), quantitat_pedidos, import_total, import_promig 
   FROM comanda_per_rep 
  ORDER BY import_total DESC;
```

## Exercici 11

Crear una vista de nom "info_comanda" amb les dades de les comandes però amb
els noms del client i venedor en lloc dels seus identificadors.

```
CREATE VIEW info_comanda AS
SELECT num_comanda, data, 
   (SELECT empresa 
      FROM clients 
     WHERE num_clie = clie), 
   (SELECT nom 
      FROM rep_vendes 
     WHERE rep = num_empl), fabricant, producte, quantitat, import FROM comandes ;
```

## Exercici 12

Crear una vista anomenada "clie_rep" que mostri l'import total de les comandes
que ha fet cada client a cada representant de vendes. Cal mostrar el nom de
l'empresa i el nom del representant de vendes.

```
CREATE VIEW clie_rep AS
SELECT SUM(import), empresa, nom 
  FROM comandes 
  JOIN clients 
    ON clie = num_clie 
  JOIN rep_vendes 
    ON rep = num_empl 
 GROUP BY nom, empresa;
```

## Exercici 13

Crear una vista temporal per substituir la taula "comandes" que mostri les
comandes amb import més gran a 20000 i ordenades per import de forma
descendent.

```
CREATE LOCAL TEMP VIEW comandes AS 
      SELECT * FROM comandes 
       WHERE import > 20000 
       ORDER BY import DESC;
```

## Exercici 14

Crea una vista anomenada "top_clie" que mostri el nom de l'empresa client i el
total dels imports de les comandes del client. S'han d'ordenar per tal que
primer es mostrin els que tenen major import total.

```
CREATE VIEW top_clie AS 
SELECT empresa, SUM(import) 
  FROM clients JOIN comandes 
    ON num_clie = clie 
 GROUP BY empresa 
 ORDER BY SUM(import) DESC;
```

## Exercici 15

Crea una vista anomenata "top_prod" que mostri les dades de tots els productes
seguit d'un camp anomenat "quant_total" en que es mostri la quantitat de cada
producte que s'ha demanat en totes les comandes. S'ha d'ordenar per tal que
primer es mostrin els productes que tenen més comandes.
    
```
CREATE VIEW top_prod
    AS SELECT *, 
       (SELECT SUM(quantitat)
          FROM comandes
         WHERE producte = id_producte
           AND fabricant = id_fabricant
         GROUP BY producte ) AS quant_total
         FROM productes 
     ORDER BY quant_total DESC;
```
    

## Exercici 16

Crea una vista anomenada "responsables" que mostri un llistat de tots els
representants de vendes. En un camp anomenat "empl" ha de mostrar el nom de
cada representant de vendes. També ha de mostrar un camp anomenat "superior"
que mostri el nom del cap del representant de vendes, en cas que el
representant de vendes tingui cap. També ha de mostrar un camp anomenat
"oficina_superior" que mostri el nom del director de l'oficina en que treballa
el representant de vendes, en cas que el representant de vendes tingui
assignada una oficina aquesta tingui un director.

```

```