# Consultes resum (PART II)

## Exercici 20

Quina és l'import promig de les comandes de cada venedor?

```
SELECT ROUND(AVG(import),2) AS "Import promig", rep AS "Venedor" 
        FROM comandes 
        GROUP BY rep;
```

```
 Import promig | Venedor 
---------------+---------
       8876.00 |     101
       8376.14 |     108
       1350.00 |     103
       7865.40 |     105
      11477.33 |     107
       5694.00 |     102
       3552.50 |     109
      16479.00 |     106
      11566.00 |     110
(9 rows)
```

## Exercici 21

Quin és el rang (màxim i mínim) de quotes dels venedors per cada oficina?

```
SELECT oficina_rep AS "oficina", 
   MAX(quota) AS "Màxim", 
   MIN(quota) AS "Mínim" 
   FROM rep_vendes 
   GROUP BY oficina_rep;
```

```
 oficina |   Màxim   |   Mínim   
---------+-----------+-----------
         |           |          
      22 | 300000.00 | 300000.00
      11 | 300000.00 | 275000.00
      21 | 350000.00 | 350000.00
      13 | 350000.00 | 350000.00
      12 | 300000.00 | 200000.00
(6 rows)
```

## Exercici 22

Quants venedors estan asignats a cada oficina?

```
SELECT oficina_rep AS "oficina", COUNT(num_empl) AS "Venedors" FROM rep_vendes GROUP BY oficina_rep;
```

```
 oficina | Venedors 
---------+----------
         |        1
      22 |        1
      11 |        2
      21 |        2
      13 |        1
      12 |        3
(6 rows)
```

## Exercici 23

Per cada venedor calcular quants clients diferents ha atès ( atès = atendre una comanda)?

```
SELECT rep_clie AS "Venedors", 
 COUNT(num_clie) AS "Clients" 
  FROM clients 
  GROUP BY rep_clie;
```

```
 Venedors | Clients 
----------+---------
      101 |       3
      108 |       2
      103 |       3
      104 |       1
      105 |       2
      107 |       1
      102 |       4
      109 |       2
      106 |       2
      110 |       1
(10 rows)
```

## Exercici 24

Calcula el total dels imports de les comandes fetes per cada client a cada
vendedor.

```
SELECT rep AS "Venedor", clie AS "Client", 
   SUM(import) AS "Total import" 
  FROM comandes 
  GROUP BY rep,clie;
```

```
 Venedor | Client | Total import 
---------+--------+--------------
     106 |   2117 |     31500.00
     107 |   2124 |      3082.00
     108 |   2114 |      7100.00
     101 |   2108 |       150.00
     102 |   2106 |      4026.00
     105 |   2103 |     35582.00
     101 |   2102 |      3978.00
     102 |   2120 |      3750.00
     108 |   2118 |      3608.00
     102 |   2114 |     15000.00
     105 |   2111 |      3745.00
     103 |   2111 |      2700.00
     107 |   2109 |     31350.00
     108 |   2112 |     47925.00
     101 |   2113 |     22500.00
     109 |   2108 |      7105.00
     110 |   2107 |     23132.00
     106 |   2101 |      1458.00
(18 rows)
```

## Exercici 25

El mateix que a la qüestió anterior, però ordenat per client i dintre de client
per venedor.

```
SELECT rep AS "Venedor", clie AS "Client", 
   SUM(import) AS "Total import" 
   FROM comandes 
   GROUP BY rep,clie 
   ORDER BY 2,1;
```

```
 Venedor | Client | Total import 
---------+--------+--------------
     106 |   2101 |      1458.00
     101 |   2102 |      3978.00
     105 |   2103 |     35582.00
     102 |   2106 |      4026.00
     110 |   2107 |     23132.00
     101 |   2108 |       150.00
     109 |   2108 |      7105.00
     107 |   2109 |     31350.00
     103 |   2111 |      2700.00
     105 |   2111 |      3745.00
     108 |   2112 |     47925.00
     101 |   2113 |     22500.00
     102 |   2114 |     15000.00
     108 |   2114 |      7100.00
     106 |   2117 |     31500.00
     108 |   2118 |      3608.00
     102 |   2120 |      3750.00
     107 |   2124 |      3082.00
(18 rows)
```

## Exercici 26

Calcula les comandes totals per a cada venedor.

```
SELECT COUNT(num_comanda) AS "Comandes", rep AS "Venedor"   
  FROM comandes 
  GROUP BY rep;
```

```
 Comandes | Venedor 
----------+---------
        3 |     101
        7 |     108
        2 |     103
        5 |     105
        3 |     107
        4 |     102
        2 |     109
        2 |     106
        2 |     110
(9 rows)
```

## Exercici 27

Quin és l'import promig de les comandes per cada venedor, les comandes dels
quals sumen més de 30000?

```
SELECT ROUND(AVG(import),2), rep 
  FROM comandes 
  GROUP BY rep 
  HAVING SUM(import) > 30000;
```

```
  round   | rep 
----------+-----
  8376.14 | 108
  7865.40 | 105
 11477.33 | 107
 16479.00 | 106
(4 rows)
```

## Exercici 28

Per cada oficina amb dos o més empleats, calcular la quota total i les vendes
totals per a tots els venedors que treballen a la oficina (volem mostrar la
ciutat de l'oficina a la consulta)

```
SELECT SUM(quota) AS "Suma quota", 
       SUM(rep_vendes.vendes) AS "Suma vendes", 
       oficina_rep AS "Oficina", ciutat 
       FROM rep_vendes 
       JOIN oficines ON rep_vendes.oficina_rep = oficines.oficina 
       GROUP BY rep_vendes.oficina_rep, oficines.ciutat HAVING COUNT(num_empl) >= 2;
```

```
 Suma quota | Suma vendes | Oficina |   ciutat    
------------+-------------+---------+-------------
  700000.00 |   835915.00 |      21 | Los Angeles
  775000.00 |   735042.00 |      12 | Chicago
  575000.00 |   692637.00 |      11 | New York
(3 rows)
```

## Exercici 29

Mostra el preu, l'estoc i la quantitat total de les comandes de cada producte
per als quals la quantitat total demanada està per sobre del 75% de l'estoc.

```
SELECT preu, estoc, SUM(quantitat) 
  FROM comandes 
  JOIN productes ON (productes.id_fabricant, productes.id_producte) = (comandes.fabricant, comandes.producte) 
  GROUP BY preu, estoc, quantitat 
  HAVING SUM(quantitat) > estoc * 0.75;
```

```
  preu   | estoc | sum 
---------+-------+-----
 1425.00 |     5 |  22
 4500.00 |    12 |  10
(2 rows)
```

## Exercici 30

Es desitja un llistat d'identificadors de fabricants de productes. Només volem
tenir en compte els productes de preu superior a 54. Només volem que apareguin
els fabricants amb un nombre total d'unitats superior a 300.

```
SELECT id_fabricant
  FROM productes
 WHERE preu > 54
 GROUP BY id_fabricant
 HAVING SUM(estoc) > 300;
```

```
 id_fabricant 
--------------
 aci
(1 row)
```

## Exercici 31

Es desitja un llistat dels productes amb les seves descripcions, ordenat per la
suma total d'imports facturats (comandes) de cada producte de l'any 1989.

```
SELECT id_producte, descripcio, SUM(import), data 
  FROM productes 
  JOIN comandes 
       ON (productes.id_fabricant, productes.id_producte) = (comandes.fabricant, comandes.producte) 
 WHERE data BETWEEN '1989-01-01' AND '1989-12-12' 
 GROUP BY id_producte, descripcio, data 
 ORDER BY SUM(import) DESC; 
```

```
 id_producte |     descripcio     |   sum    |    data    
-------------+--------------------+----------+------------
 4100z       | Muntador           | 15000.00 | 1989-10-12
 41004       | Article Tipus 4    |  3978.00 | 1989-10-12
 2a44g       | Passador Frontissa |  2100.00 | 1989-12-12
 2a45c       | V Stago Trinquet   |  1896.00 | 1989-01-04
 41002       | Article Tipus 2    |   760.00 | 1989-11-04
(5 rows)
```

## Exercici 32

Per a cada director (de personal, no d'oficina) excepte per al gerent (el
venedor que no té director), vull saber el total de vendes dels seus
subordinats. Mostreu codi i nom dels directors.

```
SELECT a.num_empl AS "Codi", a.nom AS "Nom", SUM(b.vendes) AS "Vendes"
  FROM rep_vendes a 
  JOIN rep_vendes b ON a.num_empl = b.cap 
  GROUP BY a.num_empl;
```

```
 Codi |     Nom     |  Vendes   
------+-------------+-----------
  104 | Bob Smith   | 960359.00
  106 | Sam Clark   | 897184.00
  101 | Dan Roberts |  75985.00
  108 | Larry Fitch | 660092.00
(4 rows)
```

## Exercici 33

Quins són els 5 productes que han estat venuts a més clients diferents? Mostreu
el número de clients per cada producte. A igualtat de nombre de clients es
volen ordenats per ordre decreixent d'estoc i, a igualtat d'estoc, per
descripció. Mostreu tots els camps pels quals s'ordena.

```
SELECT COUNT(clie), estoc, descripcio 
  FROM productes 
  JOIN comandes ON (productes.id_fabricant, productes.id_producte) = (comandes.fabricant, comandes.producte) 
  GROUP BY estoc, descripcio 
  ORDER BY estoc, descripcio 
  FETCH FIRST 5 ROWS ONLY;
```

```
 count | estoc |   descripcio   
-------+-------+----------------
     2 |     3 | Manovella
     1 |     5 | Riosta 1-Tm
     2 |     9 | Riosta 2-Tm
     2 |    12 | Frontissa Dta.
     1 |    12 | Frontissa Esq.
(5 rows)
```

## Exercici 34

Es vol llistar el clients (codi i empresa) tals que no hagin comprat cap tipus
de Frontissa (figura a la descripció) i hagin comprat articles de més d'un
fabricant diferent.

```
SELECT clie, empresa, COUNT(fabricant) 
  FROM comandes JOIN clients 
    ON comandes.clie = clients.num_clie 
  JOIN productes 
    ON (productes.id_fabricant, productes.id_producte) = (comandes.fabricant, comandes.producte) 
 WHERE descripcio NOT LIKE 'Frontissa%' 
 GROUP BY clie, empresa 
HAVING COUNT(fabricant) > 1;
```

```
 clie |      empresa      | count 
------+-------------------+-------
 2103 | Acme Mfg.         |     4
 2106 | Fred Lewis Corp.  |     2
 2107 | Ace International |     2
 2108 | Holm & Landis     |     3
 2111 | JCP Inc.          |     3
 2114 | Orion Corp        |     2
 2118 | Midwest Systems   |     3
 2124 | Peter Brothers    |     2
(8 rows)
```

## Exercici 35

Llisteu les oficines per ordre descendent de nombre total de clients diferents
amb comandes realitzades pels venedors d'aquella oficina, i, a igualtat de
clients, ordenat per ordre ascendent del nom del director de l'oficina. Només
s'ha de mostrar el codi i la ciutat de l'oficina.

```
SELECT oficina, ciutat 
  FROM oficines 
  JOIN rep_vendes a ON a.oficina_rep = oficina 
  JOIN rep_vendes b ON b.num_empl = director
  JOIN comandes ON a.num_empl = rep 
  GROUP BY ciutat, oficina, b.nom 
  ORDER BY COUNT(DISTINCT(clie)), b.nom;
```

```
 oficina |   ciutat    
---------+-------------
      13 | Atlanta
      22 | Denver
      11 | New York
      12 | Chicago
      21 | Los Angeles
(5 rows)
```
