
# Consultes simples

Escriu la consulta SELECT que mostra el que es demana a cada exercici i la seva sortida.

## Exercici 1:

Els identificadors de les oficines amb la seva ciutat, els objectius i les vendes reals.

```
SELECT oficina, ciutat, objectiu, vendes 
  FROM oficines ;
```
```
 oficina |   ciutat    | objectiu  |  vendes   
---------+-------------+-----------+-----------
      22 | Denver      | 300000.00 | 186042.00
      11 | New York    | 575000.00 | 692637.00
      12 | Chicago     | 800000.00 | 735042.00
      13 | Atlanta     | 350000.00 | 367911.00
      21 | Los Angeles | 725000.00 | 835915.00
(5 rows)
```

## Exercici 2:

Els identificadors de les oficines de la regió est amb la seva ciutat, els objectius i les vendes reals.

```
SELECT oficina, ciutat, objectiu, vendes 
  FROM oficines 
 WHERE regio = 'Est';
```
```
 oficina |  ciutat  | objectiu  |  vendes   
---------+----------+-----------+-----------
      11 | New York | 575000.00 | 692637.00
      12 | Chicago  | 800000.00 | 735042.00
      13 | Atlanta  | 350000.00 | 367911.00
(3 rows)
```


## Exercici 3:  

Les ciutats en ordre alfabètic de les oficines de la regió est amb els objectius i les vendes reals.

```
SELECT ciutat, objectiu, vendes 
  FROM oficines WHERE regio = 'Est' 
 ORDER BY ciutat;
```
```
  ciutat  | objectiu  |  vendes   
----------+-----------+-----------
 Atlanta  | 350000.00 | 367911.00
 Chicago  | 800000.00 | 735042.00
 New York | 575000.00 | 692637.00
(3 rows)
```


## Exercici 4:  

Les ciutats, els objectius i les vendes d'aquelles oficines que les seves vendes superin els seus objectius.

```
SELECT ciutat, objectiu, vendes 
  FROM oficines 
 WHERE vendes > objectiu;
```
```
   ciutat    | objectiu  |  vendes   
-------------+-----------+-----------
 New York    | 575000.00 | 692637.00
 Atlanta     | 350000.00 | 367911.00
 Los Angeles | 725000.00 | 835915.00
(3 rows)
```

## Exercici 5:  

Nom, quota i vendes de l'empleat representant de vendes número 107.

```
SELECT nom, quota, vendes 
  FROM rep_vendes 
 WHERE num_empl = 107;
```
```
      nom      |   quota   |  vendes   
---------------+-----------+-----------
 Nancy Angelli | 300000.00 | 186042.00
(1 row)
```

## Exercici 6:  

Nom i data de contracte dels representants de vendes amb vendes superiors a 300000.

```
 SELECT nom, data_contracte 
   FROM rep_vendes 
  WHERE vendes > 300000;
```
```
     nom     | data_contracte 
-------------+----------------
 Bill Adams  | 1988-02-12
 Mary Jones  | 1989-10-12
 Sue Smith   | 1986-12-10
 Dan Roberts | 1986-10-20
 Larry Fitch | 1989-10-12
(5 rows)
```

## Exercici 7:  

Nom dels representants de vendes dirigits per l'empleat numero 104 Bob Smith.

```
SELECT nom 
  FROM rep_vendes 
 WHERE cap = 104;
```
```
     nom     
-------------
 Bill Adams
 Dan Roberts
 Paul Cruz
(3 rows)
```

## Exercici 8:  

Nom dels venedors i data de contracte d'aquells que han estat contractats abans del 1988.

```
SELECT nom, data_contracte 
  FROM rep_vendes 
 WHERE data_contracte < 1988-01-01;
```
```
     nom     | data_contracte 
-------------+----------------
 Sue Smith   | 1986-12-10
 Bob Smith   | 1987-05-19
 Dan Roberts | 1986-10-20
 Paul Cruz   | 1987-03-01
(4 rows)
```

## Exercici 9:  

Identificador de les oficines i ciutat d'aquelles oficines que el seu objectiu és diferent a 800000.

```
SELECT oficina, ciutat 
  FROM oficines 
 WHERE objectiu <> 800000;
```
```
 oficina |   ciutat    
---------+-------------
      22 | Denver
      11 | New York
      13 | Atlanta
      21 | Los Angeles
(4 rows)
```

## Exercici 10:  

Nom de l'empresa i limit de crèdit del client número 2107.

```
SELECT empresa, limit_credit 
  FROM clients 
 WHERE num_clie = 2107;
```
```
      empresa      | limit_credit 
-------------------+--------------
 Ace International |     35000.00
(1 row)
```

## Exercici 11:  

id_fab com a "Identificador del fabricant", id_producto com a "Identificador del producte" i descripcion com a "Descripció" dels productes.

```
SELECT id_fabricant AS "Identificador del fabricant", 
       id_producte AS "Identificador del producte", 
       descripcio AS "Descripció" 
  FROM productes ;
```
```
 Identificador del fabricant | Identificador del producte |     Descripció     
-----------------------------+----------------------------+--------------------
 rei                         | 2a45c                      | V Stago Trinquet
 aci                         | 4100y                      | Extractor
 qsa                         | xk47                       | Reductor
 bic                         | 41672                      | Plate
 imm                         | 779c                       | Riosta 2-Tm
 aci                         | 41003                      | Article Tipus 3
 aci                         | 41004                      | Article Tipus 4
 bic                         | 41003                      | Manovella
 imm                         | 887p                       | Pern Riosta
 qsa                         | xk48                       | Reductor
 rei                         | 2a44l                      | Frontissa Esq.
 fea                         | 112                        | Coberta
 imm                         | 887h                       | Suport Riosta
 bic                         | 41089                      | Retn
 aci                         | 41001                      | Article Tipus 1
 imm                         | 775c                       | Riosta 1-Tm
 aci                         | 4100z                      | Muntador
 qsa                         | xk48a                      | Reductor
 aci                         | 41002                      | Article Tipus 2
 rei                         | 2a44r                      | Frontissa Dta.
 imm                         | 773c                       | Riosta 1/2-Tm
 aci                         | 4100x                      | Peu de rei
 fea                         | 114                        | Bancada Motor
 imm                         | 887x                       | Retenidor Riosta
 rei                         | 2a44g                      | Passador Frontissa
(25 rows)
```

## Exercici 12:  

Identificador del fabricant, identificador del producte i descripció del producte d'aquells productes que el seu identificador de fabricant acabi amb la lletra i.

```
SELECT id_fabricant, id_producte, descripcio 
  FROM productes 
 WHERE id_fabricant 
  LIKE '%i';
```
```
 id_fabricant | id_producte |     descripcio     
--------------+-------------+--------------------
 rei          | 2a45c       | V Stago Trinquet
 aci          | 4100y       | Extractor
 aci          | 41003       | Article Tipus 3
 aci          | 41004       | Article Tipus 4
 rei          | 2a44l       | Frontissa Esq.
 aci          | 41001       | Article Tipus 1
 aci          | 4100z       | Muntador
 aci          | 41002       | Article Tipus 2
 rei          | 2a44r       | Frontissa Dta.
 aci          | 4100x       | Peu de rei
 rei          | 2a44g       | Passador Frontissa
(11 rows)
```

## Exercici 13:  

Nom i identificador dels venedors que estan per sota la quota i tenen vendes inferiors a 300000.

```
SELECT num_empl, nom 
  FROM rep_vendes 
 WHERE vendes < quota 
   AND vendes < 300000;
```
```
 num_empl |      nom      
----------+---------------
      104 | Bob Smith
      107 | Nancy Angelli
(2 rows)
```

## Exercici 14:  

Identificador i nom dels venedors que treballen a les oficines 11 o 13.

```
SELECT num_empl, nom 
  FROM rep_vendes 
 WHERE oficina_rep = 11 
    OR oficina_rep = 13;
```
```
 num_empl |    nom     
----------+------------
      105 | Bill Adams
      109 | Mary Jones
      106 | Sam Clark
(3 rows)
```

## Exercici 15:  

Identificador, descripció i preu dels productes ordenats del més car al més barat.

```
SELECT id_producte, descripcio, preu 
  FROM productes 
 ORDER BY preu DESC;
```
```
 id_producte |     descripcio     |  preu   
-------------+--------------------+---------
 2a44r       | Frontissa Dta.     | 4500.00
 2a44l       | Frontissa Esq.     | 4500.00
 4100y       | Extractor          | 2750.00
 4100z       | Muntador           | 2500.00
 779c        | Riosta 2-Tm        | 1875.00
 775c        | Riosta 1-Tm        | 1425.00
 773c        | Riosta 1/2-Tm      |  975.00
 41003       | Manovella          |  652.00
 887x        | Retenidor Riosta   |  475.00
 xk47        | Reductor           |  355.00
 2a44g       | Passador Frontissa |  350.00
 887p        | Pern Riosta        |  250.00
 114         | Bancada Motor      |  243.00
 41089       | Retn               |  225.00
 41672       | Plate              |  180.00
 112         | Coberta            |  148.00
 xk48        | Reductor           |  134.00
 xk48a       | Reductor           |  117.00
 41004       | Article Tipus 4    |  117.00
 41003       | Article Tipus 3    |  107.00
 2a45c       | V Stago Trinquet   |   79.00
 41002       | Article Tipus 2    |   76.00
 41001       | Article Tipus 1    |   55.00
 887h        | Suport Riosta      |   54.00
 4100x       | Peu de rei         |   25.00
(25 rows)
```

## Exercici 16:  

Identificador i descripció de producte amb el valor_inventario (existencies * preu).

```
SELECT id_producte, descripcio, (estoc * preu) AS valor_inventario 
  FROM productes; 
```
```
 id_producte |     descripcio     | valor_inventario 
-------------+--------------------+------------------
 2a45c       | V Stago Trinquet   |         16590.00
 4100y       | Extractor          |         68750.00
 xk47        | Reductor           |         13490.00
 41672       | Plate              |             0.00
 779c        | Riosta 2-Tm        |         16875.00
 41003       | Article Tipus 3    |         22149.00
 41004       | Article Tipus 4    |         16263.00
 41003       | Manovella          |          1956.00
 887p        | Pern Riosta        |          6000.00
 xk48        | Reductor           |         27202.00
 2a44l       | Frontissa Esq.     |         54000.00
 112         | Coberta            |         17020.00
 887h        | Suport Riosta      |         12042.00
 41089       | Retn               |         17550.00
 41001       | Article Tipus 1    |         15235.00
 775c        | Riosta 1-Tm        |          7125.00
 4100z       | Muntador           |         70000.00
 xk48a       | Reductor           |          4329.00
 41002       | Article Tipus 2    |         12692.00
 2a44r       | Frontissa Dta.     |         54000.00
 773c        | Riosta 1/2-Tm      |         27300.00
 4100x       | Peu de rei         |           925.00
 114         | Bancada Motor      |          3645.00
 887x        | Retenidor Riosta   |         15200.00
 2a44g       | Passador Frontissa |          4900.00
(25 rows)
```

## Exercici 17:  

Vendes de cada oficina en una sola columna i format amb format "<ciutat> té unes vendes de <vendes>", exemple "Denver te unes vendes de 186042.00".

```
SELECT ciutat ||' té unes vendes de '|| vendes 
  FROM oficines ;
```
```
                ?column?                 
-----------------------------------------
 Denver té unes vendes de 186042.00
 New York té unes vendes de 692637.00
 Chicago té unes vendes de 735042.00
 Atlanta té unes vendes de 367911.00
 Los Angeles té unes vendes de 835915.00
(5 rows)
```
## Exercici 18:  

Codis d'empleats que són directors d'oficines.

```
SELECT director 
  FROM oficines;
```
```
 director 
----------
      108
      106
      104
      105
      108
(5 rows)
```
## Exercici 19:  

Identificador i ciutat de les oficines que tinguin ventes per sota el 80% del seu objectiu.

```
SELECT oficina, ciutat 
  FROM oficines 
 WHERE vendes < objectiu * 0.80;
```
```
 oficina | ciutat 
---------+--------
      22 | Denver
(1 row)
```
## Exercici 20:  

Identificador, ciutat i director de les oficines que no siguin dirigides per l'empleat 108.

```
SELECT oficina, ciutat, director 
  FROM oficines 
 WHERE director <> 108;
```
```
 oficina |  ciutat  | director 
---------+----------+----------
      11 | New York |      106
      12 | Chicago  |      104
      13 | Atlanta  |      105
(3 rows)
```
## Exercici 21:  

Identificadors i noms dels venedors amb vendes entre el 80% i el 120% de llur quota.

```
SELECT num_empl, nom 
  FROM rep_vendes 
 WHERE vendes 
 BETWEEN 0.80 * quota AND 1.20 * quota;

SELECT num_empl, nom 
  FROM rep_vendes 
 WHERE vendes > 0.80 * quota AND vendes < 1.20 * quota;

```
```
 num_empl |     nom     
----------+-------------
      105 | Bill Adams
      106 | Sam Clark
      101 | Dan Roberts
      108 | Larry Fitch
      103 | Paul Cruz
(5 rows)
```
## Exercici 22:  

Identificador, vendes i ciutat de cada oficina ordenades alfabèticament per regió i, dintre de cada regió ordenades per ciutat.

```
SELECT oficina, vendes, ciutat 
  FROM oficines 
 ORDER BY regio, ciutat;

```
```
 oficina |  vendes   |   ciutat    
---------+-----------+-------------
      13 | 367911.00 | Atlanta
      12 | 735042.00 | Chicago
      11 | 692637.00 | New York
      22 | 186042.00 | Denver
      21 | 835915.00 | Los Angeles
(5 rows)
```
## Exercici 23:  

Llista d'oficines classificades alfabèticament per regió i, per cada regió, en ordre descendent de rendiment de vendes (vendes-objectiu).

```
SELECT * 
  FROM oficines 
ORDER BY regio, vendes DESC;
```
```
 oficina |   ciutat    | regio | director | objectiu  |  vendes   
---------+-------------+-------+----------+-----------+-----------
      12 | Chicago     | Est   |      104 | 800000.00 | 735042.00
      11 | New York    | Est   |      106 | 575000.00 | 692637.00
      13 | Atlanta     | Est   |      105 | 350000.00 | 367911.00
      21 | Los Angeles | Oest  |      108 | 725000.00 | 835915.00
      22 | Denver      | Oest  |      108 | 300000.00 | 186042.00
(5 rows)
```
## Exercici 24:  

Codi i nom dels tres venedors que tinguin unes vendes superiors.

```
SELECT num_empl, nom 
  FROM rep_vendes 
 ORDER BY vendes DESC zzzzzzLIMIT 3;

 SELECT num_empl, nom 
  FROM rep_vendes 
 ORDER BY vendes DESC FETCH FIRST 3 ROWS ONLY;
```
```
 num_empl |    nom     
----------+------------
      102 | Sue Smith
      109 | Mary Jones
      105 | Bill Adams
(3 rows)
```
## Exercici 25:  

Nom i data de contracte dels empleats que les seves vendes siguin superiors a 500000.

```
SELECT nom, data_contracte 
  FROM rep_vendes 
 WHERE vendes > 500000;
```
```
 nom | data_contracte 
-----+----------------
(0 rows)
```
## Exercici 26:  

Nom i quota actual dels venedors amb el calcul d'una "nova possible quota" que serà la quota de cada venedor augmentada un 3 per cent de les seves pròpies vendes.

```
SELECT nom, quota, vendes * 0.03 + quota AS "nova possible quota" 
  FROM rep_vendes;
```
```
      nom      |   quota   | nova possible quota 
---------------+-----------+---------------------
 Bill Adams    | 350000.00 |         378411.0000
 Mary Jones    | 300000.00 |         401725.0000
 Sue Smith     | 350000.00 |         484550.0000
 Sam Clark     | 275000.00 |         308162.0000
 Bob Smith     | 200000.00 |         148594.0000
 Dan Roberts   | 300000.00 |         314673.0000
 Tom Snyder    |           |                    
 Larry Fitch   | 350000.00 |         372365.0000
 Paul Cruz     | 275000.00 |         295025.0000
 Nancy Angelli | 300000.00 |         195042.0000
(10 rows)
```
## Exercici 27:  

Identificador i nom de les oficines que les seves vendes estan per sota del 80% de l'objectiu.

```
SELECT oficina, ciutat 
  FROM oficines 
 WHERE vendes < objectiu * 0.80; 
```
```
 oficina | ciutat 
---------+--------
      22 | Denver
(1 row)
```


## Exercici 28:  

Numero i import de les comandes que el seu import oscil·li entre 20000 i 29999.

```
SELECT num_comanda, import 
  FROM comandes 
 WHERE import 
 BETWEEN 20000 AND 29999;
```
```
 num_comanda |  import  
-------------+----------
      110036 | 22500.00
      112987 | 27500.00
      113042 | 22500.00
(3 rows)
```


## Exercici 29:  

Nom, ventes i quota dels venedors que les seves vendes no estan entre el 80% i el 120% de la seva quota.

```
SELECT nom, vendes, quota 
  FROM rep_vendes 
 WHERE NOT vendes > quota * 0.80 
   AND NOT vendes < quota * 1.20; 
```
```
 nom | vendes | quota 
-----+--------+-------
(0 rows)
```


## Exercici 30:  

Nom de l'empresa i el seu limit de crèdit de les empreses que el seu nom comença per Smith.

```
SELECT empresa, limit_credit 
  FROM clients 
 WHERE empresa LIKE 'Smith%';
```
```
    empresa     | limit_credit 
----------------+--------------
 Smithson Corp. |     20000.00
(1 row)
```


## Exercici 31:  

Identificador i nom dels venedors que no tenen assignada oficina.

```
SELECT num_empl, nom 
  FROM rep_vendes 
 WHERE oficina_rep IS NULL;
```
```
 num_empl |    nom     
----------+------------
      110 | Tom Snyder
(1 row)
```


## Exercici 32:  

Identificador i nom dels venedors, amb l'identificador de l'oficina d'aquells venedors que tenen una oficina assignada.

```
SELECT num_empl, nom 
  FROM rep_vendes 
 WHERE oficina_rep IS NOT NULL;
```
```
 num_empl |      nom      
----------+---------------
      105 | Bill Adams
      109 | Mary Jones
      102 | Sue Smith
      106 | Sam Clark
      104 | Bob Smith
      101 | Dan Roberts
      108 | Larry Fitch
      103 | Paul Cruz
      107 | Nancy Angelli
(9 rows)
```


## Exercici 33:  

Identificador i descripció dels productes del fabricant identificat per imm dels quals hi hagin existències superiors o iguals 200, també del fabricant bic amb existències superiors o iguals a 50.

```
SELECT id_producte, descripcio 
  FROM productes 
 WHERE id_fabricant LIKE 'imm' AND estoc >= 200 
    OR id_fabricant LIKE 'bic' AND estoc >= 50;
```
```
 id_producte |  descripcio   
-------------+---------------
 887h        | Suport Riosta
 41089       | Retn
(2 rows)
```


## Exercici 34:  

Identificador i nom dels venedors que treballen a les oficines 11, 12 o 22 i compleixen algun dels següents suposits:

1. han estat contractats a partir de juny del 1988 i no tenen director
2. estan per sobre la quota però tenen vendes de 600000 o menors.

```
ELECT num_empl, nom 
 FROM rep_vendes 
WHERE oficina_rep IN (11, 12, 22) AND (data_contracte > '1988-06-01' AND cap is NULL OR vendes > quota AND vendes <= 600000);
```
```
 num_empl |     nom     
----------+-------------
      109 | Mary Jones
      106 | Sam Clark
      101 | Dan Roberts
      103 | Paul Cruz
(4 rows)
```


## Exercici 35:  

Identificador i descripció dels productes amb un preu superior a 1000 i siguin del fabricant amb identificador rei o les existències siguin superiors a 20.

```
SELECT id_fabricant, descripcio 
  FROM productes 
 WHERE preu > 1000 
   AND (id_fabricant LIKE 'rei' 
    OR estoc > 20);
```
```
 id_fabricant |   descripcio   
--------------+----------------
 aci          | Extractor
 rei          | Frontissa Esq.
 aci          | Muntador
 rei          | Frontissa Dta.
(4 rows)
```


## Exercici 36:  

Identificador del fabricant,identificador i descripció dels productes fabricats pels fabricants que tenen una lletra qualsevol, una lletra 'i' i una altre lletra qualsevol com a identificador de fabricant.

```
SELECT id_fabricant, id_producte, descripcio 
  FROM productes 
 WHERE id_fabricant like '_i_' ;
```
```
 id_fabricant | id_producte | descripcio 
--------------+-------------+------------
 bic          | 41672       | Plate
 bic          | 41003       | Manovella
 bic          | 41089       | Retn
(3 rows)
```


## Exercici 37:  

Identificador i descripció dels productes que la seva descripció comença per "art" sense tenir en compte les majúscules i minúscules.

```
SELECT id_producte, descripcio 
  FROM productes 
 WHERE descripcio ILIKE 'art%';
```
```
 id_producte |   descripcio    
-------------+-----------------
 41003       | Article Tipus 3
 41004       | Article Tipus 4
 41001       | Article Tipus 1
 41002       | Article Tipus 2
(4 rows)
```


## Exercici 38:  

Identificador i nom dels clients que la segona lletra del nom sigui una "a" minúscula o majuscula.

```
SELECT num_clie, empresa 
  FROM clients 
 WHERE empresa ILIKE '_a%';
```
```
 num_clie |     empresa     
----------+-----------------
     2123 | Carter & Sons
     2113 | Ian & Schmidt
     2105 | AAA Investments
(3 rows)
```


## Exercici 39:  

Identificador i ciutat de les oficines que compleixen algun dels següents supòsits:

1. És de la regió est amb unes vendes inferiors a 700000.
2. És de la regió oest amb unes vendes inferiors a 600000.

```
SELECT oficina, ciutat 
  FROM oficines 
 WHERE (regio = 'Est' AND vendes < 700000) 
    OR (regio = 'Oest' AND vendes > 600000);
```
```
 oficina |   ciutat    
---------+-------------
      11 | New York
      13 | Atlanta
      21 | Los Angeles
(3 rows)
```


## Exercici 40:  

Identificador del fabricant, identificació i descripció dels productes que compleixen tots els següents supòsits:
1. L'identificador del fabricant és "imm" o el preu és menor a 500.
2. Les existències són inferiors a 5 o el producte te l'identificador 41003.  

```
SELECT id_fabricant, id_producte, descripcio 
  FROM productes 
 WHERE id_fabricant = 'imm' OR preu < 500 AND estoc < 5 OR id_producte = '41003';
```
```
 id_fabricant | id_producte |    descripcio    
--------------+-------------+------------------
 bic          | 41672       | Plate
 imm          | 779c        | Riosta 2-Tm
 aci          | 41003       | Article Tipus 3
 bic          | 41003       | Manovella
 imm          | 887p        | Pern Riosta
 imm          | 887h        | Suport Riosta
 imm          | 775c        | Riosta 1-Tm
 imm          | 773c        | Riosta 1/2-Tm
 imm          | 887x        | Retenidor Riosta
(9 rows)
```


## Exercici 41:  

Identificador de les comandes del fabricant amb identificador "rei" amb una quantitat superior o igual a 10 o amb un import superior o igual a 10000.

```
SELECT num_comanda 
  FROM comandes WHERE fabricant = 'rei' AND (quantitat >= 10 
    OR import >= 10000) ; 
```
```
 num_comanda 
-------------
      112961
      113045
      112993
      113042
(4 rows)
```


## Exercici 42:  

Data de les comandes amb una quantitat superior a 20 i un import superior a 1000 dels clients 2102, 2106 i 2109.

```
SELECT data
  FROM comandes
 WHERE quantitat > 20 AND import > 1000 
   AND clie IN (2102,2106,2109);
```
```
    data    
------------
 1989-10-12
 1990-03-02
 1989-01-04
(3 rows)
```


## Exercici 43:  

Identificador dels clients que el seu nom no conté " Corp." o " Inc." amb crèdit major a 30000.

```
SELECT num_clie, empresa 
  FROM clients 
 WHERE empresa NOT LIKE '%Corp.%' 
   AND empresa NOT LIKE '%Inc.%' 
   AND limit_credit > 30000;
```
```
 num_clie |      empresa      
----------+-------------------
     2103 | Acme Mfg.
     2123 | Carter & Sons
     2107 | Ace International
     2101 | Jones Mfg.
     2112 | Zetacorp
     2121 | QMA Assoc.
     2124 | Peter Brothers
     2108 | Holm & Landis
     2117 | J.P. Sinclair
     2120 | Rico Enterprises
     2118 | Midwest Systems
     2105 | AAA Investments
(12 rows)
```


## Exercici 44:  

Identificador dels representants de vendes majors de 40 anys amb vendes inferiors a 400000.

```
SELECT num_empl 
  FROM rep_vendes 
 WHERE edat > 40 AND vendes < 400000;
```
```
 num_empl 
----------
      106
      101
      110
      108
      107
(5 rows)
```


## Exercici 45:  

Identificador dels representants de vendes menors de 35 anys amb vendes superiors a 350000.

```
SELECT num_empl 
  FROM rep_vendes 
 WHERE edat < 35 AND vendes > 35000;
```
```
 num_empl 
----------
      109
      104
      103
(3 rows)
```