# Consultes multitaula

## Exercici 1

Llista la ciutat de les oficines, i el nom i títol dels directors de cada oficina.

```
SELECT ciutat, nom, carrec 
  FROM oficines 
  JOIN rep_vendes 
    ON oficines.director = rep_vendes.num_empl;
```

```
   ciutat    |     nom     |       carrec        
-------------+-------------+---------------------
 Denver      | Larry Fitch | Dir Vendes
 New York    | Sam Clark   | VP Vendes
 Chicago     | Bob Smith   | Dir Vendes
 Atlanta     | Bill Adams  | Representant Vendes
 Los Angeles | Larry Fitch | Dir Vendes
(5 rows)
```

## Exercici 2

Llista totes les comandes mostrant el seu número, import, número de client i límit de crèdit.

```
SELECT num_comanda, import, clie, limit_credit 
  FROM comandes 
  JOIN clients 
    ON comandes.clie = clients.num_clie;
```

```
 num_comanda |  import  | clie | limit_credit 
-------------+----------+------+--------------
      112961 | 31500.00 | 2117 |     35000.00
      113012 |  3745.00 | 2111 |     50000.00
      112989 |  1458.00 | 2101 |     65000.00
      113051 |  1420.00 | 2118 |     60000.00
      112968 |  3978.00 | 2102 |     65000.00
      110036 | 22500.00 | 2107 |     35000.00
      113045 | 45000.00 | 2112 |     50000.00
      112963 |  3276.00 | 2103 |     50000.00
      113013 |   652.00 | 2118 |     60000.00
      113058 |  1480.00 | 2108 |     55000.00
      112997 |   652.00 | 2124 |     40000.00
      112983 |   702.00 | 2103 |     50000.00
      113024 |  7100.00 | 2114 |     20000.00
      113062 |  2430.00 | 2124 |     40000.00
      112979 | 15000.00 | 2114 |     20000.00
      113027 |  4104.00 | 2103 |     50000.00
      113007 |  2925.00 | 2112 |     50000.00
      113069 | 31350.00 | 2109 |     25000.00
      113034 |   632.00 | 2107 |     35000.00
      112992 |   760.00 | 2118 |     60000.00
      112975 |  2100.00 | 2111 |     50000.00
      113055 |   150.00 | 2108 |     55000.00
      113048 |  3750.00 | 2120 |     50000.00
      112993 |  1896.00 | 2106 |     65000.00
      113065 |  2130.00 | 2106 |     65000.00
      113003 |  5625.00 | 2108 |     55000.00
      113049 |   776.00 | 2118 |     60000.00
      112987 | 27500.00 | 2103 |     50000.00
      113057 |   600.00 | 2111 |     50000.00
      113042 | 22500.00 | 2113 |     20000.00
(30 rows)
```
## Exercici 3

Llista el número de totes les comandes amb la descripció del producte demanat.

*LEFT*

```
SELECT num_comanda, descripcio 
  FROM comandes 
  JOIN productes 
    ON (id_fabricant, id_producte) = (fabricant, producte);
```

```
 num_comanda |     descripcio     
-------------+--------------------
      112961 | Frontissa Esq.
      113012 | Article Tipus 3
      112989 | Bancada Motor
      112968 | Article Tipus 4
      110036 | Muntador
      113045 | Frontissa Dta.
      112963 | Article Tipus 4
      113013 | Manovella
      113058 | Coberta
      112997 | Manovella
      112983 | Article Tipus 4
      113024 | Reductor
      113062 | Bancada Motor
      112979 | Muntador
      113027 | Article Tipus 2
      113007 | Riosta 1/2-Tm
      113069 | Riosta 1-Tm
      113034 | V Stago Trinquet
      112992 | Article Tipus 2
      112975 | Passador Frontissa
      113055 | Peu de rei
      113048 | Riosta 2-Tm
      112993 | V Stago Trinquet
      113065 | Reductor
      113003 | Riosta 2-Tm
      113049 | Reductor
      112987 | Extractor
      113057 | Peu de rei
      113042 | Frontissa Dta.
(29 rows)
```
## Exercici 4

Llista el nom de tots els clients amb el nom del representant de vendes assignat.

```
SELECT empresa, nom 
  FROM clients 
  JOIN rep_vendes 
    ON rep_clie = num_empl;
```

```
      empresa      |      nom      
-------------------+---------------
 JCP Inc.          | Paul Cruz
 First Corp.       | Dan Roberts
 Acme Mfg.         | Bill Adams
 Carter & Sons     | Sue Smith
 Ace International | Tom Snyder
 Smithson Corp.    | Dan Roberts
 Jones Mfg.        | Sam Clark
 Zetacorp          | Larry Fitch
 QMA Assoc.        | Paul Cruz
 Orion Corp        | Sue Smith
 Peter Brothers    | Nancy Angelli
 Holm & Landis     | Mary Jones
 J.P. Sinclair     | Sam Clark
 Three-Way Lines   | Bill Adams
 Rico Enterprises  | Sue Smith
 Fred Lewis Corp.  | Sue Smith
 Solomon Inc.      | Mary Jones
 Midwest Systems   | Larry Fitch
 Ian & Schmidt     | Bob Smith
 Chen Associates   | Paul Cruz
 AAA Investments   | Dan Roberts
(21 rows)
```
## Exercici 5

Llista la data de totes les comandes amb el numero i nom del client de la comanda.

```
SELECT data, clie, empresa 
  FROM comandes 
  JOIN clients 
    ON clie = num_clie;
```

```
    data    | clie |      empresa      
------------+------+-------------------
 1989-12-17 | 2117 | J.P. Sinclair
 1990-01-11 | 2111 | JCP Inc.
 1990-01-03 | 2101 | Jones Mfg.
 1990-02-10 | 2118 | Midwest Systems
 1989-10-12 | 2102 | First Corp.
 1990-01-30 | 2107 | Ace International
 1990-02-02 | 2112 | Zetacorp
 1989-12-17 | 2103 | Acme Mfg.
 1990-01-14 | 2118 | Midwest Systems
 1990-02-23 | 2108 | Holm & Landis
 1990-01-08 | 2124 | Peter Brothers
 1989-12-27 | 2103 | Acme Mfg.
 1990-01-20 | 2114 | Orion Corp
 1990-02-24 | 2124 | Peter Brothers
 1989-10-12 | 2114 | Orion Corp
 1990-01-22 | 2103 | Acme Mfg.
 1990-01-08 | 2112 | Zetacorp
 1990-03-02 | 2109 | Chen Associates
 1990-01-29 | 2107 | Ace International
 1989-11-04 | 2118 | Midwest Systems
 1989-12-12 | 2111 | JCP Inc.
 1990-02-15 | 2108 | Holm & Landis
 1990-02-10 | 2120 | Rico Enterprises
 1989-01-04 | 2106 | Fred Lewis Corp.
 1990-02-27 | 2106 | Fred Lewis Corp.
 1990-01-25 | 2108 | Holm & Landis
 1990-02-10 | 2118 | Midwest Systems
 1989-12-31 | 2103 | Acme Mfg.
 1990-02-18 | 2111 | JCP Inc.
 1990-02-02 | 2113 | Ian & Schmidt
(30 rows)
```
## Exercici 6

Llista les oficines, noms i títols del seus directors amb un objectiu superior a 600.000.


```
SELECT oficina, nom, carrec 
  FROM oficines 
  JOIN rep_vendes 
    ON director = num_empl 
 WHERE objectiu > 600000; 

SELECT oficina, nom, carrec 
  FROM oficines, rep_vendes 
 WHERE director = num_empl 
   AND objectiu > 600000; 
```

```
 oficina |     nom     |   carrec   
---------+-------------+------------
      12 | Bob Smith   | Dir Vendes
      21 | Larry Fitch | Dir Vendes
(2 rows)
```
## Exercici 7

Llista els venedors de les oficines de la regió est.


```
SELECT nom, regio 
  FROM rep_vendes 
  JOIN oficines 
    ON oficina_rep = oficina 
 WHERE regio = 'Est';
```

```
     nom     | regio 
-------------+-------
 Bill Adams  | Est
 Mary Jones  | Est
 Sam Clark   | Est
 Bob Smith   | Est
 Dan Roberts | Est
 Paul Cruz   | Est
(6 rows)
```
## Exercici 8

Llista les comandes superiors a 25000, incloent el nom del venedor que va servir la comanda i el nom del client que el va sol·licitar.

```
SELECT import, empresa, nom 
  FROM comandes 
  JOIN rep_vendes 
    ON rep = num_empl 
  JOIN clients 
    ON clie = num_clie 
 WHERE import > 25000;
```

```
  import  |     empresa     |      nom      
----------+-----------------+---------------
 31500.00 | J.P. Sinclair   | Sam Clark
 45000.00 | Zetacorp        | Larry Fitch
 31350.00 | Chen Associates | Nancy Angelli
 27500.00 | Acme Mfg.       | Bill Adams
(4 rows)
```
## Exercici 9

Llista les comandes superiors a 25000, mostrant el client que va servir la comanda i el nom del venedor que té assignat el client.


```
SELECT import, empresa, nom 
  FROM comandes 
  JOIN clients
    ON comandes.clie = clients.num_clie
  JOIN rep_vendes
    ON clients.rep_clie = rep_vendes.num_empl
 WHERE import > 25000;
```

```
  import  |     empresa     |     nom     
----------+-----------------+-------------
 31500.00 | J.P. Sinclair   | Sam Clark
 45000.00 | Zetacorp        | Larry Fitch
 31350.00 | Chen Associates | Paul Cruz
 27500.00 | Acme Mfg.       | Bill Adams
(4 rows)
```
## Exercici 10

Llista les comandes superiors a 25000, mostrant el nom del client que el va ordenar, el venedor associat al client, i l'oficina on el venedor treballa.

```
SELECT import, empresa, nom, oficina_rep 
  FROM comandes 
  JOIN rep_vendes 
    ON rep = num_empl 
  JOIN clients 
    ON clie = num_clie 
 WHERE import > 25000;
```

```
  import  |     empresa     |      nom      | oficina_rep 
----------+-----------------+---------------+-------------
 31500.00 | J.P. Sinclair   | Sam Clark     |          11
 45000.00 | Zetacorp        | Larry Fitch   |          21
 31350.00 | Chen Associates | Nancy Angelli |          22
 27500.00 | Acme Mfg.       | Bill Adams    |          13
(4 rows)
```
## Exercici 11

Llista totes les combinacions de venedors i oficines on la quota del venedor és superior a l'objectiu de l'oficina.


```
SELECT nom, oficina
  FROM rep_vendes 
 CROSS JOIN oficines 
 WHERE quota > objectiu;
```

```
     nom     | oficina 
-------------+---------
 Bill Adams  |      22
 Sue Smith   |      22
 Larry Fitch |      22
(3 rows)
```
## Exercici 12

Informa sobre tots els venedors i les oficines en les que treballen.

*LEFT*

```
SELECT nom, oficina, ciutat 
  FROM rep_vendes 
  LEFT JOIN oficines 
    ON oficina = oficina_rep;
```

```
      nom      | oficina |   ciutat    
---------------+---------+-------------
 Bill Adams    |      13 | Atlanta
 Mary Jones    |      11 | New York
 Sue Smith     |      21 | Los Angeles
 Sam Clark     |      11 | New York
 Bob Smith     |      12 | Chicago
 Dan Roberts   |      12 | Chicago
 Larry Fitch   |      21 | Los Angeles
 Paul Cruz     |      12 | Chicago
 Nancy Angelli |      22 | Denver
(9 rows)
```



## Exercici 13

Llista els venedors amb una quota superior a la dels seus directors.

```
SELECT a.nom, a.quota, b.nom, b.quota 
  FROM rep_vendes AS a 
  JOIN rep_vendes AS b 
    ON a.cap = b.num_empl 
 WHERE a.quota > b.quota; 
```

```
     nom     |   quota   |    nom    |   quota   
-------------+-----------+-----------+-----------
 Bill Adams  | 350000.00 | Bob Smith | 200000.00
 Mary Jones  | 300000.00 | Sam Clark | 275000.00
 Dan Roberts | 300000.00 | Bob Smith | 200000.00
 Larry Fitch | 350000.00 | Sam Clark | 275000.00
 Paul Cruz   | 275000.00 | Bob Smith | 200000.00
(5 rows)
```


## Exercici 14

Llistar el nom de l'empresa i totes les comandes fetes pel client 2103.

```
SELECT empresa, clie 
  FROM clients 
  JOIN comandes 
    ON clie = num_clie 
 WHERE num_clie = 2103;
```

```
  empresa  | clie 
-----------+------
 Acme Mfg. | 2103
 Acme Mfg. | 2103
 Acme Mfg. | 2103
 Acme Mfg. | 2103
(4 rows)
```


## Exercici 15

Llista aquelles comandes que el seu import sigui superior a 10000, mostrant el numero de comanda, els imports i les descripcions del producte.

```
SELECT num_comanda, import, descripcio 
  FROM comandes 
  JOIN productes 
    ON (id_fabricant, id_producte) = (fabricant, producte) 
 WHERE import > 10000;
```

```
 num_comanda |  import  |   descripcio   
-------------+----------+----------------
      112961 | 31500.00 | Frontissa Esq.
      110036 | 22500.00 | Muntador
      113045 | 45000.00 | Frontissa Dta.
      112979 | 15000.00 | Muntador
      113069 | 31350.00 | Riosta 1-Tm
      112987 | 27500.00 | Extractor
      113042 | 22500.00 | Frontissa Dta.
(7 rows)
```


## Exercici 16

Llista les comandes superiors a 25000, mostrant el nom del client que la va demanar, el venedor associat al client, i l'oficina on el venedor treballa. També cal mostar la descripció del producte.

```
SELECT empresa, nom, oficina_rep, descripcio 
  FROM comandes 
  JOIN clients 
    ON clie = num_clie 
  JOIN rep_vendes 
    ON rep_clie = num_empl 
  JOIN oficines 
    ON oficina_rep = oficina 
  JOIN productes 
    ON (fabricant, producte) = (id_fabricant, id_producte) 
  WHERE import > 25000
```

```
     empresa     |     nom     | oficina_rep |   descripcio   
-----------------+-------------+-------------+----------------
 J.P. Sinclair   | Sam Clark   |          11 | Frontissa Esq.
 Zetacorp        | Larry Fitch |          21 | Frontissa Dta.
 Chen Associates | Paul Cruz   |          12 | Riosta 1-Tm
 Acme Mfg.       | Bill Adams  |          13 | Extractor
(4 rows)
```


## Exercici 17

Trobar totes les comandes rebudes en els dies en que un nou venedor va ser contractat. Per cada comanda mostrar un cop el número, import i data de la comanda.

```
SELECT num_comanda, import, data 
  FROM comandes 
  JOIN rep_vendes 
    ON data_contracte = data;
```

```
 num_comanda |  import  |    data    
-------------+----------+------------
      112968 |  3978.00 | 1989-10-12
      112968 |  3978.00 | 1989-10-12
      112979 | 15000.00 | 1989-10-12
      112979 | 15000.00 | 1989-10-12
(4 rows)
```


## Exercici 18

Mostra el nom, les vendes dels treballadors que tenen assignada una oficina, amb la ciutat i l'objectiu de l'oficina de cada venedor.

*LEFT*

```
SELECT nom, rep_vendes.vendes, ciutat, objectiu 
  FROM rep_vendes 
  JOIN oficines 
    ON oficina = oficina_rep;
```

```
      nom      |  vendes   |   ciutat    | objectiu  
---------------+-----------+-------------+-----------
 Bill Adams    | 367911.00 | Atlanta     | 350000.00
 Mary Jones    | 392725.00 | New York    | 575000.00
 Sue Smith     | 474050.00 | Los Angeles | 725000.00
 Sam Clark     | 299912.00 | New York    | 575000.00
 Bob Smith     | 142594.00 | Chicago     | 800000.00
 Dan Roberts   | 305673.00 | Chicago     | 800000.00
 Larry Fitch   | 361865.00 | Los Angeles | 725000.00
 Paul Cruz     | 286775.00 | Chicago     | 800000.00
 Nancy Angelli | 186042.00 | Denver      | 300000.00
(9 rows)
```


## Exercici 19

Llista el nom de tots els venedors i el del seu director en cas de tenir-ne. El camp que conté el nom del treballador s'ha d'identificar amb "empleado" i el camp que conté el nom del director amb "director".

*LEFT*

```
SELECT a.nom AS empleado, b.nom AS director 
  FROM rep_vendes AS a 
  JOIN rep_vendes AS b 
  ON b.num_empl = a.cap;
```

```
   empleado    |  director   
---------------+-------------
 Bill Adams    | Bob Smith
 Mary Jones    | Sam Clark
 Sue Smith     | Larry Fitch
 Bob Smith     | Sam Clark
 Dan Roberts   | Bob Smith
 Tom Snyder    | Dan Roberts
 Larry Fitch   | Sam Clark
 Paul Cruz     | Bob Smith
 Nancy Angelli | Larry Fitch
(9 rows)
```


## Exercici 20

Llista totes les combinacions possibles de venedors i ciutats.

```
SELECT nom, ciutat 
  FROM rep_vendes 
 CROSS JOIN oficines ;
```

```
      nom      |   ciutat    
---------------+-------------
 Bill Adams    | Denver
 Mary Jones    | Denver
 Sue Smith     | Denver
 Sam Clark     | Denver
 Bob Smith     | Denver
 Dan Roberts   | Denver
 Tom Snyder    | Denver
 Larry Fitch   | Denver
 Paul Cruz     | Denver
 Nancy Angelli | Denver
 Bill Adams    | New York
 Mary Jones    | New York
 Sue Smith     | New York
 Sam Clark     | New York
 Bob Smith     | New York
 Dan Roberts   | New York
 Tom Snyder    | New York
 Larry Fitch   | New York
 Paul Cruz     | New York
 Nancy Angelli | New York
 Bill Adams    | Chicago
 Mary Jones    | Chicago
 Sue Smith     | Chicago
 Sam Clark     | Chicago
 Bob Smith     | Chicago
 Dan Roberts   | Chicago
 Tom Snyder    | Chicago
 Larry Fitch   | Chicago
 Paul Cruz     | Chicago
 Nancy Angelli | Chicago
 Bill Adams    | Atlanta
 Mary Jones    | Atlanta
 Sue Smith     | Atlanta
 Sam Clark     | Atlanta
 Bob Smith     | Atlanta
 Dan Roberts   | Atlanta
 Tom Snyder    | Atlanta
 Larry Fitch   | Atlanta
 Paul Cruz     | Atlanta
 Nancy Angelli | Atlanta
 Bill Adams    | Los Angeles
 Mary Jones    | Los Angeles
 Sue Smith     | Los Angeles
 Sam Clark     | Los Angeles
 Bob Smith     | Los Angeles
 Dan Roberts   | Los Angeles
 Tom Snyder    | Los Angeles
 Larry Fitch   | Los Angeles
 Paul Cruz     | Los Angeles
 Nancy Angelli | Los Angeles
(50 rows)
```


## Exercici 21

Per a cada venedor, mostrar el nom, les vendes i la ciutat de l'oficina en cas de tenir-ne una d'assignada.

*LEFT*

```
SELECT nom, oficines.vendes, ciutat 
  FROM rep_vendes 
  JOIN oficines 
    ON oficina_rep = oficina;
```

```
      nom      |  vendes   |   ciutat    
---------------+-----------+-------------
 Bill Adams    | 367911.00 | Atlanta
 Mary Jones    | 692637.00 | New York
 Sue Smith     | 835915.00 | Los Angeles
 Sam Clark     | 692637.00 | New York
 Bob Smith     | 735042.00 | Chicago
 Dan Roberts   | 735042.00 | Chicago
 Larry Fitch   | 835915.00 | Los Angeles
 Paul Cruz     | 735042.00 | Chicago
 Nancy Angelli | 186042.00 | Denver
(9 rows)
```


## Exercici 22

Mostra les comandes de productes que tenen unes existències inferiors a 10. Llistar el numero de comanda, la data de la comanda, el nom del client que ha fet la comanda, identificador del fabricant i l'identificador de producte de la comanda.

*LEFT comandes*

```
SELECT num_comanda, data, nom, id_fabricant, id_producte 
  FROM productes 
  JOIN comandes 
    ON (id_fabricant, id_producte) = (fabricant, producte) 
  JOIN rep_vendes 
    ON rep = num_empl 
 WHERE estoc < 10;
```

```
 num_comanda |    data    |      nom      | id_fabricant | id_producte 
-------------+------------+---------------+--------------+-------------
      113013 | 1990-01-14 | Larry Fitch   | bic          | 41003
      112997 | 1990-01-08 | Nancy Angelli | bic          | 41003
      113069 | 1990-03-02 | Nancy Angelli | imm          | 775c 
      113048 | 1990-02-10 | Sue Smith     | imm          | 779c 
      113003 | 1990-01-25 | Mary Jones    | imm          | 779c 
(5 rows)
```


## Exercici 23

Llista les 5 comandes amb un import superior. Mostrar l'identificador de la comanda, import de la comanda, preu del producte, nom del client, nom del representant de vendes que va efectuar la comanda i ciutat de l'oficina, en cas de tenir oficina assignada.

*LEFT productes*
*LEFT oficines*

```
SELECT num_comanda, import, preu, empresa, nom, ciutat 
  FROM comandes 
  JOIN productes 
    ON (fabricant, producte) = (id_fabricant, id_producte) 
  JOIN clients 
    ON clie = num_clie 
  JOIN rep_vendes 
    ON rep = num_empl 
  JOIN oficines 
    ON oficina_rep = oficina 
 ORDER BY import DESC 
 LIMIT 5;
```

```
 num_comanda |  import  |  preu   |     empresa     |      nom      |   ciutat    
-------------+----------+---------+-----------------+---------------+-------------
      113045 | 45000.00 | 4500.00 | Zetacorp        | Larry Fitch   | Los Angeles
      112961 | 31500.00 | 4500.00 | J.P. Sinclair   | Sam Clark     | New York
      113069 | 31350.00 | 1425.00 | Chen Associates | Nancy Angelli | Denver
      112987 | 27500.00 | 2750.00 | Acme Mfg.       | Bill Adams    | Atlanta
      113042 | 22500.00 | 4500.00 | Ian & Schmidt   | Dan Roberts   | Chicago
(5 rows)
```


## Exercici 24

Llista les comandes que han estat preses per un representant de vendes que no és l'actual representant de vendes del client pel que s'ha realitzat la comanda. Mostrar el número de comanda, el nom del client, el nom de l'actual representant de vendes del client com a "rep_cliente" i el nom del representant de vendes que va realitzar la comanda com a "rep_pedido".

```
SELECT num_comanda, empresa, a.nom AS rep_cliente, b.nom AS rep_pedido 
  FROM comandes 
  JOIN clients 
    ON clie = num_clie 
  JOIN rep_vendes AS a 
    ON rep_clie = a.num_empl 
  JOIN rep_vendes AS b 
    ON rep = b.num_empl;
 WHERE a.nom != b.nom;
```

```
 num_comanda |     empresa     | rep_cliente |  rep_pedido   
-------------+-----------------+-------------+---------------
      113012 | JCP Inc.        | Paul Cruz   | Bill Adams
      113024 | Orion Corp      | Sue Smith   | Larry Fitch
      113069 | Chen Associates | Paul Cruz   | Nancy Angelli
      113055 | Holm & Landis   | Mary Jones  | Dan Roberts
      113042 | Ian & Schmidt   | Bob Smith   | Dan Roberts
(5 rows)
```


## Exercici 25

Llista les comandes amb un import superior a 5000 i també aquelles comandes realitzades per un client amb un crèdit inferior a 30000. Mostrar l'identificador de la comanda, el nom del client i el nom del representant de vendes que va prendre la comanda.

*LEFT clients*

```
SELECT num_comanda, empresa, nom 
  FROM comandes 
  JOIN clients 
    ON clie = num_clie 
  JOIN rep_vendes 
    ON rep = num_empl 
 WHERE import > 5000 
    OR limit_credit < 30000;
```

```
  num_comanda |      empresa      |      nom      
-------------+-------------------+---------------
      112961 | J.P. Sinclair     | Sam Clark
      110036 | Ace International | Tom Snyder
      113045 | Zetacorp          | Larry Fitch
      113024 | Orion Corp        | Larry Fitch
      112979 | Orion Corp        | Sue Smith
      113069 | Chen Associates   | Nancy Angelli
      113003 | Holm & Landis     | Mary Jones
      112987 | Acme Mfg.         | Bill Adams
      113042 | Ian & Schmidt     | Dan Roberts
(9 rows)
```


