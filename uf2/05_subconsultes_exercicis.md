# Subconsultes

## Exercici 0

Torna a fer l'exercici 4 de les diapositives de teoria de Subconsultes, però ara fent servir GROUP BY i la funció de group STRING_AGG. Recordem que la subconsulta era:

_Mostreu els empleats que treballen a la mateixa oficina i que tenen el mateix càrrec._
```

```
## Exercici 1

Llista els venedors que tinguin una quota igual o inferior a l'objectiu de l'oficina de vendes d'Atlanta.
```
SELECT num_empl, nom 
  FROM rep_vendes 
 WHERE quota <= (SELECT objectiu 
                   FROM oficines 
                  WHERE ciutat LIKE 'Atlanta');
```
```
 num_empl |      nom      
----------+---------------
      105 | Bill Adams
      109 | Mary Jones
      102 | Sue Smith
      106 | Sam Clark
      104 | Bob Smith
      101 | Dan Roberts
      108 | Larry Fitch
      103 | Paul Cruz
      107 | Nancy Angelli
(9 rows)
```
## Exercici 2

Tots els clients, identificador i nom de l'empresa, que han estat atesos per (que han fet comanda amb) Bill Adams.
```
SELECT num_clie, empresa 
  FROM clients 
 WHERE num_clie IN (SELECT clie 
                      FROM comandes 
                     WHERE rep = (SELECT num_empl 
                                    FROM rep_vendes 
                                   WHERE nom LIKE 'Bill Adams'));
```
```
 num_clie |  empresa  
----------+-----------
     2111 | JCP Inc.
     2103 | Acme Mfg.
(2 rows)
```
## Exercici 3

Venedors amb quotes que siguin iguals o superiors a l'objectiu de llur oficina de vendes.
```
SELECT nom, quota 
  FROM rep_vendes 
  JOIN oficines 
    ON oficina_rep = oficina 
 WHERE quota >= objectiu;

SELECT nom, quota 
  FROM rep_vendes 
 WHERE quota >= (SELECT objectiu 
                   FROM oficines 
                  WHERE oficina_rep = oficina);
```
```
      nom      |   quota   
---------------+-----------
 Bill Adams    | 350000.00
 Nancy Angelli | 300000.00
(2 rows)
```
## Exercici 4

Mostrar l'identificador de l'oficina i la ciutat de les oficines on l'objectiu de vendes de l'oficina excedeix la suma de quotes dels venedors d'aquella oficina.
```
SELECT oficina, ciutat 
  FROM oficines 
 WHERE objectiu > (SELECT SUM(quota) 
                     FROM rep_vendes 
                    WHERE oficina = oficina_rep);
```
```
 oficina |   ciutat    
---------+-------------
      12 | Chicago
      21 | Los Angeles
(2 rows)
```
## Exercici 5

Llista dels productes del fabricant amb identificador "aci" que les existències superen les existències del producte amb identificador de producte "41004" i identificador de fabricant "aci".
```
SELECT * 
  FROM productes 
 WHERE id_fabricant LIKE 'aci' 
   AND estoc > (SELECT estoc 
                  FROM productes 
                 WHERE (id_producte = '41004') 
                   AND (id_fabricant LIKE 'aci'));
```
```
 id_fabricant | id_producte |   descripcio    |  preu  | estoc 
--------------+-------------+-----------------+--------+-------
 aci          | 41003       | Article Tipus 3 | 107.00 |   207
 aci          | 41001       | Article Tipus 1 |  55.00 |   277
 aci          | 41002       | Article Tipus 2 |  76.00 |   167
(3 rows)
```
## Exercici 6

Llistar els venedors que han acceptat una comanda que representa més del 10% de la seva quota.
```
SELECT rep, import 
  FROM comandes 
 WHERE import > (SELECT quota * 0.10 
                   FROM rep_vendes 
                  WHERE rep = num_empl);

SELECT DISTINCT num_empl, nom, quota, quota * 0.1
  FROM rep_vendes
  JOIN comandes
    ON rep = num_empl
 WHERE quota * 0.1 <= ANY (SELECT import FROM comandes WHERE rep = num_empl);

```
```
 rep |  import  
-----+----------
 107 | 31350.00
 106 | 31500.00
 108 | 45000.00
(3 rows)
```
## Exercici 7

Llistar el nom i l'edat de totes les persones de l'equip de vendes que no dirigeixen una oficina.
```
SELECT nom, edat 
  FROM rep_vendes 
 WHERE num_empl NOT IN (SELECT director 
                          FROM oficines);
```
```
     nom      | edat 
---------------+------
 Mary Jones    |   31
 Sue Smith     |   48
 Dan Roberts   |   45
 Tom Snyder    |   41
 Paul Cruz     |   29
 Nancy Angelli |   49
(6 rows)
```
## Exercici 8

Llistar aquelles oficines, i els seus objectius, els venedors de les quals tenen unes vendes que superen el 50% de l'objectiu de l'oficina.
```
SELECT oficina, objectiu 
  FROM oficines 
 WHERE objectiu * 0.50 < ALL (SELECT vendes 
                                FROM rep_vendes 
                               WHERE oficina_rep = oficina);
```
```
 oficina | objectiu  
---------+-----------
      22 | 300000.00
      11 | 575000.00
      13 | 350000.00
(3 rows)
```
## Exercici 9

Llistar aquells clients els representants de vendes dels quals estàn assignats a oficines de la regió Est.
```
SELECT num_clie 
  FROM clients 
 WHERE rep_clie IN (SELECT num_empl 
                      FROM rep_vendes 
                     WHERE oficina_rep IN (SELECT oficina 
                                             FROM oficines 
                                            WHERE regio LIKE 'Est'));
```
```
 num_clie 
----------
     2111
     2102
     2103
     2115
     2101
     2121
     2108
     2117
     2122
     2119
     2113
     2109
     2105
(13 rows)
```
## Exercici 10

Llistar els venedors que treballen en oficines que superen el seu objectiu.
```
SELECT num_empl 
  FROM rep_vendes 
 WHERE oficina_rep IN (SELECT oficina 
                         FROM oficines 
                        WHERE vendes > objectiu);
```
```
 num_empl 
----------
      105
      109
      102
      106
      108
(5 rows)
```
## Exercici 11

Llistar els venedors que treballen en oficines que superen el seu objectiu. Mostrar també les següents dades de l'oficina: ciutat i la diferència entre les vendes i l'objectiu. Ordenar el resultat per aquest últim valor. Proposa dues sentències SQL, una amb subconsultes i una sense.
```
SELECT num_empl, (SELECT ciutat 
                    FROM oficines 
                   WHERE oficina_rep = oficina), (SELECT vendes - objectiu 
                                                    FROM oficines 
                                                   WHERE oficina_rep = oficina) 
  FROM rep_vendes 
 WHERE oficina_rep IN (SELECT oficina 
                         FROM oficines 
                        WHERE vendes > objectiu) 
ORDER BY 3;


SELECT num_empl, ciutat, oficines.vendes - objectiu AS "diferencia" 
  FROM rep_vendes 
  JOIN oficines 
    ON oficina_rep = oficina 
 WHERE oficines.vendes > objectiu 
 ORDER BY 3;
```
```
 num_empl |   ciutat    | diferencia 
----------+-------------+------------
      105 | Atlanta     |   17911.00
      102 | Los Angeles |  110915.00
      108 | Los Angeles |  110915.00
      109 | New York    |  117637.00
      106 | New York    |  117637.00
(5 rows)
```
## Exercici 12

Llista els venedors que no treballen en oficines dirigides per Larry Fitch, o que no treballen a cap oficina. Sense usar consultes multitaula.
```
SELECT num_empl, nom 
  FROM rep_vendes 
 WHERE oficina_rep IS NULL 
    OR oficina_rep NOT IN (SELECT oficina 
                             FROM oficines 
                            WHERE director IN (SELECT num_empl 
                                                 FROM rep_vendes 
                                                WHERE nom LIKE 'Larry Fitch'));
```
```
 num_empl |     nom     
----------+-------------
      105 | Bill Adams
      109 | Mary Jones
      106 | Sam Clark
      104 | Bob Smith
      101 | Dan Roberts
      110 | Tom Snyder
      103 | Paul Cruz
(7 rows)
```
## Exercici 13

Llista els venedors que no treballen en oficines dirigides per Larry Fitch, o que no treballen a cap oficina. Mostrant també la ciutat de l'oficina on
treballa l'empleat i l'identificador del cap de la oficina. Proposa dues sentències SQL, una amb subconsultes i una sense.
```
SELECT num_empl, (SELECT ciutat 
                    FROM oficines 
                   WHERE oficina_rep = oficina), (SELECT director 
                                                     FROM oficines 
                                                    WHERE oficina_rep = oficina) 
   FROM rep_vendes  
  WHERE oficina_rep IS NULL 
     OR oficina_rep NOT IN (SELECT oficina 
                              FROM oficines 
                             WHERE director IN (SELECT num_empl 
                                                  FROM rep_vendes 
                                                 WHERE nom LIKE 'Larry Fitch'));

SELECT num_empl, ciutat, director 
   FROM rep_vendes 
   LEFT JOIN oficines 
     ON oficina_rep = oficina 
  WHERE oficina_rep IS NULL 
     OR director != 108;
```
```
 num_empl |  ciutat  | director 
----------+----------+----------
      105 | Atlanta  |      105
      109 | New York |      106
      106 | New York |      106
      104 | Chicago  |      104
      101 | Chicago  |      104
      110 |          |         
      103 | Chicago  |      104
(7 rows)
```
## Exercici 14

Llista tots els clients que han realitzat comandes del productes de la família ACI Widgets entre gener i juny del 1990. Els productes de la famíla ACI Widgets són aquells que tenen identificador de fabricant "aci" i que l'identificador del producte comença per "4100".
```
SELECT clie, fabricant, producte,data 
  FROM comandes 
 WHERE data >= '1990-01-01' 
   AND data < '1990-07-01' 
   AND (fabricant, producte) IN (SELECT id_fabricant, id_producte 
                                   FROM productes 
                                  WHERE id_fabricant = 'aci' 
                                    AND id_producte LIKE '4100%');

SELECT num_clie
  FROM clients
 WHERE num_clie IN (SELECT clie
                      FROM comandes
                     WHERE fabricant = 'aci'
                       AND data >= '1990-01-01'
                       AND data < '1990-07-01'
                       AND producte LIKE '4100%');
```
```
 clie | fabricant | producte |    data    
------+-----------+----------+------------
 2111 | aci       | 41003    | 1990-01-11
 2107 | aci       | 4100z    | 1990-01-30
 2103 | aci       | 41002    | 1990-01-22
 2108 | aci       | 4100x    | 1990-02-15
 2111 | aci       | 4100x    | 1990-02-18
(5 rows)
```
## Exercici 15

Llista els clients que no tenen cap comanda.

```
SELECT num_clie, empresa 
  FROM clients 
 WHERE num_clie NOT IN (SELECT clie 
                          FROM comandes) ;

SELECT 'Client:', empresa 
  FROM clients 
 WHERE num_clie NOT IN (SELECT clie 
                          FROM comandes) 

UNION 

SELECT 'Venedor:', nom 
  FROM rep_vendes 
 WHERE num_empl NOT IN (SELECT rep 
                          FROM comandes);
```
```
 num_clie |     empresa     
----------+-----------------
     2123 | Carter & Sons
     2115 | Smithson Corp.
     2121 | QMA Assoc.
     2122 | Three-Way Lines
     2119 | Solomon Inc.
     2105 | AAA Investments
(6 rows)
```
## Exercici 16

Llista els clients que tenen assignat el venedor que porta més temps contractat.
```
SELECT num_clie, empresa 
  FROM clients 
 WHERE rep_clie IN (SELECT num_empl 
                      FROM rep_vendes
                     WHERE data_contracte IN (SELECT MIN(data_contracte) 
                                                FROM rep_vendes));
```
```
 num_clie |     empresa     
----------+-----------------
     2102 | First Corp.
     2115 | Smithson Corp.
     2105 | AAA Investments
(3 rows)
```
## Exercici 17

Llista els clients assignats a Sue Smith que no han fet cap comanda amb un import superior a 30000. Proposa una sentència SQL sense usar multitaula i una altre en que s'usi multitaula i subconsultes.
```
SELECT num_clie, empresa, rep_clie 
  FROM clients 
 WHERE rep_clie IN (SELECT num_empl 
                      FROM rep_vendes 
                     WHERE nom = 'Sue Smith') 
   AND num_clie NOT IN (SELECT clie 
                          FROM comandes 
                         WHERE import > 30000);

SELECT num_clie, empresa, rep_clie 
  FROM clients 
  JOIN rep_vendes 
    ON rep_clie = num_empl 
 WHERE nom = 'Sue Smith' 
   AND num_clie NOT IN (SELECT clie 
                          FROM comandes 
                         WHERE import > 30000);


SELECT num_clie, empresa, rep_clie, import
FROM clients 
JOIN comandes 
ON num_clie = clie
WHERE rep_clie = ( SELECT num_empl 
                     FROM rep_vendes 
                    WHERE nom = 'Sue Smith') 
AND import < 30000;
```
```
 num_clie |     empresa      | rep_clie 
----------+------------------+----------
     2123 | Carter & Sons    |      102
     2114 | Orion Corp       |      102
     2120 | Rico Enterprises |      102
     2106 | Fred Lewis Corp. |      102
(4 rows)
```
## Exercici 18

Llista l'identificador i el nom dels caps d'empleats que tenen més de 40 anys i que dirigeixen un venedor que té unes vendes superiors a la seva pròpia quota.
```
SELECT DISTINCT(caps.num_empl), caps.nom 
  FROM rep_vendes 
  JOIN rep_vendes AS "caps" 
    ON rep_vendes.cap = caps.num_empl 
 WHERE caps.edat > 40 
   AND rep_vendes.num_empl IN (SELECT num_empl 
                                 FROM rep_vendes 
                                WHERE vendes > quota);

SELECT num_empl, nom
FROM rep_vendes
WHERE num_empl IN (SELECT cap
                    FROM rep_vendes
                    WHERE edat >= 40)
AND vendes > quota;
```
```
 num_empl |     nom     
----------+-------------
      108 | Larry Fitch
      106 | Sam Clark
```
## Exercici 19

Llista d'oficines on hi hagi algun venedor tal que la seva quota representi més del 50% de l'objectiu de l'oficina
```
 SELECT oficina, ciutat 
   FROM oficines 
  WHERE objectiu * 0.50 < ANY (SELECT quota 
                                 FROM rep_vendes 
                                WHERE oficina_rep = oficina);
```
```
 oficina |  ciutat  
---------+----------
      22 | Denver
      11 | New York
      13 | Atlanta
(3 rows)
```
## Exercici 20

Llista d'oficines on tots els venedors tinguin la seva quota superior al 55% de l'objectiu de l'oficina.
```
SELECT oficina, ciutat 
  FROM oficines 
 WHERE objectiu * 0.55 < ALL (SELECT quota 
                                FROM rep_vendes 
                               WHERE oficina_rep = oficina);
```
```
 oficina | ciutat  
---------+---------
      22 | Denver
      13 | Atlanta
(2 rows)
```
## Exercici 21

Transforma el següent JOIN a una comanda amb subconsultes:

```
SELECT num_comanda, import, clie, num_clie, limit_credit
  FROM comandes JOIN clients
    ON clie = num_clie;
```
```
SELECT num_comanda,import,clie,(SELECT num_clie 
                                  FROM clients 
                                 WHERE clie = num_clie),(SELECT limit_credit 
                                                           FROM clients 
                                                          WHERE num_clie = clie) 
 FROM comandes;
```
```
 num_comanda |  import  | clie | num_clie | limit_credit 
-------------+----------+------+----------+--------------
      112961 | 31500.00 | 2117 |     2117 |     35000.00
      113012 |  3745.00 | 2111 |     2111 |     50000.00
      112989 |  1458.00 | 2101 |     2101 |     65000.00
      113051 |  1420.00 | 2118 |     2118 |     60000.00
      112968 |  3978.00 | 2102 |     2102 |     65000.00
      110036 | 22500.00 | 2107 |     2107 |     35000.00
      113045 | 45000.00 | 2112 |     2112 |     50000.00
      112963 |  3276.00 | 2103 |     2103 |     50000.00
      113013 |   652.00 | 2118 |     2118 |     60000.00
      113058 |  1480.00 | 2108 |     2108 |     55000.00
      112997 |   652.00 | 2124 |     2124 |     40000.00
      112983 |   702.00 | 2103 |     2103 |     50000.00
      113024 |  7100.00 | 2114 |     2114 |     20000.00
      113062 |  2430.00 | 2124 |     2124 |     40000.00
      112979 | 15000.00 | 2114 |     2114 |     20000.00
      113027 |  4104.00 | 2103 |     2103 |     50000.00
      113007 |  2925.00 | 2112 |     2112 |     50000.00
      113069 | 31350.00 | 2109 |     2109 |     25000.00
      113034 |   632.00 | 2107 |     2107 |     35000.00
      112992 |   760.00 | 2118 |     2118 |     60000.00
      112975 |  2100.00 | 2111 |     2111 |     50000.00
      113055 |   150.00 | 2108 |     2108 |     55000.00
      113048 |  3750.00 | 2120 |     2120 |     50000.00
      112993 |  1896.00 | 2106 |     2106 |     65000.00
      113065 |  2130.00 | 2106 |     2106 |     65000.00
      113003 |  5625.00 | 2108 |     2108 |     55000.00
      113049 |   776.00 | 2118 |     2118 |     60000.00
      112987 | 27500.00 | 2103 |     2103 |     50000.00
      113057 |   600.00 | 2111 |     2111 |     50000.00
      113042 | 22500.00 | 2113 |     2113 |     20000.00
(30 rows)
```
## Exercici 22

Transforma el següent JOIN a una comanda amb subconsultes:

```
SELECT empl.nom, empl.quota, dir.nom, dir.quota
  FROM rep_vendes AS empl 
  JOIN rep_vendes AS cap
    ON empl.cap = cap.num_empl
 WHERE empl.quota > cap.quota;
```
```
SELECT nom, quota, (SELECT nom 
                      FROM rep_vendes AS "cap" 
                     WHERE empl.cap = cap.num_empl), (SELECT quota 
                                                        FROM rep_vendes AS "cap" 
                                                       WHERE empl.cap = cap.num_empl) 
  FROM rep_vendes AS "empl" 
 WHERE empl.quota > (SELECT quota 
                       FROM rep_vendes AS "cap" 
                      WHERE empl.cap = cap.num_empl);
```
```
     nom     |   quota   |    nom    |   quota   
-------------+-----------+-----------+-----------
 Bill Adams  | 350000.00 | Bob Smith | 200000.00
 Mary Jones  | 300000.00 | Sam Clark | 275000.00
 Dan Roberts | 300000.00 | Bob Smith | 200000.00
 Larry Fitch | 350000.00 | Sam Clark | 275000.00
 Paul Cruz   | 275000.00 | Bob Smith | 200000.00
(5 rows)
```

## Exercici 23

Transforma la següent consulta amb un ANY a una consulta amb un EXISTS i també
en una altre consulta amb un ALL:

```
SELECT oficina
  FROM oficines
 WHERE vendes * 0.8 < ANY 
       (SELECT vendes
          FROM rep_vendes
         WHERE oficina_rep = oficina);
```
```
SELECT oficina 
  FROM oficines 
 WHERE EXISTS (SELECT * 
                 FROM rep_vendes 
                WHERE oficina_rep = oficina 
                  AND rep_vendes.vendes > oficines.vendes * 0.80);

SELECT oficina
  FROM oficines
 WHERE NOT vendes * 0.8 > ALL (SELECT vendes
                             FROM rep_vendes
                            WHERE oficina_rep = oficina);
```
```
 oficina 
---------
      22
      13
(2 rows)
```
## Exercici 24

Transforma la següent consulta amb un ALL a una consutla amb un EXISTS i també
en una altre consulta amb un ANY:

```
SELECT num_clie
  FROM clients
 WHERE limit_credit < ALL
       (SELECT import
          FROM comandes
         WHERE num_clie = clie);
```
```
SELECT num_clie 
  FROM clients 
 WHERE NOT EXISTS (SELECT * 
                     FROM comandes 
                    WHERE num_clie = clie 
                      AND clients.limit_credit > import);

SELECT num_clie
  FROM clients
 WHERE NOT limit_credit > ANY (SELECT import
                                 FROM comandes
                                WHERE num_clie = clie);
```
```
 num_clie 
----------
     2123
     2115
     2121
     2122
     2119
     2113
     2109
     2105
(8 rows)
```
## Exercici 25

Transforma la següent consulta amb un EXISTS a una consulta amb un ALL i també
a una altre consulta amb un ANY:

```
SELECT num_clie, empresa
  FROM clients
 WHERE EXISTS
       (SELECT *
          FROM rep_vendes
         WHERE rep_clie = num_empl
           AND edat BETWEEN 40 AND 50);
```
```
SELECT num_clie, empresa 
  FROM clients 
 WHERE NOT rep_clie != ALL (SELECT num_empl 
                              FROM rep_vendes 
                             WHERE num_empl = rep_clie 
                               AND edat BETWEEN 40 AND 50) ; 

SELECT num_clie, empresa 
  FROM clients 
 WHERE rep_clie = ANY (SELECT num_empl 
                         FROM rep_vendes 
                        WHERE num_empl = rep_clie 
                          AND edat BETWEEN 40 AND 50);
```
```
 num_clie |      empresa      
----------+-------------------
     2102 | First Corp.
     2123 | Carter & Sons
     2107 | Ace International
     2115 | Smithson Corp.
     2114 | Orion Corp
     2124 | Peter Brothers
     2120 | Rico Enterprises
     2106 | Fred Lewis Corp.
     2105 | AAA Investments
(9 rows)
```
## Exercici 26

Transforma la següent consulta amb subconsultes a una consulta sense
subconsultes.

```
SELECT *
  FROM productes
 WHERE id_fabricant IN 
       (SELECT fabricant
          FROM comandes
         WHERE quantitat > 30)
   AND id_producte IN 
       (SELECT producte
          FROM comandes
         WHERE quantitat > 30);
```	 
```
SELECT id_fabricant, id_producte, descripcio, preu 
  FROM productes 
  JOIN comandes 
    ON (id_fabricant, id_producte) = (fabricant, producte) 
 WHERE quantitat > 30;
```
```
 id_fabricant | id_producte |   descripcio    |  preu  | estoc 
--------------+-------------+-----------------+--------+-------
 aci          | 41003       | Article Tipus 3 | 107.00 |   207
 aci          | 41004       | Article Tipus 4 | 117.00 |   139
 aci          | 41002       | Article Tipus 2 |  76.00 |   167
(3 rows)
```
## Exercici 27

Transforma la següent consulta amb subconsultes a una consulta sense
subconsultes.

```
SELECT num_empl, nom
  FROM rep_vendes
 WHERE num_empl = ANY 
       (SELECT rep_clie
          FROM clients
         WHERE empresa LIKE '%Inc.');
```
```
SELECT num_empl, nom 
  FROM rep_vendes 
  JOIN clients 
    ON num_empl = rep_clie 
 WHERE empresa LIKE '%Inc.';
```
```
 num_empl |    nom     
----------+------------
      103 | Paul Cruz
      109 | Mary Jones
(2 rows)
```

## Exercici 28

Transforma la següent consulta amb un IN a una consulta amb un EXISTS i també a
una altre consulta amb un ALL.

```
SELECT num_empl, nom
  FROM rep_vendes
 WHERE num_empl IN
       (SELECT cap
          FROM rep_vendes);
```
```
SELECT num_empl, nom
  FROM rep_vendes
 WHERE EXISTS (SELECT num_empl
                 FROM rep_vendes AS "caps" 
                WHERE rep_vendes.num_empl = caps.cap);

SELECT num_empl, nom
  FROM rep_vendes
 WHERE NOT num_empl != ALL (SELECT cap
                              FROM rep_vendes);
```
```
 num_empl |     nom     
----------+-------------
      106 | Sam Clark
      104 | Bob Smith
      101 | Dan Roberts
      108 | Larry Fitch
(4 rows)
```

## Exercici 29

Modifica la següent consulta perquè mostri la ciutat de l'oficina, proposa una
consulta simplificada.

```
SELECT num_comanda
  FROM comandes
 WHERE rep IN
       (SELECT num_empl
	      FROM rep_vendes
		 WHERE vendes >
		       (SELECT avg(vendes)
			      FROM rep_vendes)
             AND oficina_rep IN
		       (SELECT oficina
                  FROM oficines
                 WHERE regio ILIKE 'est') );
```
```
SELECT num_comanda, ciutat 
  FROM comandes 
  JOIN rep_vendes 
    ON rep = num_empl 
  JOIN oficines 
    ON oficina_rep = oficina 
 WHERE rep_vendes.vendes > (SELECT AVG(vendes) 
                              FROM rep_vendes) 
   AND regio ILIKE 'est';
```
```
 num_comanda 
-------------
      112961
      113012
      112989
      112968
      112963
      113058
      112983
      113027
      113055
      113003
      112987
      113042
(12 rows)
```
## Exercici 30

Transforma la següent consulta amb subconsultes a una consulta amb les mínimes
subconsultes possibles.

```
SELECT num_clie, empresa,
       (SELECT nom
          FROM rep_vendes
         WHERE rep_clie = num_empl) AS rep_nom
  FROM clients
 WHERE rep_clie = ANY
       (SELECT num_empl
          FROM rep_vendes
         WHERE vendes >
               (SELECT MAX(quota)
                  FROM rep_vendes));
```
```
SELECT num_clie, empresa, nom 
  FROM clients 
  JOIN rep_vendes 
    ON rep_clie = num_empl 
 WHERE vendes > (SELECT MAX(quota) 
                   FROM rep_vendes);
```
```
 num_clie |     empresa      |   rep_nom   
----------+------------------+-------------
     2103 | Acme Mfg.        | Bill Adams
     2123 | Carter & Sons    | Sue Smith
     2112 | Zetacorp         | Larry Fitch
     2114 | Orion Corp       | Sue Smith
     2108 | Holm & Landis    | Mary Jones
     2122 | Three-Way Lines  | Bill Adams
     2120 | Rico Enterprises | Sue Smith
     2106 | Fred Lewis Corp. | Sue Smith
     2119 | Solomon Inc.     | Mary Jones
     2118 | Midwest Systems  | Larry Fitch
(10 rows)
```

SELECT descripcio
  FROM productes
 WHERE id_fabricant = 'aci'
   AND EXISTS (SELECT * 
                 FROM productes AS "b" 
                WHERE b.preu > 4500
                  AND b.id_producte = productes.id_producte
                  AND b.id_fabricant = productes.id_fabricant);

SELECT nom 
  FROM rep_vendes 
 WHERE num_empl IN (SELECT cap 
                      FROM rep_vendes AS "a" 
                     WHERE rep_vendes.num_empl = a.cap);
