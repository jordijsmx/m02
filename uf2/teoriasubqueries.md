
1. Subquery independent i monoresultat

SELECT nom
  FROM rep_vendes
 WHERE quota <= ( SELECT MAX(objectiu) 
                    FROM oficines 
                   WHERE regio = 'Est');

2. Subquery dependent i monoresultat

SELECT oficina, ciutat
  FROM oficines
 WHERE objectiu > (SELECT SUM(rep_vendes.quota)
                     FROM rep_vendes
                    WHERE rep_vendes.oficina_rep = oficina.oficina);

3. Subquery independent i multiresultat
   
SELECT empresa
  FROM clients
 WHERE num_clie IN (SELECT DISTINCT clie
                       FROM comandes
                      WHERE fabricant = 'aci'
                        AND producte LIKE '4100%'
                        AND data BETWEEN '1990-01-01' AND '1990-06-30');

SELECT empresa
  FROM clients
 WHERE num_clie NOT IN (SELECT clie 
                          FROM comandes);

4. Subquery dependent i multiresultat

SELECT oficina, ciutat, objectiu
  FROM oficines
 WHERE 0.5 * objectiu < ALL (SELECT vendes
                               FROM rep_vendes
                              WHERE oficina_rep = oficina);

SELECT num_clie
  FROM clients
 WHERE NOT limit_credit >= ANY (SELECT import
                                  FROM comandes
                                 WHERE num_clie = clie);

SELECT num_clie
  FROM clients
 WHERE NOT EXISTS (SELECT * 
                     FROM comandes
                    WHERE num_clie = clie);

SELECT num_clie, empresa
  FROM clients
 WHERE EXISTS (SELECT *
                 FROM rep_vendes
                WHERE rep_clie = num_empl
                  AND edat BETWEEN 40 AND 50);

5. Més d'un subquery al query
   
SELECT rep, (SELECT nom
               FROM rep_vendes c 
              WHERE c.num_empl = rep), SUM(import)
  FROM comandes
 GROUP BY 1,2
HAVING SUM(import) > (SELECT SUM(import)
                        FROM comandes b
                        WHERE b.rep = (SELECT c.num_empl
                                        FROM rep_vendes c
                                        WHERE c.nom = 'Mary Jones'))
  AND SUM(import) < (SELECT SUM(import)
                       FROM comandes b
                      WHERE b.rep = (SELECT c.num_empl
                                       FROM rep_vendes c
                                      WHERE c.nom = 'Dan Roberts'))
 ORDER BY 3 DESC;

 SELECT num_clie, empresa, (SELECT quota 
                              FROM rep_vendes 
                             WHERE rep_clie = num_empl) as c_r, (SELECT COUNT(*) 
                                                                   FROM comandes
                                                                  WHERE clie = num_clie);

6. El normal és fer un query amb JOINs i algun subquery quan cal fer càlculs per grup

SELECT num_comanda, ciutat
  FROM comandes
  JOIN rep_vendes ON rep = num_empl
  JOIN oficines ON oficina_rep = oficina
 WHERE rep_vendes.vendes > (SELECT AVG(vendes)
                              FROM rep_vendes)
   AND regio = 'Est';             