## Exercici 1
Volem saber quantes fàbriques diferents fabriquen els productes d’una secció determinada.Aquesta secció la formen els productes amb un preu no superior a 1000 i no inferior a 100 i que tenen un nom que acaba en ‘or’ o en ‘ra’ i que tenen un identificador de producte que conte el número 7 o el número 8.

```
SELECT COUNT(DISTINCT(id_fabricant)) 
  FROM productes 
 WHERE (preu <= 1000 AND preu >= 100) 
   AND (descripcio LIKE '%or' OR descripcio LIKE '%ra') 
   AND (id_producte LIKE '%7%' OR id_producte LIKE '%8%');
```
```
nomes surt 1

 count 
-------
     2
(1 row)
```

## Exercici 2
Quantes unitats de producte de cada fàbrica ha venut cada representant de ventes? Presenta la sortida ordenada des de qui ha venut més quantitat a qui ha venut menys.

```
SELECT rep, fabricant, SUM(quantitat) 
  FROM comandes 
 GROUP BY rep, fabricant 
 ORDER BY 3 DESC;
```
```
 rep | fab | unitats_venudes 
-----+-----+-----------------
 105 | aci |             134
 101 | aci |              40
 108 | qsa |              26
 103 | aci |              24
 102 | rei |              24
 107 | imm |              22
 108 | aci |              10
 108 | rei |              10
 107 | fea |              10
 109 | fea |              10
 110 | aci |               9
 110 | rei |               8
 106 | rei |               7
 103 | rei |               6
 102 | aci |               6
 102 | qsa |               6
 106 | fea |               6
 101 | rei |               5
 109 | imm |               3
 108 | imm |               3
 102 | imm |               2
 107 | bic |               1
 108 | bic |               1
(23 rows)
```
## Exercici 3
De quines fàbriques s’han venut més de 50 unitats de producte?

```
SELECT fabricant, SUM(quantitat) 
  FROM comandes 
 GROUP BY fabricant 
HAVING SUM(quantitat) > 50 
```
```
 fab | sum
-----+-----
 aci | 223
 rei |  60
(2 rows)
```
## Exercici 4
Quins codis d’oficines tenen 2 o més treballadors-representants_ventas assignats amb una edat inferior a 50 anys?

```
SELECT oficina_rep, COUNT(num_empl) 
  FROM rep_vendes 
 WHERE edat < 50 
 GROUP BY oficina_rep 
HAVING COUNT(num_empl) >= 2;
```
```
 oficina_rep | count 
-------------+-------
          12 |     3
(1 row)
```

## Exercici 5
Quin és el codi de director de representant de ventes que és director de menys representants?
    
```
SELECT cap, COUNT(num_empl) 
  FROM rep_vendes 
 WHERE cap IS NOT NULL 
 GROUP BY cap 
 LIMIT 1;
```
```
 director | count
----------+-------
      101 |     1
(1 row)
```

## Exercici 6
Quins són els codis dels clients que ens han fet més d’una comanda i que amb totes les comandes que han fet han pagat més de 30000?

```
SELECT clie, COUNT(num_comanda), SUM(import) 
  FROM comandes 
 GROUP BY clie 
HAVING COUNT(num_comanda) > 1 
   AND SUM(import) > 30000; 
```
```
 clie | count |   sum
------+-------+----------
 2112 |     2 | 47925.00
 2103 |     4 | 35582.00
(2 rows)
```

## Exercici 7
Quins són els codis de representants de ventes amb un codi no superior a 110 que estan assignats a clients amb un nom d’empresa que comenci per S o per A.

```
SELECT DISTINCT(rep_clie) 
  FROM clients 
 WHERE rep_clie <= 110 
   AND (empresa LIKE 'S%' OR empresa LIKE 'A%');
```
```
 rep_clie 
----------
      101
      105
      109
      110
(4 rows)
```
## Exercici 8
Mostrar els dos identificadors dels productes de les fàbriques ací i rei que s’han venut més d’una vegada 

```
SELECT fabricant, producte, COUNT(num_comanda) 
  FROM comandes 
 WHERE fabricant = 'aci' 
    OR fabricant = 'rei' 
 GROUP BY fabricant, producte 
HAVING COUNT(num_comanda) > 1;
```
```
 fab | producto | count
-----+----------+-------
 aci | 41002    |     2
 aci | 41004    |     3
 aci | 4100x    |     2
 aci | 4100z    |     2
 rei | 2a44r    |     2
 rei | 2a45c    |     2
(6 rows)
```
## Exercici 9
De quins productes n’hem venut en total més de 60 unitats? Mostrar, per cada producte diferent, el total d’unitats venudes, el total d’import facturar per cada producte i el número de comandes on s’ha venut.

```
SELECT fabricant, producte, SUM(quantitat), SUM(import), COUNT(num_comanda) 
  FROM comandes 
 GROUP BY fabricant, producte 
HAVING SUM(quantitat) > 60;
```
```
 fab | producto | unitats_venudes | total_import | numero_comandes
-----+----------+-----------------+--------------+-----------------
 aci | 41002    |              64 |      4864.00 |               2
 aci | 41004    |              68 |      7956.00 |               3
(2 rows)
```
## Exercici 10
Si només tenim en compte les comandes on el 20% de l’import és superior a 2000, quin són els 3 millor clients que tenim (els 3 clients que ens han fet un total d’import més alt d’aquestes comandes)

```
SELECT clie, import 
  FROM comandes 
 WHERE import * 0.20 > 2000 
 ORDER BY 2 DESC 
 LIMIT 3;
```
```
 clie |   sum    
------+----------
 2112 | 45000.00
 2117 | 31500.00
 2109 | 31350.00
(3 rows)
```