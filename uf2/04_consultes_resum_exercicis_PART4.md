# Consultes resum (PART IV)

## Exercici 1

Visualitza l'import total i el número de comandes per producte que s'han fet dels productes que tenen un codi que comneça per 4 i acaba en 2, en 3 o en 4 i dels quals s'han fet 3 o més comandes.
```
SELECT SUM(import), COUNT(num_comanda), producte 
  FROM comandes 
  JOIN productes 
    ON (id_fabricant, id_producte) = (fabricant, producte) 
 WHERE producte LIKE '4%2' 
    OR producte LIKE '4%3' 
    OR producte LIKE '4%4' 
 GROUP BY producte, fabricant 
HAVING COUNT(num_comanda) >= 3;
```
```
   sum   | count | producte 
---------+-------+----------
 7956.00 |     3 | 41004
(1 row)
```
>Visualitza l'import total i el número de comandes per producte que s'han fet dels productes que tenen un codi que conté un 4 i acaba en 2, en 3 o en 7**
```
SELECT SUM(import), COUNT(num_comanda), producte 
  FROM comandes 
  LEFT JOIN productes 
    ON (id_fabricant, id_producte) = (fabricant, producte) 
 WHERE producte LIKE '%4%' 
   AND (producte LIKE '%2' 
    OR producte LIKE '%3' 
    OR producte LIKE '%7') 
 GROUP BY producte, fabricant;
```
```
   sum    | count | producte 
----------+-------+----------
  3745.00 |     1 | 41003
  1304.00 |     2 | 41003
 10006.00 |     3 | xk47
  4864.00 |     2 | 41002
  1420.00 |     1 | k47
(5 rows)
```
>Visualitza l'import total i el número de comandes per producte que s'han fet dels productes que tenen un codi que conté un 4 i amés amés, un codi que acaba en 2 de la fàbrica 'aci' o un codi que acaba en 3 de al fàbrica 'bic' o un codi que acaba en 7.
```
SELECT SUM(import), COUNT(num_comanda), producte 
  FROM comandes 
  LEFT JOIN productes 
    ON (id_fabricant, id_producte) = (fabricant, producte) 
 WHERE producte LIKE '%4%' 
    AND (producte LIKE '%2' AND fabricant LIKE 'aci') 
    OR (producte LIKE '%3' AND fabricant LIKE 'bic') 
    OR producte LIKE '%7' 
 GROUP BY producte, fabricant;
```
```
   sum    | count | producte 
----------+-------+----------
  1304.00 |     2 | 41003
 10006.00 |     3 | xk47
  4864.00 |     2 | 41002
  1420.00 |     1 | k47
(4 rows)
```

## Exercici 2

Volem saber quants clients diferents ens han fet comandes de més de 7 unitats de productes de les fàbriques aci i rei
```
SELECT COUNT(DISTINCT(clie)) 
  FROM comandes 
 WHERE (fabricant LIKE 'rei' 
    OR fabricant LIKE 'aci') 
   AND quantitat > 7;
```
```
 count 
-------
     7
(1 row)
```
## Exercici 3

Estem interessats en els clients diferents ens han fet comandes de més de 7 unitats de productes de les fàbriques aci i rei. Volem saber quins codis de clients ens han fet dues o més comandes d'aquestes característiques.
```
SELECT clie, COUNT(*) 
  FROM comandes 
 WHERE (fabricant LIKE 'rei' 
    OR fabricant LIKE 'aci') 
   AND quantitat > 7 
 GROUP BY clie 
HAVING COUNT(num_comanda) >= 2;
```
```
 clie | count 
------+-------
 2103 |     3
 2107 |     2
 2111 |     2
(3 rows)
```
## Exercici 4

Volem saber quin fabricant és el que té més unitats dels seus productes als nostres magatzems. Visualitza el codi de la fàbrica i el total d'unitats dels seus productes que tenim.
```
SELECT id_fabricant, SUM(estoc)
  FROM productes 
 GROUP BY id_fabricant 
 ORDER BY 2 DESC
 LIMIT 1;
```
```
 id_fabricant | sum 
--------------+-----
 aci          | 880
(1 row)
```
## Exercici 5

Entre els nostres productes n'hi ha que tenen el mateix nom(descripcio). Visualitza la descripcio d'aquests productes i el preu del més car.
```
SELECT descripcio, MAX(preu) 
  FROM productes 
 GROUP BY descripcio 
HAVING COUNT(descripcio) >= 2;
```
```
 descripcio |  max   
------------+--------
 Reductor   | 355.00
(1 row)
```
## Exercici 6

Volem saber a quines oficines treballen venedors amb un cognom que comenci per A o per S I un nom que comenci amb T o amb S o amb B o amb N. També volem saber quants treballadors d'aquestes característiqes hi treballen I quina és la suma de les seves quotes per oficina. No volem que es visualitzi el Tom Snyder perquè ell no pertany a cap oficina en particular.
```
SELECT ciutat, COUNT(nom), SUM(quota) 
  FROM rep_vendes
  LEFT JOIN oficines
    ON oficina_rep = oficina 
 WHERE (nom LIKE '% A%' OR nom LIKE '% S%') 
   AND (nom LIKE 'T%' OR nom LIKE 'S%' OR nom LIKE 'B%' OR nom LIKE 'N%') 
    OR oficina_rep IS NULL 
 GROUP BY oficina;
```
```
   ciutat    | count |    sum    
-------------+-------+-----------
 Chicago     |     1 | 200000.00
 Atlanta     |     1 | 350000.00
 Los Angeles |     1 | 350000.00
 Denver      |     1 | 300000.00
             |     1 |          
(5 rows)
```
## Exercici 7

Quins són els codis dels representants que tenen assignats més de 2 clients amb un nom d'empresa que contingui la lletra 'o' i la lletra 'i'?
```
SELECT rep_clie 
  FROM clients 
 WHERE empresa LIKE '%o%' 
   AND empresa LIKE '%i%' 
 GROUP BY rep_clie 
HAVING COUNT(empresa) > 2;
```
```
 rep_clie 
----------
      102
(1 row)
```
## Exercici 8

Quins codis de clients ens han comprat 3 o més vegades?
```
SELECT clie, COUNT(clie) 
  FROM comandes 
 GROUP BY clie 
HAVING COUNT(clie) >= 3; 
```
```
 clie | count 
------+-------
 2108 |     3
 2103 |     4
 2118 |     4
 2111 |     3
(4 rows)
```
## Exercici 9

Quins codis de clients ens han comprat més d'una vegada el mateix producte?
```
SELECT clie, fabricant, producte, COUNT (*) 
  FROM comandes 
 GROUP BY producte, fabricant, clie 
HAVING COUNT(producte) > 1;
```
```
 clie | fabricant | producte | count 
------+-----------+----------+-------
 2103 | aci       | 41004    |     2
(1 row)
```
## Exercici 10

D'entre els productes que s'han venut més d'una vegada, quin és els que ens ha donat un total d'import més baix? Visualitza el codi del producta, la quantitat de cops que s'ha venut I el total d'import que s'ha cobrat per aquest producte.
```
SELECT fabricant, producte, COUNT(producte), SUM(import) 
  FROM comandes 
 GROUP BY fabricant, producte 
HAVING COUNT(*) > 1 
 ORDER BY 4 
 LIMIT 1;
```
```
 fabricant | producte | count |  sum   
-----------+----------+-------+--------
 aci       | 4100x    |     2 | 750.00
(1 row)
```  
## Exercici 11
    
Per cada venedor (o treballador o reprentant de ventes), mostrar l'identificador del venedor i un camp anomenat "preu0". El camp "preu0" ha de contenir el preu del producte més car que ha venut. 
```
SELECT rep, MAX(preu) AS "preu0" 
  FROM productes 
  JOIN comandes 
    ON (id_fabricant, id_producte) = (fabricant, producte) 
 GROUP BY rep 
 ORDER BY 1;
```
```
 rep |  preu0  
-----+---------
 101 | 4500.00
 102 | 2500.00
 103 |  350.00
 105 | 2750.00
 106 | 4500.00
 107 | 1425.00
 108 | 4500.00
 109 | 1875.00
 110 | 2500.00
(9 rows)
```
## Exercici 12

Mostrar l'identificador del fabricant i un camp anomenat "m_preu". El camp "m_preu" ha de mostrar la mitjana del preu dels productes de cada fabricant. 
```
SELECT id_fabricant, CAST(AVG(preu) AS NUMERIC(6,2)) AS "m_preu" 
  FROM productes 
 GROUP BY id_fabricant;
```
```
 id_fabricant | m_preu  
--------------+---------
 imm          |  842.33
 aci          |  804.29
 bic          |  352.33
 fea          |  195.50
 qsa          |  202.00
 rei          | 2357.25
(6 rows)
```
## Exercici 13

Per cada client que ha fet alguna compra, mostrar l'identificador del client i un camp anomenat "comandes_fetes". El camp "comandes_fetes" ha de mostrar quantes comandes ha fet cada client. 
```
 SELECT clie, COUNT(num_comanda) AS "comandes_fetes" 
   FROM comandes 
  GROUP BY clie 
  ORDER BY 1;
```
```
 clie | comandes_fetes 
------+----------------
 2101 |              1
 2102 |              1
 2103 |              4
 2106 |              2
 2107 |              2
 2108 |              3
 2109 |              1
 2111 |              3
 2112 |              2
 2113 |              1
 2114 |              2
 2117 |              1
 2118 |              4
 2120 |              1
 2124 |              2
(15 rows)
```
## Exercici 14

Per cada client mostrar l'identificador. Només mostrar aquells clients que la suma dels imports de les seves comandes sigui menor al limit de crèdit. 
```
 SELECT clie, SUM(import), limit_credit 
   FROM comandes 
   JOIN clients 
     ON clie = num_clie 
  GROUP BY clie, limit_credit 
 HAVING SUM(import) < limit_credit;
```
```
 clie |   sum    | limit_credit 
------+----------+--------------
 2102 |  3978.00 |     65000.00
 2118 |  3608.00 |     60000.00
 2107 | 23132.00 |     35000.00
 2101 |  1458.00 |     65000.00
 2124 |  3082.00 |     40000.00
 2103 | 35582.00 |     50000.00
 2112 | 47925.00 |     50000.00
 2106 |  4026.00 |     65000.00
 2108 |  7255.00 |     55000.00
 2111 |  6445.00 |     50000.00
 2120 |  3750.00 |     50000.00
 2117 | 31500.00 |     35000.00
(12 rows)
```
>Mostrar el nom de l'empresa-client, el nom del representant de vendes que ha fet la comanda i el nom del representant de vendes que l'empresa-client té assignat.
```
SELECT empresa, SUM(import), limit_credit, comanda.nom, assignat.nom  
   FROM comandes 
   JOIN clients 
     ON clie = num_clie 
   JOIN rep_vendes AS "comanda" 
     ON rep = comanda.num_empl 
   JOIN rep_vendes AS "assignat" 
     ON rep_clie = assignat.num_empl 
  GROUP BY empresa, limit_credit, comanda.nom, assignat.nom 
 HAVING SUM(import) < limit_credit;
```
```
      empresa      |   sum    | limit_credit |      nom      |      nom      
-------------------+----------+--------------+---------------+---------------
 Zetacorp          | 47925.00 |     50000.00 | Larry Fitch   | Larry Fitch
 Acme Mfg.         | 35582.00 |     50000.00 | Bill Adams    | Bill Adams
 Orion Corp        |  7100.00 |     20000.00 | Larry Fitch   | Sue Smith
 Midwest Systems   |  3608.00 |     60000.00 | Larry Fitch   | Larry Fitch
 Orion Corp        | 15000.00 |     20000.00 | Sue Smith     | Sue Smith
 Peter Brothers    |  3082.00 |     40000.00 | Nancy Angelli | Nancy Angelli
 JCP Inc.          |  2700.00 |     50000.00 | Paul Cruz     | Paul Cruz
 First Corp.       |  3978.00 |     65000.00 | Dan Roberts   | Dan Roberts
 JCP Inc.          |  3745.00 |     50000.00 | Bill Adams    | Paul Cruz
 Rico Enterprises  |  3750.00 |     50000.00 | Sue Smith     | Sue Smith
 Fred Lewis Corp.  |  4026.00 |     65000.00 | Sue Smith     | Sue Smith
 J.P. Sinclair     | 31500.00 |     35000.00 | Sam Clark     | Sam Clark
 Holm & Landis     |  7105.00 |     55000.00 | Mary Jones    | Mary Jones
 Jones Mfg.        |  1458.00 |     65000.00 | Sam Clark     | Sam Clark
 Ace International | 23132.00 |     35000.00 | Tom Snyder    | Tom Snyder
 Holm & Landis     |   150.00 |     55000.00 | Dan Roberts   | Mary Jones
(16 rows)
```
## Exercici 15

Mostrar l'identificador i la ciutat de les oficines i dos camps més, un anomenat "credit1" i l'altre "credit2". Per a cada oficina, el camp "credit1" ha de mostrar el límit de crèdit més petit d'entre tots els clients que el seu representant de vendes treballa a l'oficina. El camp "credit2" ha de ser el mateix però pel límit de crèdit més gran. 
```
SELECT oficina, ciutat, MIN(limit_credit) AS "credit1", MAX(limit_credit) AS "credit2" 
  FROM rep_vendes 
  LEFT JOIN oficines
    ON oficina_rep = oficina
  LEFT JOIN clients
    ON rep_clie = num_empl 
 GROUP BY oficina;
```
```
 oficina |   ciutat    | credit1  | credit2  
---------+-------------+----------+----------
         |             | 35000.00 | 35000.00
      22 | Denver      | 40000.00 | 40000.00
      13 | Atlanta     | 30000.00 | 50000.00
      11 | New York    | 25000.00 | 65000.00
      21 | Los Angeles | 20000.00 | 65000.00
      12 | Chicago     | 20000.00 | 65000.00
(6 rows)
```
## Exercici 16

Per cada venedor i cadascun dels seus clients, mostrar l'identificador del venedor, l'identificador del client i un camp anomenat "import_m". El camp "impore_m" ha de mostrar l'import mig de les comandes que ha realitzat cada venedor a cada client diferent. 
```
SELECT rep, clie, CAST(AVG(import) AS NUMERIC(7,2)) 
  FROM rep_vendes
  JOIN comandes
    ON num_empl = rep
  JOIN clients
    ON clie = num_clie
 GROUP BY rep, clie 
 ORDER BY 1;
```
```
 rep | clie |   avg    
-----+------+----------
 101 | 2108 |   150.00
 101 | 2113 | 22500.00
 101 | 2102 |  3978.00
 102 | 2114 | 15000.00
 102 | 2106 |  2013.00
 102 | 2120 |  3750.00
 103 | 2111 |  1350.00
 105 | 2111 |  3745.00
 105 | 2103 |  8895.50
 106 | 2101 |  1458.00
 106 | 2117 | 31500.00
 107 | 2124 |  1541.00
 107 | 2109 | 31350.00
 108 | 2112 | 23962.50
 108 | 2114 |  7100.00
 108 | 2118 |   902.00
 109 | 2108 |  3552.50
 110 | 2107 | 11566.00
(18 rows)
``` 

>Mostrar el nom de la ciutat també:
```
SELECT ciutat, rep, clie, CAST(AVG(import) AS NUMERIC(7,2)) 
  FROM rep_vendes
  JOIN comandes
    ON num_empl = rep
  JOIN clients
    ON clie = num_clie 
  LEFT JOIN oficines 
    ON oficina_rep = oficina
 GROUP BY rep, clie, ciutat 
 ORDER BY 1;
```
## Exercici 17

Per cada comanda que tenim, visualitzar la regió de l'oficina del representant de ventes que l'ha fet,  el nom del representant que l'ha fet, l'import i el nom de l'empresa que ha comprat el producte. Mostrar-ho ordenat pels 2 primers camps demanats.
```
SELECT regio, nom, import, empresa 
  FROM oficines 
  JOIN rep_vendes 
    ON oficina = oficina_rep 
  JOIN comandes 
    ON rep = num_empl 
  JOIN clients 
    ON clie = num_clie 
 ORDER BY 1,2;
```
```
 regio |      nom      |  import  |     empresa      
-------+---------------+----------+------------------
 Est   | Bill Adams    |   702.00 | Acme Mfg.
 Est   | Bill Adams    |  3276.00 | Acme Mfg.
 Est   | Bill Adams    | 27500.00 | Acme Mfg.
 Est   | Bill Adams    |  4104.00 | Acme Mfg.
 Est   | Bill Adams    |  3745.00 | JCP Inc.
 Est   | Dan Roberts   | 22500.00 | Ian & Schmidt
 Est   | Dan Roberts   |  3978.00 | First Corp.
 Est   | Dan Roberts   |   150.00 | Holm & Landis
 Est   | Mary Jones    |  1480.00 | Holm & Landis
 Est   | Mary Jones    |  5625.00 | Holm & Landis
 Est   | Paul Cruz     |  2100.00 | JCP Inc.
 Est   | Paul Cruz     |   600.00 | JCP Inc.
 Est   | Sam Clark     | 31500.00 | J.P. Sinclair
 Est   | Sam Clark     |  1458.00 | Jones Mfg.
 Oest  | Larry Fitch   |  1420.00 | Midwest Systems
 Oest  | Larry Fitch   |  2925.00 | Zetacorp
 Oest  | Larry Fitch   |  7100.00 | Orion Corp
 Oest  | Larry Fitch   |   760.00 | Midwest Systems
 Oest  | Larry Fitch   | 45000.00 | Zetacorp
 Oest  | Larry Fitch   |   652.00 | Midwest Systems
 Oest  | Larry Fitch   |   776.00 | Midwest Systems
 Oest  | Nancy Angelli |  2430.00 | Peter Brothers
 Oest  | Nancy Angelli | 31350.00 | Chen Associates
 Oest  | Nancy Angelli |   652.00 | Peter Brothers
 Oest  | Sue Smith     |  2130.00 | Fred Lewis Corp.
 Oest  | Sue Smith     |  1896.00 | Fred Lewis Corp.
 Oest  | Sue Smith     |  3750.00 | Rico Enterprises
 Oest  | Sue Smith     | 15000.00 | Orion Corp
(28 rows)
```
## Exercici 18

Mostrar la quantitat de comandes que cada representant ha fet per client i l'import total de comandes que cada venedor ha venut a cada client.  Mostrar la ciutat de l'oficina del representant de ventes, el nom del representant,  el nom del client, la quantitat de comandes representant-client i  el total de l'import de les seves comandes per client.  Ordenar resultats per ciutat, representant i client.
```
SELECT ciutat, nom, empresa, COUNT(num_comanda), SUM(import) 
  FROM oficines 
  JOIN rep_vendes 
    ON oficina = oficina_rep 
  JOIN clients 
    ON num_empl = rep_clie 
  JOIN comandes 
    ON num_clie = clie 
 GROUP BY rep, clie, ciutat, nom, empresa
 ORDER BY 1,2,3;
```
```
   ciutat    |      nom      |     empresa      | count |   sum    
-------------+---------------+------------------+-------+----------
 Atlanta     | Bill Adams    | Acme Mfg.        |     4 | 35582.00
 Chicago     | Bob Smith     | Ian & Schmidt    |     1 | 22500.00
 Chicago     | Dan Roberts   | First Corp.      |     1 |  3978.00
 Chicago     | Paul Cruz     | Chen Associates  |     1 | 31350.00
 Chicago     | Paul Cruz     | JCP Inc.         |     3 |  6445.00
 Denver      | Nancy Angelli | Peter Brothers   |     2 |  3082.00
 Los Angeles | Larry Fitch   | Midwest Systems  |     4 |  3608.00
 Los Angeles | Larry Fitch   | Zetacorp         |     2 | 47925.00
 Los Angeles | Sue Smith     | Fred Lewis Corp. |     2 |  4026.00
 Los Angeles | Sue Smith     | Orion Corp       |     2 | 22100.00
 Los Angeles | Sue Smith     | Rico Enterprises |     1 |  3750.00
 New York    | Mary Jones    | Holm & Landis    |     3 |  7255.00
 New York    | Sam Clark     | Jones Mfg.       |     1 |  1458.00
 New York    | Sam Clark     | J.P. Sinclair    |     1 | 31500.00
(14 rows)
```
## Exercici 19

Mostrar la quantitat de representants de ventes que hi ha a la regió Est, la quantitat de  representants de ventes que hi ha a la regió Oest i la quantitat de representants que no estan assignats a cap regió.  Només es vol comptar als representants que no tenen ciutat assignada o que tenen un 'N' minúscula o majúscula al nom de la seva ciutat  i tenen una 'm' o una 'n'  minúscula o majúscula al seu nom.
```
SELECT regio, COUNT(num_empl) 
  FROM oficines 
 RIGHT JOIN rep_vendes 
    ON oficina = oficina_rep 
 WHERE ciutat ILIKE '%n%' 
   AND (nom ILIKE '%n%' OR nom ILIKE '%m%')
    OR regio IS NULL
 GROUP BY regio;
```
```
 regio | count 
-------+-------
       |     1
 Est   |     3
 Oest  |     2
(3 rows)
```
## Exercici 20

Mostrar la quantitat de representants de ventes que hi ha a les diferents ciutats-oficines i la quantitat de clients que tenen associats als representants d''aquestes ciutats-oficines. Mostrar la regió, la ciutat, el número de representants i el número de clients.
```
SELECT regio, ciutat, COUNT(DISTINCT(num_empl)), COUNT(num_clie) 
  FROM oficines 
  JOIN rep_vendes 
    ON oficina_rep = oficina 
  JOIN clients 
    ON num_empl = rep_clie 
 GROUP BY regio, ciutat;
```
```
 regio |   ciutat    | count | count 
-------+-------------+-------+-------
 Est   | Atlanta     |     1 |     2
 Est   | Chicago     |     3 |     7
 Est   | New York    |     2 |     4
 Oest  | Denver      |     1 |     1
 Oest  | Los Angeles |     2 |     6
(5 rows)
``` 
## Exercici 21

Mostrar els noms de les empreses que han comprat productes de les fàbriques imm i rei amb un preu de catàleg (no de compra) inferior a 80 o superior a 1000. Mostrar l'empresa' el fabricant, el codi de producte, el preu, el nom del representant de ventes que ha fet la comanda i el nom del seu director.
```
SELECT DISTINCT empresa, fabricant, producte, preu
  FROM productes 
  JOIN comandes 
    ON id_fabricant = fabricant AND id_producte = producte
  JOIN clients 
    ON clie = num_clie
 WHERE (fabricant = 'imm' OR fabricant = 'rei') 
   AND (preu < 80 OR preu > 1000);
```

```
SELECT empresa, fabricant, producte, preu, rep.nom, cap.nom 
  FROM clients 
  JOIN comandes 
    ON num_clie = clie 
  JOIN productes 
    ON (fabricant, producte) = (id_fabricant, id_producte) 
  JOIN rep_vendes AS "rep" 
    ON num_empl = rep 
  LEFT JOIN rep_vendes AS "cap" 
    ON cap.num_empl = rep.cap 
 WHERE (id_fabricant LIKE 'imm' OR id_fabricant LIKE 'rei') 
   AND (preu < 80 OR preu > 1000);
```
```
      empresa      | fabricant | producte |  preu   |      nom      |     nom     
-------------------+-----------+----------+---------+---------------+-------------
 J.P. Sinclair     | rei       | 2a44l    | 4500.00 | Sam Clark     | 
 Zetacorp          | rei       | 2a44r    | 4500.00 | Larry Fitch   | Sam Clark
 Chen Associates   | imm       | 775c     | 1425.00 | Nancy Angelli | Larry Fitch
 Ace International | rei       | 2a45c    |   79.00 | Tom Snyder    | Dan Roberts
 Rico Enterprises  | imm       | 779c     | 1875.00 | Sue Smith     | Larry Fitch
 Fred Lewis Corp.  | rei       | 2a45c    |   79.00 | Sue Smith     | Larry Fitch
 Holm & Landis     | imm       | 779c     | 1875.00 | Mary Jones    | Sam Clark
 Ian & Schmidt     | rei       | 2a44r    | 4500.00 | Dan Roberts   | Bob Smith
(8 rows)
```

>El mateix, sense condició i mostrant el númer de comandes que ha fet cada empresa i el número de fabricants diferents que ha comprat cada empresa.

```
SELECT empresa, COUNT(num_comanda) AS "num comandes", COUNT(DISTINCT(fabricant)) AS "num fabr diferents"
  FROM productes 
  JOIN comandes 
    ON id_fabricant = fabricant AND id_producte = producte
  JOIN clients 
    ON clie = num_clie 
 GROUP BY 1;
```

## Exercici 22

Mostrar els venedors amb un nom que no comenci ni per I ni per J que entre totes les seves ventes ens han comprat per un total superior a 25.000.
```
SELECT nom, SUM(import) AS suma_import
  FROM comandes
  JOIN rep_vendes 
    ON rep = num_empl             
 WHERE nom NOT LIKE 'I%' 
   AND nom NOT LIKE 'J%' 
 GROUP BY nom 
HAVING SUM(import) > 25000;
```
```
      nom      | suma_import 
---------------+-------------
 Larry Fitch   |    58633.00
 Nancy Angelli |    34432.00
 Bill Adams    |    39327.00
 Dan Roberts   |    26628.00
 Sam Clark     |    32958.00
(5 rows)
```
## Exercici 23

Mostrar les comandes que els clients han fet a representants de ventes que no són el que tenen assignat. Mostrar l'import de la comanda, el nom del client, el nom del representant de ventes que ha fet la comanda, la seva oficina si en té, el nom del representant de ventes que el client té assignat i la seva oficina si en té.
```
SELECT import, empresa, rep_vendes.nom, rep_vendes.oficina_rep, rep.nom, rep.oficina_rep 
  FROM comandes 
  JOIN clients 
    ON num_clie = clie 
  JOIN rep_vendes 
    ON rep = num_empl 
  JOIN oficines 
    ON oficina_rep = oficina 
  JOIN rep_vendes AS "rep" 
    ON rep_clie = rep.num_empl 
  JOIN oficines AS "rep2" 
    ON rep.oficina_rep = rep2.oficina 
 WHERE rep != rep_clie;
```
```
  import  |     empresa     |      nom      | oficina_rep |    nom     | oficina_rep 
----------+-----------------+---------------+-------------+------------+-------------
  3745.00 | JCP Inc.        | Bill Adams    |          13 | Paul Cruz  |          12
  7100.00 | Orion Corp      | Larry Fitch   |          21 | Sue Smith  |          21
 31350.00 | Chen Associates | Nancy Angelli |          22 | Paul Cruz  |          12
   150.00 | Holm & Landis   | Dan Roberts   |          12 | Mary Jones |          11
 22500.00 | Ian & Schmidt   | Dan Roberts   |          12 | Bob Smith  |          12
(5 rows)
```
## Exercici 24

Mostrar tots els productes del catàleg de productes de l'empresa, amb tots els seus camps, ordenats de menor a major preu, el total d'unitats que se n'han venut i quants representants diferents els han venut.
```
SELECT id_fabricant, id_producte, descripcio, preu, estoc, SUM(quantitat), COUNT(DISTINCT(rep)) 
  FROM productes 
  LEFT JOIN comandes 
    ON (fabricant, producte) = (id_fabricant, id_producte) 
 GROUP BY id_fabricant, id_producte 
 ORDER BY preu ASC;
```
```
 id_fabricant | id_producte |     descripcio     |  preu   | estoc | sum | count 
--------------+-------------+--------------------+---------+-------+-----+-------
 aci          | 4100x       | Peu de rei         |   25.00 |    37 |  30 |     2
 imm          | 887h        | Suport Riosta      |   54.00 |   223 |     |     0
 aci          | 41001       | Article Tipus 1    |   55.00 |   277 |     |     0
 aci          | 41002       | Article Tipus 2    |   76.00 |   167 |  64 |     2
 rei          | 2a45c       | V Stago Trinquet   |   79.00 |   210 |  32 |     2
 aci          | 41003       | Article Tipus 3    |  107.00 |   207 |  35 |     1
 aci          | 41004       | Article Tipus 4    |  117.00 |   139 |  68 |     2
 qsa          | xk48a       | Reductor           |  117.00 |    37 |     |     0
 qsa          | xk48        | Reductor           |  134.00 |   203 |     |     0
 fea          | 112         | Coberta            |  148.00 |   115 |  10 |     1
 bic          | 41672       | Plate              |  180.00 |     0 |     |     0
 bic          | 41089       | Retn               |  225.00 |    78 |     |     0
 fea          | 114         | Bancada Motor      |  243.00 |    15 |  16 |     2
 imm          | 887p        | Pern Riosta        |  250.00 |    24 |     |     0
 rei          | 2a44g       | Passador Frontissa |  350.00 |    14 |   6 |     1
 qsa          | xk47        | Reductor           |  355.00 |    38 |  28 |     2
 imm          | 887x        | Retenidor Riosta   |  475.00 |    32 |     |     0
 bic          | 41003       | Manovella          |  652.00 |     3 |   2 |     2
 imm          | 773c        | Riosta 1/2-Tm      |  975.00 |    28 |   3 |     1
 imm          | 775c        | Riosta 1-Tm        | 1425.00 |     5 |  22 |     1
 imm          | 779c        | Riosta 2-Tm        | 1875.00 |     9 |   5 |     2
 aci          | 4100z       | Muntador           | 2500.00 |    28 |  15 |     2
 aci          | 4100y       | Extractor          | 2750.00 |    25 |  11 |     1
 rei          | 2a44l       | Frontissa Esq.     | 4500.00 |    12 |   7 |     1
 rei          | 2a44r       | Frontissa Dta.     | 4500.00 |    12 |  15 |     2
(25 rows)
```
## Exercici 25

Per cada client mostrar el nom del representant de ventes que té assignat, el nom del seu director si en té, el nom del cap de l'oficina on treballa si té oficina i el nom del cap de l'oficina on treballa el seu director si té director i el seu director té oficina.
```
SELECT num_clie, vene.nom AS "vene ass", caps.nom AS "cap nom", nom_director.nom, nom_director_cap.nom
  FROM clients
  LEFT JOIN rep_vendes AS "vene"
    ON vene.num_empl = rep_clie
  LEFT JOIN rep_vendes AS "caps"
    ON vene.cap = caps.num_empl
  LEFT JOIN oficines 
    ON vene.oficina_rep = oficina
  LEFT JOIN rep_vendes AS "nom_director"
    ON nom_director.num_empl = oficines.director
  LEFT JOIN oficines AS "director_cap"
    ON caps.oficina_rep = director_cap.oficina
  LEFT JOIN rep_vendes AS "nom_director_cap"
    ON nom_director_cap.num_empl = director_cap.director;
```
```
 num_clie |   vene ass    |   cap nom   |     nom     |     nom     
----------+---------------+-------------+-------------+-------------
     2111 | Paul Cruz     | Bob Smith   | Bob Smith   | Bob Smith
     2102 | Dan Roberts   | Bob Smith   | Bob Smith   | Bob Smith
     2103 | Bill Adams    | Bob Smith   | Bill Adams  | Bob Smith
     2123 | Sue Smith     | Larry Fitch | Larry Fitch | Larry Fitch
     2107 | Tom Snyder    | Dan Roberts |             | Bob Smith
     2115 | Dan Roberts   | Bob Smith   | Bob Smith   | Bob Smith
     2101 | Sam Clark     |             | Sam Clark   | 
     2112 | Larry Fitch   | Sam Clark   | Larry Fitch | Sam Clark
     2121 | Paul Cruz     | Bob Smith   | Bob Smith   | Bob Smith
     2114 | Sue Smith     | Larry Fitch | Larry Fitch | Larry Fitch
     2124 | Nancy Angelli | Larry Fitch | Larry Fitch | Larry Fitch
     2108 | Mary Jones    | Sam Clark   | Sam Clark   | Sam Clark
     2117 | Sam Clark     |             | Sam Clark   | 
     2122 | Bill Adams    | Bob Smith   | Bill Adams  | Bob Smith
     2120 | Sue Smith     | Larry Fitch | Larry Fitch | Larry Fitch
     2106 | Sue Smith     | Larry Fitch | Larry Fitch | Larry Fitch
     2119 | Mary Jones    | Sam Clark   | Sam Clark   | Sam Clark
     2118 | Larry Fitch   | Sam Clark   | Larry Fitch | Sam Clark
     2113 | Bob Smith     | Sam Clark   | Bob Smith   | Sam Clark
     2109 | Paul Cruz     | Bob Smith   | Bob Smith   | Bob Smith
     2105 | Dan Roberts   | Bob Smith   | Bob Smith   | Bob Smith
(21 rows)
```