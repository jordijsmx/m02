## 1. Productes amb un preu entre 50 i 500
```
SELECT preu FROM productes  WHERE (preu >= 50 AND preu <= 100);
```

```
```

## 2. Productes amb un preu que no inclou el rang 50 i 500

```
SELECT preu FROM productes WHERE NOT preu >= 50 OR NOT preu <= 500;

SELECT preu FROM productes  WHERE NOT (preu >= 50 AND preu <= 500);

SELECT preu FROM productes  WHERE (preu < 50 OR preu > 500) ;
```

```
  preu   
---------
 2750.00
 1875.00
  652.00
 4500.00
 1425.00
 2500.00
 4500.00
  975.00
   25.00
(9 rows)
```

## 3. Productes de les fabriques 'aci' i 'imm' i amb preu entre 50 i 500 

```
SELECT * 
  FROM productes 
 WHERE (id_fabricant = 'aci' OR id_fabricant = 'imm') 
   AND (preu >= 50 AND preu <= 500);


SELECT * 
  FROM productes 
 WHERE id_fabricant IN ('aci', 'imm') 
   AND preu >= 50 AND preu <= 500;
```

```
 id_fabricant | id_producte |    descripcio    |  preu   | estoc 
--------------+-------------+------------------+---------+-------
 aci          | 4100y       | Extractor        | 2750.00 |    25
 aci          | 41003       | Article Tipus 3  |  107.00 |   207
 aci          | 41004       | Article Tipus 4  |  117.00 |   139
 imm          | 887p        | Pern Riosta      |  250.00 |    24
 imm          | 887h        | Suport Riosta    |   54.00 |   223
 aci          | 41001       | Article Tipus 1  |   55.00 |   277
 aci          | 4100z       | Muntador         | 2500.00 |    28
 aci          | 41002       | Article Tipus 2  |   76.00 |   167
 aci          | 4100x       | Peu de rei       |   25.00 |    37
 imm          | 887x        | Retenidor Riosta |  475.00 |    32
(10 rows)
```

## 4. Productes de les fabriques 'aci' i 'imm' i amb preu que no inclou el rang 50 i 100

```
SELECT * 
  FROM productes 
 WHERE (id_fabricant = 'aci' OR id_fabricant = 'imm') 
   AND NOT (preu >= 50 AND preu <= 500);

SELECT * 
  FROM productes 
 WHERE id_fabricant IN ('aci', 'imm') 
   AND NOT preu >= 50 AND preu <= 500;
```

```
 id_fabricant | id_producte |  descripcio   |  preu   | estoc 
--------------+-------------+---------------+---------+-------
 aci          | 4100y       | Extractor     | 2750.00 |    25
 imm          | 779c        | Riosta 2-Tm   | 1875.00 |     9
 imm          | 775c        | Riosta 1-Tm   | 1425.00 |     5
 aci          | 4100z       | Muntador      | 2500.00 |    28
 imm          | 773c        | Riosta 1/2-Tm |  975.00 |    28
 aci          | 4100x       | Peu de rei    |   25.00 |    37
(6 rows)
```

## 5. Productes que no siguin de 'aci' ni de 'imm' i amb preu que no inclou el rang 50 i 100

```
SELECT * 
  FROM productes 
 WHERE id_fabricant NOT IN ('aci', 'imm') 
   AND NOT (preu >= 50 AND preu <= 500);

SELECT * 
  FROM productes 
 WHERE NOT (id_fabricant = 'aci' OR id_fabricant = 'imm') 
   AND (NOT preu >= 50 OR NOT preu <= 500);
```

```
 id_fabricant | id_producte |   descripcio   |  preu   | estoc 
--------------+-------------+----------------+---------+-------
 bic          | 41003       | Manovella      |  652.00 |     3
 rei          | 2a44l       | Frontissa Esq. | 4500.00 |    12
 rei          | 2a44r       | Frontissa Dta. | 4500.00 |    12
(3 rows)
```