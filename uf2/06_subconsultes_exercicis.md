# Subconsultes

## Exercici 1 

Selecciona els treballadors que han venut menys quantitat de productes que la Sue Smith
```
SELECT num_empl, nom 
  FROM rep_vendes 
 WHERE EXISTS (SELECT rep 
                 FROM comandes 
                WHERE rep = num_empl 
                GROUP BY rep 
               HAVING SUM(quantitat) < (SELECT SUM(quantitat) 
                                          FROM comandes 
                                         WHERE rep IN (SELECT num_empl 
                                                         FROM rep_vendes 
                                                        WHERE nom = 'Sue Smith')
                                        )
                );

SELECT num_empl, nom 
  FROM rep_vendes 
 WHERE num_empl = ANY (SELECT rep 
                 FROM comandes 
                WHERE rep = num_empl 
                GROUP BY rep 
               HAVING SUM(quantitat) < (SELECT SUM(quantitat) 
                                          FROM comandes 
                                         WHERE rep IN (SELECT num_empl 
                                                         FROM rep_vendes 
                                                        WHERE nom = 'Sue Smith')
                                        )
                );
```
```
 num_empl |      nom      
----------+---------------
      109 | Mary Jones
      106 | Sam Clark
      110 | Tom Snyder
      103 | Paul Cruz
      107 | Nancy Angelli
(5 rows)
```
## Exercici 2 

Llista els treballadors que han venut mes en import que la Sue Smith, la Mary Jones i els Will Adams
```
SELECT num_empl, nom 
  FROM rep_vendes 
 WHERE EXISTS (SELECT rep 
                 FROM comandes 
                WHERE rep = num_empl 
                GROUP BY rep 
               HAVING SUM(import) > ALL (SELECT SUM(import) 
                                           FROM comandes 
                                          WHERE rep IN (SELECT num_empl 
                                                          FROM rep_vendes 
                                                         WHERE nom = 'Sue Smith' 
                                                            OR nom = 'Mary Jones' 
                                                            OR nom = 'Bill Adams') 
                                                         GROUP BY rep
                                        )
                );
```
```
 num_empl |     nom     
----------+-------------
      108 | Larry Fitch
(1 row)
```
## Exercici 3 

Llista els treballadors que han venut mes que alguns dels seguents : Sue Smith, la Mary Jones i els Bill Adams
```
SELECT num_empl, nom 
  FROM rep_vendes 
 WHERE EXISTS (SELECT rep 
                 FROM comandes 
                WHERE rep = num_empl 
                GROUP BY rep 
               HAVING SUM(import) > ANY (SELECT SUM(import) 
                                           FROM comandes 
                                          WHERE rep IN (SELECT num_empl 
                                                          FROM rep_vendes 
                                                         WHERE nom = 'Sue Smith' 
                                                            OR nom = 'Mary Jones' 
                                                            OR nom = 'Bill Adams') 
                                                         GROUP BY rep
                                        )
                );
```
```
 num_empl |      nom      
----------+---------------
      105 | Bill Adams
      102 | Sue Smith
      106 | Sam Clark
      101 | Dan Roberts
      110 | Tom Snyder
      108 | Larry Fitch
      107 | Nancy Angelli
(7 rows)
```
## Exercici 4 

Llista els treballadors que han fet mes comandes que els seus directors.
```     
SELECT num_empl, nom, (SELECT COUNT(*) 
                         FROM comandes 
                        WHERE r ep = num_empl), cap, (SELECT nom 
                                                       FROM rep_vendes 
                                                      WHERE num_empl=cap), (SELECT COUNT(*) 
                                                                              FROM comandes 
                                                                             WHERE rep = cap)
FROM rep_vendes rv
WHERE cap IS NOT NULL 
  AND (SELECT COUNT(*) 
         FROM comandes 
        WHERE rep = rv.num_empl) > (SELECT COUNT(*) 
                                   FROM comandes 
                                  WHERE rep = rv.cap);
```
```
 num_empl |     nom     | count | cap | nom | count 
----------+-------------+-------+-----+-----+-------
      105 | Bill Adams  |     5 | 104 |     |     0
      101 | Dan Roberts |     3 | 104 |     |     0
      108 | Larry Fitch |     7 | 106 |     |     2
      103 | Paul Cruz   |     2 | 104 |     |     0
(4 rows)
```
## Exercici 5 

Llista els treballadors que en el rànking de ventes estan entre el Dan Roberts i la Mary Jones
```
SELECT rep, nom, SUM(import) 
  FROM comandes 
  JOIN rep_vendes 
    ON rep = num_empl  
 GROUP BY rep, nom 
 ORDER BY 3 desc;

SELECT rep, (SELECT nom FROM rep_vendes c WHERE c.num_empl=rep), SUM(import) 
  FROM comandes 
 GROUP BY 1,2 
HAVING SUM(import) > (SELECT SUM(import) 
                        FROM comandes b 
                       WHERE b.rep = (SELECT c.num_empl 
                                        FROM rep_vendes c 
                                       WHERE c.nom='Mary Jones'))
AND SUM(import) < (SELECT SUM(import) 
                     FROM comandes b 
                    WHERE b.rep = (SELECT c.num_empl 
                                     FROM rep_vendes c 
                                    WHERE c.nom='Dan Roberts'))
ORDER BY 3 desc;
```
```
 rep |      nom      |   sum    
-----+---------------+----------
 108 | Larry Fitch   | 58633.00
 105 | Bill Adams    | 39327.00
 107 | Nancy Angelli | 34432.00
 106 | Sam Clark     | 32958.00
 101 | Dan Roberts   | 26628.00 <----
 110 | Tom Snyder    | 23132.00
 102 | Sue Smith     | 22776.00
 109 | Mary Jones    |  7105.00 <----
 103 | Paul Cruz     |  2700.00
(9 rows)
```
## Exercici 6 

Mostra les oficines (codi i ciutat) tals que el seu objectiu sigui inferior o igual a les quotes de tots els seus treballadors.
```
SELECT oficina, ciutat
  FROM oficines
 WHERE objectiu <= ALL (SELECT quota 
                          FROM rep_vendes 
                         WHERE oficina_rep = oficina);

SELECT oficina, ciutat
  FROM oficines
 WHERE NOT objectiu > ANY (SELECT quota 
                          FROM rep_vendes 
                         WHERE oficina_rep = oficina);

SELECT oficina, ciutat
  FROM oficines
 WHERE EXISTS (SELECT * 
                 FROM rep_vendes 
                WHERE oficina_rep = oficina 
                  AND objectiu <= quota);
```
```
 oficina | ciutat  
---------+---------
      22 | Denver
      13 | Atlanta
(2 rows)
```
## Exercici 7 

Llista els representants de vendes (codi de treballador i nom) que tenen un director més jove que algun dels seus empleats.
```
SELECT a.num_empl, a.nom
  FROM rep_vendes a
 WHERE EXISTS (SELECT b.num_empl 
                 FROM rep_vendes b 
                WHERE b.num_empl = a.cap 
                  AND b.edat < ANY (SELECT c.edat 
                                      FROM rep_vendes c 
                                     WHERE c.cap=a.cap));

SELECT a.num_empl, a.nom
  FROM rep_vendes a
 WHERE a.cap = ANY (SELECT b.num_empl 
                 FROM rep_vendes b 
                WHERE b.num_empl = a.cap 
                  AND b.edat < ANY (SELECT c.edat 
                                      FROM rep_vendes c 
                                     WHERE c.cap=a.cap));
```
```
 num_empl |     nom     
----------+-------------
      105 | Bill Adams
      109 | Mary Jones
      104 | Bob Smith
      101 | Dan Roberts
      108 | Larry Fitch
      103 | Paul Cruz
(6 rows)
```
## Exercici 8 

Mostrar el codi de treballador, el seu nom i un camp anomenat i_m. El camp i_m és l'import més gran de les comandes que ha fet aquest treballador. Només s'han de llistar els treballadors que tinguin tots els clients amb alguna comanda amb import superior a la mitjana dels imports de totes les comandes.
```
SELECT num_empl, nom, (SELECT MAX(import) 
                         FROM comandes 
                        WHERE rep=num_empl) AS i_m
FROM rep_vendes
WHERE (SELECT AVG(import) 
         FROM comandes) < ALL (SELECT MAX(import) 
                                 FROM comandes 
                                WHERE rep=num_empl 
                                GROUP BY clie) 
AND 
EXISTS (SELECT * 
          FROM comandes 
         WHERE rep=num_empl);
```
```
 num_empl |    nom     |   i_m    
----------+------------+----------
      110 | Tom Snyder | 22500.00
(1 row)
```
## Exercici 9 

Mostra el codi de fabricant i de producte i un camp de nom n_p. n_p és el nombre de comandes que s'han fet d'aquell producte. Només s'han de llistar aquells productes tals que se n''ha fet alguna comanda amb una quantitat inferior a les seves existències. En el llistat només han d''aparèixer els tres productes amb més comandes, ordenats per codi de fabricant i de producte.
```
SELECT id_fabricant, id_producte, (SELECT COUNT(*) 
                                     FROM comandes 
                                    WHERE id_fabricant = fabricant 
                                      AND id_producte =producte) AS n_p 
  FROM productes 
 WHERE EXISTS (SELECT * 
                 FROM comandes 
                WHERE id_fabricant = fabricant 
                  AND id_producte =producte 
                  AND quantitat < estoc);
```
```
 id_fabricant | id_producte | n_p 
--------------+-------------+-----
 rei          | 2a45c       |   2
 aci          | 4100y       |   1
 qsa          | xk47        |   3
 imm          | 779c        |   2
 aci          | 41003       |   1
 aci          | 41004       |   3
 bic          | 41003       |   2
 rei          | 2a44l       |   1
 fea          | 112         |   1
 aci          | 4100z       |   2
 aci          | 41002       |   2
 rei          | 2a44r       |   2
 imm          | 773c        |   1
 aci          | 4100x       |   2
 fea          | 114         |   2
 rei          | 2a44g       |   1
(16 rows)
```
## Exercici 10 

Mostra el codi de client, el nom de client, un camp c_r i un camp n_p. El camp c_r ha de mostrar la quota del representant de vendes del client. El camp n_p ha demostrar el nombre de comandes que ha fet aquest client. Només s''han de mostrar els clients que l''import total de totes les seves comandes sigui superior a la mitjana de l''import de totes les comandes.
```
SELECT num_clie, empresa, (SELECT quota 
                             FROM rep_vendes 
                            WHERE rep_clie=num_empl) as c_r, (SELECT COUNT(*) 
                                                                FROM comandes 
                                                               WHERE clie=num_clie)
  FROM clients
 WHERE (SELECT SUM(import) 
          FROM comandes 
         WHERE clie=num_clie) > (SELECT AVG(import) 
                                   FROM comandes);
```
```
 num_clie |      empresa      |    c_r    | count 
----------+-------------------+-----------+-------
     2103 | Acme Mfg.         | 350000.00 |     4
     2107 | Ace International |           |     2
     2112 | Zetacorp          | 350000.00 |     2
     2114 | Orion Corp        | 350000.00 |     2
     2117 | J.P. Sinclair     | 275000.00 |     1
     2113 | Ian & Schmidt     | 200000.00 |     1
     2109 | Chen Associates   | 275000.00 |     1
(7 rows)
```
## Exercici 11 

Mostrar l'identificador i el nom de l'empresa dels clients i un camp anomenat m_importe. Només s'ha de llistar aquells clients que han fet comandes de productes que tenen un preu que supera la mitjana del preu de tots els productes. El camp m_importe ha de mostrar l'import més petit de les comandes del client.
```
SELECT num_clie, empresa, (SELECT MIN(import) 
                             FROM comandes 
                            WHERE clie=num_clie) AS m_importe
  FROM clients 
 WHERE EXISTS (SELECT * 
                 FROM comandes 
                WHERE clie=num_clie 
                  AND EXISTS (SELECT * 
                                FROM productes 
                               WHERE fabricant = id_fabricant 
                                 AND producte=id_producte 
                                 AND preu > (SELECT AVG(preu) 
                                               FROM productes)));
```
```
 num_clie |      empresa      |   min    
----------+-------------------+----------
     2112 | Zetacorp          |  2925.00
     2114 | Orion Corp        |  7100.00
     2109 | Chen Associates   | 31350.00
     2120 | Rico Enterprises  |  3750.00
     2108 | Holm & Landis     |   150.00
     2103 | Acme Mfg.         |   702.00
     2107 | Ace International |   632.00
     2113 | Ian & Schmidt     | 22500.00
     2117 | J.P. Sinclair     | 31500.00
(9 rows)
```