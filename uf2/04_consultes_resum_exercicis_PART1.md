# Consultes resum

# Exercici 0
En una mateixa consulta, calcula la suma de les quotes, quants venedors n'hi ha, i finalment mostra la quota promig calculada a partir de les funcions anteriors i també amb una funció especial per al promig. Compara aquests dos últims resultats. Si volguéssim que la darrera funció actui com la penúltima com ho faries?

```
SELECT ROUND(SUM(quota) / COUNT(nom),2) AS "promig 1", 
       ROUND(AVG(COALESCE(quota,0)),2) AS "promig 2" 
  FROM rep_vendes;
```

```
 promig 1  |  promig 2  
-----------+-----------
 270000.00 | 270000.00
(1 row)
```

## Exercici 1
Quina és la quota promig mostrada com a "prom_quota" i la venda promig
mostrades com a "prom_vendes" dels venedors?

```
SELECT ROUND(AVG(quota),2) AS "prom_quota", 
       ROUND(AVG(vendes),2) AS "prom_vendes"
  FROM rep_vendes ;
```

```
 prom_quota | prom_vendes 
------------+-------------
  300000.00 |   289353.20
(1 row)
```

## Exercici 2
Quin és el promig del rendiment dels venedors (promig del percentatge de les vendes
respecte la quota)?

```
SELECT ROUND(AVG(vendes * 100 / quota),2) 
  FROM rep_vendes ;
```

```
 round  
--------
 102.60
(1 row)
```
## Exercici 3

Quines són les quotes totals com a *t_quota* i vendes totals com a *t_vendes*
de tots els venedors?
```
SELECT SUM(quota) AS "t_quota", SUM(vendes) AS "t_vendes" 
  FROM rep_vendes ;

```

```
  t_quota   |  t_vendes  
------------+------------
 2700000.00 | 2893532.00
(1 row)
```
## Exercici 4
Calcula el preu mig dels productes del fabricant amb identificador "aci".
```
SELECT ROUND(AVG(preu),2) 
  FROM productes 
 WHERE id_fabricant = 'aci';
```

```
 round  
--------
 804.29
(1 row)
```
## Exercici 5
Quines són les quotes assignades mínima i màxima?
```
SELECT MIN(quota), MAX(quota) 
  FROM rep_vendes ;
```

```
    min    |    max    
-----------+-----------
 200000.00 | 350000.00
(1 row)
```
## Exercici 6
Quina és la data de comanda més antiga?
```
SELECT MIN(data) 
  FROM comandes ;
```

```
    min     
------------
 1989-01-04
(1 row)
```
## Exercici 7
Quin és el major percentatge de rendiment de vendes respecte les quotes de tots els venedors?
(o sigui el major percentatge de les vendes respecte a la quota)
```
SELECT ROUND(MAX(vendes * 100 / quota),2) 
  FROM rep_vendes ;
```

```
 round  
--------
 135.44
(1 row)
```
## Exercici 8
Quants clients hi ha?
```
SELECT COUNT(num_clie) 
  FROM clients ;
```

```
 count 
-------
    21
(1 row)
```
## Exercici 9
Quants venedors superen la seva quota?
```
SELECT COUNT(num_empl) 
  FROM rep_vendes 
 WHERE vendes > quota;
```

```
 count 
-------
     7
(1 row)
```
## Exercici 10
Quantes comandes amb un import superior a 25000 hi ha en els registres?
```
SELECT COUNT(*) 
  FROM comandes
 WHERE import > 25000;
```

```
 count 
-------
     4
(1 row)
```
## Exercici 11

Trobar l'import mitjà de les comandes, l'import total de les comandes, la
mitjana del percentatge de l'import de les comandes respecte del límit de
crèdit del client i la mitjana del percentatge de l'import de les comandes
respecte a la quota del venedor.
```
 SELECT CAST(AVG(import) AS NUMERIC(6,2)), 
       SUM(import), 
       CAST(AVG(import * 100 / limit_credit) AS NUMERIC(4,2)), 
       CAST(AVG(import * 100 / quota) AS NUMERIC(3,2)) 
  FROM comandes 
  JOIN clients 
    ON comandes.clie = clients.num_clie 
  JOIN rep_vendes 
    ON comandes.rep = rep_vendes.num_empl; 
   avg   |    sum    |  avg  | avg  
---------+-----------+-------+------
 8256.37 | 247691.00 | 24.45 | 2.51
(1 row)
```

```
  round  |    sum    | round | round 
---------+-----------+-------+-------
 8256.37 | 247691.00 | 24.45 |  2.51
(1 row)
```
## Exercici 12
Compta les files que hi ha a repventas, les files del camp vendes i les del camp quota.
```
SELECT COUNT(rep_vendes), 
       COUNT(vendes), 
       COUNT(quota) 
  FROM rep_vendes ;
```

```
 count | count | count 
-------+-------+-------
    10 |    10 |     9
(1 row)
```
## Exercici 13
Demostra que la suma de restar vendes menys quota és diferent que sumar vendes i restar-li la suma de quotes.
```
SELECT SUM(vendes - quota), 
       SUM(vendes) - SUM(quota) 
  FROM rep_vendes ;
```

```
    sum    | ?column?  
-----------+-----------
 117547.00 | 193532.00
(1 row)
```
## Exercici 14
Quants tipus de càrrecs hi ha de venedors?
```
SELECT COUNT(DISTINCT(carrec)) 
  FROM rep_vendes ;
```

```
 count 
-------
     3
(1 row)
```
## Exercici 15
Quantes oficines de vendes tenen venedors que superen les seves quotes?
```
SELECT COUNT(DISTINCT(oficina_rep)) 
  FROM rep_vendes 
 WHERE vendes > quota ;
```

```
 count 
-------
     4
(1 row)
```
## Exercici 16
De la taula clients quants clients diferents i venedors diferents hi ha.
```
SELECT COUNT(num_clie), 
       COUNT(DISTINCT(rep_clie)) 
  FROM clients ;
```

```
 count | count 
-------+-------
    21 |    10
(1 row)
```
## Exercici 17
De la taula comandes seleccionar quantes comandes diferents i clients diferents hi ha
```
SELECT COUNT(num_comanda), 
       COUNT(DISTINCT(clie)) 
  FROM comandes ;
```

```
 count | count 
-------+-------
    30 |    15
(1 row)
```
## Exercici 18
Calcular la mitjana dels imports de les comandes.
```
SELECT ROUND(AVG(import),2) 
  FROM comandes;
```

```
  round  
---------
 8256.37
(1 row)
```
## Exercici 19
Calcula la mitjana de l'import d'una comanda realitzada pel client amb nom d'empresa "Acme Mfg."
```
SELECT ROUND(AVG(import),2) 
  FROM comandes 
  JOIN clients 
    ON comandes.clie = clients.num_clie 
 WHERE empresa = 'Acme Mfg.';
```

```
  round  
---------
 8895.50
(1 row)
```
