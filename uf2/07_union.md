# Union

## Exercici 1
Mostar les ventes individuals dels productes dels fabricants 'aci' i 'rei' que comencin per 'Frontissa' o 'Article'. Mostrar també el total venut d'aquests productes.
```
 SELECT 'Venta' AS "Tipus", descripcio AS "producte", num_comanda AS "comanda" 
   FROM comandes 
   JOIN productes 
     ON (fabricant,producte) = (id_fabricant, id_producte) 
  WHERE (fabricant LIKE 'aci' 
     OR fabricant LIKE 'rei')
    AND (descripcio LIKE 'Frontissa%' 
     OR descripcio LIKE 'Article%')

SELECT 'Total', descripcio
```
```

```
## Exercici 2       
Mostrar les ventes individuals fetes pels venedors de New York i de Chicago que superin els 2500€. Mostrar també el total de ventes de cada oficina.
```
SELECT 'Venedor:' AS "Entitat", ciutat, nom AS venedor, num_comanda AS comanda, import 
  FROM comandes 
  JOIN rep_vendes 
    ON rep = num_empl 
  JOIN oficines 
    ON oficina_rep = oficina 
 WHERE (ciutat = 'New York' OR ciutat = 'Chicago') 
   AND import > 2500 

UNION 

SELECT 'Total Oficina', ciutat, '-----', 0, SUM(import) 
  FROM comandes 
  JOIN rep_vendes 
    ON rep = num_empl 
  JOIN oficines 
    ON oficina_rep = oficina 
 WHERE (ciutat = 'New York' OR ciutat = 'Chicago') 
   AND import > 2500 
 GROUP BY oficina 
 ORDER BY ciutat, 1 DESC;
```
```
    Entitat    |  ciutat  |   venedor   | comanda |  import  
---------------+----------+-------------+---------+----------
 Venedor:      | Chicago  | Dan Roberts |  113042 | 22500.00
 Venedor:      | Chicago  | Dan Roberts |  112968 |  3978.00
 Total Oficina | Chicago  | -----       |       0 | 26478.00
 Venedor:      | New York | Sam Clark   |  112961 | 31500.00
 Venedor:      | New York | Mary Jones  |  113003 |  5625.00
 Total Oficina | New York | -----       |       0 | 37125.00
(6 rows)
```
## Exercici 3
Mostrar quantes ventes ha fet cada venedor, la mitjana de numero de ventes dels venedors de cada oficina i el numero de ventes total.
```
SELECT 'Venedor:' AS "Tipus", ciutat, num_empl AS "id", nom AS "identitat", COUNT(*) AS "Total" 
  FROM rep_vendes 
  LEFT JOIN comandes 
    ON rep = num_empl 
  LEFT JOIN oficines 
    ON oficina_rep = oficina 
 GROUP BY num_empl, oficina 
 
UNION 

SELECT 'Oficina:', ciutat, '0', ' ', COUNT(*)/COUNT(distinct num_empl) 
  FROM oficines 
 RIGHT JOIN rep_vendes 
    ON oficina = oficina_rep 
  LEFT JOIN comandes 
    ON rep = num_empl 
 GROUP BY oficina 
 
UNION 

SELECT 'Total:', ' ', '0', ' ', COUNT(*) 
  FROM comandes 
 ORDER BY 2 DESC, 1 DESC;
```
```
  Tipus   |   ciutat    | id  |   identitat   | Total 
----------+-------------+-----+---------------+-------
 Venedor: |             | 110 | Tom Snyder    |     2
 Oficina: |             |   0 |               |     2
 Venedor: | New York    | 106 | Sam Clark     |     2
 Venedor: | New York    | 109 | Mary Jones    |     2
 Oficina: | New York    |   0 |               |     2
 Venedor: | Los Angeles | 102 | Sue Smith     |     4
 Venedor: | Los Angeles | 108 | Larry Fitch   |     7
 Oficina: | Los Angeles |   0 |               |     5
 Venedor: | Denver      | 107 | Nancy Angelli |     3
 Oficina: | Denver      |   0 |               |     3
 Venedor: | Chicago     | 104 | Bob Smith     |     1
 Venedor: | Chicago     | 101 | Dan Roberts   |     3
 Venedor: | Chicago     | 103 | Paul Cruz     |     2
 Oficina: | Chicago     |   0 |               |     2
 Venedor: | Atlanta     | 105 | Bill Adams    |     5
 Oficina: | Atlanta     |   0 |               |     5
 Total:   |             |   0 |               |    30
(17 rows)
```
## Exercici 4
Mostrar les compres de productes de la fabrica 'aci' que han fet els clients del Bill Adams i el Dan Roberts. Mostrar també l'import total per cada client.
```
SELECT 'Venta' AS "Tpus", empresa, num_comanda, id_fabricant, num_empl, nom, import 
  FROM comandes 
  JOIN productes 
    ON (id_fabricant, id_producte) = (fabricant, producte) 
  JOIN clients 
    ON clie = num_clie 
  JOIN rep_vendes 
    ON rep_clie = num_empl 
 WHERE (nom = 'Bill Adams' OR nom = 'Dan Roberts') 
   AND id_fabricant = 'aci' 
   
UNION 

SELECT 'Total Client', empresa, '0', '0', num_clie, empresa, SUM(import) 
  FROM comandes 
  JOIN productes 
    ON (id_fabricant, id_producte) = (fabricant, producte) 
  JOIN clients 
    ON clie = num_clie 
  JOIN rep_vendes 
    ON rep_clie = num_empl 
 WHERE (nom = 'Bill Adams' OR nom = 'Dan Roberts') 
   AND id_fabricant = 'aci' 
 GROUP BY num_clie 
 ORDER BY 2, 1 DESC;
```
```
     Tpus     |   empresa   | num_comanda | id_fabricant | num_empl |     nom     |  import  
--------------+-------------+-------------+--------------+----------+-------------+----------
 Venta        | Acme Mfg.   |      112983 | aci          |      105 | Bill Adams  |   702.00
 Venta        | Acme Mfg.   |      112963 | aci          |      105 | Bill Adams  |  3276.00
 Venta        | Acme Mfg.   |      112987 | aci          |      105 | Bill Adams  | 27500.00
 Venta        | Acme Mfg.   |      113027 | aci          |      105 | Bill Adams  |  4104.00
 Total Client | Acme Mfg.   |           0 | 0            |     2103 | Acme Mfg.   | 35582.00
 Venta        | First Corp. |      112968 | aci          |      101 | Dan Roberts |  3978.00
 Total Client | First Corp. |           0 | 0            |     2102 | First Corp. |  3978.00
(7 rows)
```
## Exercici 5
Mostrar el total de ventes de cada oficina i el total de ventes de cada regió
```
SELECT 'Oficina', regio, ciutat, '0', ' ', SUM(import) 
  FROM oficines 
 RIGHT JOIN rep_vendes 
    ON oficina = oficina_rep 
  LEFT JOIN comandes 
    ON rep = num_empl 
 GROUP BY regio, oficina

UNION

SELECT 'Regio', regio, ' ', '0', ' ', SUM(import) 
  FROM oficines 
 RIGHT JOIN rep_vendes 
    ON oficina_rep = oficina 
  LEFT JOIN comandes 
    ON rep = num_empl 
 GROUP BY regio 
 ORDER BY regio, 1;
```
```
 ?column? | regio |   ciutat    | ?column? | ?column? |    sum    
----------+-------+-------------+----------+----------+-----------
 Oficina  | Est   | Atlanta     | 0        |          |  39327.00
 Oficina  | Est   | Chicago     | 0        |          |  29328.00
 Oficina  | Est   | New York    | 0        |          |  40063.00
 Regio    | Est   |             | 0        |          | 108718.00
 Oficina  | Oest  | Denver      | 0        |          |  34432.00
 Oficina  | Oest  | Los Angeles | 0        |          |  81409.00
 Regio    | Oest  |             | 0        |          | 115841.00
 Oficina  |       |             | 0        |          |  23132.00
 Regio    |       |             | 0        |          |  23132.00
(9 rows)
```
## Exercici 6
Mostrar els noms dels venedors de cada ciutat i el numero total de venedors per ciutat
```
SELECT 'Nom venedor:', nom, ciutat, '0' 
  FROM rep_vendes 
  LEFT JOIN oficines 
    ON oficina_rep = oficina 

UNION 

SELECT 'Total ciutat', ciutat, ' ', COUNT(*) 
  FROM rep_vendes 
  LEFT JOIN oficines 
    ON oficina_rep = oficina 
 GROUP BY ciutat
 ORDER BY ciutat, 1;
```
```
   ?column?   |      nom      |   ciutat    | ?column? 
--------------+---------------+-------------+----------
 Total ciutat | Chicago       |             |        3
 Total ciutat | Los Angeles   |             |        2
 Total ciutat | Atlanta       |             |        1
 Total ciutat | Denver        |             |        1
 Total ciutat |               |             |        1
 Total ciutat | New York      |             |        2
 Nom venedor: | Bill Adams    | Atlanta     |        0
 Nom venedor: | Paul Cruz     | Chicago     |        0
 Nom venedor: | Dan Roberts   | Chicago     |        0
 Nom venedor: | Bob Smith     | Chicago     |        0
 Nom venedor: | Nancy Angelli | Denver      |        0
 Nom venedor: | Larry Fitch   | Los Angeles |        0
 Nom venedor: | Sue Smith     | Los Angeles |        0
 Nom venedor: | Mary Jones    | New York    |        0
 Nom venedor: | Sam Clark     | New York    |        0
 Nom venedor: | Tom Snyder    |             |        0
(16 rows)
```
# Exercici 7
Mostrat els noms dels clients de cada ciutat i el numero total de clients per ciutat.
```
SELECT 'Nom client:', ciutat, empresa, '0' 
  FROM clients 
  JOIN rep_vendes 
    ON rep_clie = num_empl 
  LEFT JOIN oficines 
    ON oficina_rep = oficina 

UNION 

SELECT 'Total ciutat:', ciutat, ' ',COUNT(*) 
  FROM clients 
  JOIN rep_vendes 
    ON rep_clie = num_empl 
  LEFT JOIN oficines 
    ON oficina_rep = oficina 
 GROUP BY ciutat 
 ORDER BY ciutat, 1;
```
```
   ?column?    |   ciutat    |      empresa      | ?column? 
---------------+-------------+-------------------+----------
 Nom client:   | Atlanta     | Three-Way Lines   |        0
 Nom client:   | Atlanta     | Acme Mfg.         |        0
 Total ciutat: | Atlanta     |                   |        2
 Nom client:   | Chicago     | AAA Investments   |        0
 Nom client:   | Chicago     | First Corp.       |        0
 Nom client:   | Chicago     | Chen Associates   |        0
 Nom client:   | Chicago     | Ian & Schmidt     |        0
 Nom client:   | Chicago     | JCP Inc.          |        0
 Nom client:   | Chicago     | QMA Assoc.        |        0
 Nom client:   | Chicago     | Smithson Corp.    |        0
 Total ciutat: | Chicago     |                   |        7
 Nom client:   | Denver      | Peter Brothers    |        0
 Total ciutat: | Denver      |                   |        1
 Nom client:   | Los Angeles | Rico Enterprises  |        0
 Nom client:   | Los Angeles | Carter & Sons     |        0
 Nom client:   | Los Angeles | Orion Corp        |        0
 Nom client:   | Los Angeles | Midwest Systems   |        0
 Nom client:   | Los Angeles | Fred Lewis Corp.  |        0
 Nom client:   | Los Angeles | Zetacorp          |        0
 Total ciutat: | Los Angeles |                   |        6
 Nom client:   | New York    | J.P. Sinclair     |        0
 Nom client:   | New York    | Jones Mfg.        |        0
 Nom client:   | New York    | Solomon Inc.      |        0
 Nom client:   | New York    | Holm & Landis     |        0
 Total ciutat: | New York    |                   |        4
 Nom client:   |             | Ace International |        0
 Total ciutat: |             |                   |        1
(27 rows)
```
## Exercici 8
Mostrat els noms dels treballadors que son -caps- d'algú, els noms dels seus -subordinats- i el numero de treballadors que té assignat cada cap.
```
SELECT 'Caps:', cap.nom 
  FROM rep_vendes 
  JOIN rep_vendes AS "cap" 
    ON cap.num_empl = rep_vendes.cap 

UNION 

SELECT 'Subordinat:', rep_vendes.nom 
  FROM rep_vendes 
  JOIN rep_vendes AS "subordinat" 
    ON subordinat.num_empl = rep_vendes.cap
```
```

```