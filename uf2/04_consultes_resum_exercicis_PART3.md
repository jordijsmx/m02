# Consultes resum (PART III)

## Exercici 36

Quantes oficines tenim a cada regió?

```
SELECT regio, COUNT(oficines) 
  FROM oficines 
 GROUP BY regio;
```

```
 regio | count 
-------+-------
 Est   |     3
 Oest  |     2
(2 rows)

```

## Exercici 37

Quants representants de vendes hi ha a cada oficina?

```
SELECT oficina_rep, COUNT(num_empl) 
  FROM rep_vendes 
 WHERE oficina_rep IS NOT NULL 
 GROUP BY oficina_rep;
```

```
 oficina_rep | count 
-------------+-------
          22 |     1
          11 |     2
          21 |     2
          13 |     1
          12 |     3
(5 rows)
```

## Exercici 38

Quants representants de vendes té assignats cada cap de respresentants?

```
SELECT cap, COUNT(num_empl) 
  FROM rep_vendes 
 WHERE cap IS NOT NULL 
 GROUP BY cap ;
```

```
 cap | count 
-----+-------
 101 |     1
 108 |     2
 104 |     3
 106 |     3
(4 rows)
```

## Exercici 39

Per cada venedor calcular quants clients diferents ha atès ( atès = atendre una comanda)?

```
SELECT rep, COUNT(DISTINCT(clie)) 
  FROM comandes 
 GROUP BY rep;
```

```
 rep | count 
-----+-------
 101 |     3
 102 |     3
 103 |     1
 105 |     2
 106 |     2
 107 |     2
 108 |     3
 109 |     1
 110 |     1
(9 rows)
```

## Exercici 40

Quina és, per cada oficina, la suma de les quotes dels seus representants? I la mitjana de les quotes per oficina?

```
SELECT oficina_rep, SUM(quota), CAST(AVG(quota) AS NUMERIC(8,2))
  FROM rep_vendes 
 WHERE oficina_rep IS NOT NULL
 GROUP BY oficina_rep 
 ORDER BY 1;
```

```
 oficina_rep |    sum    |    avg    
-------------+-----------+-----------
          11 | 575000.00 | 287500.00
          12 | 775000.00 | 258333.33
          13 | 350000.00 | 350000.00
          21 | 700000.00 | 350000.00
          22 | 300000.00 | 300000.00
(5 rows)
```

## Exercici 41

Quina és la quota més alta de cada oficina?

```
SELECT oficina_rep, MAX(quota) 
  FROM rep_vendes 
 WHERE oficina_rep IS NOT NULL 
 GROUP BY oficina_rep;
```

```
 oficina_rep |    max    
-------------+-----------
          22 | 300000.00
          11 | 300000.00
          21 | 350000.00
          13 | 350000.00
          12 | 300000.00
(5 rows)
```

## Exercici 42

Quants clients representa cada venedor-rep_vendes?

```
SELECT rep_clie, COUNT(num_clie) 
  FROM clients 
 GROUP BY rep_clie 
 ORDER BY 1;
```

```
 rep_clie | count 
----------+-------
      101 |     3
      102 |     4
      103 |     3
      104 |     1
      105 |     2
      106 |     2
      107 |     1
      108 |     2
      109 |     2
      110 |     1
(10 rows)
```

## Exercici 43

Quin és límit de crèdit més alt dels clients de cada venedor-repvendes?

```
SELECT rep_clie, MAX(limit_credit) 
  FROM clients 
 GROUP BY rep_clie 
 ORDER BY 1;
```

```
 rep_clie |   max    
----------+----------
      101 | 65000.00
      102 | 65000.00
      103 | 50000.00
      104 | 20000.00
      105 | 50000.00
      106 | 65000.00
      107 | 40000.00
      108 | 60000.00
      109 | 55000.00
      110 | 35000.00
(10 rows)
```

## Exercici 44

Per cada codi de fàbrica diferents, quants productes hi ha?

```
SELECT id_fabricant, COUNT(id_producte) 
  FROM productes 
 GROUP BY id_fabricant;
```

```
 id_fabricant | count 
--------------+-------
 imm          |     6
 aci          |     7
 bic          |     3
 fea          |     2
 qsa          |     3
 rei          |     4
(6 rows)
```

## Exercici 45

Per cada id_producte diferent, a quantes fàbriques es fabrica?

```
SELECT id_producte, COUNT(id_fabricant) 
  FROM productes 
 GROUP BY id_producte;
```

```
 id_producte | count 
-------------+-------
 41003       |     2
 41004       |     1
 887h        |     1
 xk48        |     1
 41001       |     1
 41089       |     1
 887x        |     1
 41002       |     1
 4100x       |     1
 775c        |     1
 41672       |     1
 114         |     1
 4100z       |     1
 887p        |     1
 2a44g       |     1
 2a44r       |     1
 2a45c       |     1
 779c        |     1
 4100y       |     1
 xk48a       |     1
 xk47        |     1
 773c        |     1
 2a44l       |     1
 112         |     1
(24 rows)
```

## Exercici 46

Per cada nom de producte diferent, quants codis (id_fab + id_prod) tenim?

```
SELECT descripcio, 
 COUNT(id_fabricant) AS "fabricant", 
 COUNT(id_producte) AS "producte" 
  FROM productes 
 GROUP BY descripcio;
```

```
     descripcio     | fabricant | producte 
--------------------+-----------+----------
 Pern Riosta        |         1 |        1
 Article Tipus 3    |         1 |        1
 Passador Frontissa |         1 |        1
 Riosta 1-Tm        |         1 |        1
 Reductor           |         3 |        3
 Frontissa Esq.     |         1 |        1
 Coberta            |         1 |        1
 Manovella          |         1 |        1
 Frontissa Dta.     |         1 |        1
 Plate              |         1 |        1
 Article Tipus 4    |         1 |        1
 Suport Riosta      |         1 |        1
 Retenidor Riosta   |         1 |        1
 Article Tipus 2    |         1 |        1
 Riosta 1/2-Tm      |         1 |        1
 Riosta 2-Tm        |         1 |        1
 Article Tipus 1    |         1 |        1
 Peu de rei         |         1 |        1
 Extractor          |         1 |        1
 V Stago Trinquet   |         1 |        1
 Retn               |         1 |        1
 Bancada Motor      |         1 |        1
 Muntador           |         1 |        1
(23 rows)
```

## Exercici 47

Per cada producte (id_fab + id_prod), quantes comandes tenim?

```
SELECT COUNT(num_comanda), fabricant, producte 
  FROM comandes 
 GROUP BY fabricant, producte;

SELECT descripcio, 
 id_fabricant AS "fabricant", 
 id_producte AS "producte", 
 COUNT(clie) AS "num_clie_dif", 
 COUNT(DISTINCT(rep)) AS "ven_dif" 
  FROM productes 
  JOIN comandes 
    ON (id_producte, id_fabricant) = (producte, fabricant)
 GROUP BY id_fabricant, id_producte;
```

```
 count | fabricant | producte 
-------+-----------+----------
     2 | imm       | 779c 
     2 | rei       | 2a44r
     1 | rei       | 2a44l
     1 | aci       | 41003
     2 | aci       | 41002
     2 | aci       | 4100z
     1 | imm       | 775c 
     2 | fea       | 114  
     1 | imm       | 773c 
     2 | bic       | 41003
     1 | fea       | 112  
     1 | aci       | 4100y
     2 | aci       | 4100x
     1 | qsa       | k47  
     1 | rei       | 2a44g
     3 | qsa       | xk47 
     3 | aci       | 41004
     2 | rei       | 2a45c
(18 rows)
```

## Exercici 48

Per cada client, quantes comandes tenim? Incloure els clients que no han realitzat cap comanda

```
SELECT num_clie, COUNT(num_comanda) 
  FROM comandes 
 RIGHT JOIN clients 
    ON comandes.clie = clients.num_clie 
 GROUP BY num_clie;
```

```
 num_clie | count 
----------+-------
     2108 |     3
     2124 |     2
     2115 |     0
     2101 |     1
     2106 |     2
     2121 |     0
     2119 |     0
     2109 |     1
     2118 |     4
     2105 |     0
     2114 |     2
     2122 |     0
     2112 |     2
     2107 |     2
     2102 |     1
     2113 |     1
     2117 |     1
     2103 |     4
     2123 |     0
     2120 |     1
     2111 |     3
(21 rows)
```

## Exercici 49

Quantes comandes ha realitzat cada representant de vendes? Incloure els venedors que no han realitzat cap comanda

```
SELECT num_empl, COUNT(num_comanda) 
  FROM rep_vendes 
  LEFT JOIN comandes 
    ON rep_vendes.num_empl = comandes.rep 
 GROUP BY num_empl;
```

```
 num_empl | count 
----------+-------
      103 |     2
      104 |     0
      105 |     5
      107 |     3
      109 |     2
      106 |     2
      110 |     2
      101 |     3
      108 |     7
      102 |     4
(10 rows)
```

## Exercici 50

Quantes comandes s'han fet, quina és la suma total dels imports d'aquestes comandes i quina és la mitjana de l'import de la comada : les comandes amb una quantitat d'entre 20 i 30 productes i de les fàbriques que continguin una 'i' o una 'a'.

```
SELECT COUNT(num_comanda),                                                                  
        SUM(import), 
        CAST(AVG(import) AS NUMERIC(7,2)) 
  FROM comandes  
 WHERE (quantitat >= 20 
   AND quantitat <= 30) 
   AND (fabricant LIKE '%i%' 
   OR fabricant LIKE '%a%');
```

```
 count |   sum    |   avg   
-------+----------+---------
     5 | 44222.00 | 8844.40
(1 row)
```

## Exercici 51

De quina fàbrica s'han venut menys unitats de producte (alerta : no menys comandes, menys unitats)

```
SELECT fabricant, MIN(quantitat) 
  FROM comandes 
 GROUP BY fabricant 
 ORDER BY 2 
 FETCH FIRST ROW ONLY;
```

```
 fabricant | min 
-----------+-----
 bic       |   1
(1 row)
```

## Exercici 52

Quins són els 3 venedors que han fet més comandes?

```
SELECT nom, COUNT(num_comanda) 
  FROM rep_vendes 
  JOIN comandes 
    ON rep_vendes.num_empl = comandes.rep 
 GROUP BY nom 
 ORDER BY 2 DESC 
 FETCH FIRST 3 ROWS ONLY;
```

```
     nom     | count 
-------------+-------
 Larry Fitch |     7
 Bill Adams  |     5
 Sue Smith   |     4
(3 rows)
```

## Exercici 53

Mostra quants clients amb un nom que contingui 2 espais en blanc té assignat cada venedor.

```
SELECT COUNT(num_clie), nom, rep_clie 
  FROM clients 
  JOIN rep_vendes 
    ON clients.rep_clie = rep_vendes.num_empl 
 WHERE empresa LIKE '% % %' 
 GROUP BY rep_clie, num_empl; 
```

```
 count |    nom     | rep_clie 
-------+------------+----------
     1 | Mary Jones |      109
     2 | Sue Smith  |      102
     1 | Bob Smith  |      104
(3 rows)
```

## Exercici 54

Mostra quantes comandes ha fet cada venedor a cada client que tingui un codi múltiple de 5 o múltiple de 3 i, a més a més, que el codi de client no sigui ni el 110 ni el 102.

```
SELECT COUNT(num_comanda), rep, clie 
  FROM comandes 
 WHERE (clie % 5 = 0 OR clie % 3 = 0) 
   AND (rep != 110 AND rep != 102) 
 GROUP BY rep,clie;
```

```
 count | rep | clie 
-------+-----+------
     4 | 105 | 2103
     1 | 107 | 2109
     2 | 107 | 2124
     2 | 108 | 2112
     4 | 108 | 2118
(5 rows)
```