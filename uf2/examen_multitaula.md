## 1 

SELECT DISTINCT empresa
  FROM productes
  JOIN comandes ON id_fabricant = fabricant AND id_producte = producte

## 6

SELECT empresa, id_fabricant, id_producte, preu, rep_vendes.nom, caps.nom
  FROM productes
  JOIN comandes 
    ON id_fabricant = fabricant AND id_producte = producte
  JOIN clients 
    ON clie = num_clie
  JOIN rep_vendes 
    ON rep = rep_vendes.num_empl
  LEFT JOIN rep_vendes AS caps 
    ON rep_vendes.cap = caps.num_empl
 WHERE (preu < 80 OR preu > 1000) 
   AND (id_fabricant = 'imm' OR id_fabricant = 'rei')
 ORDER BY 1,2,3; 

## 8 

SELECT nom, regio, ciutat
  FROM rep_vendes
  LEFT JOIN oficines 
    ON oficina_rep = oficina
 WHERE (regio = 'Est' AND edat > 35
    OR regio = 'Oest' AND edat < 60
    OR regio IS NULL AND edat >= 40 AND edat <= 60)
   AND (nom ILIKE '%n%' OR nom ILIKE '%m%') 
 ORDER BY 1,2

## 5

SELECT num_comanda, import, empresa, c_fet.nom, rep_nom.nom, oficina
  FROM clients
  JOIN comandes 
    ON num_clie = clie
  JOIN rep_vendes AS "c_fet" 
    ON rep = c_fet.num_empl
  JOIN rep_vendes AS "rep_nom" 
    ON rep_clie = rep_nom.num_empl
  JOIN oficines 
    ON rep_nom.oficina_rep = oficina 
 WHERE rep != rep_clie;

## 2

SELECT id_fabricant, id_producte, descripcio, preu, data, empresa, ROUND(import/quantitat,2)
  FROM productes
  LEFT JOIN comandes
    ON id_fabricant = fabricant AND id_producte = producte
  LEFT JOIN clients
    ON clie = num_clie
  LEFT JOIN rep_vendes 
    ON rep = rep_vendes.num_empl
 ORDER BY 1,2,3,4,5;

## 3

SELECT empresa, assignat.nom, caps.nom, oficina, ciutat, directors.nom
  FROM clients
  JOIN rep_vendes 
    AS assignat 
    ON rep_clie = num_empl
  LEFT JOIN rep_vendes 
    AS caps 
    ON assignat.cap = caps.num_empl
  LEFT JOIN oficines 
    ON assignat.oficina_rep = oficina
  LEFT JOIN rep_vendes 
    AS directors 
    ON oficines.director = directors.num_empl;