 SELECT num_comanda, clie, empresa, rep, rep_assignat.num_empl AS "rep assignat", descripcio, oficina_comanda.ciutat AS "oficina comanda", oficina2.ciutat AS "oficina assignat" 
   FROM comandes 
   JOIN clients 
     ON num_clie = clie 
   JOIN rep_vendes AS "venedor_comanda" 
     ON num_empl = rep 
   JOIN productes 
     ON (fabricant, producte) = (id_fabricant, id_producte) 
   JOIN oficines AS "oficina_comanda" 
     ON oficina = oficina_rep 
   JOIN rep_vendes AS "rep_assignat" 
     ON rep_assignat.num_empl = rep_clie 
   JOIN oficines AS "oficina2" 
     ON oficina2.oficina = rep_assignat.oficina_rep 
  WHERE ((oficina_comanda.ciutat = 'New York' 
    AND venedor_comanda.edat > 40) 
     OR (oficina_comanda.ciutat = 'Chicago' AND  venedor_comanda.edat < 60) 
     OR (venedor_comanda.oficina_rep IS NULL AND venedor_comanda.edat > 30)) 
    AND fabricant = 'aci';