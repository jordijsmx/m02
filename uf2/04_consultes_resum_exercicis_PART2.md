# Consultes resum (PART II)

## Exercici 20

Quina és l'import promig de les comandes de cada venedor?

```
SELECT ROUND(AVG(import),2), rep 
  FROM comandes 
 GROUP BY rep ORDER BY rep;
```

```
  round   | rep 
----------+-----
  8876.00 | 101
  5694.00 | 102
  1350.00 | 103
  7865.40 | 105
 16479.00 | 106
 11477.33 | 107
  8376.14 | 108
  3552.50 | 109
 11566.00 | 110
(9 rows)
```

## Exercici 21

Quin és el rang (màxim i mínim) de quotes dels venedors per cada oficina?

```
SELECT oficina_rep, MAX(quota), MIN(quota) 
  FROM rep_vendes 
 GROUP BY oficina_rep;
```

```
 oficina_rep |    max    |    min    
-------------+-----------+-----------
             |           |          
          22 | 300000.00 | 300000.00
          11 | 300000.00 | 275000.00
          21 | 350000.00 | 350000.00
          13 | 350000.00 | 350000.00
          12 | 300000.00 | 200000.00
(6 rows)
```

## Exercici 22

Quants venedors estan asignats a cada oficina?

```
SELECT oficina_rep, COUNT(num_empl) 
  FROM rep_vendes 
 WHERE oficina_rep IS NOT NULL 
 GROUP BY oficina_rep ;
```

```
 oficina_rep | count 
-------------+-------
          22 |     1
          11 |     2
          21 |     2
          13 |     1
          12 |     3
(5 rows)
```

## Exercici 23

Per cada venedor calcular quants clients diferents ha atès ( atès = atendre una comanda)?

```
SELECT rep,nom, COUNT(DISTINCT(clie)) 
  FROM comandes 
 RIGHT JOIN rep_vendes 
    ON rep = num_empl 
 GROUP BY rep,nom 
 ORDER BY rep;
```

```
 rep |      nom      | count 
-----+---------------+-------
 101 | Dan Roberts   |     3
 102 | Sue Smith     |     3
 103 | Paul Cruz     |     1
 105 | Bill Adams    |     2
 106 | Sam Clark     |     2
 107 | Nancy Angelli |     2
 108 | Larry Fitch   |     3
 109 | Mary Jones    |     1
 110 | Tom Snyder    |     1
     | Bob Smith     |     0
(10 rows)
```

## Exercici 24

Calcula el total dels imports de les comandes fetes per cada client a cada
vendedor.

```
SELECT SUM(import), clie, rep 
  FROM comandes 
 GROUP BY clie,rep;
```

```
   sum    | clie | rep 
----------+------+-----
 22500.00 | 2113 | 101
 23132.00 | 2107 | 110
 35582.00 | 2103 | 105
  3750.00 | 2120 | 102
  4026.00 | 2106 | 102
 47925.00 | 2112 | 108
 31500.00 | 2117 | 106
  3608.00 | 2118 | 108
   150.00 | 2108 | 101
  3745.00 | 2111 | 105
  7100.00 | 2114 | 108
  3978.00 | 2102 | 101
  2700.00 | 2111 | 103
 31350.00 | 2109 | 107
 15000.00 | 2114 | 102
  1458.00 | 2101 | 106
  3082.00 | 2124 | 107
  7105.00 | 2108 | 109
(18 rows)
```

## Exercici 25

El mateix que a la qüestió anterior, però ordenat per client i dintre de client
per venedor.

```
SELECT SUM(import), clie, rep 
  FROM comandes 
 GROUP BY clie,rep 
 ORDER BY clie, rep;
```

```
   sum    | clie | rep 
----------+------+-----
  1458.00 | 2101 | 106
  3978.00 | 2102 | 101
 35582.00 | 2103 | 105
  4026.00 | 2106 | 102
 23132.00 | 2107 | 110
   150.00 | 2108 | 101
  7105.00 | 2108 | 109
 31350.00 | 2109 | 107
  2700.00 | 2111 | 103
  3745.00 | 2111 | 105
 47925.00 | 2112 | 108
 22500.00 | 2113 | 101
 15000.00 | 2114 | 102
  7100.00 | 2114 | 108
 31500.00 | 2117 | 106
  3608.00 | 2118 | 108
  3750.00 | 2120 | 102
  3082.00 | 2124 | 107
(18 rows)
```

## Exercici 26

Calcula les comandes totals per a cada venedor.

```
SELECT COUNT(num_comanda), rep 
  FROM comandes 
 GROUP BY rep;
```

```
 count | rep 
-------+-----
     3 | 101
     7 | 108
     2 | 103
     5 | 105
     3 | 107
     4 | 102
     2 | 109
     2 | 106
     2 | 110
(9 rows)
```

## Exercici 27

Quin és l'import promig de les comandes per cada venedor, les comandes dels
quals sumen més de 30000?

```
SELECT ROUND(AVG(import),2), rep 
  FROM comandes 
 GROUP BY rep 
HAVING SUM(import) > 30000;
```

```
  round   | rep 
----------+-----
  8376.14 | 108
  7865.40 | 105
 11477.33 | 107
 16479.00 | 106
(4 rows)
```

## Exercici 28

Per cada oficina amb dos o més empleats, calcular la quota total i les vendes
totals per a tots els venedors que treballen a la oficina (volem mostrar la
ciutat de l'oficina a la consulta)

```
SELECT oficina_rep, ciutat, SUM(quota), SUM(rep_vendes.vendes) 
  FROM rep_vendes 
  JOIN oficines 
    ON rep_vendes.oficina_rep = oficines.oficina 
 GROUP BY oficina_rep, ciutat 
HAVING COUNT(num_empl) >= 2;
```

```
 oficina_rep |   ciutat    |    sum    |    sum    
-------------+-------------+-----------+-----------
          21 | Los Angeles | 700000.00 | 835915.00
          12 | Chicago     | 775000.00 | 735042.00
          11 | New York    | 575000.00 | 692637.00
(3 rows)
```

## Exercici 29

Mostra el preu, l'estoc i la quantitat total de les comandes de cada producte
per als quals la quantitat total demanada està per sobre del 75% de l'estoc.

```
SELECT preu, estoc, SUM(quantitat) 
  FROM productes 
  JOIN comandes 
    ON (id_fabricant, id_producte) = (fabricant, producte)
 GROUP BY preu, estoc 
HAVING SUM(quantitat) > estoc * 0.75;
```

```
  preu   | estoc | sum 
---------+-------+-----
   25.00 |    37 |  30
  243.00 |    15 |  16
 1425.00 |     5 |  22
 4500.00 |    12 |  22
(4 rows)
```

## Exercici 30

Es desitja un llistat d'identificadors de fabricants de productes. Només volem
tenir en compte els productes de preu superior a 54. Només volem que apareguin
els fabricants amb un nombre total d'unitats superior a 300.

```
SELECT id_fabricant, SUM(estoc) 
  FROM productes 
 WHERE preu > 54 
 GROUP BY id_fabricant 
HAVING SUM(estoc) > 300;
```

```
 id_fabricant | sum 
--------------+-----
 aci          | 843
(1 row)
```

## Exercici 31

Es desitja un llistat dels productes amb les seves descripcions, ordenat per la
suma total d'imports facturats (comandes) de cada producte de l'any 1989.

```
SELECT descripcio, SUM(import) 
  FROM productes 
  JOIN comandes 
    ON (id_fabricant, id_producte) = (fabricant, producte) 
 WHERE data >= '1989-01-01' AND data < '1990-01-01' 
 GROUP BY id_fabricant, id_producte
 ORDER BY SUM(import) DESC;
```

```
     descripcio     |   sum    
--------------------+----------
 Frontissa Esq.     | 31500.00
 Extractor          | 27500.00
 Muntador           | 15000.00
 Article Tipus 4    |  7956.00
 Passador Frontissa |  2100.00
 V Stago Trinquet   |  1896.00
 Article Tipus 2    |   760.00
(7 rows)
```

## Exercici 32

Per a cada director (de personal, no d'oficina) excepte per al gerent (el
venedor que no té director), vull saber el total de vendes dels seus
subordinats. Mostreu codi i nom dels directors.

```
SELECT caps.num_empl, caps.nom, SUM(sub.vendes) 
  FROM rep_vendes AS caps 
  JOIN rep_vendes AS sub 
    ON sub.cap = caps.num_empl 
 WHERE caps.cap IS NOT NULL 
 GROUP BY caps.num_empl, caps.nom; 
```

```
 num_empl |     nom     |    sum    
----------+-------------+-----------
      104 | Bob Smith   | 960359.00
      101 | Dan Roberts |  75985.00
      108 | Larry Fitch | 660092.00
(3 rows)
```

## Exercici 33

Quins són els 5 productes que han estat venuts a més clients diferents? Mostreu
el número de clients per cada producte. A igualtat de nombre de clients es
volen ordenats per ordre decreixent d'estoc i, a igualtat d'estoc, per
descripció. Mostreu tots els camps pels quals s'ordena.

```
SELECT descripcio, estoc, COUNT(DISTINCT(clie)) AS "clients" 
  FROM productes 
  JOIN comandes 
    ON (id_fabricant, id_producte) = (fabricant, producte) 
 GROUP BY id_fabricant, id_producte 
 ORDER BY clients DESC, estoc DESC, descripcio 
 LIMIT 5;
```

```
    descripcio    | estoc | clients 
------------------+-------+---------
 Reductor         |    38 |       3
 V Stago Trinquet |   210 |       2
 Article Tipus 2  |   167 |       2
 Article Tipus 4  |   139 |       2
 Peu de rei       |    37 |       2
(5 rows)
```

## Exercici 34

Es vol llistar el clients (codi i empresa) tals que no hagin comprat cap tipus
de Frontissa (figura a la descripció) i hagin comprat articles de més d'un
fabricant diferent.

```
SELECT num_clie, empresa 
  FROM clients 
  JOIN comandes 
    ON clients.num_clie = comandes.clie 
  JOIN productes 
    ON (fabricant, producte) = (id_fabricant, id_producte) 
 WHERE descripcio NOT LIKE '%Frontissa%' 
 GROUP BY num_clie 
HAVING COUNT(DISTINCT(fabricant)) > 1;
```

```
 num_clie |      empresa      
----------+-------------------
     2106 | Fred Lewis Corp.
     2107 | Ace International
     2108 | Holm & Landis
     2114 | Orion Corp
     2118 | Midwest Systems
     2124 | Peter Brothers
(6 rows)
```

## Exercici 35

Llisteu les oficines per ordre descendent de nombre total de clients diferents
amb comandes realitzades pels venedors d'aquella oficina, i, a igualtat de
clients, ordenat per ordre ascendent del nom del director de l'oficina. Només
s'ha de mostrar el codi i la ciutat de l'oficina.

```
SELECT oficina, ciutat, COUNT(DISTINCT(clie)) 
  FROM oficines 
  JOIN rep_vendes 
    ON oficines.oficina = rep_vendes.oficina_rep 
  JOIN comandes 
    ON comandes.rep = rep_vendes.num_empl   
 GROUP BY oficina 
 ORDER BY COUNT(DISTINCT(clie)) DESC, director ASC;
```

```
 oficina |   ciutat    | count 
---------+-------------+-------
      21 | Los Angeles |     5
      12 | Chicago     |     4
      11 | New York    |     3
      13 | Atlanta     |     2
      22 | Denver      |     2
(5 rows)
```